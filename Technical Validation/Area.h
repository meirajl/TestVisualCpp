// Area.h: interface for the CArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AREA_H__B9CF7EF4_E994_4E7E_8985_C4E6DB54D2CE__INCLUDED_)
#define AFX_AREA_H__B9CF7EF4_E994_4E7E_8985_C4E6DB54D2CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFplDoc;

typedef struct
{
	CPoint Pos;
	int	   Alt;
	int    SectorNb;
	float  t;
	bool   Entry;
	bool   Out;
	int    FixNb;
	int	   VolumeNb;
} IntersectDef;


typedef enum {transit,departure,arrival,internal} flightkinds;

class CLayer : public CObject  
{
	friend class CVolume;
public:
	CLayer();
	virtual ~CLayer();
	static int LayerValueIs(int layernb);

private:
	int m_Nb;
	int m_UpperLimit;
	static CArray<CLayer*,CLayer*> m_LayerList;

};

class CVolume : public CObject  
{	
	friend class CArea;
public:
	CVolume();
	virtual ~CVolume();
private:
	int m_Nb;
	int m_LowLevel;
	int m_HighLevel;
	CDWordArray m_PointId;
	CArray<CPoint,CPoint> m_PointList;
	CRect m_BoundingRect;
	CPoint FindIntersection(CPoint FirstPoint,CPoint Vector);
	int GetIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,IntersectDef IntersectTable[2]);
	CPoint FindFirstIntersection(CPoint FirstPoint,CPoint Vector);
public:
	bool IsInside(CPoint position,int level);
	bool IsInside2D(CPoint position);
	static void LoadVolumes(CString IniName,CFplDoc* pFplDoc);
	static CVolume* VolumeId(CString nb);
//private:
	static CArray<CVolume*,CVolume*> m_VolumeList;
};




class CArea : public CObject  
{
	friend class CVolume;
public:
	CArea();
	virtual ~CArea();
private:
	CString m_Name;
	int m_SectorNb;
	int m_FirNb;
	int m_HldNb;
	CString m_SectorName;
	CArray<CVolume*,CVolume*> m_ListIn;
	CArray<CVolume*,CVolume*> m_ListOut;
	flightkinds m_Type;
public:
	bool IsInside(CPoint Target,int Altitude);
	bool IsInsideIn(CPoint Target,int Altitude);
	bool IsInsideOut(CPoint Target,int Altitude);
	bool IsInside2D(CPoint Target);
	void CreateAreaMap(int MapNb,CString SubMenu);
	static int GetSectorNbInSequence(CPoint Target,int Altitude,int Seq[32],int CurSeqNb);
	static int GetSectorNb(CPoint Target,int Altitude,flightkinds kind=transit);
	static CArea* GetSectorArea(CString Name);
	static CArea* GetSectorAreaByNb(int SectorNb);
	static bool IsInDisabledEhsClamArea(CPoint Target,int Altitude);
	static CPoint GetXptCd(CPoint FirstPoint,CPoint Vector);
	static bool IsInXptCdArea(CPoint Target);
	static bool IsInUncompleteRouteArea(CPoint Target,int Xfl);
	static bool IsInMtcaFilterArea(CPoint Target);
	static bool IsInLtcaFilterAreaGva(CPoint Target);
	static bool IsInLtcaFilterAreaZrh(CPoint Target);
	static bool IsInsideLocalFir(CPoint pnt);
	static bool IsInsideLocalFir2(CPoint pnt);
	static bool IsInsideCorrelationarea(CPoint pnt);
	static bool GetSectorIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,CArray<IntersectDef,IntersectDef> &IntersectTable,flightkinds kind);
	static bool GetSectorAoiIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,CArray<IntersectDef,IntersectDef> &IntersectTable,flightkinds kind);
	static bool GetLocalFirIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,CArray<IntersectDef,IntersectDef> &IntersectTable);
	static bool GetFirIntersect(CPoint Pnt1, int Alt1, CPoint Pnt2, int Alt2, CArray<IntersectDef, IntersectDef> &IntersectTable, flightkinds kind);
	static bool IsInXptConflictArea(CPoint Target,int WindowNb);
	static CPoint GetXptConflictAreaIntersection(CPoint FirstPoint,CPoint Vector,int WindowNb);
	static CString GetFirSectorName(CString SectorName,CPoint pos,int Level);
	static bool IsInsideFirSector(CPoint pnt, int Level);
//private:
	static CArray<CArea*,CArea*> m_SectorList;
	static CArray<CArea*,CArea*> m_SectorAoiList;
	static CArray<CArea*,CArea*> m_EhsClamDisabledList;
	static CArea* m_XptCdArea;
	static CArea* m_RouteUncompleteArea;
	static CArea* m_MtcaFilterArea;
	static CArea* m_LtcaFilterAreaGva;
	static CArea* m_LtcaFilterAreaZrh;
	static CArray<CArea*,CArea*> m_FirList;
	static CArray<CArea*, CArea*> m_FirList2;
	static CArea* m_pLocalFir;
	static CArea* m_pLocalFir2;
	static CArea* m_pCorrelationArea;
	static CArray<CArea*,CArea*> m_XptConflictArea;
	static CArray<CArea*, CArea*> m_FirSectorArea;
};

#endif // !defined(AFX_AREA_H__B9CF7EF4_E994_4E7E_8985_C4E6DB54D2CE__INCLUDED_)
