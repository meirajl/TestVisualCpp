#include "StdAfx.h"
#include <stdlib.h>
#include "adexp.h"
//#include "inifile.h"
#include "StringUtil.h"
//#include "AsdApp.h"
typedef enum LogTypes{LogInformation=0,LogWarning=1,LogError=2} LogTypes;

void WriteTraceMsg(CString msg){/*TRACE(msg.Left(500)+"\n");*/}
void WriteLogMsg(CString msg,LogTypes type){}

enum FieldTypes {Int,Str,Enum,Compound,CompoundMulti,CompoundList};

typedef struct {
	AdexpSubFields Field;
	int MinNumber;
	int MaxNumber;
	} CompoundItem;

typedef struct {
	LPCSTR Name;
	FieldTypes Type;
	const CompoundItem* SubFldTable;
	int MinLen;
	int MaxLen;
} FieldDef;


typedef struct {
	AdexpFields Field;
	int MinNumber;
	int MaxNumber;
	} MsgItem;

typedef struct {
	LPCSTR Name;
	const MsgItem* ItemTable;
	const int* AftnItemTable;
	const AdexpFields* FieldsForAutomatic;
} MsgDef;

typedef struct
{
	LPCSTR Seq;
	AdexpFields Field;
} OthersInf;

const OthersInf Field18[17]={{"EET/",EETPT},{"RIF/",RIF},{"REG/",REG},{"SEL/",SEL},{"OPR/",OPR},{"STS/",STS},{"TYP/",TYPZ},{"PER/",PER},{"COM/",COM},{"DAT/",DAT},{"NAV/",NAV},{"DEP/",DEPZ},{"DEST/",DESTZ},{"ALTN/",ALTNZ},{"RALT/",RALT},{"CODE/",ARCADDR},{"RMK/",RMK}};
const OthersInf Field19[9]={{"E/",SPLE},{"P/",SPLP},{"R/",SPLR},{"S/",SPLS},{"J/",SPLJ},{"D/",SPLDNB},{"A/",SPLA},{"N/",SPLN},{"C/",SPLC}};


LPCSTR Eq833Values[]={"Y","N","?"};
LPCSTR FplCatValues[]={"T","E","S","I"};
LPCSTR FltRulValues[]={"I","V","Y","Z"};
LPCSTR HoldDtValues[]={"INIT","HOLD","TERMINATED"};
LPCSTR RvsmValues[]={"Y","N","E","U"};
LPCSTR WktrcValues[]={"H","M","L"};
LPCSTR DevStsValues[]={"ON","OFF","FLD"};
LPCSTR CplTypValues[]={"TAG","FPL"};

const CompoundItem AdDef[]={{ADID,1,1},{FL,0,1},{FLBLOCK,0,1},{ETO,0,1},{TO,0,1},{CTO,0,1},{STO,0,1},{PTSTAY,0,1},{PTRFL,0,1},{PTRULCHG,0,1},{PTSPEED,0,1},{PTMACH,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem AdexpTxtDef[]={{PREPROCTXT,0,1},{POSTPROCTXT,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem AfildataDef[]={{PTID,0,1},{FL,0,1},{ETO,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem CflDef[]={{FL,1,1},{PTID,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem CoordataDef[]={{PTID,1,1},{TO,0,1},{STO,1,1},{TFL,1,1},{SFL,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem CrsclimbDef[]={{PTID,1,1},{CRSPEED,0,1},{CRMACH,1,1},{CRFL1,1,1},{CRFL2,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem CstatDef[]={{STATID,1,1},{STATREASON,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem EetlatDef[]={{LATTD,1,1},{TIME,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem EetlongDef[]={{LONGTD,1,1},{TIME,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem EntrydataDef[]={{PTID,0,1},{AIRSPDES,0,1},{FL,0,1},{PTRFL,0,1},{PTSPEED,0,1},{PTMACH,0,1},{PTFLRUL,0,1},{PTMILRUL,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem EstdataDef[]={{PTID,1,1},{ETO,1,1},{FL,1,1},{SFL,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem ExtaddrDef[]={{FAC,0,1},{NUM,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem FlbandDef[]={{FL,2,2},{NONE_SUBFIELD,0,0}};
const CompoundItem GeoDef[]={{GEOID_SUBFLD,1,1},{LATTD,1,1},{LONGTD,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem MsgrefDef[]={{SENDER,1,1},{RECVR,1,1},{SEQNUM,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem OriginDef[]={{NETWORKTYPE,0,1},{FAC,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem PartDef[]={{NUM,0,1},{LASTNUM,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem PositionDef[]={{ADID,0,1},{PTID,0,1},{TO,0,1},{STO,0,1},{FL,0,1},{CTO,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem PropflDef[]={{TFL,0,1},{SFL,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RefDef[]={{REFID,0,1},{PTID,0,1},{BRNG,0,1},{DISTNC,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RefdataDef[]={{SENDER,0,1},{RECVR,0,1},{SEQNUM,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RenameDef[]={{RENID,0,1},{PTID,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RrtefromDef[]={{TFVID,1,1},{REFLOC,1,1},{FLOWLST,1,1},{FLBLOCK,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RvrperiodDef[]={{FROM,1,1},{UNTIL,1,1},{RVRLIMIT,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem StayDef[]={{STAYIDENT,1,1},{TIME,1,1},{ADID,0,2},{PTID,0,2},{PTSPEED,0,1},{PTRFL,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem StayinfoDef[]={{STAYIDENT,1,1},{REMARK,0,2},{NONE_SUBFIELD,0,0}};
const CompoundItem TfvDef[]={{TFVID,1,1},{REFLOC,1,1},{FLOWLST,1,1},{FLBLOCK,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem UnitDef[]={{UNITID,1,1},{ADDR_INFO,0,1},{NONE_SUBFIELD,0,0}};
//subfield compound list
const CompoundItem AddrinfoDef[]={{NETWORKTYPE,1,1},{FAC,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem AirrouteDef[]={{NUM,0,1},{REFATSRTE,1,1},{FLBLOCK,1,1},{VALPERIOD,1,1},{REMARK,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem AirspaceDef[]={{NUM,0,1},{AIRSPDES,1,1},{FLBLOCK,1,1},{VALPERIOD,1,1},{RESPUNIT,1,1},{REMARK,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem AtsDef[]={{FAC,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem ExccondDef[]={{REGNUM,0,1},{REFLOC,1,1},{REGREASON,1,1},{STARTREG,1,1},{ENDREG,1,1},{FLBLOCK,0,1},{RVRLIMIT,0,1},{REMARK,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem FlowDef[]={{FROMPOS,1,1},{VIA1,0,1},{VIA2,0,1},{TOPOS,1,1},{VIA3,0,1},{VIA4,0,1},{FLOWROLE,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem IfpdsumDef[]={{ARCID_SUBFLD,1,1},{ADEP_SUBFLD,1,1},{ADES_SUBFLD,1,1},{EOBT_SUBFLD,1,1},{ORIGIN_SUBLFD,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem PtDef[]={{PTID,1,1},{FL,0,1},{FLBLOCK,0,1},{SFL,0,1},{ETO,0,1},{TO,0,1},{CTO,0,1},{STO,0,1},{PTRTE,0,1},{PTSTAY,0,1},{PTRFL,0,1},{PTRULCHG,0,1},{PTSPEED,0,1},{PTMACH,0,1},{PTCRSCLIMB,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem PtcrsclimbDef[]={{CRSPEED,0,1},{CRMACH,0,1},{CRFL1,1,1},{CRFL2,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RateperiodDef[]={{FROM,0,1},{UNTIL,0,1},{FLOWRATE,1,1},{PENRATE,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RegulationDef[]={{REGNUM,1,1},{REGID,1,1},{REGDESC,1,1},{REFLOC,1,1},{STARTREG,1,1},{ENDREG,1,1},{FLBLOCK,0,1},{REMARK,0,1},{TFVID,0,1},{REGREASON,0,1},{REGCOND,0,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RfpdsumDef[]={{ARCID_SUBFLD,1,1},{ADEP_SUBFLD,1,1},{ADES_SUBFLD,1,1},{EOBT_SUBFLD,1,1},{ORIGIN_SUBLFD,0,1},{NONE_SUBFIELD,0,0}};


//field compoundlist
const CompoundItem AddrListDef[]={{FAC,0,400},{NONE_SUBFIELD,0,0}};
const CompoundItem EqcstListDef[]={{EQPT,0,400},{NONE_SUBFIELD,0,0}};
const CompoundItem FmpListDef[]={{FMP_SUBFLD,0,400},{REGLIST,0,400},{NONE_SUBFIELD,0,0}};
const CompoundItem IfpdListDef[]={{IFPDLONG,0,400},{NONE_SUBFIELD,0,0}};
const CompoundItem IfpdsListDef[]={{IFPDSUM,0,400},{NONE_SUBFIELD,0,0}};
const CompoundItem IgnoreListDef[]={{CONDITION,1,1},{PTID,0,2},{NONE_SUBFIELD,0,0}};
const CompoundItem AirrouteListDef[]={{AIRROUTE,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem AirspaceListDef[]={{AIRSPACE,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RatepdListDef[]={{RATEPERIOD,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RfpdListDef[]={{RFPDLONG,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RfpdsListDef[]={{RFPDSUM,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RteptsListDef[]={{PT,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RvrcondListDef[]={{RVRPERIOD_SUBFLD,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem SendtoListDef[]={{UNIT_SUBFLD,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem FlowListDef[]={{FLOW,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RegcondListDef[]={{RATEPERIOD,1,1},{NONE_SUBFIELD,0,0}};
const CompoundItem RegListDef[]={{REGULATION,1,1},{NONE_SUBFIELD,0,0}};

const FieldDef FieldTable[]=
{
	{"",Str,NULL,0,0},
	{"AD",Compound,&AdDef[0],0,0},
	{"ADA",Str,NULL,0,0},
	{"ADARR",Str,NULL,4,4},
	{"ADARRZ",Str,NULL,0,20},
	{"ADD",Str,NULL,6,8},
	{"ADDR",CompoundList,&AddrListDef[0],0,0},
	{"ADEP",Str,NULL,4,4},
	{"ADEPK",Str,NULL,4,4},
	{"ADEPOLD",Str,NULL,4,4},
	{"ADES",Str,NULL,4,4},
	{"ADESK",Str,NULL,4,4},
	{"ADESOLD",Str,NULL,4,4},
	{"ADEXPTXT",Compound,&AdexpTxtDef[0],0,0},
	{"AFILDATA",Compound,&AfildataDef[0],4,4},
	{"AHEAD",Str,NULL,2,10},
	{"ALTNZ",Str,NULL,1,20},
	{"ALTRNT1",Str,NULL,4,4},
	{"ALTRNT2",Str,NULL,4,4},
	{"AOBD",Str,NULL,6,6},
	{"AOBT",Str,NULL,4,4},
	{"ARCADDR",Str,NULL,1,6},
	{"ARCID",Str,NULL,1,7},
	{"ARCIDK",Str,NULL,1,7},
	{"ARCIDOLD",Str,NULL,1,7},
	{"ARCTYP",Str,NULL,1,4},
	{"ASPEED",Str,NULL,1,6},
	{"ATA",Str,NULL,4,4},
	{"ATD",Str,NULL,4,4},
	{"ATOT",Str,NULL,4,4},
	{"ATSRT",Str,NULL,9,19},
	{"CASSADDR",CompoundList,&AddrListDef[0],0,0},
	{"CEQPT",Str,NULL,1,25},
	{"CFL",Compound,&CflDef[0],4,4},
	{"CHGRUL",Str,NULL,4,10},
	{"COBD",Str,NULL,6,6},
	{"COBT",Str,NULL,4,4},
	{"COM",Str,NULL,1,20},
	{"COMMENT",Str,NULL,1,400},
	{"CONDID",Str,NULL,1,30},
	{"COORDATA",Compound,&CoordataDef[0],4,4},
	{"COP",Str,NULL,1,5},
	{"CRSCLIMB",Compound,&CrsclimbDef[0],1,30},
	{"CSTAT",Compound,&CstatDef[0],1,5},
	{"CTOD",Str,NULL,6,6},
	{"CTOT",Str,NULL,4,4},
	{"DAT",Str,NULL,1,5},
	{"DAYS",Str,NULL,7,7},
	{"DAYSK",Str,NULL,7,7},
	{"DAYSOLD",Str,NULL,7,7},
	{"DCT",Str,NULL,5,11},
	{"DELAY",Str,NULL,4,4},
	{"DEPZ",Str,NULL,1,50},
	{"DESC",Str,NULL,1,400},
	{"DESTZ",Str,NULL,1,50},
	{"EETFIR",Str,NULL,1,20},
	{"EETLAT",Compound,&EetlatDef[0],7,7},
	{"EETLONG",Compound,&EetlongDef[0],7,7},
	{"EETPT",Str,NULL,6,10},
	{"ENDTIME",Str,NULL,1,10},
	{"ENTRYDATA",Compound,&EntrydataDef[0],7,7},
	{"EOBD",Str,NULL,6,6},
	{"EOBDK",Str,NULL,6,6},
	{"EOBDOLd",Str,NULL,6,6},
	{"EOBT",Str,NULL,4,4},
	{"EOBTK",Str,NULL,4,4},
	{"EOBTOLD",Str,NULL,4,4},
	{"EQCST",CompoundList,&EqcstListDef[0],0,0},
	{"ERRFIELD",Str,NULL,4,4},
	{"ERROR",Str,NULL,1,400},
	{"ESTDATA",Compound,&EstdataDef[0],7,7},
	{"ETOD",Str,NULL,6,6},
	{"ETOT",Str,NULL,4,4},
	{"EXTADDR",Compound,&ExtaddrDef[0],7,7},
	{"FILRTE",Str,NULL,1,400},
	{"FILTIM",Str,NULL,6,6},
	{"FLBAND",Compound,&FlbandDef[0],4,4},
	{"FLTRUL",Str,NULL,1,1},
	{"FLTTYP",Str,NULL,1,1},
	{"FMP",Str,NULL,4,4},
	{"FMPLIST",CompoundList,&FmpListDef[0],0,0},
	{"FREQ",Str,NULL,1,7},
	{"FSTDAY",Str,NULL,6,6},
	{"FURTHRTE",Str,NULL,1,400},
	{"GEO",CompoundMulti,&GeoDef[0],4,4},
	{"IFP",Str,NULL,1,400},
	{"IFPDLIST",CompoundList,&IfpdListDef[0],0,0},
	{"IFPDSLIST",CompoundList,&IfpdsListDef[0],0,0},
	{"IFPLID",Str,NULL,1,10},
	{"IFPSMOD",Str,NULL,1,20},
	{"IFPURESP",Str,NULL,1,20},
	{"IGNORE",CompoundList,&IgnoreListDef[0],0,0},
	{"IOBD",Str,NULL,6,6},
	{"IOBT",Str,NULL,4,4},
	{"LACDR",CompoundList,&AirrouteListDef[0],0,0},
	{"LATSA",CompoundList,&AirspaceListDef[0],0,0},
	{"LCATSRTE",CompoundList,&AirrouteListDef[0],0,0},
	{"LFIR",Str,NULL,4,4},//to be defined later
	{"LRAR",CompoundList,&AirspaceListDef[0],0,0},
	{"LRCA",CompoundList,&AirspaceListDef[0],0,0},
	{"LSTDAY",Str,NULL,6,6},
	{"MACH",Str,NULL,3,12},
	{"MESVALPERIOD",Str,NULL,10,22},
	{"MINLINEUP",Str,NULL,4,4},
	{"MODIFNB",Str,NULL,1,3},
	{"MSGREF",Compound,&MsgrefDef[0],4,4},
	{"MSGSUM",Str,NULL,4,4},//to be defined later
	{"MSGTXT",Str,NULL,1,400},
	{"MSGTYP",Str,NULL,1,20},
	{"NAV",Str,NULL,1,30},
	{"NBARC",Str,NULL,1,2},
	{"NBRFPD",Str,NULL,1,3},
	{"NEWCTOT",Str,NULL,4,4},
	{"NEWENDTIME",Str,NULL,1,4},
	{"NEWEOBD",Str,NULL,6,6},
	{"NEWEOBT",Str,NULL,4,4},
	{"NEWPTOT",Str,NULL,4,4},
	{"NEWRTE",Str,NULL,1,400},
	{"NEWSTARTTIME",Str,NULL,1,4},
	{"OLDMSG",Str,NULL,1,400},
	{"OPR",Str,NULL,1,400},
	{"OPRMSG",Str,NULL,1,20},
	{"ORGN",Str,NULL,1,30},
	{"ORGNID",Str,NULL,1,20},
	{"ORGRTE",Str,NULL,1,400},
	{"ORIGIN",Compound,&OriginDef[0],4,4},
	{"ORIGINDT",Str,NULL,10,10},
	{"PART",Compound,&PartDef[0],4,4},
	{"PER",Str,NULL,1,20},
	{"POSITION",Compound,&PositionDef[0],4,4},
	{"PREVARCID",Str,NULL,1,7},
	{"PREVSSRCODE",Str,NULL,4,4},
	{"PROPFL",Compound,&PropflDef[0],4,4},
	{"PTOT",Str,NULL,4,4},
	{"QRORGN",Str,NULL,1,20},
	{"RALT",Str,NULL,1,40},
	{"RATE",Str,NULL,2,4},
	{"RATEPDLST",CompoundList,&RatepdListDef[0],0,0},
	{"REASON",Str,NULL,4,12},
	{"REF",Compound,&RefDef[0],4,4},
	{"REFDATA",Compound,&RefdataDef[0],4,4},
	{"REG",Str,NULL,1,7},
	{"REGCAUSE",Str,NULL,1,20},
	{"REGLOC",Str,NULL,1,15},
	{"REGUL",Str,NULL,1,20},
	{"REJCTOT",Str,NULL,4,4},
	{"RELEASE",Str,NULL,1,1},
	{"RENAME",Compound,&RenameDef[0],4,4},
	{"RESPBY",Str,NULL,4,4},
	{"RFL",Str,NULL,4,17},
	{"RFP",Str,NULL,2,2},
	{"RFPDLIST",CompoundList,&RfpdListDef[0],0,0},
	{"RFPDSLIST",CompoundList,&RfpdsListDef[0],0,0},
	{"RIF",Str,NULL,4,400},
	{"RMK",Str,NULL,1,400},
	{"ROUTE",Str,NULL,1,400},
	{"RRTEFROM",Compound,&RrtefromDef[0],4,4},
	{"RRTEREF",Str,NULL,1,20},
	{"RRTETO",Compound,&RrtefromDef[0],4,4},
	{"RTEPTS",CompoundList,&RteptsListDef[0],4,4},
	{"RVR",Str,NULL,1,3},
	{"RVRCOND",CompoundList,&RvrcondListDef[0],4,4},
	{"RVRPERIOD",Compound,&RvrperiodDef[0],4,4},
	{"SECTOR",Str,NULL,1,8},
	{"SEL",Str,NULL,4,5},
	{"SENDTO",CompoundList,&SendtoListDef[0],4,4},
	{"SEQPT",Str,NULL,1,5},
	{"SID",Str,NULL,2,7},
	{"SPEED",Str,NULL,1,17},
	{"SPLA",Str,NULL,1,50},
	{"SPLADDR",CompoundList,&AddrListDef[0],0,0},
	{"SPLC",Str,NULL,1,50},
	{"SPLDCAP",Str,NULL,1,3},
	{"SPLDCOL",Str,NULL,1,50},
	{"SPLDCOV",Str,NULL,1,1},
	{"SPLDNB",Str,NULL,1,2},
	{"SPLE",Str,NULL,4,4},
	{"SPLJ",Str,NULL,1,20},
	{"SPLN",Str,NULL,1,400},
	{"SPLP",Str,NULL,1,3},
	{"SPLR",Str,NULL,1,10},
	{"SPLS",Str,NULL,1,20},
	{"SRC",Str,NULL,3,3},
	{"SSRCODE",Str,NULL,3,5},
	{"STAR",Str,NULL,2,7},
	{"STARTTIME",Str,NULL,1,4},
	{"STAY",Compound,&StayDef[0],4,4},
	{"STAYINFO",Compound,&StayinfoDef[0],4,4},
	{"STS",Str,NULL,1,50},
	{"TAXITIME",Str,NULL,4,4},
	{"TFCVOL",Str,NULL,1,15},
	{"TFV",Compound,&TfvDef[0],4,4},
	{"TITLE",Str,NULL,1,20},
	{"TTLEET",Str,NULL,1,12},
	{"TYPZ",Str,NULL,1,20},
	{"UNIT",Compound,&UnitDef[0],4,4},
	{"VALFROM",Str,NULL,6,6},
	{"VALFROMK",Str,NULL,6,6},
	{"VALFROMOLD",Str,NULL,6,6},
	{"VALIDITYDATE",Str,NULL,6,6},
	{"VALUNTIL",Str,NULL,6,6},
	{"VALUNTILK",Str,NULL,6,6},
	{"VALUNTILOLD",Str,NULL,6,6},
	{"FIELD18",Str,NULL,6,6},
	{"WKTRC",Str,NULL,1,1}
};



const FieldDef SubFieldTable[]=
{
	{"",Str,NULL,0,0},
	{"ADDRINFO",Compound,&AddrinfoDef[0],4,4},
	{"ADEP",Str,NULL,4,4},
	{"ADES",Str,NULL,4,4},
	{"ADID",Str,NULL,4,4},
	{"AIRROUTE",Compound,&AirrouteDef[0],4,4},
	{"AIRSPACE",Compound,&AirspaceDef[0],4,4},
	{"AIRSPDES",Str,NULL,3,12},
	{"ARCID",Str,NULL,4,4},
	{"BRNG",Str,NULL,1,10},
	{"CONDITION",Str,NULL,2,20},
	{"CRFL1",Str,NULL,2,5},
	{"CRFL2",Str,NULL,2,5},
	{"CRMACH",Str,NULL,2,5},
	{"CRSPEED",Str,NULL,2,5},
	{"CTO",Str,NULL,4,4},
	{"DISTNC",Str,NULL,1,3},
	{"EFL",Str,NULL,3,5},
	{"ENDREG",Str,NULL,4,4},
	{"EOBT",Str,NULL,4,4},
	{"EQPT",Str,NULL,1,4},
	{"ETO",Str,NULL,4,12},
	{"EXCCOND",Compound,&ExccondDef[0],4,4},
	{"FAC",Str,NULL,1,30},
	{"FIR",Str,NULL,7,7},
	{"FL",Str,NULL,2,7},
	{"FLBLOCK",Str,NULL,6,12},
	{"FLOW",Compound,&FlowDef[0],4,4},
	{"FLOWLST",CompoundList,&FlowListDef[0],4,4},
	{"FLOWRATE",Str,NULL,3,7},
	{"FLOWROLE",Str,NULL,2,2},
	{"FMP",Str,NULL,4,4},
	{"FROM",Str,NULL,1,4},
	{"FROMPOS",Str,NULL,1,15},
	{"GEOID",Str,NULL,1,5},
	{"IFPDLONG",CompoundList,NULL,4,4},
	{"IFDPSUM",Compound,&IfpdsumDef[0],4,4},
	{"LASTNUM",Str,NULL,3,3},
	{"LATTD",Str,NULL,1,12},
	{"LONGTD",Str,NULL,1,15},
	{"NETWORKTYPE",Str,NULL,2,10},
	{"NUM",Str,NULL,3,3},
	{"ORIGIN",Compound,&OriginDef[0],4,4},
	{"PENRATE",Str,NULL,3,7},
	{"POSTPROCTXT",Str,NULL,1,400},
	{"PREPROCTXT",Str,NULL,1,400},
	{"PT",Compound,&PtDef[0],4,4},
	{"PTCRSCLIMB",Compound,&PtcrsclimbDef[0],4,4},
	{"PTFLRUL",Str,NULL,3,3},
	{"PTID",Str,NULL,2,7},
	{"PTMACH",Str,NULL,3,7},
	{"PTMILRUL",Str,NULL,3,3},
	{"PTRFL",Str,NULL,3,3},
	{"PTRTE",Str,NULL,1,400},
	{"PTRULCHG",Str,NULL,1,5},
	{"PTSPEED",Str,NULL,1,6},
	{"PTSTAY",Str,NULL,1,20},
	{"RATEPERIOD",Compound,&RateperiodDef[0],4,4},
	{"RECVR",Compound,&AtsDef[0],1,30},
	{"REFATSRTE",Str,NULL,1,100},
	{"REFID",Str,NULL,4,10},
	{"REFLOC",Str,NULL,1,15},
	{"REGCOND",CompoundList,&RegcondListDef[0],4,4},
	{"REGDESC",Str,NULL,1,400},
	{"REDID",Str,NULL,4,10},
	{"REGLIST",CompoundList,&RegListDef[0],4,4},
	{"REDID",Str,NULL,2,6},
	{"REGREASON",Str,NULL,4,12},
	{"REGULATION",Compound,&RegulationDef[0],4,4},
	{"REMARK",Str,NULL,1,400},
	{"RENID",Str,NULL,4,10},
	{"RESPUNIT",Str,NULL,12,12},
	{"RFDPLONG",CompoundList,NULL,4,4},
	{"RFPDSUM",Compound,&RfpdsumDef[0],4,4},
	{"RVRLIMIT",Str,NULL,3,3},
	{"RVRPERIOD",Compound,&RvrperiodDef[0],4,4},
	{"SENDER",Compound,&AtsDef[0],4,4},
	{"SEQNUM",Str,NULL,3,3},
	{"SFL",Str,NULL,2,5},
	{"STARTREG",Str,NULL,2,5},
	{"STATID",Str,NULL,2,40},
	{"STATREASON",Str,NULL,2,40},
	{"STAYIDENT",Str,NULL,2,20},
	{"STO",Str,NULL,4,4},
	{"TFL",Str,NULL,3,5},
	{"TFVID",Str,NULL,3,7},
	{"TIME",Str,NULL,4,4},
	{"TO",Str,NULL,4,4},
	{"TOPOS",Str,NULL,1,15},
	{"UNIT",Compound,&UnitDef[0],4,4},
	{"UNITID",Str,NULL,2,10},
	{"UNTIL",Str,NULL,4,8},
	{"VALPERIOD",Str,NULL,2,16},
	{"VIA1",Str,NULL,1,15},
	{"VIA2",Str,NULL,1,15},
	{"VIA3",Str,NULL,1,15},
	{"VIA4",Str,NULL,1,15},
};


const AdexpFields LastField=WKTRC;
const AdexpSubFields LastSubField=VIA4;
const AdexpMsgTypes LastMsgType=SPL;



const int AlrDef[]={3,5,7,8,9,10,13,15,16,18,19,20,0};
const int RcfDef[]={3,5,21,0};
const int FplDef[]={3,7,8,9,10,13,15,16,18,0};
const int CplDef[]={3,7,8,9,10,13,14,15,16,18,0};
const int StdDef[]={3,7,13,16,0};
const int ChgDef[]={3,7,13,16,22,0};
const int ArrDef[]={3,7,13,16,17,0};
const int OldiDef[]={3,7,13,14,16,0};
const int CdnDef[]={3,7,13,16,22,0};
const int LamDef[]={3,0};
const int SplDef[]={3,7,13,16,18,19,0};


const AdexpFields AftnValid[]={ARCID,ADEP,ADES,EOBT,NONE_FIELD};
const MsgDef MsgTable[]=
  { {"",NULL},
	{"ABI",NULL,&OldiDef[0]},
	{"ACK",NULL},
	{"ACP",NULL},
	{"ACT",NULL,&OldiDef[0]},
	{"AUP",NULL},
	{"BFD",NULL},
	{"CFP",NULL},
	{"CNLCOND",NULL},
	{"CNLREG",NULL},
	{"COD",NULL},
	{"COF",NULL},
	{"CRAM",NULL},
	{"DES",NULL},
	{"ERR",NULL},
	{"EXCOND",NULL},
	{"FCM",NULL},
	{"FLS",NULL},
	{"FSA",NULL},
	{"HOP",NULL},
	{"IACH",NULL},
	{"IAFP",NULL},
	{"IAPL",NULL},
	{"IARR",NULL},
	{"ICHG",NULL},
	{"ICNL",NULL},
	{"IDEP",NULL},
	{"IDLA",NULL},
	{"IFPL",NULL},
	{"INF",NULL},
	{"IRPL",NULL},
	{"IRQS",NULL},
	{"ISPL",NULL},
	{"LAM",NULL,&OldiDef[0]},
	{"LRM",NULL},
	{"MAC",NULL,&OldiDef[0]},
	{"MAN",NULL},
	{"MAS",NULL},
	{"MODCOND",NULL},
	{"MODREG",NULL},
	{"NTA",NULL},
	{"NTACNL",NULL},
	{"NTAMOD",NULL},
	{"OLRA",NULL},
	{"OLRCNL",NULL},
	{"OLRMOD",NULL},
	{"PAC",NULL,&OldiDef[0]},
	{"PNT",NULL},
	{"RAP",NULL},
	{"RCHG",NULL},
	{"RCNL",NULL},
	{"RDY",NULL},
	{"REJ",NULL},
	{"REV",NULL,&OldiDef[0]},
	{"RLS",NULL},
	{"RJC",NULL},
	{"RJT",NULL},
	{"ROF",NULL},
	{"RRP",NULL},
	{"RRQ",NULL},
	{"RRV",NULL},
	{"RTI",NULL},
	{"SAM",NULL},
	{"SBY",NULL},
	{"SDM",NULL},
	{"SIP",NULL},
	{"SLC",NULL},
	{"SMM",NULL},
	{"SPA",NULL},
	{"SRJ",NULL},
	{"SRM",NULL},
	{"SRR",NULL},
	{"TIM",NULL},
	{"TIP",NULL},
	{"UUP",NULL},
	{"XAP",NULL},
	{"XCM",NULL},
	{"XIN",NULL},
	{"XRQ",NULL},
	//AFTN MSG
	{"ALR",NULL,&AlrDef[0],&AftnValid[0]},
	{"RCF",NULL,&RcfDef[0],&AftnValid[0]},
	{"FPL",NULL,&FplDef[0],&AftnValid[0]},
	{"CHG",NULL,&ChgDef[0],&AftnValid[0]},
	{"CNL",NULL,&StdDef[0],&AftnValid[0]},
	{"DLA",NULL,&StdDef[0],&AftnValid[0]},
	{"DEP",NULL,&StdDef[0],&AftnValid[0]},
	{"ARR",NULL,&ArrDef[0],&AftnValid[0]},
	{"CPL",NULL,&CplDef[0],&AftnValid[0]},
	{"EST",NULL,&OldiDef[0],&AftnValid[0]},
	{"CDN",NULL,&CdnDef[0],&AftnValid[0]},
	{"RQP",NULL,&StdDef[0],&AftnValid[0]},
	{"RQS",NULL,&StdDef[0],&AftnValid[0]},
	{"SPL",NULL,&SplDef[0],&AftnValid[0]},
  };




CAdexpMsg::CAdexpMsg()
{
	m_IsValid=true;
	m_IsValidAuto=true;
	m_IsAftn=false;
	m_IsCheck=false;
	m_IsService=false;
	m_MsgType=NONE_TITLE;
	for (int fld=0;fld<=LastField;fld++)
	{
		m_FieldTable[fld].Valid=true;
		m_FieldTable[fld].Presence=false;
	}
}

CAdexpMsg::~CAdexpMsg()
{
}

CAdexpMsg* CAdexpMsg::ReceiveMessage(BYTE* pMsg,int len)
{
	CAdexpMsg* pCurMsg= new CAdexpMsg;
	//pCurMsg->DecodeMessage(pMsg,len);
	return pCurMsg;
}

bool CAdexpMsg::DecodeMessage(CString AftnMsg)
{
	int EndHeader=0, fnd1=-1, fnd2=-1;
	CString CurMsgStr = "";
	m_Msg = AftnMsg;

	fnd1 = m_Msg.Find("(", EndHeader);
	if (fnd1 == -1)
	{
		CurMsgStr = m_Msg.Right(m_Msg.GetLength() - EndHeader);
		CurMsgStr.TrimLeft();
		if (CurMsgStr.Left(2) == "CH")
		{
			m_IsCheck = true;
			m_MessageBody = CurMsgStr;
			return true;
		}
		if (CurMsgStr.Left(3) == "SVC")
		{
			m_IsService = true;
			m_MessageBody = CurMsgStr;
		}
		return false;
	}
	fnd2 = FindHyphen(m_Msg, fnd1 + 1);
	CurMsgStr = m_Msg.Mid(fnd1 + 1, fnd2 - fnd1 - 1);
	CurMsgStr.TrimRight();
	CurMsgStr.TrimLeft();
	fnd2 = CurMsgStr.Find("/", 0);
	if (fnd2 != -1)
		CurMsgStr = CurMsgStr.Left(fnd2);
	m_MsgType = NONE_TITLE;
	for (int msg = 0; msg <= LastMsgType; msg++)
	{
		CurMsgStr.MakeUpper();
		if (CurMsgStr.Left(3).Compare(MsgTable[msg].Name) == 0)
		{
			m_MsgType = (AdexpMsgTypes)msg;
			break;
		}
	}
	if (m_MsgType == NONE_TITLE)
		return false;
	m_IsAftn = true;
	return DecodeAftnMessage(fnd1);
}

/*bool CAdexpMsg::DecodeMessage(BYTE* pMsg,int len)
{
	CString CurFldStr,CurFldVal,CurMsgStr,SubFld,Wrd;
	int i;
	for (i=0;i<len;i++)
	{
		if ((pMsg[i]!=0x0D) && (pMsg[i]!=0x0A) && (pMsg[i]!=0x0B) && (pMsg[i]!=0x03))
			m_Msg+=pMsg[i];
		else
			if (pMsg[i]==0x0A)
				m_Msg+=' ';
	}
	WriteTraceMsg(m_Msg);
	AdexpFields CurFld=NONE_FIELD;
	int EndHeader=-1;
	if ((pMsg[0]==0x02) && (pMsg[1]=='H'))
	{
		//Oldi Message
		EndHeader=m_Msg.Find("(");
		if ((pMsg[0]!=0x02) || (EndHeader==-1) || (pMsg[len-1]!=0x03))
		{
			WriteLogMsg("Oldi Msg : Invalid header",LogError);
			return false;
		}
		m_Header=m_Msg.Mid(1,EndHeader-1);
	}
	else
	{
		//decode header
		
		EndHeader=m_Msg.Find(0x02);
		if ((pMsg[0]!=0x01) || (EndHeader==-1) || (pMsg[len-1]!=0x03))
		{
			WriteLogMsg("Adexp Msg : Invalid header",LogError);
			//return false;
			EndHeader=0;
		}
		else
		{
			m_Header=m_Msg.Mid(1,EndHeader-2);
			if (m_Header.GetLength())
				DecodeHeader();
		}
	}
	m_Msg+="  ";
	int fnd1,fnd2;
	//decode body
	//find message type
	fnd1=m_Msg.Find("-TITLE ",EndHeader);
	if (fnd1!=-1)
	{
		fnd2=FindHyphen(m_Msg,fnd1+1);
		CurMsgStr=m_Msg.Mid(fnd1+7,fnd2-fnd1-7);
		CurMsgStr.TrimRight();
		CurMsgStr.TrimLeft();
		m_MsgType=NONE_TITLE;
		for (int msg=0;msg<=LastMsgType;msg++)
		{
			CurMsgStr.MakeUpper();
			if (CurMsgStr.Compare(MsgTable[msg].Name)==0)
			{
				m_MsgType=(AdexpMsgTypes)msg;
				m_FieldTable[TITLE].Presence= true;
				m_FieldTable[TITLE].Str=MsgTable[msg].Name;
				break;
			}
		}
		if (m_MsgType==NONE_TITLE)
			return false;
		if ((m_MsgType==ACT) || (m_MsgType==ABI) || (m_MsgType==REV))
		{
			m_Msg.Replace("-PROPFL ","");
		}
		return DecodeAdexpMessage(fnd2-1);
	}
	else
	{// is it Aftn Format ?
		fnd1=m_Msg.Find("(",EndHeader);
		if (fnd1==-1)
		{
			CurMsgStr=m_Msg.Right(m_Msg.GetLength()-EndHeader);
			CurMsgStr.TrimLeft();
			if (CurMsgStr.Left(2)=="CH")
			{
				m_IsCheck=true;
				m_MessageBody=CurMsgStr;
				return true;
			}
			if (CurMsgStr.Left(3)=="SVC")
			{
				m_IsService=true;
				m_MessageBody=CurMsgStr;
			}
			return false;
		}
		fnd2=FindHyphen(m_Msg,fnd1+1);
		CurMsgStr=m_Msg.Mid(fnd1+1,fnd2-fnd1-1);
		CurMsgStr.TrimRight();
		CurMsgStr.TrimLeft();
		fnd2=CurMsgStr.Find("/",0);
		if (fnd2!=-1)
			CurMsgStr=CurMsgStr.Left(fnd2);
		m_MsgType=NONE_TITLE;
		for (int msg=0;msg<=LastMsgType;msg++)
		{
			CurMsgStr.MakeUpper();
			if (CurMsgStr.Left(3).Compare(MsgTable[msg].Name)==0)
			{
				m_MsgType=(AdexpMsgTypes)msg;
				break;
			}
		}
		if (m_MsgType==NONE_TITLE)
			return false;
		m_IsAftn=true;
		return DecodeAftnMessage(fnd1);

	}
		return false;
}*/
bool CAdexpMsg::DecodeAdexpMessage(int CurPos)
{
	CString CurFldStr,CurFldVal,CurMsgStr,SubFld,Wrd;
	int curpos=CurPos,posfld,subpos,subfldpos,IndInList;
	int TmpValue,i;
	int fnd1,fnd2,tmp;
	LPCSTR* EnumTable;
	bool fnd;
	AdexpFields CurFld=NONE_FIELD;
	//first find a field name
	CurFld=GetNextField(m_Msg,curpos,posfld);
	while (CurFld!=NONE_FIELD)
	{
		m_FieldTable[CurFld].Presence=true;
		AdexpFields NxtField=NONE_FIELD;
		int nxtpos=curpos,nxtfldpos;
		switch (FieldTable[CurFld].Type)
		{
		case Str:
			NxtField = GetNextField(m_Msg,nxtpos,nxtfldpos);
			m_FieldTable[CurFld].Str=m_Msg.Mid(curpos+1,nxtfldpos-curpos-2);
			m_FieldTable[CurFld].Str.TrimRight();
			m_FieldTable[CurFld].Str.TrimLeft();
			if ((FieldTable[CurFld].MinLen>m_FieldTable[CurFld].Str.GetLength()) ||
				(FieldTable[CurFld].MaxLen<m_FieldTable[CurFld].Str.GetLength()))
			{
				WriteLogMsg(CString("Adexp Msg : Field ") + FieldTable[CurFld].Name + " has a wrong length",LogError);
				return false;
			}
			break;
		case Enum:
			NxtField = GetNextField(m_Msg,nxtpos,nxtfldpos);
			m_FieldTable[CurFld].Str=m_Msg.Mid(curpos+1,nxtfldpos-curpos-2);
			m_FieldTable[CurFld].Str.TrimRight();
			m_FieldTable[CurFld].Str.TrimLeft();
			EnumTable=(LPCSTR*)FieldTable[CurFld].MinLen;
			fnd=false;
			for (i=0;i<FieldTable[CurFld].MaxLen;i++)
			{
				if (m_FieldTable[CurFld].Str.Compare(EnumTable[i])==0)
				{
					fnd=true;
					break;
				}
			}
			if (!fnd)
			{
				WriteLogMsg(CString("Adexp Msg : Field ") + FieldTable[CurFld].Name + " is not an expected field",LogError);
				return false;
			}
			break;
		case Int:
			NxtField= GetNextField(m_Msg,nxtpos,nxtfldpos);
			m_FieldTable[CurFld].Str=m_Msg.Mid(curpos+1,nxtfldpos-curpos-2);
			TmpValue=atoi(m_FieldTable[CurFld].Str);
			if ((FieldTable[CurFld].MinLen>TmpValue) ||
				(FieldTable[CurFld].MaxLen<TmpValue))
			{
				WriteLogMsg(CString("Adexp Msg : Field ") + FieldTable[CurFld].Name + " is outside of range",LogError);
				return false;
			}
			break;
		case CompoundList:
			fnd1=m_Msg.Find("-END",curpos);
			tmp=fnd1;
			if (fnd1==-1)
			{
				WriteLogMsg("Adexp Msg : -BEGIN without -END ",LogError);
				return false;
			}
			fnd1+=4;
			CurFldVal=GetNextWord(m_Msg,fnd1,fnd2);
			posfld+=5;
			Wrd=GetNextWord(m_Msg,posfld,fnd2);
			if (Wrd.Compare(CurFldVal)!=0)
			{
				WriteLogMsg("Adexp Msg : -BEGIN field is different of -END field",LogError);
				return false;
			}
			CurFldVal=m_Msg.Mid(curpos,tmp-curpos);
			m_FieldTable[CurFld].Str=CurFldVal;
			nxtpos=fnd2+curpos-posfld;
			NxtField=GetNextField(m_Msg,nxtpos,nxtfldpos);
			subpos=0;
			IndInList=1;
			if (FieldTable[CurFld].SubFldTable)
			{
				CurFldStr=SubFieldTable[FieldTable[CurFld].SubFldTable[0].Field].Name;
//				subpos=CurFldVal.find("-");
				subpos=FindHyphen(CurFldVal);
				subpos++;
				Wrd = GetNextWord(CurFldVal,subpos,subfldpos);
				if (Wrd.Compare(CurFldStr)==0)
				{
					fnd1=subpos;
					while (true)
					{
//						fnd1=CurFldVal.find("-",fnd1);
						fnd1=FindHyphen(CurFldVal,fnd1);
						if (fnd1==-1)
						{
							SubFld="-"+CurFldVal.Mid(subpos,CurFldVal.GetLength()-subpos);
							DecodeSubField(CurFld,SubFld,1,IndInList++);
							break;
						}
						else
						{
							tmp=fnd1;
							fnd1++;
							Wrd = GetNextWord(CurFldVal,fnd1,fnd2);
							if (Wrd.Compare(CurFldStr)==0)
							{
								SubFld="-"+CurFldVal.Mid(subpos,tmp-subpos);
								DecodeSubField(CurFld,SubFld,1,IndInList++);
								subpos=fnd1;
							}
							fnd1=fnd2;
						}
					}
				}
			}
			break;
		case Compound:
			NxtField= GetNextField(m_Msg,nxtpos,nxtfldpos);
			SubFld="-"+m_Msg.Mid(posfld,nxtfldpos-posfld-2);
			m_FieldTable[CurFld].Str=m_Msg.Mid(curpos+1,nxtfldpos-curpos-3);
			DecodeSubField(CurFld,SubFld,0,1,true);
			break;
		case CompoundMulti:
			NxtField= GetNextField(m_Msg,nxtpos,nxtfldpos);
			SubFld="-"+m_Msg.Mid(posfld,nxtfldpos-posfld-2);
			m_FieldTable[CurFld].Str+=m_Msg.Mid(curpos+1,nxtfldpos-curpos-3);
			DecodeSubField(CurFld,SubFld,0,1,true);
			break;
		default:
			break;
		}
		curpos=nxtpos;
		posfld=nxtfldpos;
		CurFld=NxtField;
	}
	return true;
}

AdexpFields CAdexpMsg::GetNextField(CString Msg,int& curpos,int& posfld)
{
	CString CurFldStr;
	int fnd1,fnd2,tmp;
	while (true)
	{
		fnd1=Msg.Find("-",curpos);
		if (fnd1==-1)
		{
			posfld=Msg.GetLength();
			curpos=Msg.GetLength();
			return NONE_FIELD;
		}
		fnd1++;
		CurFldStr=CurFldStr=GetNextWord(Msg,fnd1,fnd2);
		if (CurFldStr.IsEmpty())
			return NONE_FIELD;
		if (CurFldStr.Compare("BEGIN")==0)//is it list field ?
		{
			tmp=fnd1;
			fnd1=fnd2+1;
			CurFldStr=GetNextWord(Msg,fnd1,fnd2);
			fnd1=tmp;
			if (CurFldStr.IsEmpty())
				return NONE_FIELD;
		}
		for (int fld=0;fld<=LastField;fld++)
		{
			if (CurFldStr.Compare(FieldTable[fld].Name)==0)
			{
				posfld=fnd1;
				curpos=fnd2;
				return (AdexpFields)fld;
			}
		}
		curpos=fnd2;
	
	}

}

AdexpSubFields CAdexpMsg::GetNextSubField(CString Msg,int& curpos,int& posfld)
{
	CString CurFldStr;
	int fnd1,fnd2;
	while (true)
	{
		fnd1=Msg.Find("-",curpos);
		if (fnd1==-1)
			return NONE_SUBFIELD;
		fnd1++;
		CurFldStr=CurFldStr=GetNextWord(Msg,fnd1,fnd2);
		if (CurFldStr.IsEmpty())
			return NONE_SUBFIELD;
		for (int fld=NONE_SUBFIELD;fld<=LastSubField;fld++)
		{
			if (CurFldStr.Compare(SubFieldTable[fld].Name)==0)
			{
				posfld=fnd1;
				curpos=fnd2;
				return (AdexpSubFields)fld;
			}
		}
		curpos=fnd2;
	}

}

CString CAdexpMsg::GetNextWord(CString Msg,int& firstpos,int& lastpos)
{

	while ((firstpos<(int)Msg.GetLength()) && (Msg[firstpos]==' '))
		firstpos++;
	if (firstpos>=(int)Msg.GetLength())
		return "";
	lastpos=firstpos+1;
	while ((lastpos<(int)Msg.GetLength()) && (Msg[lastpos]!=' '))
		lastpos++;
	return Msg.Mid(firstpos,lastpos-firstpos);
}

void CAdexpMsg::DecodeSubField(AdexpFields CurFld,CString CurFldStr,int Level,int IndexInLevel,bool IsField)
{
	int nxtpos=0,nxtfldpos,curfldpos,curpos,SubIndexInLevel=1;
	AdexpSubFields cursubfld,nxtsubfld;
	SubFieldDef subflddef;
	CString subfldstr;
	FieldTypes CurType;
	if (IsField)
	{
		cursubfld=NONE_SUBFIELD;
		AdexpFields tmpfld=GetNextField(CurFldStr,nxtpos,nxtfldpos);
		CurType=FieldTable[tmpfld].Type;
	}
	else
	{
		cursubfld=GetNextSubField(CurFldStr,nxtpos,nxtfldpos);
		CurType=SubFieldTable[cursubfld].Type;
	}
	switch (CurType)
	{
	case Str:
		subflddef.Str=CurFldStr.Mid(nxtpos+1,CurFldStr.GetLength()-nxtpos);
		subflddef.SubFld=cursubfld;
		subflddef.Level=Level;
		subflddef.IndexInLevel=IndexInLevel;
		m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		break;
	case Enum:
		subflddef.Str=CurFldStr.Mid(nxtpos+1,CurFldStr.GetLength()-nxtpos);
		subflddef.SubFld=cursubfld;
		subflddef.Level=Level;
		subflddef.IndexInLevel=IndexInLevel;
		m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		break;
	case Int:
		subflddef.Str=CurFldStr.Mid(nxtpos+1,CurFldStr.GetLength()-nxtpos);
		subflddef.SubFld=cursubfld;
		subflddef.Level=Level;
		subflddef.IndexInLevel=IndexInLevel;
		m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		break;
	case CompoundMulti:
		if (m_FieldTable[CurFld].SubFieldTable.GetSize())
		{
			for (INT_PTR l=m_FieldTable[CurFld].SubFieldTable.GetSize()-1;l>=0;l--)
			{
				if (m_FieldTable[CurFld].SubFieldTable[l].Level==Level+1)
				{
					SubIndexInLevel=m_FieldTable[CurFld].SubFieldTable[l].IndexInLevel+1;
					break;
				}
			}
		}
	case Compound:
		if (!IsField)
		{
			subflddef.Str=CurFldStr.Mid(nxtpos+1,CurFldStr.GetLength()-nxtpos);
			subflddef.SubFld=cursubfld;
			subflddef.Level=Level;
			subflddef.IndexInLevel=IndexInLevel;
			m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		}
		cursubfld=GetNextSubField(CurFldStr,nxtpos,curfldpos);
		curpos=nxtpos;
		while (cursubfld!=NONE_FIELD)
		{
			if ((SubFieldTable[cursubfld].Type==Compound) && (SubFieldTable[cursubfld].SubFldTable))
			{
				bool fnd=true;
				while (fnd)
				{
					nxtsubfld=GetNextSubField(CurFldStr,nxtpos,nxtfldpos);
					fnd=false;
					for (int i=0;;i++)
					{
						if (SubFieldTable[cursubfld].SubFldTable[i].Field==NONE_FIELD)
							break;
						if (SubFieldTable[cursubfld].SubFldTable[i].Field==nxtsubfld)
						{
							fnd=true;
							break;
						}
					}
				}
			}
			else
				nxtsubfld=GetNextSubField(CurFldStr,nxtpos,nxtfldpos);
			if (nxtsubfld==NONE_FIELD)
				subfldstr="-"+CurFldStr.Mid(curfldpos,CurFldStr.GetLength()-curfldpos);
			else
				subfldstr="-"+CurFldStr.Mid(curfldpos,nxtfldpos-curfldpos-2);
			DecodeSubField(CurFld,subfldstr,Level+1,SubIndexInLevel++);
			cursubfld=nxtsubfld;
			curfldpos=nxtfldpos;
		}

		break;
	default:
		break;
	}
}

int CAdexpMsg::GetMsgNumber()
{
	return m_MessageNumber;
}

CString CAdexpMsg::GetDeviceId()
{
	return m_DeviceId;
}

CString CAdexpMsg::GetPriority()
{
	return m_Priority;
}

INT_PTR CAdexpMsg::GetAddresses(CStringArray& Addr)
{
	Addr.RemoveAll();
	for (int i=0;i<m_Addresses.GetSize();i++)
		Addr.Add(m_Addresses[i]);
	return m_Addresses.GetSize();
}

CString CAdexpMsg::GetOriginator()
{
	return m_Originator;
}


AdexpMsgTypes CAdexpMsg::GetType()
{
	return m_MsgType;
}
bool CAdexpMsg::IsAftn()
{
	return m_IsAftn;
}

bool CAdexpMsg::IsCheckMessage()
{
	return m_IsCheck;
}

bool CAdexpMsg::IsSvcMessage()
{
	return m_IsService;
}

CString CAdexpMsg::GetMsgBody()
{
	return m_MessageBody;
}


bool CAdexpMsg::IsValidForAutomatic()
{
	return m_IsValidAuto;
}

CString CAdexpMsg::GetText()
{
	CString msg= m_Msg;
	msg.Replace("\x01","");
	msg.Replace("\x02","\r\n");
	msg.Replace("\x03","");
	return msg;
}

bool CAdexpMsg::IsPresent(AdexpFields field)
{
	return m_FieldTable[field].Presence;
}


CString CAdexpMsg::GetField(AdexpFields field)
{
	return m_FieldTable[field].Str;
}


CString CAdexpMsg::GetName(AdexpFields field)
{
	return FieldTable[field].Name;
}

bool CAdexpMsg::IsCompoundList(AdexpFields field)
{
	return (FieldTable[field].Type == CompoundList);
}

void CAdexpMsg::AddSubField(AdexpFields Fld,AdexpSubFields SubFld,CString FldStr,int Level,int IndexInLevel)
{
	SubFieldDef subflddef;
	subflddef.Str=FldStr;
	subflddef.SubFld=SubFld;
	subflddef.Level=Level;
	subflddef.IndexInLevel=IndexInLevel;
	m_FieldTable[Fld].SubFieldTable.Add(subflddef);
}

bool CAdexpMsg::GetSubField(AdexpFields Field,AdexpSubFields& SubField,CString& SubFieldStr,int IndexInLevel1,int IndexInLevel2,int IndexInLevel3)
{
	SubFieldStr="";//.Empty();
	SubField=NONE_SUBFIELD;
	for (int i=0;i<m_FieldTable[Field].SubFieldTable.GetSize();i++)
	{
		if ((m_FieldTable[Field].SubFieldTable[i].Level==1) && (m_FieldTable[Field].SubFieldTable[i].IndexInLevel==IndexInLevel1))
		{
			if (IndexInLevel2==0)
			{
				SubFieldStr=m_FieldTable[Field].SubFieldTable[i].Str;
				SubField=m_FieldTable[Field].SubFieldTable[i].SubFld;
				return true;
			}
			else
			{
				i++;
				while ((i<m_FieldTable[Field].SubFieldTable.GetSize()) && (m_FieldTable[Field].SubFieldTable[i].Level>=2))
				{
					if ((m_FieldTable[Field].SubFieldTable[i].Level==2) && (m_FieldTable[Field].SubFieldTable[i].IndexInLevel==IndexInLevel2))
					{
						if (IndexInLevel3==0)
						{
							SubFieldStr=m_FieldTable[Field].SubFieldTable[i].Str;
							SubField=m_FieldTable[Field].SubFieldTable[i].SubFld;
							return true;
						}
						else
						{
							i++;
							while ((i<m_FieldTable[Field].SubFieldTable.GetSize()) && (m_FieldTable[Field].SubFieldTable[i].Level==3))
							{
								if (m_FieldTable[Field].SubFieldTable[i].IndexInLevel==IndexInLevel3)
								{
									SubFieldStr=m_FieldTable[Field].SubFieldTable[i].Str;
									SubField=m_FieldTable[Field].SubFieldTable[i].SubFld;
									return true;
								}
								i++;
							}
							return false;
						}
					}
					i++;
				}
				return false;
			}

		}
	}
	return false;
}

int CAdexpMsg::FindSubFieldLevel1(AdexpFields Field,AdexpSubFields SubField,CString& SubFieldStr)
{
	SubFieldStr="";
	for (int i=0;i<m_FieldTable[Field].SubFieldTable.GetSize();i++)
	{
		if ((m_FieldTable[Field].SubFieldTable[i].Level==1) && (m_FieldTable[Field].SubFieldTable[i].SubFld ==SubField))
		{
			SubFieldStr=m_FieldTable[Field].SubFieldTable[i].Str;
			return m_FieldTable[Field].SubFieldTable[i].IndexInLevel;
		}
	}
	return 0;
}

int CAdexpMsg::FindHyphen(CString Msg,int curpos)
{
	bool fndDelimiter=false;
	for (int i=curpos;i<(int)Msg.GetLength();i++)
	{
		if (!fndDelimiter && (Msg[i]=='['))
		{
			fndDelimiter=true;
		}
		if (fndDelimiter && (Msg[i]==']'))
		{
			fndDelimiter=false;
		}
		if (!fndDelimiter && (Msg[i]=='-'))
		{
			return i;
		}
	}
	return -1;
}


bool CAdexpMsg::DecodeHeader()
{

	CString Header=m_Header+" ",Tmp;
	Header.TrimLeft();
	int fnd=Header.Find(" ");
	Tmp=Header.Left(fnd);
	m_DeviceId=Tmp.Left(3);
	m_MessageNumber=atoi(Tmp.Right(Tmp.GetLength()-3));
	Header=Header.Right(Header.GetLength()-fnd-1);
	Header.TrimLeft();
	fnd=Header.Find(" ");
	Tmp=Header.Left(fnd);
	SYSTEMTIME stime;
	GetSystemTime(&stime);
	stime.wDay=atoi(Tmp.Left(2));
	stime.wHour=atoi(Tmp.Mid(2,2));
	stime.wMinute=atoi(Tmp.Right(2));
	m_MessageTime=stime;
	Header=Header.Right(Header.GetLength()-fnd-1);
	Header.TrimLeft();
	fnd=Header.Find(" ");
	Tmp=Header.Left(fnd);
	if (Tmp.GetLength()==2)
		m_Priority=Tmp;
	Header=Header.Right(Header.GetLength()-fnd-1);
	Header.TrimLeft();
	while (true)
	{
		fnd=Header.Find(" ");
		if (fnd==-1)
			break;
		Tmp=Header.Left(fnd);
		if (Tmp.GetLength()==8)
			m_Addresses.Add(Tmp);
		else
			break;
		Header=Header.Right(Header.GetLength()-fnd-1);
		Header.TrimLeft();
	}
	Header=Header.Right(Header.GetLength()-fnd-1);
	Header.TrimLeft();
	fnd=Header.Find(" ");
	Tmp=Header.Left(fnd);
	m_Originator=Tmp;
	return true;
}

bool CAdexpMsg::DecodeAftnMessage(int CurPos)
{
	const int* AftnDef=&MsgTable[m_MsgType].AftnItemTable[0];
	if (!AftnDef)
		return false;
	int curitem=0,curpos=CurPos,fnd1,fnd2;
	CString CurMsgStr,TmpStr,Ptid,Time,Level,SLevel;
	while (AftnDef[curitem])
	{
		if (curitem==0)
			fnd1=curpos;
		else
			fnd1=FindHyphen(m_Msg,curpos);
		if (fnd1==-1)
			return false;
		fnd2=FindHyphen(m_Msg,fnd1+1);
		if (fnd2==-1)
		{
			fnd2=m_Msg.Find(")",fnd1+1);
			if (fnd2==-1)
				return false;
		}
		CurMsgStr=m_Msg.Mid(fnd1+1,fnd2-fnd1-1);
		curpos=fnd2;
		switch (AftnDef[curitem])
		{
		case 22:
			fnd1=CurMsgStr.Find("/");
			if (fnd1!=-1)
			{
				int fldchg=atoi(CurMsgStr.Left(fnd1));
				CurMsgStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1-1);
				switch (fldchg)
				{
				case 7:
					if (m_FieldTable[ARCID].Presence)
					{
						m_FieldTable[ARCIDK].Presence=true;
						m_FieldTable[ARCIDK].Str=m_FieldTable[ARCID].Str;
						m_FieldTable[ARCID].Presence=false;
					}
					break;
				case 13:
					if (m_FieldTable[ADEP].Presence)
					{
						m_FieldTable[ADEPK].Presence=true;
						m_FieldTable[ADEPK].Str=m_FieldTable[ADEP].Str;
						m_FieldTable[ADEP].Presence=false;
					}
					break;
				case 16:
					if (m_FieldTable[ADES].Presence)
					{
						m_FieldTable[ADESK].Presence=true;
						m_FieldTable[ADESK].Str=m_FieldTable[ADES].Str;
						m_FieldTable[ADES].Presence=false;
					}
					break;

				}
				DecodeAftnSubField(fldchg,CurMsgStr);
			}
			break;
		default:
			DecodeAftnSubField(AftnDef[curitem],CurMsgStr);
			curitem++;
			break;
		}
	}
	const AdexpFields* ValidDef=&MsgTable[m_MsgType].FieldsForAutomatic[0];
	if (ValidDef)
	{
		m_IsValidAuto=true;
		curitem=0;
		while (ValidDef[curitem]!=NONE_FIELD)
		{
			if (!m_FieldTable[ValidDef[curitem]].Presence)
			{
				m_IsValidAuto=false;
				break;
			}
			curitem++;
		}
	}
	return true;
}

void CAdexpMsg::DecodeAftnSubField(int fld,CString CurMsgStr)
{
	int fnd1,i,fnd2,fnd3;
	CString TmpStr="",DecStr[2],Ptid,Time,Level,SLevel,Sender,Receiver,Csn;
	SubFieldDef subflddef;
	if (CurMsgStr.IsEmpty())
		return;
		switch (fld)
		{
		case 3:
			if (CurMsgStr.GetLength()>=3)
			{
				TmpStr=CurMsgStr.Right(CurMsgStr.GetLength()-3);
				fnd1=TmpStr.FindOneOf("0123456789");
				if ((fnd1!=-1) && (TmpStr.GetLength()>=fnd1+3))
				{
					DecStr[0]=TmpStr.Left(fnd1+3);
					DecStr[1]=TmpStr.Right(TmpStr.GetLength()-fnd1-3);
					for (int d=0;d<2;d++)
					{
						if (DecStr[d].GetLength())
						{
							AdexpFields CurFld=REFDATA;
							if (d==1)
								CurFld=MSGREF;
							fnd1=DecStr[d].FindOneOf("0123456789");
							fnd2=DecStr[d].Find("/");
							if (fnd2!=-1)
							{
								m_FieldTable[CurFld].Presence=true;
								m_FieldTable[CurFld].Str=TmpStr;
								AddSubField(CurFld,SENDER,DecStr[d].Left(fnd2),1,1);
								AddSubField(CurFld,RECVR,DecStr[d].Mid(fnd2+1,fnd1-fnd2-1),1,2);
								AddSubField(CurFld,SEQNUM,DecStr[d].Right(TmpStr.GetLength()-fnd1),1,3);
							}
						}
					}
				}

			}
			break;
		case 5:
			break;
		case 7:
			fnd1=CurMsgStr.Find("/");
			if (fnd1!=-1)
			{
				TmpStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1-1);
				CurMsgStr=CurMsgStr.Left(fnd1);
				m_FieldTable[SSRCODE].Presence=true;
				m_FieldTable[SSRCODE].Str=TmpStr;
			}
			m_FieldTable[ARCID].Presence=true;
			m_FieldTable[ARCID].Str=CurMsgStr;
			break;
		case 8:
			TmpStr=CurMsgStr.Left(1);
			if (TmpStr.GetLength())
			{
				m_FieldTable[FLTRUL].Presence=true;
				m_FieldTable[FLTRUL].Str=TmpStr;
			}
			TmpStr=CurMsgStr.Mid(1,1);
			if (TmpStr.GetLength())
			{
				m_FieldTable[FLTTYP].Presence=true;
				m_FieldTable[FLTTYP].Str=TmpStr;
			}
			break;
		case 9:
			fnd1=CurMsgStr.Find("/");
			if (fnd1!=-1)
			{
				TmpStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1-1);
				CurMsgStr=CurMsgStr.Left(fnd1);
				m_FieldTable[WKTRC].Presence=true;
				m_FieldTable[WKTRC].Str=TmpStr;

			}
			TmpStr="";
			for (i=0;i<CurMsgStr.GetLength();i++)
			{
				if (!isdigit(CurMsgStr[i]))
					break;
			}
			if (i!=0)
			{
				TmpStr=CurMsgStr.Left(i);
				CurMsgStr=CurMsgStr.Right(CurMsgStr.GetLength()-i);
				m_FieldTable[NBARC].Presence=true;
				m_FieldTable[NBARC].Str=TmpStr;
			}
			if (CurMsgStr.GetLength())
			{
				m_FieldTable[ARCTYP].Presence=true;
				m_FieldTable[ARCTYP].Str=CurMsgStr;
			}
			break;
		case 10:
			fnd1=CurMsgStr.Find("/");
			if (fnd1!=-1)
			{
				TmpStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1-1);
				CurMsgStr=CurMsgStr.Left(fnd1);
				m_FieldTable[SEQPT].Presence=true;
				m_FieldTable[SEQPT].Str=TmpStr;

			}
			if (CurMsgStr.GetLength())
			{
				m_FieldTable[CEQPT].Presence=true;
				m_FieldTable[CEQPT].Str=CurMsgStr;
			}
			break;
		case 13:
			TmpStr=CurMsgStr.Left(4);
			if (TmpStr.GetLength()==4)
			{
				m_FieldTable[ADEP].Presence=true;
				m_FieldTable[ADEP].Str=TmpStr;
			}
			TmpStr=CurMsgStr.Mid(4,4);
			if (TmpStr.GetLength()==4)
			{
				m_FieldTable[EOBT].Presence=true;
				m_FieldTable[EOBT].Str=TmpStr;
			}
			break;
		case 14:
			fnd1=CurMsgStr.Find("/");
			if (fnd1!=-1)
			{
				TmpStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1-1);
				CurMsgStr=CurMsgStr.Left(fnd1);
				if (TmpStr.GetLength()>=4)
				{
					Time=TmpStr.Left(4);
					TmpStr=TmpStr.Right(TmpStr.GetLength()-4);
					if (TmpStr[0]=='F' || TmpStr[0]=='S' || TmpStr[0]=='A' || TmpStr[0]=='M')
					{
						for (i=1;i<TmpStr.GetLength();i++)
						{
							if (!isdigit(TmpStr[i]))
								break;
						}
						Level=TmpStr.Mid(0,i);
						SLevel=TmpStr.Right(TmpStr.GetLength()-i);
					}
				}

			}
			if (CurMsgStr.GetLength())
			{
				Ptid=CurMsgStr;
				if (Time.GetLength() && Level.GetLength())
				{
					TmpStr="-ESTDATA -PTID "+Ptid+" -ETO "+Time+" -FL "+Level;
					if (SLevel.GetLength())
						TmpStr+=" -SFL "+SLevel;
					m_FieldTable[ESTDATA].Presence=true;
					m_FieldTable[ESTDATA].Str=TmpStr;
					DecodeSubField(ESTDATA,TmpStr,0,1,true);
				}
			}
			break;
		case 15:
			fnd1=CurMsgStr.Find(" ");
			if (fnd1!=-1)
			{
				TmpStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1-1);
				CurMsgStr=CurMsgStr.Left(fnd1);
				m_FieldTable[ROUTE].Presence=true;
				m_FieldTable[ROUTE].Str=TmpStr;
			}
			if (CurMsgStr[0]=='K' || CurMsgStr[0]=='N' || CurMsgStr[0]=='M')
			{
				for (i=1;i<CurMsgStr.GetLength();i++)
				{
					if (!isdigit(CurMsgStr[i]))
						break;
				}
				TmpStr=CurMsgStr.Mid(0,i);
				m_FieldTable[SPEED].Presence=true;
				m_FieldTable[SPEED].Str=TmpStr;
				CurMsgStr=CurMsgStr.Right(CurMsgStr.GetLength()-i);
				if (CurMsgStr[0]=='F' || CurMsgStr[0]=='S' || CurMsgStr[0]=='A' || CurMsgStr[0]=='M' || CurMsgStr[0]=='V')
				{
					m_FieldTable[RFL].Presence=true;
					m_FieldTable[RFL].Str=CurMsgStr;
				}
			}

			break;
		case 16:
			TmpStr=CurMsgStr.Left(4);
			if (TmpStr.GetLength()==4)
			{
				m_FieldTable[ADES].Presence=true;
				m_FieldTable[ADES].Str=TmpStr;
			}
			TmpStr=CurMsgStr.Mid(4,4);
			if (TmpStr.GetLength()==4)
			{
				m_FieldTable[TTLEET].Presence=true;
				m_FieldTable[TTLEET].Str=TmpStr;
			}
			fnd1=CurMsgStr.Find(" ");
			if (fnd1!=-1)
			{
				CurMsgStr=CurMsgStr.Right(CurMsgStr.GetLength()-fnd1);
				CurMsgStr.TrimLeft();
				TmpStr=CurMsgStr.Left(4);
				if (TmpStr.GetLength()==4)
				{
					m_FieldTable[ALTRNT1].Presence=true;
					m_FieldTable[ALTRNT1].Str=TmpStr;
				}
				CurMsgStr=CurMsgStr.Right(CurMsgStr.GetLength()-4);
				CurMsgStr.TrimLeft();
				TmpStr=CurMsgStr.Left(4);
				if (TmpStr.GetLength()==4)
				{
					m_FieldTable[ALTRNT2].Presence=true;
					m_FieldTable[ALTRNT2].Str=TmpStr;
				}
			}
			break;
		case 17:
			TmpStr=CurMsgStr.Left(4);
			if (TmpStr.GetLength()==4)
			{
				m_FieldTable[ADES].Presence=true;
				m_FieldTable[ADES].Str=TmpStr;
			}
			TmpStr=CurMsgStr.Mid(4,4);
			if (TmpStr.GetLength()==4)
			{
				m_FieldTable[ATA].Presence=true;
				m_FieldTable[ATA].Str=TmpStr;
			}
			break;
		case 18:
			m_FieldTable[FIELD18].Presence=true;
			m_FieldTable[FIELD18].Str=CurMsgStr;
			CurMsgStr+=" /";
			for (i=0;i<17;i++)
			{
				fnd1=CurMsgStr.Find(Field18[i].Seq);
				if (fnd1!=-1)
				{
					//fnd2=CurMsgStr.Find("/",fnd1+CString(Field18[i].Seq).GetLength());
					fnd2 = CurMsgStr.ReverseFind('/');
					TmpStr=CurMsgStr.Mid(fnd1+CString(Field18[i].Seq).GetLength(),fnd2-fnd1-CString(Field18[i].Seq).GetLength());
					//TmpStr = CurMsgStr.Mid(fnd1 + CString(Field18[i].Seq).GetLength(), CurMsgStr.GetLength());
					fnd3=TmpStr.ReverseFind(' ');
					if (fnd3!=-1)
						TmpStr=TmpStr.Left(fnd3);
					if (Field18[i].Field!=NONE_FIELD)
					{
						m_FieldTable[Field18[i].Field].Presence=true;
						m_FieldTable[Field18[i].Field].Str = TmpStr;
					}
				}
			}
			break;
		case 19:
			CurMsgStr+=" ";
			for (i=0;i<9;i++)
			{
				fnd1=CurMsgStr.Find(Field19[i].Seq);
				if (fnd1!=-1)
				{
					fnd2=CurMsgStr.Find(" ",fnd1);
					TmpStr=CurMsgStr.Mid(fnd1+CString(Field19[i].Seq).GetLength(),fnd2-fnd1-CString(Field19[i].Seq).GetLength());
					if (Field19[i].Field==SPLDNB)
					{
						NStringUtil::GetArgumentNb(TmpStr,1,' ',Ptid);
					}
					else
						if (Field19[i].Field!=NONE_FIELD)
						{
							m_FieldTable[Field19[i].Field].Presence=true;
							m_FieldTable[Field19[i].Field].Str=TmpStr;
						}
				}
			}
			break;
			}
}


void CAdexpMsg::SetAftn()
{
	m_IsAftn=true;
}


void CAdexpMsg::SetType(AdexpMsgTypes type)
{
	m_MsgType = type;
}

CString CAdexpMsg::EncodeMessage(bool WithHeader)
{
	if (m_IsAftn)
		return EncodeAftnMessage(WithHeader);
	else
		return EncodeAdexpMessage(WithHeader);
	return "";
}

int  CAdexpMsg::GetMessage(BYTE* buff,int MaxLen)
{
	return 0;
}

bool CAdexpMsg::SetField(AdexpFields Field,CString str)
{
	m_FieldTable[Field].Presence=true;
	m_FieldTable[Field].Str=str;
	return true;
}

bool CAdexpMsg::SetSubField(AdexpFields Field,AdexpSubFields SubField,CString,int IndexInLevel1,int IndexInLevel2,int IndexInLevel3)
{
	return true;
}

CString CAdexpMsg::EncodeAftnMessage(bool WithHeader)
{
	int curitem=0,i;
	const int* AftnDef=&MsgTable[m_MsgType].AftnItemTable[0];
	if (!AftnDef)
		return "";
	CString msg,subfld;
	bool firstitem;
	while (AftnDef[curitem])
	{
		switch (AftnDef[curitem])
		{
		case 3:
			msg+=CString("-")+MsgTable[m_MsgType].Name;
			if (m_FieldTable[REFDATA].Presence)
			{
				if (m_FieldTable[REFDATA].SubFieldTable.GetSize())
				{
					FindSubFieldLevel1(REFDATA,SENDER,subfld);
					msg+=subfld+"/";
					FindSubFieldLevel1(REFDATA,RECVR,subfld);
					msg+=subfld;
					FindSubFieldLevel1(REFDATA,SEQNUM,subfld);
					msg+=subfld;

				}
				else
				{
					msg+=m_FieldTable[REFDATA].Str;
				}
				if (m_FieldTable[MSGREF].Presence)
				{
					if (m_FieldTable[MSGREF].SubFieldTable.GetSize())
					{
						FindSubFieldLevel1(MSGREF,SENDER,subfld);
						msg+=subfld+"/";
						FindSubFieldLevel1(MSGREF,RECVR,subfld);
						msg+=subfld;
						FindSubFieldLevel1(MSGREF,SEQNUM,subfld);
						msg+=subfld;
						
					}
					else
					{
						msg+=m_FieldTable[MSGREF].Str;
					}
				}
			}
			break;
		case 5:
			break;
		case 7:
			if (m_FieldTable[ARCID].Presence)
			{
				msg+="-"+m_FieldTable[ARCID].Str;
				if (m_FieldTable[SSRCODE].Presence)
					msg+=m_FieldTable[SSRCODE].Str;
			}
			break;
		case 8:
			if (m_FieldTable[FLTRUL].Presence)
			{
				msg+="-"+m_FieldTable[FLTRUL].Str;
				if (m_FieldTable[FLTTYP].Presence)
					msg+=m_FieldTable[FLTTYP].Str;
			}
			break;
		case 9:
			if (m_FieldTable[ARCTYP].Presence)
			{
				msg+="-";
				if (m_FieldTable[NBARC].Presence)
					msg+=m_FieldTable[NBARC].Str;
				msg+=m_FieldTable[ARCTYP].Str;
				if (m_FieldTable[WKTRC].Presence)
					msg+="/"+m_FieldTable[WKTRC].Str;
			}
			break;
		case 10:
			if (m_FieldTable[SEQPT].Presence)
			{
				msg+="-"+m_FieldTable[SEQPT].Str;
				if (m_FieldTable[CEQPT].Presence)
					msg+=m_FieldTable[CEQPT].Str;

			}
			break;		
		case 13:
			if (m_FieldTable[ADEP].Presence)
			{
				msg="-"+m_FieldTable[ADEP].Str;
				if (m_FieldTable[EOBT].Presence)
					msg+=m_FieldTable[EOBT].Str;
			}
			break;
		case 14:
			if (m_FieldTable[ESTDATA].Presence)
			{
				msg+="-";
				if (m_FieldTable[ESTDATA].SubFieldTable.GetSize())
				{
						FindSubFieldLevel1(ESTDATA,PTID,subfld);
						msg+=subfld+"/";
						FindSubFieldLevel1(ESTDATA,ETO,subfld);
						msg+=subfld;
						FindSubFieldLevel1(ESTDATA,FL,subfld);
						msg+=subfld;
						FindSubFieldLevel1(ESTDATA,SFL,subfld);
						msg+=subfld;

				}
			}
			break;
		case 15:
			msg+="-";
			if (m_FieldTable[SPEED].Presence)
				msg+=m_FieldTable[SPEED].Str;
			if (m_FieldTable[RFL].Presence)
				msg+=m_FieldTable[RFL].Str;
			if (m_FieldTable[ROUTE].Presence)
				msg+=" "+m_FieldTable[ROUTE].Str;
			break;
		case 16:
			if (m_FieldTable[ADES].Presence)
			{
				msg+="-"+m_FieldTable[ADES].Str;
				if (m_FieldTable[TTLEET].Presence)
					msg+=m_FieldTable[TTLEET].Str;
				if (m_FieldTable[ALTRNT1].Presence)
					msg+=" "+m_FieldTable[ALTRNT1].Str;
				if (m_FieldTable[ALTRNT2].Presence)
					msg+=" "+m_FieldTable[ALTRNT2].Str;

			}
			break;
		case 17:
			if (m_FieldTable[ADES].Presence)
			{
				msg+="-"+m_FieldTable[ADES].Str;
				if (m_FieldTable[ATA].Presence)
					msg+=m_FieldTable[ATA].Str;

			}
			break;
		case 18:
			firstitem=true;
			for (i=0;i<17;i++)
			{
				if (m_FieldTable[Field18[i].Field].Presence)
				{
					if (firstitem)
						msg+="-";
					else
						msg+=" ";
					msg+=CString(Field18[i].Seq)+"/"+m_FieldTable[Field18[i].Field].Str;
					firstitem=false;					
				}
			}
			break;
		case 19:
			firstitem=true;
			for (i=0;i<9;i++)
			{
				if (m_FieldTable[Field19[i].Field].Presence)
				{
					if (firstitem)
						msg+="-";
					else
						msg+=" ";
					msg+=CString(Field19[i].Seq)+"/"+m_FieldTable[Field19[i].Field].Str;
					firstitem=false;					
				}
			}
			break;
		}
		curitem++;
	}
	return msg;
}

CString CAdexpMsg::EncodeAdexpMessage(bool WithHeader)
{
	CString Msg;
	if (WithHeader)
		Msg="  ";
	Msg+=CString("-TITLE ")+MsgTable[m_MsgType].Name;
	Msg+=CString(" -REFDATA ")+m_FieldTable[REFDATA].Str+" ";
	for (int i=0;i<=WKTRC;i++)
	{
		if ((i!=REFDATA) && (m_FieldTable[i].Presence))
		{
			Msg+=CString("-")+FieldTable[i].Name+" "+m_FieldTable[i].Str+" ";
		}
	}
	if (WithHeader)
	{
		Msg+=" ";
		Msg.SetAt(0,0x01);
		Msg.SetAt(1,0x02);
		Msg.SetAt(Msg.GetLength()-1,0x03);
	}
	return Msg;
}