#pragma once
#include "afxcmn.h"

class CFplDoc;

class CFplWnd :
	public CListCtrl
{
public:
	CFplWnd();
	~CFplWnd();

	CFplDoc* m_pDoc;
	CFont m_font;
	int m_hext;
	bool IsOpen;
	CFile m_file;
	CMenu m_Popup;
	int m_CurFplNb;
	int m_CurColNb;
	int m_CurSorting;
	static int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

	void AddFpl();
	void UpdateFpl(int fplnb);
	CString DecodeIcaoRoute(int fplnb);
	CString DecodeRemark(int fplnb);
	bool OfflineError(int FplNb);
	bool TrafficError(int FplNb);

protected:
	//{{AFX_MSG(CFplWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNcMButtonDown(UINT nHitTest, CPoint point);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSequenceError();
	afx_msg void OnOfflineError();
	afx_msg void OnTrafficError();
	afx_msg void OnRouteInformation();
	afx_msg void OnIcaoRoute();
	afx_msg void OnRemark();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

