
#if !defined(AFX_ADJCENTFIR_H)
#define AFX_ADJACENTFIR_H

#include "CommonObject.h"
//#include "ClientSocket.h"
#include "Adexp.h"
class CFlightPlan;
class CAdexpMsg;

class CAdjacentFir : public CCommonObject
{
public:
	CAdjacentFir();
	~CAdjacentFir();
	void OnReceiveMsg(BYTE* ,int,sockaddr_in* =NULL,int =0,int =0);
	CString m_Name;
	CString m_AtsName;
	char m_SsrFamilly;
	int m_Nbr;
	SOCKADDR_IN m_Addr;
	UINT m_Port;
	bool m_IsLocal;
	bool m_IsSysco;
	int GetNextCsn();
	bool IsConnected();
	typedef enum {NoTyp,OperationalMsg,OperatorMsg,SystemMsg,StatusMsg} OldiMsgTyp;
	static CAdjacentFir* GetFirByName(CString Name);
	static CAdjacentFir* GetFirByAtsName(CString AtsName);
	static CAdjacentFir* GetFirByNbr(int Nbr);
	static bool IsAutoLam();
	static void InitTables(CString IniName);
private:
	int	m_SbyTimeOut;
	int m_LamTimeOut;
	bool m_IsConnected;
	bool m_FirstConnect;
	//CClientSocket m_Socket;
	int m_Csn;
	CTime m_LastConnectTime;
	typedef struct 
	{
		int Csn;
		CTime TimeoutTime;
		CFlightPlan* pFpl;
		CString RefData;
		int TransactionTimeout;
	} TimeoutDef;
	CArray<TimeoutDef,TimeoutDef> m_WaitingLamSbyTable;
	void TryToConnect();
	static CArray<CAdjacentFir*,CAdjacentFir*> m_FirTable;
	//static CCommonObject* m_pMngr;
	static int m_Typ;
	static bool m_AutoLam;
	static int m_OldiHeaderLen;
	static bool m_OldiHeader;
	static int m_CoordinationTimeout;
};

#endif