#include "stdafx.h"
#include "TechnicalValidationFile.h"
#include "Technical Validation.h"
#include "Adexp.h"
#include "Flightplan.h"
#include "FplDoc.h"

CTechnicalValidationFile::CTechnicalValidationFile()
{
	m_Modified = false;
	m_Open = false;
}

CTechnicalValidationFile::CTechnicalValidationFile(CString FileName, FileType fileType)
{
	m_Modified = false;
	m_Open = false;
	Open(FileName, fileType);
}


CTechnicalValidationFile::~CTechnicalValidationFile()
{
}

bool CTechnicalValidationFile::Open(CString FileName, FileType fileType)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	if (CStdioFile::Open(FileName, CFile::modeRead))
	{
		CString tmp;
		while (1)
		{
			if (!ReadString(tmp))
				break;
			tmp = tmp.Left(tmp.GetLength());
			m_StringTable.Add(tmp);
			if (fileType== AFTN)
			{
				pApp->m_pDoc->ReceiveFromAftn(tmp);
			}
		}
		m_Modified = false;
		m_Open = true;
		return true;
	}
	else
		return false;
}

void CTechnicalValidationFile::Close()
{
	if (m_Open)
	{
		CString FileName = GetFilePath();
		CFile::Close();
		if (m_Modified)
		{
			CFile::Open(FileName, CFile::modeWrite | CFile::modeCreate);
			for (int line = 0; line < m_StringTable.GetSize(); line++)
			{
				WriteString(m_StringTable[line]);
			}
			CFile::Close();
		}
		m_StringTable.RemoveAll();
		m_Open = false;
	}
}

bool CTechnicalValidationFile::IsOpen()
{
	return m_Open;
}
