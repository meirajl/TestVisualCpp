#include <stdafx.h>
#include "resource.h"       // main symbols
#include "AdjacentFir.h"
#include "IniFile.h"
#include "StringUtil.h"
#include "FplDoc.h"
#include "Adexp.h"
#include "FlightPlan.h"



CArray<CAdjacentFir*,CAdjacentFir*> CAdjacentFir::m_FirTable;
//CCommonObject* CAdjacentFir::m_pMngr=NULL;
int CAdjacentFir::m_Typ=0;
bool CAdjacentFir::m_AutoLam= false;
int CAdjacentFir::m_OldiHeaderLen=8;
bool CAdjacentFir::m_OldiHeader=true;
int CAdjacentFir::m_CoordinationTimeout=90;

const CString Startup="01";
const CString Shutdown="00";
const CString Heartbeat="03";


CAdjacentFir::CAdjacentFir()
{
	m_Csn=0;
	m_IsConnected=false;
	m_LastConnectTime = CTime::GetCurrentTime();
	m_FirstConnect=false;
}

CAdjacentFir::~CAdjacentFir()
{
}

void CAdjacentFir::OnReceiveMsg(BYTE* pMsg,int Len,sockaddr_in* addr,int addrlen,int)
{

}

CAdjacentFir* CAdjacentFir::GetFirByName(CString Name)
{
	for (int i=0;i<m_FirTable.GetSize();i++)
		if (m_FirTable[i]->m_Name.CompareNoCase(Name)==0)
			return m_FirTable[i];
	return NULL;
}

CAdjacentFir* CAdjacentFir::GetFirByAtsName(CString AtsName)
{
	for (int i=0;i<m_FirTable.GetSize();i++)
		if (m_FirTable[i]->m_AtsName.CompareNoCase(AtsName)==0)
			return m_FirTable[i];
	return NULL;
}
CAdjacentFir* CAdjacentFir::GetFirByNbr(int Nbr)
{
	if ((Nbr>=0) && (Nbr<m_FirTable.GetSize()))
		return m_FirTable[Nbr];
	return NULL;
}

bool CAdjacentFir::IsConnected()
{
	return m_IsConnected;
}

int CAdjacentFir::GetNextCsn()
{
	return m_Csn%999+1;
}


bool CAdjacentFir::IsAutoLam()
{
	return m_AutoLam;
}

void CAdjacentFir::InitTables(CString IniName)
{
	//CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	CIniFile File(IniName);
	CString AllKey, Key, tmp;
	CAdjacentFir* pCurFir;
	if (File.GetIniProfileString("FIRS", "AUTOLAM", AllKey))
		m_AutoLam = (AllKey == "YES");
	if (File.GetIniProfileString("FIRS", "OLDIHEADER", AllKey))
		m_OldiHeader = (AllKey == "YES");
	if (File.GetIniProfileString("FIRS", "OLDIHEADERLEN", AllKey))
		m_OldiHeaderLen = atoi(AllKey);
	m_CoordinationTimeout = 90;
	if (File.GetIniProfileString("FIRS", "COORDINATIONTIMEOUT", AllKey))
		m_CoordinationTimeout = atoi(AllKey);
	for (int i = 0;; i++)
	{
		if (i == 0)
			Key = "LOCAL";
		else
			Key.Format("FIR%d", i);
		if (!File.GetIniProfileString("FIRS", Key, AllKey))
			break;
		if (NStringUtil::GetNbrOfArgument(AllKey, '|') == 8)
		{
			pCurFir = new CAdjacentFir();
			pCurFir->m_IsLocal = (i == 0);
			NStringUtil::GetArgumentNb(AllKey, 1, '|', pCurFir->m_Name);
			NStringUtil::GetArgumentNb(AllKey, 2, '|', pCurFir->m_AtsName);
			NStringUtil::GetArgumentNb(AllKey, 3, '|', tmp);
			pCurFir->m_SbyTimeOut = atoi(tmp);
			NStringUtil::GetArgumentNb(AllKey, 4, '|', tmp);
			pCurFir->m_LamTimeOut = atoi(tmp);
			NStringUtil::GetArgumentNb(AllKey, 5, '|', tmp);
			if (tmp.GetLength())
			{
				pCurFir->m_Addr.sin_addr = NStringUtil::DecodeAddr(tmp);
				if (pCurFir->m_Addr.sin_addr.s_addr != 0)
				{
					pCurFir->m_IsConnected = true;
					//					pApp->m_ErrorList.SetError("Fir "+pCurFir->m_Name);
					//pApp->LogMsg("Fir " + pCurFir->m_Name + " is OFF");
				}
			}
			NStringUtil::GetArgumentNb(AllKey, 6, '|', tmp);
			if (tmp.GetLength())
			{
				pCurFir->m_Port = NStringUtil::DecodePort(tmp, true);
				if (pCurFir->m_Port != 0)
				{
					pCurFir->m_IsConnected = true;
					//					pApp->m_ErrorList.SetError("Fir "+pCurFir->m_Name);
					//pApp->LogMsg("Fir " + pCurFir->m_Name + " is OFF");
				}
			}
			NStringUtil::GetArgumentNb(AllKey, 7, '|', tmp);
			pCurFir->m_IsSysco = (tmp == "Y");
			if (i != 0)
			{
			}
			else
			{
			}
			pCurFir->m_Nbr = i;
			NStringUtil::GetArgumentNb(AllKey, 8, '|', tmp);
			pCurFir->m_SsrFamilly = tmp[0];
			m_FirTable.Add(pCurFir);
		}
	}

}