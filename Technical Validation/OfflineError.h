#pragma once
#include "afxwin.h"
#include "resource.h"

class COfflineError :
	public CDialog
{
public:
	COfflineError(CWnd* pParent = NULL);   // standard constructor
	~COfflineError();

	// Dialog Data
	//{{AFX_DATA(COfflineError)
	enum { IDD = IDD_SEQUENCE_ERROR };
	CString	m_OfflineError;
	CString m_Arcid;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COfflineError)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COfflineError)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

