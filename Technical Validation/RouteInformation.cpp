#include "stdafx.h"
#include "RouteInformation.h"


CRouteInformation::CRouteInformation(CWnd* pParent /*=NULL*/)
	: CDialog(CRouteInformation::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTrafficError)
	m_RouteInformation = _T("");
	m_Arcid = _T("");
	//}}AFX_DATA_INIT
}


CRouteInformation::~CRouteInformation()
{
}

void CRouteInformation::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrafficError)
	DDX_Text(pDX, IDC_EDIT2, m_RouteInformation);
	DDX_Text(pDX, IDC_ARCIDEDIT, m_Arcid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRouteInformation, CDialog)
	//{{AFX_MSG_MAP(CRouteInformation)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()