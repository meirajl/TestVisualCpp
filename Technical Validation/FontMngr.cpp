#include <stdafx.h>
#include "FontMngr.h"

typedef struct
{
	CString Name;
	LOGFONT LogFont;
	CFont* pFont;
} FontData;
CArray<FontData,FontData> m_FontTable;
LOGFONT m_DefaultFont;

void CFontMngr::InitTables()
{
	CString AllKey,Key,FontKey,IniName,fontname;
	FontData curdata;
	int len,ln,fnd,fontsize;
	GetCurrentDirectory(256, IniName.GetBuffer(256));
	IniName.ReleaseBuffer();
	IniName = IniName + "\\tp.ini";
	//IniName =((CTpApp*)AfxGetApp())->GetIniName();
	len = GetPrivateProfileString("FONTS",NULL,"",AllKey.GetBufferSetLength(2000),2000,IniName);
	AllKey.ReleaseBuffer(len);
	CFont font;
	bool Italic,Underline,Bold;
	for (int i=0;i<len;i++)
	{
		if ((AllKey[i]==0) || (i==len-1))
		{
			Italic=Underline=Bold=false;
			ln = GetPrivateProfileString("FONTS",Key,"",FontKey.GetBufferSetLength(200),200,IniName);
			FontKey.ReleaseBuffer();
			fnd=FontKey.Find("|",0);
			fontname=FontKey.Left(fnd);
			FontKey=FontKey.Right(FontKey.GetLength()-fnd-1);
			fnd=FontKey.Find('|');
			if (fnd!=-1)
			{
				sscanf(FontKey.Left(fnd),"%d",&fontsize);
				FontKey=FontKey.Right(FontKey.GetLength()-fnd-1);
				Italic = (FontKey.Find('I')!=-1);
				Bold = (FontKey.Find('B')!=-1);
				Underline = (FontKey.Find('U')!=-1);
			}
			else
				sscanf(FontKey,"%d",&fontsize);
			font.CreatePointFont(fontsize,fontname);
			font.GetLogFont(&curdata.LogFont);
			font.DeleteObject();
			HDC hDC = GetDC(NULL);
			POINT pt;
			pt.y = ::GetDeviceCaps(hDC, LOGPIXELSY) * fontsize;
			pt.y /= 720;
			::DPtoLP(hDC, &pt, 1);
			POINT ptOrg = { 0,0 };
			::DPtoLP(hDC, &ptOrg, 1);
			curdata.LogFont.lfHeight = -abs(pt.y - ptOrg.y);
			ReleaseDC(NULL, hDC);
			if (Bold)
				curdata.LogFont.lfWeight=FW_BOLD;
			if (Italic)
				curdata.LogFont.lfItalic = TRUE;
			if (Underline)
				curdata.LogFont.lfUnderline  = TRUE;
			curdata.Name=Key;
			curdata.pFont= new CFont();
			curdata.pFont->CreateFontIndirect(&curdata.LogFont);
			m_FontTable.Add(curdata);
			Key.Empty();
		}
		else
			Key+=AllKey[i];
	}
	font.CreatePointFont(120,"system");
	font.GetLogFont(&m_DefaultFont);
	
	
}

void CFontMngr::RemoveTables()
{
}

int CFontMngr::GetFontNb(CString Name)
{
	for (int i=0;i<m_FontTable.GetSize();i++)
	{
		if (m_FontTable[i].Name.CompareNoCase(Name)==0)
			return i;
	}
	return -1;
}

	
LOGFONT CFontMngr::GetLogFont(int fontnb)
{
	if (fontnb==-1)
		return m_DefaultFont;
	ASSERT(fontnb<m_FontTable.GetSize());
	return m_FontTable[fontnb].LogFont;
}

CFont* CFontMngr::GetFont(int fontnb)
{
	ASSERT(fontnb<m_FontTable.GetSize());
	return m_FontTable[fontnb].pFont;
}
