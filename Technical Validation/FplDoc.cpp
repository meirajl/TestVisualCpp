#include "stdafx.h"
#include "FplDoc.h"
#include "Adexp.h"
#include "Technical Validation.h"
#include "Geo.h"

CFplDoc::CFplDoc()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	pApp->m_pDoc = this;
	m_FplWnd.m_pDoc = this;

	LoadDefAirspaceFile();
	CGeo::LoadIntermediatePoints("def_airspace.ofl");
	CGeo::InitCenterInIni("def_airspace.ofl");
	LoadDefToolsFile();
	LoadDefWindowsFile();
}


CFplDoc::~CFplDoc()
{
}

void CFplDoc::LoadDefAirspaceFile()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString IniName, DirName;

	GetCurrentDirectory(256, DirName.GetBuffer(256));
	DirName.ReleaseBuffer();

	IniName = DirName + "\\def_airspace.ofl";

	pApp->m_pOffline->LoadDepartureList(IniName);
	pApp->m_pOffline->LoadArrivalList(IniName);
	pApp->m_pOffline->LoadLogicalSectorsDefinition(IniName);
	pApp->m_pOffline->LoadEntryPoint(IniName);
	pApp->m_pOffline->LoadExitPoint(IniName);
	pApp->m_pOffline->LoadSectors(IniName);
}

void CFplDoc::LoadDefToolsFile()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString DirName, IniName;

	GetCurrentDirectory(256, DirName.GetBuffer(256));
	DirName.ReleaseBuffer();

	IniName = DirName + "\\def_tools.ofl";
	pApp->m_pOffline->LoadSid(IniName);
	pApp->m_pOffline->LoadStar(IniName);
	pApp->m_pOffline->LoadAirport(IniName);	
}

void CFplDoc::LoadDefWindowsFile()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString DirName, IniName;

	GetCurrentDirectory(256, DirName.GetBuffer(256));
	DirName.ReleaseBuffer();

	IniName = DirName + "\\def_windows.ofl";
	pApp->m_pOffline->LoadDefWindows(IniName);
}

CFlightPlan* CFplDoc::FindFpl(CString Arcid, CString Adep, CString Ades, CString Eobt)
{
	for (int i = 0; i<m_FplTable.GetSize(); i++)
	{
		if ((m_FplTable[i]) && (Arcid.CompareNoCase(m_FplTable[i]->m_Arcid) == 0) && (Adep.CompareNoCase(m_FplTable[i]->m_Adep) == 0) && (Ades.CompareNoCase(m_FplTable[i]->m_Ades) == 0) && ((Eobt.GetLength() == 0) || (Eobt.CompareNoCase(m_FplTable[i]->m_Eobt) == 0)))
		{
			return m_FplTable[i];
		}
	}
	for (int fpl = 0; fpl<m_FplInactiveTable.GetSize(); fpl++)
	{
		if (m_FplInactiveTable[fpl] && (m_FplInactiveTable[fpl]->m_Arcid.CompareNoCase(Arcid) == 0) && (Adep.CompareNoCase(m_FplInactiveTable[fpl]->m_Adep) == 0) && (Ades.CompareNoCase(m_FplInactiveTable[fpl]->m_Ades) == 0) && ((Eobt.GetLength() == 0) || (Eobt.CompareNoCase(m_FplInactiveTable[fpl]->m_Eobt) == 0)))
		{
			return m_FplInactiveTable[fpl];
		}
	}
	return NULL;
}

CFlightPlan* CFplDoc::CreateNewFpl(bool Inactive)
{
	CFlightPlan* pFpl = new CFlightPlan();
	for (int i = 0; i<m_FplTable.GetSize(); i++)
	{
		if (!m_FplTable[i] && !m_FplInactiveTable[i])
		{
			if (Inactive)
				m_FplInactiveTable[i] = pFpl;
			else
				m_FplTable[i] = pFpl;
			pFpl->m_FplNbr = i;
			return pFpl;
		}
	}
	INT_PTR ret;
	if (Inactive)
	{
		ret = m_FplTable.Add(NULL);
		m_FplInactiveTable.Add(pFpl);
	}
	else
	{
		ret = m_FplTable.Add(pFpl);
		m_FplInactiveTable.Add(NULL);
	}
	pFpl->m_FplNbr = ret;
	return pFpl;
}

void CFplDoc::ReceiveFromAftn(CString AftnMsg)
{
	CFlightPlan* pFpl = NULL;
	CString Arcid, Adep, Ades;
	int error = 0;

	CAdexpMsg* pAdexpMsg = new CAdexpMsg();
	pAdexpMsg->DecodeMessage(AftnMsg);
	if (pAdexpMsg->IsPresent(ARCID) && pAdexpMsg->IsPresent(ADEP) && pAdexpMsg->IsPresent(ADES))
	{
		if (pAdexpMsg->IsPresent(ARCIDK))
			Arcid = pAdexpMsg->GetField(ARCIDK);
		else
			Arcid = pAdexpMsg->GetField(ARCID);
		if (pAdexpMsg->IsPresent(ADEPK))
			Adep = pAdexpMsg->GetField(ADEPK);
		else
			Adep = pAdexpMsg->GetField(ADEP);
		if (pAdexpMsg->IsPresent(ADESK))
			Ades = pAdexpMsg->GetField(ADESK);
		else
			Ades = pAdexpMsg->GetField(ADES);
		pFpl = FindFpl(Arcid, Adep, Ades, "");
		if ((pAdexpMsg->GetType() == FPL) || (pAdexpMsg->GetType() == CPL))
		{
			if (!pFpl)
				pFpl = CreateNewFpl(pAdexpMsg->GetType() == FPL);
			else
				pFpl = NULL;

		}
		if (pFpl)
		{
			error = pFpl->UpdateFromAftn(pAdexpMsg);
			if (error && ((pAdexpMsg->GetType() == FPL) || (pAdexpMsg->GetType() == CPL)))
			{
				delete pFpl;
				pFpl = NULL;
			}
		}
	}
}

bool CFplDoc::IsEntryPoint(CString Name)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	for (int i = 0; i<pApp->m_pOffline->m_EntryPoints.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_EntryPoints[i].Name == Name)
			return true;
	}
	return false;
}



BOOL CFplDoc::IsExitPoint(CString point)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	point.TrimRight();
	for (int i = 0; i<pApp->m_pOffline->m_ExitCops.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_ExitCops[i].Name == point)
			return TRUE;
	}
	return FALSE;
}

BYTE CFplDoc::ComputeSector(int FL, CString PTID)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	int FLinput = FL;

	for (int i = 0; i<pApp->m_pOffline->m_SectorLogicList.GetSize(); i++)
	{
		if ((pApp->m_pOffline->m_SectorLogicList[i].PTIDs.Find("," + PTID + ",") != -1) && (pApp->m_pOffline->m_SectorLogicList[i].FLmin <= FLinput) &&
			(pApp->m_pOffline->m_SectorLogicList[i].FLmax >= FLinput))
			return pApp->m_pOffline->m_SectorLogicList[i].SL;
	}
	return 0;	// there is no corresponding sector logic...
}

bool CFplDoc::IsRwyAirport(CString Airport)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	for (int i = 0; i < pApp->m_pOffline->m_AirportTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_AirportTable[i].Airport == Airport)
			return true;
	}
	return false;
}

bool CFplDoc::IsSidPoint(CString Adep, CString Point)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	int CurRwy = GetCurrentDepRwy(Adep);
	for (int i = 0; i<pApp->m_pOffline->m_SidTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_SidTable[i].Airport == Adep && pApp->m_pOffline->m_SidTable[i].RwyNb == CurRwy && pApp->m_pOffline->m_SidTable[i].Pnt1 == Point)
		{
			return true;
		}
	}
	return false;
}

int CFplDoc::GetCurrentDepRwy(CString Adep)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	for (int i = 0; i < pApp->m_pOffline->m_AirportTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_AirportTable[i].Airport == Adep)
			return pApp->m_pOffline->m_AirportTable[i].CurDepRwy;
	}
	return 0;
}

int CFplDoc::GetCurrentArrRwy(CString Ades)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	for (int i = 0; i < pApp->m_pOffline->m_AirportTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_AirportTable[i].Airport == Ades)
			return pApp->m_pOffline->m_AirportTable[i].CurArrRwy;
	}
	return 0;
}

CString CFplDoc::FindSid(CString Adep, CString FirstPoint, CString& PointToAdd)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	int CurRwy = GetCurrentDepRwy(Adep);
	for (int i = 0; i<pApp->m_pOffline->m_SidTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_SidTable[i].Airport == Adep && pApp->m_pOffline->m_SidTable[i].RwyNb == CurRwy && pApp->m_pOffline->m_SidTable[i].Pnt1 == FirstPoint)
		{
			PointToAdd = pApp->m_pOffline->m_SidTable[i].PointsToAdd;
			return pApp->m_pOffline->m_SidTable[i].Name;
		}
	}
	return "";

}

CString CFplDoc::FindStar(CString Ades, CString FirstPoint, CString LastPoint, CString& PointToAdd)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	int CurRwy = GetCurrentArrRwy(Ades);
	for (int i = 0; i<pApp->m_pOffline->m_StarTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_StarTable[i].Airport == Ades && pApp->m_pOffline->m_StarTable[i].RwyNb == CurRwy && !pApp->m_pOffline->m_StarTable[i].Pnt2.IsEmpty() && pApp->m_pOffline->m_StarTable[i].Pnt2 == LastPoint)
		{
			PointToAdd = pApp->m_pOffline->m_StarTable[i].PointsToAdd;
			return pApp->m_pOffline->m_StarTable[i].Name;
		}
	}
	return "";

}

bool CFplDoc::IsStarPoint(CString Ades, CString Point)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	int CurRwy = GetCurrentArrRwy(Ades);
	for (int i = 0; i<pApp->m_pOffline->m_StarTable.GetSize(); i++)
	{
		if (pApp->m_pOffline->m_StarTable[i].RwyNb == CurRwy  && pApp->m_pOffline->m_StarTable[i].Airport == Ades && pApp->m_pOffline->m_StarTable[i].Pnt2 == Point)
		{
			return true;
		}
	}
	return false;
}

char CFplDoc::GetAdjSectorType(int SectorNb1, int SectorNb2)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	if (SectorNb1<pApp->m_pOffline->m_SectorTable.GetSize())
	{
		for (int i = 0; i<10; i++)
		{
			if (pApp->m_pOffline->m_SectorTable[SectorNb1].Adj[i] == 0)
				break;
			if (pApp->m_pOffline->m_SectorTable[SectorNb1].Adj[i] == SectorNb2)
				return pApp->m_pOffline->m_SectorTable[SectorNb1].AdjType[i];
		}

	}
	return ' ';
}

CString CFplDoc::GetSectorSite(int SectorNb)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	if (SectorNb<pApp->m_pOffline->m_SectorTable.GetSize())
	{
		return pApp->m_pOffline->m_SectorTable[SectorNb].Site;
	}
	return "";
}

CString CFplDoc::GetAdjSectorXptList(int SectorNb1, int SectorNb2)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	if (SectorNb1<pApp->m_pOffline->m_SectorTable.GetSize())
	{
		for (int i = 0; i<10; i++)
		{
			if (pApp->m_pOffline->m_SectorTable[SectorNb1].Adj[i] == 0)
				break;
			if (pApp->m_pOffline->m_SectorTable[SectorNb1].Adj[i] == SectorNb2)
				return pApp->m_pOffline->m_SectorTable[SectorNb1].AdjXptList[i];
		}

	}
	return "";
}

double CFplDoc::GetXptDistanceDelta(CString Xpt)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	if (Xpt.GetLength())
	{
		for (int xpt = 0; xpt<pApp->m_pOffline->m_ExitCops.GetSize(); xpt++)
		{
			if (pApp->m_pOffline->m_ExitCops[xpt].Name.Find(Xpt) != -1)
				return pApp->m_pOffline->m_ExitCops[xpt].DistAman;
		}
	}
	return -1;
}

int CFplDoc::GetWindowNum(CString XPT)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	if (XPT.GetLength())
	{
		for (int xpt = 0; xpt<pApp->m_pOffline->m_ExitCops.GetSize(); xpt++)
		{
			if (pApp->m_pOffline->m_ExitCops[xpt].Name.Find(XPT) != -1)
				return pApp->m_pOffline->m_ExitCops[xpt].WindowNum;
		}
	}
	return -1;
}

void CFplDoc::GetExCdtError()
{
	UpdateTrig();
	UpdateIflTrig();
}

void CFplDoc::UpdateTrig()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	CFlightPlan*        DD[2];
	CArray<CFlightPlan*, CFlightPlan*> SortTable;

	int NumWindow = 0, IndexNumWindow = 0, sNumWindow = 0;

	bool found = false;
	int m_XptIndex = -1;
	CString Site1 = "", Site2 = "";

	for (int fpl = 0; fpl < pApp->m_pDoc->m_FplTable.GetSize(); fpl++)
	{
		NumWindow = pApp->m_pDoc->GetWindowNum(pApp->m_pDoc->m_FplTable[fpl]->m_Xpt);
		if (NumWindow >= 0)
		{
			if ((pApp->m_pDoc->m_FplTable[fpl])/* && (!pApp->m_pDoc->m_FplTable[fpl]->IsArrival())*/)
			{
				CFlightPlan* pFpl = pApp->m_pDoc->m_FplTable[fpl];
				SortTable.Add(pFpl);
			}
		}
	}

	if (SortTable.GetSize() != 0)
	{
		for (int index = 0; index < SortTable.GetSize(); index++)
		{
			DD[0] = SortTable[index];
			DD[1] = NULL;
			IndexNumWindow = pApp->m_pDoc->GetWindowNum(SortTable[index]->m_Xpt);
			for (int s = 0/*index+1*/; s < SortTable.GetSize(); s++)
			{
				sNumWindow = pApp->m_pDoc->GetWindowNum(SortTable[s]->m_Xpt);
				if ((s != index) && (SortTable[s]->m_Xfl != -1) && (IndexNumWindow == sNumWindow))
				{
					if (TestConflict(SortTable[index], SortTable[s], true) == 1)
					{
						DD[1] = SortTable[s];
						break;
					}
				}
			}
		}
	}
}

void CFplDoc::UpdateIflTrig()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	const int DISPLAY_RANGE = 350;
	const int COMP_DISPLAY_RANGE = 0;

	int					ConfNumb = 0;
	CFlightPlan*        DD[2];
	CArray<CFlightPlan*, CFlightPlan*> SortTable;

	//FBK ModifOps 200912 Removal of XTP equivalence hardcoding
	//	RulesExec			Crules;
	//FBK ModifOps 200912 Removal of XTP equivalence hardcoding End
	CArray<CFlightPlan*, CFlightPlan*> UpdTable;

	int NumWindow, IndexNumWindow, sNumWindow;

	for (int fpl = 0; fpl<pApp->m_pDoc->m_FplTable.GetSize(); fpl++)
	{
		NumWindow = pApp->m_pDoc->GetWindowNum(pApp->m_pDoc->m_FplTable[fpl]->m_IptCentre);
		if (NumWindow >= 0)
		{
			CFlightPlan* pFpl = pApp->m_pDoc->m_FplTable[fpl];
			if (NumWindow == pApp->m_pDoc->GetWindowNum(pFpl->m_IptCentre))
			{
				SortTable.Add(pFpl);
			}
		}
	}

	if (SortTable.GetSize() != 0)
	{

		// Detection et resolution de conflits pour tous les vols
		for (int index = 0; index < SortTable.GetSize(); index++)
		{
			DD[0] = SortTable[index];
			DD[1] = NULL;
			IndexNumWindow = pApp->m_pDoc->GetWindowNum(SortTable[index]->m_IptCentre);
			for (int s = 0/*index+1*/; s<SortTable.GetSize(); s++)
			{
				sNumWindow = pApp->m_pDoc->GetWindowNum(SortTable[s]->m_IptCentre);
				if ((s != index) && (SortTable[s]->m_IflCentre != -1) && (SortTable[s]->m_IflCentre != 0) && (IndexNumWindow == sNumWindow))
				{
					if (TestConflict(SortTable[index], SortTable[s], true, true) == 1)
					{
						DD[1] = SortTable[s];
						break;
					}
				}
			}
		}
	}
}

const int  CFplDoc::TestConflict(CFlightPlan*  pFplA, CFlightPlan*  pFplB, bool OnXfl, bool IflMode)

// "delta"  is in minutes (from 3 to 5 minutes)
//
// This Function tests conflicts between two aircraft, it returns three values:
//	- 0 : No conflict is detected
//	- 1 : A conflict of degree 1 is detected (i.e. a conflict of a high priority )
//	- 2 : A conflict of degree 2 is detected (in fact, it is just a warning)
//
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	//	WORD thelevel,level;
	CString XptA, XptB;
	int XflA, XflB, FdpXptFixA, FdpXptFixB;
	CTime XtnA, XtnB;

	if (IflMode)
	{
		if (!pFplA->m_IptCentre.IsEmpty())
		{
			XptA = pFplA->m_IptCentre;
			XflA = pFplA->m_IflCentre;
			//XtnA = pFplA->m_ItnCentre;
			FdpXptFixA = pFplA->m_IptFdpFix;
		}
		else
		{
			XptA = pFplA->m_Xpt;
			XflA = pFplA->m_Xfl;
			//XtnA = pFplA->m_Etx;
			FdpXptFixA = pFplA->m_XptFdpFix;
		}
		if (!pFplB->m_IptCentre.IsEmpty())
		{
			XptB = pFplB->m_IptCentre;
			XflB = pFplB->m_IflCentre;
			//XtnB = pFplB->m_ItnCentre;
			FdpXptFixB = pFplB->m_IptFdpFix;
		}
		else
		{
			XptB = pFplB->m_Xpt;
			XflB = pFplB->m_Xfl;
			//XtnB = pFplB->m_Etx;
			FdpXptFixB = pFplB->m_XptFdpFix;
		}
		if (OnXfl)
		{
			if ((XflA != XflB) || (XflA == 0))
				return 0;
			//			thelevel=pFplA->m_IflCentre;
		}
		else
		{
			if ((pFplA->m_IflIni != pFplB->m_IflIni) || (pFplA->m_IflIni == 0))
				return 0;
			//			thelevel=pFplA->m_IflIni;
		}

	}
	else
	{
		/*if (pFplA->m_Etx > pFplB->m_Etx)
		{
		CFlightPlan* pFpl = pFplA;
		pFplA = pFplB;
		pFplB = pFpl;
		}*/
		XptA = pFplA->m_Xpt;
		XptB = pFplB->m_Xpt;
		XflA = pFplA->m_Xfl;
		XflB = pFplB->m_Xfl;
		//XtnA = pFplA->m_Etx;
		//XtnB = pFplB->m_Etx;

		if (OnXfl)
		{
			if ((pFplA->m_Xfl != pFplB->m_Xfl) || (pFplA->m_Xfl == 0))
				return 0;
			//			thelevel=pFplA->m_Xfl;
		}
		else
		{
			if ((pFplA->m_XflIni != pFplB->m_XflIni) || (pFplA->m_XflIni == 0))
				return 0;
			//			thelevel=pFplA->m_XflIni;
		}
	}

	//unsigned int deltaConflict = pFplA->m_pDoc->GetXptDistanceDelta(XptA) * 32;// (unsigned int )m_pDoc->CDT_LATERAL_SEPARATION*32;
	//double TMax = m_pDoc->CDT_TMAX * 60;

	//CSegList TheSegs;

	//	WORD DSfiltmin = 0, DSfiltMax = 700;
	//	int DSfilter = (int)m_pDoc->HST_CFL_MIN;
	//	int Testalt = 242;



	//CSegList Segs;
	//if (pFplA->Load2DPistesWithArea(TheSegs, m_pDoc->GetWindowNum(XptA)) && pFplB->Load2DPistesWithArea(Segs, m_pDoc->GetWindowNum(XptA)))
	//{
	//CRCPPosition Cin1, Cin2;
	//CRCPPosition Cout1, Cout2;
	//CRCPPosition Cmin1, Cmin2;
	//unsigned int dlat = deltaConflict;
	//if (TheSegs.RCP2D(Segs, Cin1, Cin2, Cout1, Cout2, Cmin1, Cmin2, dlat))
	//{
	//double dmin = Cmin1.LengthTo(Cmin2) / 32;
	//CTime CurTime = CVisuApp::GetCurrentTime();
	//CTime SystemTimeMin = Cmin1.T();
	//CTime SystemTimeIn = Cin1.T();
	//double tbmin = (SystemTimeMin - CurTime).GetTotalSeconds();
	//double tbIn = (SystemTimeIn - CurTime).GetTotalSeconds();
	// fly - modif ops 2006.09 - point 18
	//double HystMax = TMax;
	//if (tbIn <= HystMax)
	// fly - modif ops 2006.09 - point 18
	//{
	// fly - modif ops 2007.05 - CDTTS1762
	/*	bool tocalcul = false;
	if (tbmin > 0)
	tocalcul = true;
	else
	{ // calcul d'un conflit en separation
	CTime timeact = CurTime;
	CRCPPosition posact, theposact;
	bool bposact = Segs.Position(CurTime.GetTime(), posact);
	bool btheposact = TheSegs.Position(CurTime.GetTime(), theposact);
	if (bposact && btheposact)
	{
	double dact = posact.LengthTo(theposact);
	if (dact / 32. < 5)
	tocalcul = true;
	else
	{
	CTimeSpan more2min(0, 0, 2, 0);
	CTime timemore2min = timeact + more2min;
	CRCPPosition posact2m, theposact2m;
	bool bposact2m = Segs.Position(timemore2min.GetTime(), posact2m);
	bool btheposact2m = TheSegs.Position(timemore2min.GetTime(), theposact2m);
	if (bposact2m && btheposact2m)
	{
	double d2m = posact2m.LengthTo(theposact2m);
	double ratio2m = d2m / dact;
	if (ratio2m <= 1.20)
	tocalcul = true;
	}
	}
	}
	}*/
	//if (tocalcul)
	//	int merd = 7;
	/*				if (tocalcul && !CArea::IsInXptConflictArea(CPoint(Cin1.m_x,Cin1.m_y),m_pDoc->GetWindowNum(XptA)) && !CArea::IsInXptConflictArea(CPoint(Cin2.m_x,Cin2.m_y),m_pDoc->GetWindowNum(XptA)))
	{
	tocalcul=false;
	}*/

	//if (tocalcul)
	//{
	// fly - modif ops 2007.05 - CDTTS1762
	//CRCPPosition Cross1, Cross2;
	//ORDERRCP order = NONE;
	int SectorNbA = 0, SectorNbB = 0;
	CString SiteA = "", SiteB = "", msglog = "", XptCrossGVA = "", XptCrossZRH = "";
	XptCrossGVA = "EN4G/EN5G/EN6G/LIMMG";
	XptCrossZRH = "EN4Z/EN5Z/EN6Z/LIMMZ/PAD";
	//if (TheSegs.Cross(Segs, Cross1, Cross2))
	//	if (fabs(Cross1.Timesec() - Cross2.Timesec()) > m_pDoc->MTCA_CROSS_OVER_TIME)
	//		if (Cross1.Timesec() <= Cross2.Timesec())
	//			order = FIRSTCS;
	//		else
	//			order = SECONDCS;
	//int rulenmbr;
	//int delaidisp = 0;//RCP2DDisplayFilter(pFpl->m_Ept, pFpl->m_Xpt, pFpl->m_Efl, pFpl->m_Xfl, pFpl2->m_Ept, pFpl2->m_Xpt, pFpl2->m_Efl, pFpl2->m_Xfl, rulenmbr);
	//BOOL localhst = TRUE;
	//if (delaidisp == 0 || tbIn < delaidisp * 60)
	//	localhst = FALSE;


	//CRCPPosition Cvect1, Cvect2;
	//TheSegs.CalcVect2mn(Cmin1, 120, Cvect1);
	//Segs.CalcVect2mn(Cmin2, 120, Cvect2);
	int ExitSectA, ExitSectB;
	if (OnXfl)
	{
		/*if (FdpXptFixA != -1)
		SectorNbA = CArea::GetSectorNb(pFplA->m_FdpRoute[FdpXptFixA].GeoPos, XflA);*/
		//if (SectorNbA == 0)
		SectorNbA = pApp->m_pDoc->ComputeSector(XflA, XptA);
		/*if (FdpXptFixB != -1)
		SectorNbB = CArea::GetSectorNb(pFplB->m_FdpRoute[FdpXptFixB].GeoPos, XflB);*/
		//if (SectorNbB == 0)
		SectorNbB = pApp->m_pDoc->ComputeSector(XflB, XptB);
		SiteA = pApp->m_pDoc->GetSectorSite(SectorNbA);
		SiteB = pApp->m_pDoc->GetSectorSite(SectorNbB);
		if (SiteA.IsEmpty())
		{
			ExitSectA = pApp->m_pDoc->ComputeSector(XflA, XptA);
			SiteA = pApp->m_pDoc->GetSectorSite(ExitSectA);
		}
		if (SiteB.IsEmpty())
		{
			ExitSectB = pApp->m_pDoc->ComputeSector(XflB, XptB);
			SiteB = pApp->m_pDoc->GetSectorSite(ExitSectB);
		}
		if (SiteA.IsEmpty())
		{
			if (XptCrossGVA.Find(XptA) != -1)
				SiteA = "GVA";
			else if (XptCrossZRH.Find(XptA) != -1)
				SiteA = "ZRH";
		}
		if (SiteB.IsEmpty())
		{
			if (XptCrossGVA.Find(XptB) != -1)
				SiteB = "GVA";
			else if (XptCrossZRH.Find(XptB) != -1)
				SiteB = "ZRH";
		}
		if (SiteA.IsEmpty() || SiteB.IsEmpty())
		{
			msglog.Format("ERROR: CallsignA=%s XptA=%s XflA=%d SiteA=%s CallsignB=%s XptB=%s XflB=%d SiteB=%s\r\n", pFplA->m_Arcid, XptA, XflA, SiteA, pFplB->m_Arcid, XptB, XflB, SiteB);
			pApp->m_pOffline->m_pExCdt->m_ExCDTLog += msglog;
			//pApp->LogMsg(msglog);
		}
		if (SiteA.CompareNoCase(SiteB) != 0)
		{
			msglog.Format("CallsignA=%s XptA=%s XflA=%d SiteA=%s CallsignB=%s XptB=%s XflB=%d SiteB=%s\r\n", pFplA->m_Arcid, XptA, XflA, SiteA, pFplB->m_Arcid, XptB, XflB, SiteB);
			//pApp->LogMsg(msglog);
			pApp->m_pOffline->m_pExCdt->m_ExCDTLog += msglog;
		}

		/*if (IflMode)
		{
		if (SiteA.CompareNoCase(SiteB) == 0)
		{
		pFplA->m_IflConfNumSite = SiteA;
		pFplB->m_IflConfNumSite = SiteB;
		}
		// pFplA->m_ExitIflConflictVectors.Add(CPoint(Cin1.m_x,Cin1.m_y));
		pFplA->m_ExitIflConflictVectors.Add(CPoint(Cmin1.m_x, Cmin1.m_y));
		pFplA->m_ExitIflConflictVectors.Add(CPoint(Cvect1.m_x, Cvect1.m_y));
		// pFplB->m_ExitIflConflictVectors.Add(CPoint(Cin2.m_x,Cin2.m_y));
		pFplB->m_ExitIflConflictVectors.Add(CPoint(Cmin2.m_x, Cmin2.m_y));
		pFplB->m_ExitIflConflictVectors.Add(CPoint(Cvect2.m_x, Cvect2.m_y));
		pFplA->m_ExitIflConflictDistances.Add(CPoint(Cmin1.m_x, Cmin1.m_y));
		pFplA->m_ExitIflConflictDistances.Add(CPoint(Cmin2.m_x, Cmin2.m_y));
		pFplB->m_ExitIflConflictDistances.Add(CPoint(Cmin2.m_x, Cmin2.m_y));
		pFplB->m_ExitIflConflictDistances.Add(CPoint(Cmin1.m_x, Cmin1.m_y));

		//Array to keep 2nd arcid with conflict to give a kpi
		pFplA->m_ExitIflConflict2ndArcid.Add(pFplB->m_Arcid);
		pFplB->m_ExitIflConflict2ndArcid.Add(pFplA->m_Arcid);
		}
		else
		{

		if (SiteA.CompareNoCase(SiteB) == 0)
		{
		pFplA->m_ConfNumSite = SiteA;
		pFplB->m_ConfNumSite = SiteB;
		}

		pFplA->m_ExitConflictVectors.Add(CPoint(Cmin1.m_x, Cmin1.m_y));
		pFplA->m_ExitConflictVectors.Add(CPoint(Cvect1.m_x, Cvect1.m_y));
		pFplB->m_ExitConflictVectors.Add(CPoint(Cmin2.m_x, Cmin2.m_y));
		pFplB->m_ExitConflictVectors.Add(CPoint(Cvect2.m_x, Cvect2.m_y));
		pFplA->m_ExitConflictDistances.Add(CPoint(Cmin1.m_x, Cmin1.m_y));
		pFplA->m_ExitConflictDistances.Add(CPoint(Cmin2.m_x, Cmin2.m_y));
		pFplB->m_ExitConflictDistances.Add(CPoint(Cmin2.m_x, Cmin2.m_y));
		pFplB->m_ExitConflictDistances.Add(CPoint(Cmin1.m_x, Cmin1.m_y));

		//Array to keep 2nd arcid with conflict to give a kpi
		pFplA->m_ExitConflict2ndArcid.Add(pFplB->m_Arcid);
		pFplB->m_ExitConflict2ndArcid.Add(pFplA->m_Arcid);
		}*/
	}
	return 1;
	//}
	//}
	//}
	//	}
	return 0;
}

CFlightPlan* CFplDoc::GetFplByNbr(int FplNb)
{
	if (FplNb<m_FplTable.GetSize() && (m_FplTable[FplNb]))
	{
		return m_FplTable[FplNb];
	}
	else if (FplNb<m_FplInactiveTable.GetSize() && (m_FplInactiveTable[FplNb]))
	{
		return m_FplInactiveTable[FplNb];
	}
	return NULL;
}