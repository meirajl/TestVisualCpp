
// Technical ValidationDlg.h : header file
//

#pragma once


// CTechnicalValidationDlg dialog
class CTechnicalValidationDlg : public CDialogEx
{
// Construction
public:
	CTechnicalValidationDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TECHNICALVALIDATION_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnChooseAFTNFile();
	afx_msg void OnSequence();
	afx_msg void OnChooseLatLongFile();
	afx_msg void OnConvertLatLongIntoXY();
	afx_msg void OnOfflineCheck();
	afx_msg void OnExCdtCheck();
	afx_msg void OnXflSet();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	DECLARE_MESSAGE_MAP()
public:
	CString m_AftnFileName;
	CString m_NavPointsFileName;
};
