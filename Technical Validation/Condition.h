#if !defined(Condition_h)
#define Condition_h
#include "StdAfx.h"

#include "CommonObject.h"

/*class CCondition: public CObject
{
public:
typedef enum {none,not,and,or,equal,diff,plus,minus,multiply,divide,sup,inf} operations;

typedef struct {
	operations ope;
	int value;
	CString var;
} argument;

CArray<argument,argument> m_arglist;

void Decode_Expression(CString calcul);
bool Compute_Expression();

};*/

class CCondition : public CObject
{
public:
	CCondition();
	~CCondition();

	typedef enum {none,not,and,or,equal,diff,plus,minus,multiply,divide,sup,inf,supeq,infeq,right,left,mid,Abs,Min,Max,getat,find,remove,insert,set} operations;
	bool DecodeCondition(CCommonObject* pObject,CString Name,int Prio,CString Rule,CString Set);
	static int FindRuleAndSet(CArray<CCondition*,CCondition*>& CondTable,CCommonObject* pObject1,CCommonObject* pObject2=NULL,CString* pName=NULL,int FirstCond=0);
	static int LoadConditionTable(CCommonObject* pObject,CString FileName,CString Section,CArray<CCondition*,CCondition*>& CondTable);
typedef struct {
	operations ope;
	Var value;
	WORD ObjectNb;
	short VarNb;
	WORD SubVar1;
	WORD SubVar2;
} argument;
CString m_Name;
int m_Prio;
CArray<argument,argument> m_ArgTable;
CArray<argument,argument> m_SetTable;

bool   DecodeExpression(CString calcul,CCommonObject* pObject,CArray<argument,argument>& ArgTable);
int    ComputeCondition(CCommonObject* pObject,CCommonObject* pObject2=NULL);
int    SetData(CCommonObject* pObject,CCommonObject* pObject2=NULL);
static bool LoadConstants(CString Filename);
static bool RemoveConstants();
private:
int    ComputeExpression(CArray<argument,argument>& ArgTable,CCommonObject* pObject,CCommonObject* pObject2=NULL);
bool CheckExpression(CArray<argument,argument>& ArgTable,CCommonObject* pObject);
int    SetExpression(CCommonObject* pObject,CCommonObject* pObject2=NULL);

typedef struct {
	CString Name;
	Var Value;
} ConstantDef;

static CArray<ConstantDef,ConstantDef> m_ConstantTable;
};
#endif