#include "stdafx.h"
#include "ExCDT.h"
#include "Technical Validation.h"


CExCDT::CExCDT(CWnd* pParent /*=NULL*/)
	: CDialog(CExCDT::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExCDT)
	m_ExCDTLog = _T("");
	//}}AFX_DATA_INIT
}


CExCDT::~CExCDT()
{
}

void CExCDT::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExCDT)
	DDX_Text(pDX, IDC_EDIT2, m_ExCDTLog);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExCDT, CDialog)
	//{{AFX_MSG_MAP(CExCDT)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CExCDT::OnDestroy()
{
	if (this)
		delete this;
}

void CExCDT::OnClose()
{
	if (this)
		delete this;
}