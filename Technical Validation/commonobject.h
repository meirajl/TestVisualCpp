#if !defined(COMMONOBJECT_H)
#define COMMONOBJECT_H

class CCommonObject;

typedef enum {Notyp,Float,Int,String,Object,Array} vartyp;

typedef enum {SetAt,Insert,Remove} actions;

	typedef struct {
		vartyp type;
		int Int;
		double Float;
		CString String;
		CCommonObject* pObject;
	} Var;

class CCommonObject : public CObject
{
public:
	CCommonObject();
	virtual ~CCommonObject();
	virtual void OnCommand(UINT ){}
	virtual void OnUpdateCmdUI(CCmdUI* ){}
	virtual void OnCloseMenu(){}
	virtual void OnReceiveMsg(BYTE* ,int,sockaddr_in* =NULL,int =0,int = 0){}
	virtual void TimerEvent(UINT id){}




	virtual int GetVarNb(CString VarName){return 0;}
	virtual int GetSubVarNb(int Varnb,CString SubVarName){return 0;}
	virtual vartyp GetVarTyp(int varnb,int subvarnb=0){return Notyp;}
	virtual Var GetVarValue(int varnb,int indexnb=0,int subvarnb=0){ Var Def;Def.type=Notyp; return Def;}
	virtual void SetVarValue(int varnb,Var value,int indexnb=0,int indexnb2=0,actions action=SetAt){}
	virtual int Find(int varnb,CString name){return -1;}

};



#endif
