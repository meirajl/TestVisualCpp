#pragma once
#include "afxwin.h"
#include "resource.h"

class CSequenceError :
	public CDialog
{
public:
	CSequenceError(CWnd* pParent = NULL);   // standard constructor
	~CSequenceError();

	// Dialog Data
	//{{AFX_DATA(CSequenceError)
	enum { IDD = IDD_SEQUENCE_ERROR };
	CString	m_SequenceError;
	CString m_Arcid;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSequenceError)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSequenceError)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:

};

