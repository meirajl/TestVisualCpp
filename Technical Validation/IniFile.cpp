#include "StdAfx.h"
#include "IniFile.h"

CIniFile::CIniFile()
{
	m_Modified=false;
	m_Open=false;
}

CIniFile::CIniFile(CString FileName)
{
	m_Modified=false;
	m_Open=false;
	Open(FileName);
}

bool  CIniFile::Open(CString FileName)
{
//	SetFilePath(FileName);
	if (CStdioFile::Open(FileName,CFile::modeRead))
	{
		CString tmp;
		while (1)
		{
			if (!ReadString(tmp))
				break;
			tmp=tmp.Left(tmp.GetLength());
			m_StringTable.Add(tmp);
		}
		m_Modified=false;
		m_Open=true;
		return true;
	}
	else
		return false;
}

void CIniFile::Close()
{
	if (m_Open)
	{
		CString FileName=GetFilePath();
		CFile::Close();
		if (m_Modified)
		{
			CFile::Open(FileName,CFile::modeWrite|CFile::modeCreate);
			for (int line=0;line<m_StringTable.GetSize();line++)
			{
				WriteString(m_StringTable[line]);
			}
			CFile::Close();
		}
		m_StringTable.RemoveAll();
		m_Open=false;
	}
}

bool CIniFile::IsOpen()
{
	return m_Open;
}

DWORD CIniFile::GetIniProfileString(CString SecName,CString KeyName,CString& String)
{
	CString current,key;
	String="";//.Empty();
	bool sectionfnd=false;
	int fnd;
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
		current.TrimLeft();
		current.TrimRight();
//		current=current.stripWhiteSpace();
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']'))//section found
			{
				if (sectionfnd)
					return 0;
				if (current.Compare("["+SecName+"]")==0)
				{
					sectionfnd=true;
				}
			}
			else
			{
				if (sectionfnd)
				{
					fnd=current.Find("=");
					if (fnd!=-1)
					{
						key=current.Left(fnd);
						key.TrimRight();
						key.TrimLeft();
						if (key.Compare(KeyName)==0)
						{
							String=current.Right(current.GetLength()-fnd-1);
//							String.TrimLeft();
//							String.TrimRight();
							return String.GetLength();
						}
					}
				}
			}
		}
	}
	return 0;
}

BOOL CIniFile::WriteIniProfileString(CString SecName,CString KeyName,CString String)
{
	CString current,key;
	bool sectionfnd=false;
	int fnd;
	m_Modified=true;
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
//		current.TrimLeft();
//		current.TrimRight();
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']'))//section found
			{
				if (sectionfnd)
				{
					m_StringTable.InsertAt(line-1,KeyName+"="+String);
					return TRUE;
				}
				if (current.Compare("["+SecName+"]")==0)
				{
					sectionfnd=true;
				}
			}
			else
			{
				if (sectionfnd)
				{
					fnd=current.Find("=");
					if (fnd!=-1)
					{
						key=current.Left(fnd);
//						key.TrimRight();
//						key.TrimLeft();
						if (key.Compare(KeyName)==0)
						{
							m_StringTable[line]=KeyName+"="+String;
							return TRUE;
						}
					}
				}
			}
		}
	}
	if (!sectionfnd)
		m_StringTable.Add("["+SecName+"]");
	m_StringTable.Add(KeyName+"="+String);
	return TRUE;
}

DWORD CIniFile::GetIniProfileSection(CString SecName,CString& String)
{
	CString current,key;
	String="";//.Empty();
	bool sectionfnd=false;
	int fnd;
	int i;
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
		current.TrimLeft();
		current.TrimRight();
//		current=current.stripWhiteSpace();
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']'))//section found
			{
				if (sectionfnd)
				{
					break;
				}
				if (current.Compare("["+SecName+"]")==0)
				{
					sectionfnd=true;
				}
			}
			else
			{
				if (sectionfnd)
				{
					fnd=current.Find("=");
					if (fnd!=-1)
					{
						key=current.Left(fnd);
//						key.TrimRight();
//						key.TrimLeft();
						String+=key+"=";
					}
				}
			}
		}
	}
	for (i=0;i<(int)String.GetLength();i++)
	{
		if (String[i]=='=')
		{
			String.SetAt(i,'\0');//.setAt(i,'\0');
		}
	}
	return i;
}

DWORD CIniFile::GetIniProfileSection(CString SecName,CArray<CString,CString>& ReturnedStringArray)
{
	CString current,key;
	bool sectionfnd=false;
	int fnd,i=0;
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
		current.TrimLeft();
		current.TrimRight();
//		current=current.stripWhiteSpace();
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']'))//section found
			{
				if (sectionfnd)
				{
					break;
				}
				if (current.Compare("["+SecName+"]")==0)
				{
					sectionfnd=true;
				}
			}
			else
			{
				if (sectionfnd)
				{
					fnd=current.Find("=");
					if (fnd!=-1)
					{
						key=current.Left(fnd);
						ReturnedStringArray.Add(key);
					}
				}
			}
		}
	}
	return i;
}

DWORD CIniFile::GetIniProfileSectionNames(CString& String)
{
	CString current;
	String="";//.Empty();
	int i;
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
//		current.TrimLeft();
//		current.TrimRight();
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']'))//section found
			{
				String+=current.Right(current.GetLength()-1);
			}
		}
	}
	for (i=0;i<(int)String.GetLength();i++)
	{
		if (String[i]==']')
		{
			String.SetAt(i,'\0');
		}
	}
	return i;
}

CIniFile::~CIniFile()
{
	Close();
}



DWORD CIniFile::GetIniProfileSectionData(CString SecName,CArray<CString,CString>& ReturnedStringArray,bool WithKey)
{
	CString current,key;
	bool sectionfnd=false;
	int fnd,i=0;
	ReturnedStringArray.RemoveAll();
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
		current.TrimLeft();
		current.TrimRight();
//		current=current.stripWhiteSpace();
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']'))//section found
			{
				if (sectionfnd)
				{
					break;
				}
				if (current.Compare("["+SecName+"]")==0)
				{
					sectionfnd=true;
				}
			}
			else
			{
				if (sectionfnd)
				{
					if ((current.GetLength()!=0) && (current.Find("--") != 0) && (current.Find(";") != 0) && (current.Find("//") != 0))
					{
						fnd=current.Find("=");
						if ((fnd!=-1) || (!WithKey))
							ReturnedStringArray.Add(current);
					}
					
				}
			}
		}
	}
	return i;
}

DWORD CIniFile::GetIniProfileSectionNames(CArray<CString,CString>& ReturnedStringArray)
{
	CString current;
	int i=0;
	for (int line=0;line<m_StringTable.GetSize();line++)
	{
		current=m_StringTable[line];
		if (current.GetLength())
		{
			if ((current[0]=='[') && (current[current.GetLength()-1]==']')) //section found
			{
				ReturnedStringArray.Add(current.Mid(1,current.GetLength()-2));
			}
		}
	}
	return i;
}
