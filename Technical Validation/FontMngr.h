#if !defined(FontMngr_H)
#define FontMngr_H

#define DefWindowFont 0
#define DefMenuFont 1

namespace CFontMngr
{
	void InitTables();
	void RemoveTables();
	int GetFontNb(CString Name);
	LOGFONT GetLogFont(int fontnb);
	CFont* GetFont(int fontnb);
};
#endif