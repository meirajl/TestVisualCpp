#include "Stdafx.h"
#include "Condition.h"
#include "StringUtil.h"
#include "IniFile.h"

CArray<CCondition::ConstantDef,CCondition::ConstantDef> CCondition::m_ConstantTable;



CCondition::CCondition()
{
}

CCondition::~CCondition()
{
}



bool CCondition::DecodeExpression(CString calcul,CCommonObject* pObject,CArray<argument,argument>& ArgTable)
{
	LPCSTR OpeStr[]={"","not","and","or","==","!=","+","-","*","/",">","<",">=","<=","right","left","mid","abs","min","max","getat","find","remove","insert","="};
	CArray<operations,operations> partable;
	int parlevel = 0;
	int curind =0;
	bool lastope = false;
	argument arg;
	CString curarg,cursubarg,tmp;
	CString exp(calcul);
	exp.TrimLeft();
	exp.TrimRight();
	exp+='\0';
	partable.Add(none);
	Var DefValue;
	DefValue.type =Notyp;
	DefValue.Int=0;
	DefValue.Float=0.0;
	DefValue.String="";
	DefValue.pObject=NULL;
	Var CurVar,CurVar2;
	bool IsAlpha = false;
	ArgTable.RemoveAll();
	for (curind=0;curind<exp.GetLength();curind++)
	{
		bool DelFound = false;
		switch (exp[curind])
		{
		case '"':
			if (curarg.GetLength())
				DelFound=true;
			break;
		case '(':
		case ')':
		case ' ':
		case ',':
		case '\0':
		case '+':
		case '-':
		case '*':
		case '/':
			DelFound=true;
			break;
		case '!':
		case'<':
		case'>':
		case '=':
			if (curarg.GetLength())
			{
				if  (IsAlpha)
					DelFound=true;
			}
			else
			{
				IsAlpha=false;
			}
			break;
		default:
			if (curarg.GetLength())
			{
				if  (!IsAlpha)
					DelFound=true;
			}
			else
			{
				IsAlpha=true;
			}
			break;
		}
		if ((DelFound) && (curarg.GetLength()!=0))
		{
			lastope = false;
			//decode arg string
			if (curarg[0]=='$')//is it a var
			{
				arg.SubVar1=0;
				arg.SubVar2=0;
				int fnd1=curarg.Find(".");
				if (fnd1==-1)
				{
					//not possible anymore
					arg.VarNb = -1;//pObject->GetVarNb(curarg.Right(curarg.GetLength()-1));
				}
				else
				{
					tmp = curarg.Mid(1,fnd1-1);
					if (isdigit(tmp[tmp.GetLength()-1]))
						arg.ObjectNb=atoi(tmp.Right(1));
					else
						arg.ObjectNb=1;
					int fnd2 = curarg.Find(".",fnd1+1);
					int fnd3 = -1;
					if (fnd2!=-1)
						arg.VarNb = pObject->GetVarNb(curarg.Mid(fnd1+1,fnd2-fnd1-1));
					else
					{
						arg.VarNb = pObject->GetVarNb(curarg.Right(curarg.GetLength()-fnd1-1));
						fnd3 = curarg.Find(".",fnd2+1);
					}
					if (fnd3==-1)
					{
						CurVar = pObject->GetVarValue(arg.VarNb);
						cursubarg=curarg.Mid(fnd2+1,curarg.GetLength()-fnd2+1);
						if ((CurVar.type==Object) && (CurVar.pObject))
							arg.SubVar1=CurVar.pObject->GetVarNb(curarg.Mid(fnd2+1,curarg.GetLength()-fnd2+1));
						if (CurVar.type==Array)
							arg.SubVar1=pObject->GetSubVarNb(arg.VarNb,curarg.Mid(fnd2+1,curarg.GetLength()-fnd2+1));
					}
					else
					{
						CurVar = pObject->GetVarValue(arg.VarNb);
						cursubarg=curarg.Mid(fnd2+1,fnd3-fnd2-1);
						if ((CurVar.type==Object) && (CurVar.pObject))
						{
							arg.SubVar1=CurVar.pObject->GetVarNb(cursubarg);
							cursubarg=curarg.Mid(fnd3+1,curarg.GetLength()-fnd3);
							CurVar2 = CurVar.pObject->GetVarValue(arg.SubVar1);
							if ((CurVar2.type==Object) && (CurVar2.pObject))
								arg.SubVar2=CurVar2.pObject->GetVarNb(cursubarg);
							if (CurVar2.type==Array)
								arg.SubVar2=CurVar.pObject->GetSubVarNb(arg.SubVar1,cursubarg);
						}
						
					}
					
				}
				arg.value = DefValue;
				arg.ope = none;
				ArgTable.Add(arg);
			}
			else
			{
				int i;
				for (i=1;i<set+1;i++)
				{
					if (curarg.CompareNoCase(OpeStr[i])==0) // is an operation
					{
						arg.VarNb=0;
						arg.SubVar1=0;
						arg.SubVar2=0;
						arg.value = DefValue;
						arg.ope = partable[parlevel];
						if (arg.ope!=none)
							ArgTable.Add(arg);
						partable[parlevel] = (operations)i;
						if (partable[parlevel]==set)
						{
							if (&ArgTable==&m_ArgTable)
							{
/*								CFdpApp* pApp = (CFdpApp*)AfxGetApp();
								CString err;
								err.Format("Warning:Set At on condition %s",m_Name);
								pApp->LogMsg(err);*/
							}
							if (ArgTable[ArgTable.GetUpperBound()].VarNb!=0) 
								ArgTable[ArgTable.GetUpperBound()].VarNb=-ArgTable[ArgTable.GetUpperBound()].VarNb;
						}
						lastope = true;
						break;
					}
				}
				if (i==set+1) // so it is a value
				{
					arg.value = DefValue;
					if (curarg[0]=='"')
					{
						arg.value.String=curarg.Mid(1,curarg.GetLength()-2);
						arg.value.type= String;
					}
					else	
					{
						if (NStringUtil::IsNumericOrFloat(curarg))
						{
							int fnd=curarg.Find(".");
							if (fnd==-1)
							{
								arg.value.Int=atoi(curarg);
								arg.value.type = Int;
							}
							else
							{
								arg.value.Float=atof(curarg);
								arg.value.type = Float;
							}
						}
						else
						{
							//is it a constant 
							int c;
							for (c=0;c<m_ConstantTable.GetSize();c++)
							{
								if (curarg.Compare(m_ConstantTable[c].Name)==0)
								{
									arg.value=m_ConstantTable[c].Value;
									break;
								}
							}
							if (c==m_ConstantTable.GetSize())
								int merd=7;
						}
					}
					arg.ope = none;
					arg.VarNb=0;
					ArgTable.Add(arg);
				}
				
			}
			
			curarg.Empty();
		}
		arg.VarNb=0;
		arg.SubVar1=0;
		arg.SubVar2=0;
		arg.value = DefValue;
		arg.ope = partable[parlevel];
		switch (exp[curind])
		{
		case '(':
			parlevel++;
			partable.Add(none);
			break;
		case ')':
			if (arg.ope!=none)
				ArgTable.Add(arg);
			partable.RemoveAt(parlevel);
			parlevel--;
			break;
		case ',':
			if (((arg.ope==set) || (arg.ope==insert) || (arg.ope==remove)) && (parlevel==0))
			{
				ArgTable.Add(arg);
				partable[parlevel]=none;
			}
		case ' ':
			break;
		case '\0':
			if (arg.ope!=none)
				ArgTable.Add(arg);
			break;		
		case '+':
		case '-':
			if (curarg.GetLength()==0)
				IsAlpha=lastope || (ArgTable.GetSize()==0);
			curarg += exp[curind];
			break;
		case '=':
		case '*':
		case '/':
		case '!':
		case'<':
		case'>':
			if (curarg.GetLength()==0)
				IsAlpha=false;
			curarg += exp[curind];
			break;
		case '"':
			if (curarg.GetLength())
				int merd=7;
			curarg += exp[curind++];
			while (curind<exp.GetLength())
			{
				curarg += exp[curind];
				 if (exp[curind]=='"')
					 break;
				 curind++;
			}
			break;
		default:
			if (curarg.GetLength()==0)
				IsAlpha=true;
			curarg += exp[curind];
			break;
		}
	}
	CheckExpression(ArgTable,pObject);
	return true;
}



int CCondition::ComputeExpression(CArray<argument,argument>& ArgTable,CCommonObject* pObject1,CCommonObject* pObject2)
{
	CArray<Var,Var> m_Stack;
	m_Stack.SetSize(__min(10,ArgTable.GetSize()));
	Var val1,val2,val3;
	CCommonObject* pObject;
	for (int arg=0;arg<ArgTable.GetSize();arg++)
	{
		if ((ArgTable[arg].VarNb) && (ArgTable[arg].ope==none))//it is a var, find it and push it
		{
			if (ArgTable[arg].ObjectNb==1)
				pObject = pObject1;
			else
				pObject = pObject2;
			if (ArgTable[arg].VarNb>0)
			{
				val1 = pObject->GetVarValue(ArgTable[arg].VarNb,-1);
				if ((ArgTable[arg].SubVar1) && (val1.type==Object))
				{
					val1=val1.pObject->GetVarValue(ArgTable[arg].SubVar1);
					if ((ArgTable[arg].SubVar2) && (val1.type==Object))
					{
						val1=val1.pObject->GetVarValue(ArgTable[arg].SubVar2);
					}
					if ((ArgTable[arg].SubVar2) && (val1.type==Array))
					{
						//					val1.type=Array;
						//					val1.pObject=val1.pObject;
						val1.Int=ArgTable[arg].SubVar1;
						val1.Float=ArgTable[arg].SubVar2;
					}
				}
				if (/*(ArgTable[arg].SubVar1) &&*/ (val1.type==Array))
				{
					//				val1.type=Array;
					val1.pObject=pObject;
					val1.Int=ArgTable[arg].VarNb;
					val1.Float=ArgTable[arg].SubVar1;
				}
			}
			else
			{
				val1.type=Object;
				val1.pObject=pObject;
				val1.Int=-ArgTable[arg].VarNb;
				val1.Float=ArgTable[arg].SubVar1;
			}
			m_Stack.Add(val1);
		}
		if ((ArgTable[arg].VarNb==0) && (ArgTable[arg].ope==none))//it is a value, push it
			m_Stack.Add(ArgTable[arg].value);
		if (ArgTable[arg].ope!=none)
		{
			if ((ArgTable[arg].ope==mid) || (ArgTable[arg].ope==remove) || (ArgTable[arg].ope==insert))
			{
				val3 = m_Stack[m_Stack.GetUpperBound()];
				m_Stack.RemoveAt(m_Stack.GetUpperBound());
			}
			if ((ArgTable[arg].ope!=not) && (ArgTable[arg].ope!=Abs))
			{
				val2 = m_Stack[m_Stack.GetUpperBound()];
				m_Stack.RemoveAt(m_Stack.GetUpperBound());
			}
			val1 = m_Stack[m_Stack.GetUpperBound()];
			m_Stack.RemoveAt(m_Stack.GetUpperBound());
			switch (ArgTable[arg].ope)
			{
			case and:
				val1.Int = val1.Int & val2.Int;
				break;
			case or:
				val1.Int = val1.Int | val2.Int;
				break;
			case not:
				val1.Int = !val1.Int;
				break;
			case plus:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int+val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Float = val1.Float+val2.Float;
				if ((val1.type==Int) && (val2.type==Float))
				{
					val1.Float = val1.Int+val2.Float;
					val1.type=Float;
				}
				if ((val1.type==Float) && (val2.type==Int))
				{
					val1.Float = val1.Float+val2.Int;
					val1.type=Float;
				}
				break;
			case minus:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int-val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Float = val1.Float-val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Float = val1.Float-val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
				{
					val1.Float = val1.Int-val2.Float;
					val1.type=Float;
				}
				break;
			case multiply:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int*val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Float = val1.Float*val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Float = val1.Float*val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
				{
					val1.Float = val1.Int*val2.Float;
					val1.type=Float;
				}
				break;
			case divide:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int/val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Float = val1.Float/val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Float = val1.Float/val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
				{
					val1.Float = val1.Int/val2.Float;
					val1.type=Float;
				}
				break;
			case equal:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int==val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
					val1.Int = val1.Int==val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Int = val1.Float==val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Int = val1.Float==val2.Float;
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String==val2.String;
				val1.type=Int;
				break;
			case diff:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int!=val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
					val1.Int = val1.Int!=val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Int = val1.Float!=val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Int = val1.Float!=val2.Float;
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String!=val2.String;
				val1.type=Int;
				break;
			case sup:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int>val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
					val1.Int = val1.Int>val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Int = val1.Float>val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Int = val1.Float>val2.Float;
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String>val2.String;
				val1.type=Int;
				break;
			case supeq:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int>=val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
					val1.Int = val1.Int>=val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Int = val1.Float>=val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Int = val1.Float>=val2.Float;
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String>=val2.String;
				val1.type=Int;
				break;
			case inf:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int<val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
					val1.Int = val1.Int<val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Int = val1.Float<val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Int = val1.Float<val2.Float;
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String<val2.String;
				val1.type=Int;
				break;
			case infeq:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int = val1.Int<=val2.Int;
				if ((val1.type==Int) && (val2.type==Float))
					val1.Int = val1.Int<=val2.Float;
				if ((val1.type==Float) && (val2.type==Int))
					val1.Int = val1.Float<=val2.Int;
				if ((val1.type==Float) && (val2.type==Float))
					val1.Int = val1.Float<=val2.Float;
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String<=val2.String;
				val1.type=Int;
				break;
			case right:
				if ((val1.type==String) && (val2.type==Int))
					val1.String = val1.String.Right(val2.Int);
				val1.type=String;
				break;
			case left:
				if ((val1.type==String) && (val2.type==Int))
					val1.String = val1.String.Left(val2.Int);
				val1.type=String;
				break;
			case mid:
				if ((val1.type==String) && (val2.type==Int) && (val3.type==Int))
					val1.String = val1.String.Mid(val2.Int,val3.Int);
				val1.type=String;
				break;
			case Abs:
				val1.Int = abs(val1.Int);
				break;
			case Max:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int=__max(val1.Int,val2.Int);
				if ((val1.type==Float) && (val2.type==Float))
					val1.Float=__max(val1.Float,val2.Float);
				if ((val1.type==Float) && (val2.type==Int))
					val1.Float=__max(val1.Float,val2.Int);
				if ((val1.type==Int) && (val2.type==Float))
				{
					val1.Float=__max(val1.Int,val2.Float);
					val1.type=Float;
				}
				break;
			case Min:
				if ((val1.type==Int) && (val2.type==Int))
					val1.Int=__min(val1.Int,val2.Int);
				if ((val1.type==Float) && (val2.type==Float))
					val1.Float=__min(val1.Float,val2.Float);
				if ((val1.type==Float) && (val2.type==Int))
					val1.Float=__min(val1.Float,val2.Int);
				if ((val1.type==Int) && (val2.type==Float))
				{
					val1.Float=__min(val1.Int,val2.Float);
					val1.type=Float;
				}
				break;
			case getat:
				if ((val1.type==Array) && (val2.type==Int))
					val1=val1.pObject->GetVarValue(val1.Int,val2.Int,(int)val2.Float);
				val1.type=Int;
				break;
			case find:
				if ((val1.type==String) && (val2.type==String))
					val1.Int = val1.String.Find(val2.String);
				if ((val1.type==Array) && (val2.type==String))
					val1.Int=val1.pObject->Find(val1.Int,val2.String);
				val1.type=Int;
				break;
			case insert:
				if ((val1.type==Array) && (val2.type==Int) && (val3.type==String))
					val1.pObject->SetVarValue(val1.Int,val3,val2.Int,0,Insert);
				break;
			case remove:
				if ((val1.type==Array) && (val2.type==Int) && (val3.type==Int))
					val1.pObject->SetVarValue(val1.Int,val3,val2.Int,val3.Int,Remove);
				break;
			case set:
				if ((val1.type==Object) && (val2.type!=Notyp))
					val1.pObject->SetVarValue(val1.Int,val2);
				break;
			}
			m_Stack.Add(val1);
		}
		
	}
	val1 = m_Stack[m_Stack.GetUpperBound()];
	m_Stack.RemoveAt(m_Stack.GetUpperBound());
	return val1.Int;
}

bool CCondition::CheckExpression(CArray<argument,argument>& ArgTable,CCommonObject* pObject)
{
	CList<Var,Var> m_Stack;
	Var val1,val2,val3;
	bool OK;
	for (int arg=0;arg<ArgTable.GetSize();arg++)
	{
		if ((ArgTable[arg].VarNb) && (ArgTable[arg].ope==none))//it is a var, find it and push it
		{
			if (ArgTable[arg].VarNb>0)
			{
				val1 = pObject->GetVarValue(ArgTable[arg].VarNb);
				if ((ArgTable[arg].SubVar1) && (val1.type==Object))
				{
					val1=val1.pObject->GetVarValue(ArgTable[arg].SubVar1);
				}
			}
			else
			{
				val1.type=Object;
				val1.pObject=pObject;
				val1.Int=-ArgTable[arg].VarNb;
				val1.Float=ArgTable[arg].SubVar1;
			}
			m_Stack.AddTail(val1);
		}
		if ((ArgTable[arg].VarNb==0) && (ArgTable[arg].ope==none))//it is a value, push it
			m_Stack.AddTail(ArgTable[arg].value);
		if (ArgTable[arg].ope!=none)
		{
			if (ArgTable[arg].ope==mid)
				val3 = m_Stack.RemoveTail();
			if ((ArgTable[arg].ope!=not) && (ArgTable[arg].ope!=Abs))
				val2 = m_Stack.RemoveTail();
			val1 = m_Stack.RemoveTail();
			OK=false;
			switch (ArgTable[arg].ope)
			{
			case and:
			case or:
				OK= ((val1.type==Int) && (val2.type==Int));
				break;
			case not:
			case Abs:
				OK= (val1.type==Int);
			case plus:
			case minus:
			case multiply:
			case divide:
			case Min:
			case Max:
				if ((val1.type==Int) && (val2.type==Int))
					OK=true;
				if ((val1.type==Int) && (val2.type==Float))
				{
					OK=true;
					val1.type=Float;
				}
				if ((val1.type==Float) && (val2.type==Int))
				{
					OK=true;
					val1.type=Float;
				}
				if ((val1.type==Float) && (val2.type==Float))
					OK=true;
				break;
			case equal:
			case diff:
			case sup:
			case supeq:
			case inf:
			case infeq:
				if ((val1.type==Int) && (val2.type==Int))
					OK=true;
				if ((val1.type==Int) && (val2.type==Float))
					OK=true;
				if ((val1.type==Float) && (val2.type==Int))
					OK=true;
				if ((val1.type==Float) && (val2.type==Float))
					OK=true;
				if ((val1.type==String) && (val2.type==String))
					OK=true;
				if (!OK)
					int merd=7;
				val1.type=Int;
				break;
			case right:
			case left:
				if ((val1.type==String) && (val2.type==Int))
					OK=true;
				val1.type=String;
				break;
			case mid:
				if ((val1.type==String) && (val2.type==Int) && (val3.type==Int))
					OK=true;
				val1.type=String;
				break;
			case find:
				if ((val1.type==String) && (val2.type==String))
					OK=true;
				if ((val1.type==Array) && (val2.type==String))
					OK=true;
				val1.type=Int;
				break;
			case getat:
				if ((val1.type==Array) && (val2.type==Int))
					OK=true;
				val1.type=Int;
				break;
			case set:
				OK= ((val1.type==Object) && (val2.type!=Notyp));
				val1.type=Int;
				break;
			default:
				int merd=7;
				break;
			}
			m_Stack.AddTail(val1);
			if (!OK)
				return false;
		}
		
	}
	val1 = m_Stack.RemoveTail();
	return val1.type==Int;
}


bool CCondition::LoadConstants(CString Filename)
{
	CIniFile file(Filename);
	CArray<CString,CString> ConstantList;
	CString ConstantName,ConstantVal;
	ConstantDef CurConst;
	file.GetIniProfileSectionData("CONSTANTS",ConstantList);
	for (int i=0;i<ConstantList.GetSize();i++)
	{
		int fnd=ConstantList[i].Find("=");
		if (fnd!=-1)
		{
			ConstantName=ConstantList[i].Left(fnd);
			ConstantVal=ConstantList[i].Right(ConstantList[i].GetLength()-fnd-1);
			CurConst.Name=ConstantName;
			CurConst.Value.type=Notyp;
			if (ConstantVal.GetLength())
			{
				if (ConstantVal[0]=='"')
				{
					CurConst.Value.String=ConstantVal;
					CurConst.Value.type= String;
				}
				else
				{
					if (NStringUtil::IsNumericOrFloat(ConstantVal))
					{
						int fnd=ConstantVal.Find(".");
						if (fnd==-1)
						{
							CurConst.Value.Int=atoi(ConstantVal);
							CurConst.Value.type = Int;
						}
						else
						{
							CurConst.Value.Float=atof(ConstantVal);
							CurConst.Value.type = Float;
						}
					}		
				}
				m_ConstantTable.Add(CurConst);
			}
		}
	}
	return true;
}
bool CCondition::RemoveConstants()
{
	m_ConstantTable.RemoveAll();
	return true;
}

bool CCondition::DecodeCondition(CCommonObject* pObject,CString Name,int Prio,CString Rule,CString Set)
{
	m_Name=Name;
	m_Prio=Prio;
	DecodeExpression(Rule,pObject,m_ArgTable);
	DecodeExpression(Set,pObject,m_SetTable);
	return true;
}

int CCondition::LoadConditionTable(CCommonObject* pObject,CString FileName,CString Section,CArray<CCondition*,CCondition*>& CondTable)
{
	CIniFile file(FileName);
	CString ConditionName,ConditionPrio,ConditionRule,ConditionSet,Tmp,Key;
	ConstantDef CurConst;
	int lasti=0,fnd;
	CArray<CString,CString> KeyTable;
	file.GetIniProfileSectionData(Section,KeyTable);
	for (int i=0;i<KeyTable.GetSize();i+=3)
	{
		fnd=KeyTable[i].Find("=");
		if (fnd!=-1)
		{
			Key=KeyTable[i].Left(fnd);
			if (Key.CompareNoCase("NAME_PRIO")!=0)
				int merd=7;
			Tmp=KeyTable[i].Right(KeyTable[i].GetLength()-fnd-1);
			fnd=Tmp.Find('|');
			if (fnd!=-1)
			{
				ConditionName=Tmp.Left(fnd);
				ConditionPrio=Tmp.Right(Tmp.GetLength()-fnd-1);
			}
		}
		fnd=KeyTable[i+1].Find("=");
		if (fnd!=-1)
		{
			Key=KeyTable[i+1].Left(fnd);
			if (Key.CompareNoCase("CONDITION")!=0)
				int merd=7;
			ConditionRule=KeyTable[i+1].Right(KeyTable[i+1].GetLength()-fnd-1);
		}
		fnd=KeyTable[i+2].Find("=");
		if (fnd!=-1)
		{
			Key=KeyTable[i+2].Left(fnd);
			if (Key.CompareNoCase("RESOLUTION")!=0)
				int merd=7;
			ConditionSet=KeyTable[i+2].Right(KeyTable[i+2].GetLength()-fnd-1);
		}
		CCondition* pCond = new CCondition();
		pCond->DecodeCondition(pObject,ConditionName,atoi(ConditionPrio),ConditionRule,ConditionSet);
		lasti=i;
		int p;
		for (p=0;p<CondTable.GetSize();p++)
		{
			if (pCond->m_Prio>CondTable[p]->m_Prio)
			{
				CondTable.InsertAt(p,pCond);
				break;
			}
		}
		if (p==CondTable.GetSize())
			CondTable.Add(pCond);

	}
	return CondTable.GetSize();
}


int CCondition::FindRuleAndSet(CArray<CCondition*,CCondition*>& CondTable,CCommonObject* pObject1,CCommonObject* pObject2,CString* pCondName,int FirstCond)
{
	int LastPrio=-32768;
	int LastCond=-1;
	bool Reverse=false;
	for (int i=FirstCond;i<CondTable.GetSize();i++)
	{
		if ((CondTable[i]->m_Prio>LastPrio) && (CondTable[i]->ComputeExpression(CondTable[i]->m_ArgTable,pObject1,pObject2)!=0))
		{
			LastPrio=CondTable[i]->m_Prio;
			LastCond=i;
			Reverse=false;
		}
		if ((pObject2!=NULL) && (CondTable[i]->m_Prio>LastPrio) && (CondTable[i]->ComputeExpression(CondTable[i]->m_ArgTable,pObject2,pObject1)!=0))
		{
			LastPrio=CondTable[i]->m_Prio;
			LastCond=i;
			Reverse=true;
		}
	}
	if (LastCond!=-1)
	{
		if (!Reverse)
			CondTable[LastCond]->ComputeExpression(CondTable[LastCond]->m_SetTable,pObject1,pObject2);
		else
			CondTable[LastCond]->ComputeExpression(CondTable[LastCond]->m_SetTable,pObject2,pObject1);
		if (pCondName)
			*pCondName=CondTable[LastCond]->m_Name;
	}
	return LastCond;
}