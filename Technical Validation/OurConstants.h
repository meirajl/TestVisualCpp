///Definitions of constants

#if !defined(OurConstants_h)
#define OurConstants_h

typedef enum {Even,Odd,Both} LevelParity;




///************* TRACKS CONSTANTS *****************
	const int TRKrefresh = 4;//radar scan refresh period

	const int no_alt = -32767;
	const int maxpastpos = 500;

	const int systsize = 24000;			// 48000 values for 1500NM => 1 quantum for 1/32NM
	const int systhalfsize = 12000;		// 48000 values for 1500NM => 1 quantum for 1/32NM

	// RVSM constants
	const int Alt_RVSM_min		= 290;
	const int Alt_RVSM_max		= 410;
	const int Alt_RVSM_delta	= 600;	// In feet (ft) ==> A/C should be within 600 ft boundary from RVSM level limits to change its color									
									// from STD color to RVSM color & vice versa.
// fly - modif ops 2007.05 - CDTTS1699
//	const Precoor_Level	= 70; // flights can not precorr under this level
// fly - modif ops 2007.05 - CDTTS1699
//*************** TIMER CONSTANTS *********
	const UINT AGINGTimerID = 10;
	const UINT AGINGTimerVAL = 20000U;
	const UINT AGINGFPLTimerID = 23;
	const UINT AGINGFPLTimerVAL = 60000U;//1 min
	const UINT INIT1TimerID = 11;
	const UINT INIT1TimerVAL = 100U;
	const UINT INIT2TimerID = 17;
	const UINT INIT2TimerVAL = 15000U;
	const UINT TIMETimerVAL = 1000U;
	const UINT TIMETimerID = 19;
	const UINT HeartBeatTimerID = 18;
	const UINT HeartBeatTimerVAL = 10000U;
	const UINT SYCOoutTimerID = 21;
	const UINT SYCOoutTimerVAL = 10000U;
	const UINT INFONETTimerID = 22;
	const UINT INFONETTimerVAL = 20000U;
	const UINT HeartBeatMVTimerID = 29;
	const UINT HeartBeatMVTimerVAL = 5000U;
	const UINT ReceiveMVTimerID = 24;
	const UINT ReceiveMVTimerVAL = 15000U;
	const UINT UpdateSYCOTimerID = 26;
	const UINT UpdateSYCOTimerVAL = 120000U;
	const UINT ConnectSYCOTimerID = 27;
	const UINT ConnectSYCOTimerVAL = 5000U;
	const UINT MVStatusTimerID = 28;
	const UINT MVStatusTimerVAL = 20000U;
	const UINT CorrelTimerID = 30;
	const UINT CorrelTimerVAL = 10000U;
	const UINT BlockingID = 31;
	const UINT BlockingVAL = 60000U;

//*************** STEREO => LAT/LONG CONVERSION CONSTANTS *********
	const double PI = 3.14159265359;


	const int syconbr = 3;

	const int FLevNb = 35;


//////////////////////////////////////////:
#define MaxSectorNb 160
#define MaxConsoleNb 320
#define MaxSeqNb 32

#endif //!defined(OurConstants.h)