// LimitPoints.h: interface for the CLimitPoints class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIMITPOINTS_H__BCC63A26_C016_41BE_BDF6_EEE44AD82549__INCLUDED_)
#define AFX_LIMITPOINTS_H__BCC63A26_C016_41BE_BDF6_EEE44AD82549__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Geo.h"

typedef struct 
{
	bool IsDefined;
	long x;
	long y;
}LPoint;


class CLimitPoints : public CObject  
{
public:
	CLimitPoints();
	virtual ~CLimitPoints();
	static bool  GetLimitPoint(int rank, LPoint& point);
	static bool  GetPoint(int rank, LPoint& point);
	static void LoadLimitPointsLatLong(CString IniName);
	static void LoadLimitPointsXY(CString IniName);
	static CArray<LPoint*,LPoint*> m_LimitPointsTable;
	static void GeographicToStereographic(Latitude lat, Longitude lng, double& Lat, double& Lng);

};

#endif // !defined(AFX_LIMITPOINTS_H__BCC63A26_C016_41BE_BDF6_EEE44AD82549__INCLUDED_)
