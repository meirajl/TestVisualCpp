#include "stdafx.h"
#include "FplDlg.h"
#include "Technical Validation.h"


CFplDlg::CFplDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFplDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFplDlg)
	m_FplXfl = _T("");
	//}}AFX_DATA_INIT
}


CFplDlg::~CFplDlg()
{
}

void CFplDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFplDlg)
	DDX_CBString(pDX, IDC_COMBO1, m_FplXfl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFplDlg, CDialog)
	//{{AFX_MSG_MAP(CFplDlg)
	ON_BN_CLICKED(IDOK, &CFplDlg::OnSetXfl)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CFplDlg::OnSetXfl()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	int XflNew = 350;//atoi(m_FplXfl);
	for (int i = 0; i < pApp->m_pDoc->m_FplTable.GetSize(); i++)
	{
		pApp->m_pDoc->m_FplTable[i]->m_Xfl = XflNew;
		pApp->m_pDoc->m_FplTable[i]->m_IflCentre = XflNew;
	}
}

void CFplDlg::OnDestroy()
{
	if (this)
		delete this;
}

void CFplDlg::OnClose()
{
	if (this)
		delete this;
}