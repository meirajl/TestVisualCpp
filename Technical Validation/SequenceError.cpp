#include "stdafx.h"
#include "SequenceError.h"


CSequenceError::CSequenceError(CWnd* pParent /*=NULL*/)
	: CDialog(CSequenceError::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSequenceError)
	m_SequenceError = _T("");
	m_Arcid = _T("");
	//}}AFX_DATA_INIT
}


CSequenceError::~CSequenceError()
{
}

void CSequenceError::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSequenceError)
	DDX_Text(pDX, IDC_EDIT2, m_SequenceError);
	DDX_Text(pDX, IDC_ARCIDEDIT, m_Arcid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSequenceError, CDialog)
	//{{AFX_MSG_MAP(CSequenceError)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
