#include "CommonObject.h"

class CAirways : public CCommonObject
{
public:
	CAirways();
	~CAirways();
	CString GetName();
	bool GetListOfPoints(CString Point1,CString Point2,CStringArray& ListOfPoint);
	static CAirways* GetAirway(CString AirwayName);
	static CAirways* GetSid(CString Adep);
	static CAirways* GetStar(CString Ades);
	static void LoadFile(CString FileName);
private:
	CString m_Name;
	CStringArray m_ListOfPoint;
	typedef enum {Awy,Sid,Star} AwyTypes;
	AwyTypes m_Type;
	int m_RwyNb;
	char m_Way;
	CString m_Airport;
	static CArray<CAirways*,CAirways*> m_AirwaysTable;
	static CStringArray m_AirportTable;
	static CWordArray m_RwyTable;
};