#if !defined(AFX_INIFILE_H_INCLUDED_)
#define AFX_INIFILE_H_INCLUDED_
class CIniFile : public CStdioFile
{
public:
	CIniFile();
	CIniFile(CString FileName);
	~CIniFile();
	bool  Open(CString FileName);
	void Close();
	bool IsOpen();
	DWORD GetIniProfileSectionNames(CString& ReturnedString);
	DWORD GetIniProfileSectionNames(CArray<CString,CString>& ReturnedStringArray);
	DWORD GetIniProfileSection(CString SecName,CString& ReturnedString);
	DWORD GetIniProfileSection(CString SecName,CArray<CString,CString>& ReturnedStringArray);
	DWORD GetIniProfileSectionData(CString SecName,CArray<CString,CString>& ReturnedStringArray,bool WithKey=true);
	DWORD GetIniProfileString(CString SecName,CString KeyName,CString& ReturnedString);
	BOOL WriteIniProfileString(CString SecName,CString KeyName,CString String);
	BOOL DeleteIniProfileSection(CString SecName);



private:
	CArray<CString,CString> m_StringTable;
	bool m_Modified;
	bool m_Open;
};


#endif
