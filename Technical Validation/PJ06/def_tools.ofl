;===================================================================================================================================
[GENERAL]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;    Defintions related to the tools used by the SkyServer
;
;===================================================================================================================================
; 
VERSION=def_tools.ofl Version 7.x GVA 2015-09-04

ANTIIDLE=NO
SYNCHRO_ROUTE=NO
POLLINGSTOP=YES
SEQUENCE_INFO=YES
ROUTE_INFO=YES

[AIRPORT_LEVEL]
AIRPORT1=LSGG|013|013|

[SNIFFER]
ENABLED=NO
LAT=180
LATWX=300

[RECORDING]
ENABLED=YES


[SYCO_WARNING_LIST]
1=OLDI AIX
2=MV-FDP LINK
3=IFP1
4=IFP2
5=IFP3
6=INN
7=INS
8=IMS
9=IMA
10=UL4
11=UL3
12=UK3
13=UL2
14=UK2
15=INE
16=IT4
17=IT6
18=DLT
19=FIC
20=FIA
21=FMP
22=HLP
23=MM2
24=GND
25=ARR
26=FIN
27=DEP
28=ITM
29=GS1
30=GS2
31=OLDI MILAN
32=OLDI PARIS
33=OLDI REIMS
34=OLDI ROME
35=SYLEX
36=OLDI ZURICH
37=PRN
38=AD2
39=AD1
40=INL

;===================================================================================================================================
[MTCA]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: MTCA parameters
;   
;  Reference Document: 
;   
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

ENABLED=YES
TIME_DELAY=10
DISTANCE_THRESHOLD=5.5
LAT=150
THRESHOLD_BUFFER=1.0
LAT_BUFFER=60
THRESHOLD_EXIT=120
RATIO=1.2
DISTANCE_THREXIT=5.0

#SSR range for inhibition

EX_SSR1=1400-1400
EX_SSR2=1520-1577
EX_SSR3=1600-1677
EX_SSR4=2400-2477
EX_SSR5=6100-6130
EX_SSR6=7777-7777
EX_SSR7=7000-7000

EPT_GROUP1=MKP/LUS/KOG
EPT_GROUP2=DJL/DOL
EPT_GROUP3=MOK/TUR/MOD
EPT_GROUP4=BEN/BED/ULM/ULD/TEL/FRI
EPT_GROUP5=BER/DZB/GUD
EPT_GROUP6=ASL/GEM/LTP/NAV/SOP/BEL/CBY/BES/OMA/GIG/NRU/BAW
EPT_GROUP7=RON
EPT_GROUP8=IRM
EPT_GROUP9=AOS/AOD/AON/AOE/KEV/KED/ROP/ORS
EPT_GROUP10=CER
EPT_GROUP11=PUN
EPT_GROUP12=DKM
EPT_GROUP13=GIR/RB1

;===================================================================================================================================
[EX-CDT]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: EX-CDT parameters
;   
;  Reference Document: 
;   
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

TMAX=40
;===================================================================================================================================
[LTCA]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: LTCA parameters
;   
;  Reference Document: 
;   
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

ENABLED=YES
TIME_DELAY=10
CPDLC_TIME_DELAY=300
DISTANCE_THRESHOLD=15.0
THRESHOLD_BUFFER=1.0
LOCAL_THRESHOLD=7.9
THRESHOLD_EXIT=120
RATIO=1.2
DISTANCE_THREXIT=5.0

#SSR range for inhibition

EX_SSR1=1400-1400
EX_SSR2=1520-1577
EX_SSR3=1600-1677
EX_SSR4=2400-2477
EX_SSR5=6100-6130
EX_SSR6=7777-7777
EX_SSR7=7000-7000

EPT_GROUP1=MKP/LUS/KOG
EPT_GROUP2=DJL/DOL
EPT_GROUP3=MOK/TUR/MOD
EPT_GROUP4=BEN/BED/ULM/ULD/TEL/FRI
EPT_GROUP5=BER/DZB/GUD
EPT_GROUP6=ASL/GEM/LTP/NAV/SOP/BEL/CBY/BES/OMA/GIG/NRU/BAW
EPT_GROUP7=RON
EPT_GROUP8=IRM
EPT_GROUP9=AOS/AOD/AON/AOE/KEV/KED/ROP/ORS
EPT_GROUP10=CER
EPT_GROUP11=PUN
EPT_GROUP12=DKM
EPT_GROUP13=GIR/RB1


;===================================================================================================================================
[RAM]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: Route Adherence Monitoring Tool (RAM) parameters
;   
;  Reference Document: 
;   
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

ENABLED=YES
;Distance in NM before the next point without any RAM alarm detection
POINT_LIMIT = 8
;Distance in NM after the previous point without any RAM alarm detection
POINT_LIMIT_AFTER = 3
;Distance in 1/32 NM around non turning WPT without any RAM alarm detection
POINT_LIMIT_NONTURNING = 64
;Maximum allowed angle in degrees between the speed vector and the current route segment
RAM_LIMIT = 7
;Maximum allowed angle in degrees between the speed vector and the current heading clearance (wind is taken into account)
;NB: WXHDG is the used in case "WX" is selected with the heading value
RAM_LIMIT_HDG = 30
RAM_LIMIT_WXHDG = 30
;Number of Track updates during which the RAM alarm detection process is deactivated after a DCT or HDG input
RAM_LATENCE = 10

; definition of first WPT of the SID where the circle defined in FIXPOINTS shall be applied to avoid wrong passing of WPT of the tactical route from this WPT
RAMAPPACCLATENCE=3

; defintion of last WPT not blocked in term of passing if track never  at less than the distance defined in FIXPOINT SIDx=SID name|last unprotected WPT|XPT(optional)
;SID1=T5N|GOGOL|
;SID2=M4N|GG605|
;SID3=M4P|GG605|
;SID4=M4Q|GG605|
;SID5=L3N|PETAL|
;SID6=R4N|GG605|
;SID7=R4P|GG605|
;SID8=R4Q|GG605|
;SID9=W2P|
;SID10=W2T||
;SID11=B7N|BEVEN|
;BALSI
;SID12=S4N|PETAL|
;SID13=A7N|LEGVO|
;SID14=P5A|KELUK|
;SID15=K5C|KONIL|
;SID16=K4J|KONIL|
;SID17=K3D|KONIL|
;SID18=K5A|KONIL|
;SID19=M4A|PAS|
;SID20=M4B|PAS|
;SID21=M4C|PAS|
;SID22=R4A|PAS|
;SID23=R4B|PAS|
;SID24=R4C|PAS|
;SID25=W2A|| 
;SID26=B6A|BEVEN| 
;SID27=L3A|TINAM|
;SID28=U5A||
;SID29=U5N||
;SID30=U5P||
;SID31=P4P||


;===================================================================================================================================
[HECT]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: HECT parameters
;   
;  Reference Document: Chap 1 - WINDOWS (ENTRY EXIT APP.).doc 2.5 21.06.2011
;   
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

ENABLED=YES
; flight with these ADES are managed by HECT 
ADES=LSGG/LFLI/LSGL/LSGP/LFLB/LFLP/LFHN
; HLD published WPT managed by HECT
IPT=GOLEB/CBH/DINIG/VADAR/NEMOS/ROMOM
; EAT is shown in white of PrevEAT-EAT> this value and EAT transmitted
EAT_DIFF=300
; number of track updates after which the time estimates are frozen for EAT/PLT calculations
DELAY=1
; number of track updates after which the flight is displayed in its new HW in case of swap holding
DELAYSWAPH=3
; max number of seconds between PLT1-EAT1 and PLT2-EAT2 to allow flight swapping
DELAYSWAP=300
; delay for go around in minutes
DELAYG=5
;===================================================================================================================================
[EATPLT]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: HECT parameters
;   
;  Reference Document: Chap. 15 HECT parameters v1.0 01.05.2009
;   
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================
AIRPORTS=LSGG/LFLI/LFHN/LSGL/LSGP

LR3=01:11
LR4=01:35
LR5=01:58
LR6=02:22
LR7=02:46
LR8=03:09
LR9=03:33
LR10=03:57
LR11=04:20
LR12=04:44
LR13=05:08
LR14=05:31
LR15=05:55
LR16=06:19
LR17=06:42
LR18=07:06
LR19=07:30
LR20=07:55
[EATPLTLFLB]
; used to switch from HLD to SLV
AIRPORTS=LFLB/LFLP
;===================================================================================================================================
[NOTLISTED]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;           
;   
;  Reference Document: 
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

; List of forbidden XPT used for the NotListed computation 
XPTNOTLISTED=SEQ/ATA

; List of forbidden flight Rules used for the NotListed computation 
RULESNOTLISTED=CVNE

; List of XPT used for the NotListed computation 
XPTLISTED=VBE

; List of ADEP used for the NotListed computation 
ADEPNOTLISTED=LSGG/LSGP/LSGL/LSGK/LFHN

;FPL with Flight rules which are displayed in BIN
RULESBIN=Y

;===================================================================================================================================
[NOCALCUL]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;           
;   
;  Reference Document: 
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

ADESLISTNOTRACK=LSGG
ADEPLISTNOTRACK=LSGG/LSZH
DISTLOWSPEED=10
DISTHIGHSPEED=20
ADEPNODIST=LSGG
XPTNODIST=AMK/ARG/NIN
ADEPCORRTIME=0

;===================================================================================================================================
[ROUTEPROJECTION]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: General condition to activate automatic route projection in case if it already enabled by a rule  
;           
;   
;  Reference Document: 
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

DIFFCAP=1
DISTEXIT=20
DIFFCAPTRANSFERT=7

;===================================================================================================================================
[CONFLICT_DETECTION]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: Additional to ensure that a conflict detection calculation was made at least each MAXIMAL-DETECTION_INTERVAL * 2
;           
;   
;  Reference Document: 
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

MAXIMAL_DETECTION_INTERVAL=15

;===================================================================================================================================
[SPEEDDIFFERENCE] 
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;      Defines the number of seconds to be added for the conflict detection interval as a flight behind is faster. 
;  Syntax:
;    X=Speed_lowLimit|Speed_highLimit|Add Interval in sec e.g. between 21 and 50knots faster flight behind then 
;      add 60sec to interval of the exit conflict detection
;
;===================================================================================================================================
;
1=21|50|60 
2=50|100|180 
3=100|900|300 


;===================================================================================================================================
[DEP_NO_SID]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;      List of ADEP where the flight is forced to stay in leg 1 as it is with in the defined circular area. 
;  Reference Document:
;  Syntax:
;    X=ADEP|ADEP_X|ADEP_Y|DistanceToForceLegTo1|Distance to start estimates
;
;===================================================================================================================================
;




;===================================================================================================================================
[CLAM]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose: CLAM parameters
;           
;   
;  Reference Document: 
;  Syntax:
;    Refer to the Reference Manual
;===================================================================================================================================

ACTIVE=YES
MINTRKUPDATE=10
MINSTABLETRKUPDATE=6
MINSTABLEEFLTRKUPDATE=10

; stability time parameter after CFL input: 
MINSTABLECFLEHSTRKUPDATE=4

; stability time parameter after SAL change: 
MINSTABLESELALTEHSTRKUPDATE=1

;list of ADES for which the section [EHSCLAMDISABLED] is applied
ARRIVALAIRPORTS=LSGG/LSGC

;enable the BASIC and EHS CLAM log information into LOG_DIR folder
LOG=YES

;===================================================================================================================================
[FPL_DELETE]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;      Definition of timeouts (minutes) for FPL deletion
;  Reference Document:
;      N/A
;  Syntax:
;    N/A
;
;===================================================================================================================================
;
TRACK_CANCEL_TIMEOUT=20
TRACK_AGING_TIMEOUT=10
FPL_DELETE_TIMEOUT=2160
FPL_DELETE_TIMEOUTOTHERS=1000

;===================================================================================================================================
[CPDLC]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;      Definition of CPDLC parameters
;  Reference Document:
;      N/A
;  Syntax:
;    N/A
;
;===================================================================================================================================
;
; TIMEBEFOREETN = refers to the X1 timeout in CPDLC use cases
TIMEBEFOREETN=1800
; TIMEBEFOREETX = refers to the X3 timeout in CPDLC use cases

TIMEBEFOREETX=720
; This value should theoretically = MIN(ACT_OUT time before ETX) - 1'
; To be checked with JJR

;In case of negative answer of CpcStart, a CpcStart request is resent after: (seconds number)
STARTRETRY=120

;To enable a message to indicate that the CPDLC connection is done after the recepion of the CDA  
TESTMODE=NO

;===================================================================================================================================
[SECTORTIME]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;    Definition of rate used for Entry time calculation in SEW and CDW
;  Reference Document:
;    N/A
;  Syntax:
;  	 ft/min.
;
;===================================================================================================================================
;
CLIMBRATE=1200
DESCENTRATE=1200

;===================================================================================================================================
[PTIDBIS]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;    Definition of nWPT between two syco fixes
;  Reference Document:
;    N/A
;  Syntax:
;  	SYCO fix 1|Syco Fix 2|WPT1|WPT2...|WPTn 
;
;===================================================================================================================================
1=T5N|PDU||ARBOS|||||
2=A7N|PDU|||||||
3=AAT|CBH|||||||
4=AAT|MOL||VENAT|||||
5=AAT|SOS||VENAT|||||
6=AAT|PAS|||||||
7=AAT|SPR||PAS|GVA||||
8=FRI|AMP|||||||
9=FRI|QAE|||||||
10=MOL|AMP||SOSAL|FRI||||
11=SPR|AMP||FRI|||||
12=AOD|FRI||ORSUD|MOLUS|SOSAL|||
13=AOD|GOL||BANKO|||||
14=AOD|GVA||ORSUD|||||
15=AOD|MOL||ORSUD|||||
16=AOD|ORS|||||||
17=AOD|SPR||ORSUD|||||
18=AOD|VLR||BANKO|||||
19=AOE|FRI||ORSUD|MOLUS|SOSAL|||
20=AOE|GOL||BANKO|| |||
21=AOE|GVA||ORSUD|PERAK||||
22=AOE|MOL||ORSUD|||||
23=AOE|ORS|||||||
24=AOE|SPR||ORSUD|||||
25=AOE|VLR||BANKO|||||
26=AON|FRI||ORSUD|MOLUS||||
27=AON|GOL||BANKO|||||
28=AON|GVA||ORSUD|PERAK||||
29=AON|MOL||ORSUD|||||
30=AON|ORS|||||||
31=AON|VLR||BANKO|||||
32=AOS|FRI||ORSUD|MOLUS||||
33=AOS|GOL||BANKO|||||
34=AOS|GVA||ORSUD|PERAK||||
35=AOS|MOL||ORSUD|||||
36=AOS|ORS|||||||
37=AOS|SPR||ORSUD|||||
38=AOS|VLR||BANKO|||||
39=MPA|ARG||NUSBA|||||
40=PAS|ARG||TOKDO|||||
41=SPR|ARG||GVA|PAS|TOKDO|||
42=ASL|MOL||VENAT|||||
43=ASL|SOS||VENAT|||||
44=ASL|VAT|||||||
45=AZB|FRI|||||||
46=AZB|LPS||DEKAM|||||
47=MOL|AZB||SOSAL|TELNO||||
48=PAS|AZB||GVA|SPR|FRI|||
49=SPR|AZB||FRI|||||
50=SOS|AZB||FRI|||||
51=B6A|BAS|||||||
52=B7N|BAS|||||||
53=BAS|GIR|||||||
54=MPA|BAS||GIRKU|||||
55=PAS|BAS||RUMIL|BEVEN||||
56=SPR|BAS||PAS|RUMIL|BEVEN|||
57=BAW|BLO||OBIGO|ROBEX||||
58=KIN|BAW||GIGUS|||||
59=KIN|GRA||VALOR|||||
60=GVA|INV||PERAK|ORSUD|BIBAN|GARLO||
61=MPA|INV||PERAK|ORSUD|BIBAN|GARLO||
62=INV|PAS||PERAK|ORSUD|BIBAN|GARLO||
63=BEK|MOL||VENAT|||||
64=BEK|PAS|||||||
65=BEK|SOS||VENAT|||||
66=BEK|SPR||PAS|GVA||||
67=BEK|VAT|||||||
68=BEL|CBH|||||||
69=BEL|FRI||PAS|GVA|SPR|||
70=BEL|GVA||PAS|||||
71=BEL|MOL||SOPLO|OMASI||||
72=BEL|PAS|||||||
73=BEL|SPR||PAS|GVA||||
74=BEL|VLR||SOPLO|OMASI||||
75=BED|MPA||NEMOS|VEROX||||
76=BED|NEM|||||||
77=BED|PAS||NEMOS|VADAR|SPR|GVA||
78=BED|SPR||NEMOS|VADAR||||
79=BED|VAD||NEMOS|||||
80=BEN|MPA||NEMOS|VEROX||||
81=BEN|NEM|||||||
82=BEN|PAS||NEMOS|VADAR|SPR|GVA||
83=BEN|SPR||NEMOS|VADAR||||
84=BEN|VAD||NEMOS|||||
85=BER|GVA||FRI|SPR||||
86=BER|LPS||FRI|||||
87=BER|GRA||FRI|VADAR|VALOR|||
88=BER|MPA||ESEVA|VADAR||||
89=BER|PAS||FRI|SPR|GVA|||
90=BER|SPR||FRI|||||
91=TOS|BER||SPR|FRI||||
92=BER|VAD||FRI|ESEVA||||
93=BES|CBH|||||||
94=BLO|KUM||TOSMI|LAPRI||||
95=NRU|BLO||RILIP|||||
96=CBY|CBH|||||||
97=CER|GVI||MOLUS|NIVIN|UNKIR|||
98=CER|DJL||MOLUS|NIVIN|UNKIR|||
99=CER|MOL|||||||
100=CER|PDU||MOLUS|VADEM||||
101=CER|SPR|||||||
102=CER|VEM||MOLUS|||||
103=CXD|KOV|||||||
104=T5N|DJL|||||||
105=T5N|DOL|||||||
106=MOL|GVI||NIVIN|UNKIR||||
107=DJL|DIN||GG517|LIRKO||||
108=MOL|DJL||NIVIN|UNKIR||||
109=DJL|MPA||IBABA|DIPIR|KELUK|KOVAR||
110=DJL|PAS||IBABA|DIPIR|KELUK|KOVAR|
111=DJL|MPA|AMK|LISMO|SIROD|DINOX|||
112=SPR|DJL||DINOX|SIROD|LISMO|||
113=DLE|FRI|||||||
114=DLE|SPR|||||||
115=DMP|FRI|||||||
116=DMP|MPA||SPR|||||
117=DMP|PAS||SPR|GVA||||
118=DMP|SPR|||||||
119=DMP|VAD|||||||
120=DOL|DIN||LIRKO|||||
121=DOL|GVA||LIRKO|DINIG|SOVAD|||
122=DOL|MPA||DIPIR|KELUK|KOVAR|||
123=DOL|PAS||DIPIR|KELUK|KOVAR||
124=DOL|SPR||LISMO|SIROD|DINOX|||
125=DOL|MPA|AMK|LISMO|SIROD|DINOX|||
126=DSI|SPR|||||||
127=DZB|FRI||AMRID|||||
128=DZB|LPS||AMRID|FRI|||||
129=DZB|GRA||AMRID|ESEVA|VADAR|VALOR|||
130=DZB|MPA||AMRID|ESEVA|VADAR||||
131=DZB|SPR||AMRID|FRI|||||
132=DZB|VAD||AMRID|ESEVA|||||
133=GIG|LUL|||||||
134=GIR|KUM||VANAS|MEDAM|ATMAD|NITAM||
135=MOK|GIR|||GILIR|TUROM|MILPA||
136=GIR|VEV||VANAS|MEDAM|TOSMI|||
137=GEM|MED||VANAS|||||
138=GIG|MOB|||||||
139=GIG|MOL||MOBLO|UBIMA||||
140=MOB|GUD||LAMUR|||||
141=GUD|FRI|||||||
142=GUD|GRA||FRI|VADAR|VALOR|||
143=GUD|LPS||FRI|||||
144=GUD|MPA||FRI|VADAR||||
145=OMA|GUD||VENAT|LAMUR||||
146=GUD|PAS||FRI|SPR|GVA|||
147=GUD|SPR||FRI|||||
148=GUD|VAD||FRI|ESEVA||||
149=VAT|GUD||LAMUR|||||
150=GVA|ETA|||||||
151=GVA|GRA||VALOR|||||
152=GVA|VOB||MOBLO|LURAG||||
153=IRM|GOL||BLONA|KINES||||
154=IRM|MOB||BLONA|KINES|VANAS|||
155=IRM|MOL||BLONA|KINES|VANAS|MOBLO|UBIMA|
156=IRM|SPR||BLONA|KINES|VANAS|MOBLO|UBIMA|MOLUS
157=IRM|VAN||BLONA|KINES||||
158=IRM|VLR||BLONA|KINES|VANAS|MOBLO||
159=K3D|AMP||SOSAL|FRI||||
160=K3D|AZB||SOSAL|TELNO||||
161=K3D|BER||SOSAL|FRI||||
162=K3D|DKM||LORBU|FLORY|LPS|||
163=K3D|KOR||SOSAL|TELNO||||
164=K3D|LPS||LORBU|FLORY||||
165=K3D|GRA||SOSAL|VALOR||||
166=K3D|TEL||SOSAL|||||
167=K3D|QAE||SPR|LS102|LS103|FRI||
168=K4J|AMP||SOSAL|FRI||||
169=K4J|AZB||SOSAL|TELNO||||
170=K4J|BER||SOSAL|FRI||||
171=K4J|DKM||LORBU|FLORY|LPS|||
172=K4J|GUD||SOSAL|LAMUR||||
173=K4J|KOR||SOSAL|TELNO||||
174=K4J|LPS||LORBU|FLORY||||
175=K4J|GRA||SOSAL|VALOR||||
176=K4J|TEL||SOSAL|||||
177=K4J|QAE||SPR|LS102|LS103|FRI||
178=K5A|AMP||SOSAL|FRI||||
179=K5A|AZB||SOSAL|TELNO||||
180=K5A|BER||SOSAL|FRI||||
181=K5A|DKM||LORBU|FLORY|LPS|||
182=K5A|GUD||SOSAL|LAMUR||||
183=K5A|KOR||SOSAL|TELNO||||
184=K5A|LPS||LORBU|FLORY||||
185=K5A|GRA||SOSAL|VALOR||||
186=K5A|TEL||SOSAL|||||
187=K5A|QAE||SPR|LS102|LS103|FRI||
188=K5C|AMP||SOSAL|FRI||||
189=K5C|AZB||SOSAL|TELNO||||
190=K5C|BER||SOSAL|FRI||||
191=K5C|DKM||LORBU|FLORY|LPS|||
192=K5C|GUD||SOSAL|LAMUR||||
193=K5C|KOR||SOSAL|TELNO||||
194=K5C|LPS||LORBU|FLORY||||
195=K5C|GRA||SOSAL|VALOR||||
196=K5C|TEL||SOSAL|||||
197=K5C|QAE||SPR|LS102|LS103|FRI||
198=QAE|SPR||FRI|LS103|LS102|||
199=QAE|||FRI|||||
200=KED|FRI||BANKO|MOLUS|SOSAL|||
201=KED|GOL||BANKO|||||
202=KED|MOL||BANKO|||||
203=KEL|DJL||DIPIR|IBABA||||
204=KEV|FRI||BANKO|MOLUS|SOSAL|||
205=KEV|GOL||BANKO|||||
206=KEV|MOL||BANKO|||||
207=KEV|SPR||BANKO|||||
208=KIN|GIG|||||||
209=KOG|BAS|||||||
210=ULM|KOG||SONOD|||||
211=KOG|VEV|||||||
212=MOL|KOR||SOSAL|TELNO||||
213=SPR|KOR||SOSAL|TELNO||||
214=KOV|DJL||KELUK|DIPIR|IBABA|||
215=MPA|KUM||KOGAS|VANAS|MEDAM|ATMAD|NITAM|
216=PAS|KUM||ESAPI|VANAS|MEDAM|ATMAD|NITAM|
217=KXD|KEL|||||||
218=L3A|BER||FRI|||||
219=L3A|GRA||SOSAL|VALOR||||
220=L3A|KOR||SOSAL|TELNO||||
221=L3A|TEL||SOSAL|||||
222=L3N|GRA||SOSAL|VALOR||||
223=L3N|KOR||SOSAL|TELNO||||
224=LPS|DKM|||||||
225=LPS|FRI|||||||
226=LPS|MPA||FLORY|SPR||||
227=LPS|PAS||FLORY|SPR|GVA|||
228=LPS|SPR||FLORY|||||
229=LTP|CBH||BELUS|||||
230=LTP|MOL||GIPNO|NAVLA|SOPLO|OMASI||
231=LTP|OMA||GIPNO|NAVLA|SOPLO|||
232=LTP|SPR||BELUS|PAS|GVA|||
233=LUS|DIN||SAUNI|LIRKO||||
234=LUS|GVA||SAUNI|LIRKO|DINIG|SOVAD||
235=LUS|MOL||SAUNI|LIRKO||||
236=LUS|MPA|||||||
237=LUS|SPR||SAUNI|LIRKO||||
238=LXD|MOL||VENAT|||||
239=LXD|SOS||VENAT|||||
240=LXD|VAT|||||||
241=M4A|KUM||ATMAD|NITAM||||
242=M4A|VEV||TOSMI|||||
243=M4B|KUM||ATMAD|NITAM||||
244=M4B|VEV||TOSMI|||||
245=M4C|KUM||ATMAD|NITAM||||
246=M4C|VEV||TOSMI|||||
247=M4N|KUM||ATMAD|NITAM||||
248=M4N|VEV||TOSMI|||||
249=M4P|KUM||ATMAD|NITAM||||
250=M4P|VEV||TOSMI|||||
251=M4Q|KUM||ATMAD|NITAM||||
252=M4Q|VEV||TOSMI|||||
253=MED|KUM||ATMAD|NITAM||||
254=MKP|GIR||MILPA|||||
255=MKP|GVA||MILPA|||||
256=MKP|MPA|||||||
257=MOB|KOR||LAMUR|||||
258=MOB|LAM||LAMUR|||||
259=MOB|SNS|||||||
260=MOK|DIN||AKITO|DOMIL|BOLGI|LIRKO||
261=MOK|GVA||GILIR|TUROM|BOLGI|MILPA||
262=MOD|MPA||GILIR|TUROM|BOLGI|||
263=MOK|MPA||GILIR|TUROM|BOLGI|||
264=MOD|MPA|ARG|AKITO|DOMIL|BOLGI|||
265=MOK|MPA|ARG|AKITO|DOMIL|BOLGI|||
266=MOD|MPA|AMK|AKITO|DOMIL|BOLGI|||
267=MOK|MPA|AMK|AKITO|DOMIL|BOLGI|||
268=MOK|SPR||AKITO|||||
269=MOK|TUR||GILIR|||||
270=MOL|GRA||SOSAL|VALOR||||
271=MOL|GTD||IBODI|||||
272=MPA|GRA||VALOR|||||
273=PAS|GRA||VALOR|||||
274=SPR|GRA||SOSAL|VALOR||||
275=VAD|GRA||SOSAL|VALOR||||
276=VLR|GRA|||||||
277=MPA|LRG||LURAG|||||
278=NAV|CBH|||||||
279=NAV|MOL||SOPLO|OMASI||||
280=NAV|SPR|||||||
281=MPA|NIN|||||||
282=OMA|LAM||LAMUR||||
283=OMA|GSA|||||||
284=OMA|SNN|||||||
285=ORS|LUL|||||||
286=MPA|AMK||KUDUP|||||
287=PAS|AMK|||||||
288=SPR|AMK||MILPA|KUDUP||||
289=P5A|DJL||IBABA|||||
290=P5A|DOL|||||||
291=P5A|IBA|||||||
292=P5A|PDU||LERDU|ARBOS||||
293=MOL|PDU||VADEM|DOMIL|GILIR|||
294=SPR|PDU||SIROD|ARBOS||||
295=SPR|QAE||LS102|LS103|FRI|||
296=PUN|SOS|||||||
297=PUN|VEM||SOSAL|||||
298=PXD|PAS|||||||
299=R4A|VOB||ABULO|LURAG||||
300=R4B|VOB||ABULO|LURAG||||
301=R4C|VOB||ABULO|LURAG||||
302=R4N|VOB||ABULO|LURAG||||
303=R4P|VOB||ABULO|LURAG||||
304=R4Q|VOB||ABULO|LURAG||||
305=RB1|KUM|||||||
306=ROC|KUM||VANAS|MEDAM|ATMAD|NITAM||
307=ROC|VOB||ABULO|LURAG||||
308=ROC|VEV||VANAS|MEDAM|TOSMI|||
309=RON|MEG|||||||
310=RON|MOL||MOREG|GLA||||
311=MEG|PDU||SIROD|ARBOS||||
312=RON|SPR|||||||
313=ROP|KIN||MEDAM|||||
314=S4N|AMP|||||||
315=S4N|AZB||SOSAL|TELNO||||
316=S4N|BER||FRI|||||
317=S4N|DKM||LORBU|FLORY|LPS|||
318=S4N|KOR||SOSAL|TELNO||||
319=S4N|LPS||LORBU|FLORY||||
320=S4N|GRA||SOSAL|||||
321=S4N|TEL||SOSAL|||||
322=DIN|SLV||SOVAD|GVA||||
323=FRI|SLV||ROMOM|SPR|GVA|||
324=GOL|SLV||VALBU|SUVEL|BIVLO|||
325=GVA|SLV|||||||
326=SLV|PAS||GVA|||||
327=SOS|KOR||TELNO|||||
328=SOS|TEL|||||||
329=T5N|DJL|||||||
330=T5N|IBA|||||||
331=TEL|FRI|||||||
332=TEL|GVA||FRI|SPR||||
333=MOL|TEL||SOSAL|||||
334=SPR|TEL||SOSAL|||||
335=TEL|VAD||FRI|ESEVA||||
336=TOS|SPR||BERAR|||||
337=TOS|ROC||BERAR|||||
338=TOS|GOL||BERAR|||||
339=TOS|BER||SPR|FRI||||
340=TUR|GVA||MILPA|||||
341=TUR|MPA||BOLGI|||||
342=TUR|NIN|||||||
343=U5A|CBY|||||||
344=U5N|CBY|||||||
345=U5P|CBY|||||||
346=ULD|MPA||ESEVA|VADAR||||
347=ULD|PAS||ESEVA|SPR|GVA|||
348=ULD|SPR||ESEVA|VADAR||||
349=ULD|VAD||ESEVA|||||
350=ULD|VLR||ESEVA|VADAR||||
351=ULM|MPA||ESEVA|VADAR||||
352=ULM|NIN||ESEVA|||||
353=ULM|PAS||ESEVA|VADAR|SPR|GVA||
354=ULM|SPR||ESEVA|VADAR||||
355=ULM|VAD||ESEVA|||||
356=VAN|GOD||ESOKO|||||
357=VAT|LAM||LAMUR|||||
358=SOS|DJL||NIVIN|UNKIR||||
359=VEM|PDU||DOMIL|GILIR||||
360=MPA|VOB||LURAG|||||
361=PAS|VOB||ODIKI|ROCCA|ABULO|LURAG||
362=MPA|VEV||KOGAS|VANAS|MEDAM|TOSMI||
363=PAS|VEV||ESAPI|VANAS|MEDAM|TOSMI||
364=W2A|ARG|||||||
365=W2A|AMK|||||||
366=W2P|ARG|||||||
367=W2P|AMK|||||||
368=W2T|ARG|||||||
369=W2T|AMK|||||||
370=ZGL|ARG|||||||
371=ZGL|AZB||SOSAL|TELNO||||
372=ZGL|BAS|||||||
373=ZGL|BER||FRI|||||
374=ZGL|DJL||SPR|SIROD|LERDU|||
375=ZGL|DKM||SPR|LORBU|FLORY|LPS||
376=ZGL|DOL||SPR|SIROD|LERDU|||
377=ZGL|IBA||SPR|SIROD|LERDU|||
378=ZGL|KOR||SPR|SOSAL|TELNO|||
379=ZGL|KUM||ESAPI|VANAS|MEDAM|ATMAD|NITAM|
380=ZGL|PDU||LEGVO|ARBOS||||
381=ZGL|SLV|||||||
382=ZGL|TEL||SOSAL|||||
383=ZGL|VEV||ESAPI|VANAS|MEDAM|TOSMI||
384=ZGL|VOB||ODIKI|ROCCA|ABULO|LURAG||
385=ZGL|QAE||SPR|LS102|LS103|FRI||
386=ZGP|ARG|||||||
387=ZGP|AZB||SOSAL|TELNO||||
388=ZGP|BAS|||||||
389=ZGP|BER||FRI|||||
390=ZGP|DJL||SPR|SIROD|LERDU|||
391=ZGP|DKM||SPR|LORBU|FLORY|LPS||
392=ZGP|DOL||SPR|SIROD|LERDU|||
393=ZGP|IBA||SPR|SIROD|LERDU|||
394=ZGP|KOR||SPR|SOSAL|TELNO|||
395=ZGP|KUM||ESAPI|VANAS|MEDAM|ATMAD|NITAM|
396=ZGP|AMK|||||||
397=ZGP|PDU||LEGVO|ARBOS||||
398=ZGP|SLV|||||||
399=ZGP|TEL||SOSAL|||||
400=ZGP|VEV||ESAPI|VANAS|MEDAM|TOSMI||
401=ZGP|VOB||ODIKI|ROCCA|ABULO|LURAG||
402=ZGP|QAE||SPR|LS102|LS103|FRI||
403=ZHN|ARG|||||||
404=ZHN|AZB||SOSAL|TELNO||||
405=ZHN|BAS|||||||
406=ZHN|BER||FRI|||||
407=ZHN|DJL||SPR|SIROD|LERDU|||
408=ZHN|DKM||SPR|LORBU|FLORY|LPS||
409=ZHN|DOL||SPR|SIROD|LERDU|||
410=ZHN|IBA||SPR|SIROD|LERDU|||
411=ZHN|KOR||SPR|SOSAL|TELNO|||
412=ZHN|KUM||ESAPI|VANAS|MEDAM|ATMAD|NITAM|
413=ZHN|AMK|||||||
414=ZHN|PDU||LEGVO|ARBOS||||
415=ZHN|SLV|||||||
416=ZHN|TEL||SOSAL|||||
417=ZHN|VEV||ESAPI|VANAS|MEDAM|TOSMI||
418=ZHN|VOB||ODIKI|ROCCA|ABULO|LURAG||
419=ZHN|QAE||SPR|LS102|LS103|FRI||
420=ZLI|ARG|||||||
421=ZLI|AZB||SOSAL|TELNO||||
422=ZLI|BAS|||||||
423=ZLI|BER||FRI|||||
424=ZLI|DJL||SPR|SIROD|LERDU|||
425=ZLI|DKM||SPR|LORBU|FLORY|LPS||
426=ZLI|DOL||SPR|SIROD|LERDU|||
427=ZLI|IBA||SPR|SIROD|LERDU|||
428=ZLI|KOR||SPR|SOSAL|TELNO|||
429=ZLI|KUM||ESAPI|VANAS|MEDAM|ATMAD|NITAM|
430=ZLI|PDU||LEGVO|ARBOS||||
431=ZLI|SLV|||||||
432=ZLI|TEL||SOSAL|||||
433=ZLI|VEV||ESAPI|VANAS|MEDAM|TOSMI||
434=ZLI|VOB||ODIKI|ROCCA|ABULO|LURAG||
435=ZLI|QAE||SPR|LS102|LS103|FRI||
436=NEM|SLV||VADAR|GVA||||
437=VAD|SLV||GVA|||||
438=SOS|GVI||NIVIN|UNKIR||||
439=IBA|||TUTAX|OBURO||||
440=GVA|LRG||MOBLO|LURAG||||
441=SPR|GVI||NIVIN|UNKIR||||
442=GVA|GVI||NIVIN|UNKIR||||
443=SPR|LPS||LORBU|FLORY||||
444=VEM|GTD||IBODI|||||
445=SOP|MOL||OMASI|||||




;===================================================================================================================================

;===================================================================================================================================
[PREVEPT]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;  WPT before EPT on the planned route definition
;  Reference Document:
;    N/A
;  Syntax:
;              x=EPT 3 letter|ADEP|ADES|WPT1|WPT2|....
;
;===================================================================================================================================
1=BEN|||OLBEN|LUTIX
2=BED|||OLBEN|LUTIX
3=ULM|||ROTOS|BADEP
4=ULD|||ROTOS|BADEP
5=RON|||BOBSI|MABES
6=ASL|||RISOR
7=GEM|||RISOR
8=KEV|||ADISO
9=KED|||ADISO
10=AOS|||PIMOT
11=AOD|||PIMOT
12=AOE|||OMETO
13=AON|||OMETO
14=CER|||OMETO


;===================================================================================================================================
[TACTICALFIRSTPOINT]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;  Point of the planned route used as first point in the tactical route at the tactical route initialisation definition
;  Reference Document:
;    N/A
;  Syntax:
;              x=EPT 3 letter|ADEP|ADES|WPT in five letters| Y or N to activate snifing criteria with angle 
;
;===================================================================================================================================
1=NRU|||BLONA|
2=ULM|||BADEP|
3=RON|||MABES|RONLA
4=LTP|||OMASI|
5=BEN||LSGG|LUTIX|
6=BEN||LSGL|LUTIX|
7=BEN||LFLI|LUTIX|
8=BEN||LSGP|LUTIX|
9=BEN||LFLB|LUTIX|
10=BEN||LFLP|LUTIX|
11=BEN||LFHN|LUTIX|
12=BED||LSGG|LUTIX|
13=BED||LSGL|LUTIX|
14=BED||LFLI|LUTIX|
15=BED||LSGP|LUTIX|
16=BED||LFLB|LUTIX|
17=BED||LFLP|LUTIX|
18=BED||LFHN|LUTIX|
19=BAW|||BLONA|
20=GEM|||RISOR|GEMLA
21=ASL|||RISOR|ASLEG
22=MOK|LFSB||MOROK
23=MOK|LFSN||MOROK
24=MOK|||MOROK|GILIR
25=MOD|||MOROK|GILIR
26=SOP|||OMASI|
27=ULD|||BADEP|
28=BER|LSZB|LSGG|FRI


; sniffing tolerance angle
ANGLE=2.0

;===================================================================================================================================
[CDTHLD]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;  CDT in holding 
;  Reference Document:
;    N/A
;  Syntax:
;  	
;
;===================================================================================================================================

H_RADIUS=40
H_RADIUSMIN=4
D_BUFFER=60
HLD1=DINIG||-186|495|40|3|60
HLD2=NEMOS||950|1350|40|3|60
HLD3=VADAR||770|831|40|3|60
HLD4=GOLEB|IRM|667|-276|40|3|60
HLD5=GOLEB|BES|710|-342|40|3|60
HLD6=GOLEB||627|-264|40|3|60
HLD7=ROMOM||1064|875|40|3|60

;===================================================================================================================================
[O4D]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;  O4D
;  Reference Document:
;    N/A
;  Syntax:
;  	
;
;===================================================================================================================================

ENABLED=YES
WINDTIMEOUT=1019
LTCA_STEEPEST_MASSFACTOR=1
MTCA_STEEPEST_MASSFACTOR=1
LTCA_LOWANGLE_MASSFACTOR=1
MTCA_LOWANGLE_MASSFACTOR=1

;===================================================================================================================================
[CRYSTAL]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;     Defines the parameter concerning the interface to CRYSTAL
;  Reference Document:
;      N/A
;
;===================================================================================================================================
;
ENABLED=YES
IDLE_PERIOD=4
HEARTBEAT_PERIOD=5

;------
; Needed until SLCH Step 4 to ensure all tracks precorrelated initially are not excluded from HST later on 
;(they are if they are precorrelated and don't stay at least three track updates above LEVELNOCALCUL
;-----
[PRECORRELATION]
LEVELNOCALCUL=70
;===================================================================================================================================
[TDIPRN]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;     Defines the parameter concerning the interface to TDIPRN
;  Reference Document:
;      N/A
;
;===================================================================================================================================
;
ENABLED=NO

[SSRBANK]
FAM1=W|2740|2777
FAM2=W|7510|7535

[SSRRETENTION]
;FAM1=W|301|377
FAM1=W|501|577
FAM2=W|1140|1177
FAM3=W|2701|2737
FAM4=W|3201|3277
FAM5=W|5201|5270
FAM6=W|7501|7507
FAM7=W|7570|7577

[AIRPORT]
1=LSGG|05/23|05/23|
2=LSZH|18/36|14/28|



;===================================================================================================================================
;[SID]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;  SID definition
;  Reference Document:
;    N/A
;  Syntax:
;  	SID 3 letters|point 1|point 2..(end of SID point excluded)
;
;===================================================================================================================================

;SID1=LSGG|T6N|SIROD|5|PETAL|GOGOL|KOVIM
;SID2=LSGG|M4N|MEDAM|5|PETAL|GG604|GG605|ESAPI|VANAS
;SID3=LSGG|L3N|MOLUS|5|PETAL
;SID4=LSGG|R4N|ROCCA|5|PETAL|GG604|GG605|ODIKI
;SID5=LSGG|W3T|DEPUL|5|PETAL|PAS|ARGIS
;SID6=LSGG|B7N|BEVEN|5|PETAL|GG604|SALEV|RUMIL
;SID7=LSGG|S4N|SPR|5|PETAL
;SID8=LSGG|A7N|ARBOS|5|PETAL|LEGVO
;SID9=LSGG|P5A|DIPIR|23|PAS|KELUK
;SID10=LSGG|K4J|KONIL|23|GG603|DEREM|GLA
;SID11=LSGG|M4A|MEDAM|23|PAS|ESAPI|VANAS
;SID12=LSGG|R4A|ROCCA|23|PAS|ODIKI
;SID13=LSGG|W3A|DEPUL|23|PAS|ARGIS
;SID14=LSGG|B6A|BEVEN|23|PAS|RUMIL
;SID15=LSGG|L3A|MOLUS|23|PAS|GG602|TINAM
;SID16=LSGG|U5A|BELUS|23|PAS|CBY
;SID17=LSGG|U5N|BELUS|5|PETAL|GG604|CBY

;===================================================================================================================================
[STAR]
;-----------------------------------------------------------------------------------------------------------------------------------
;  Purpose:
;  STAR definition
;  Reference Document:
;    N/A
;  Syntax:
;              SID 7 letters|point 1|Rwy|IAF|Y/N to dedfine default star for HECT|PntToAdd1|PntToAdd2|�..
; Listed from highest to lowest prio
;===================================================================================================================================

1=LSGG|AKITO2R| |23|AKITO|Y|GG518|BOLGI|LIRKO|DINIG|SOVAD|GG507|GG514|SPR|PETAL|LSGG/N0000F013
2=LSGG|DJL1R|   |23|DJL|Y|GG517|LIRKO|DINIG|SOVAD|GG507|GG514|SPR|PETAL|LSGG/N0000F013
3=LSGG|LUSAR1R| |23|SAUNI|Y|LIRKO|DINIG|SOVAD|GG507|GG514|SPR|PETAL|LSGG/N0000F013
4=LSGG|BENOT1R| |23|BENOT|Y|NEMOS|VADAR|SPR|PETAL|LSGG/N0000F013
5=LSGG|ULMES1R| |23|ULMES|Y|VADAR|SPR|PETAL|LSGG/N0000F013
6=LSGG|BANKO2R| |23|BANKO|Y|GG520|GOLEB|VALBU|SUVEL|BIVLO|GG525|GG512|SPR|PETAL|LSGG/N0000F013
7=LSGG|BELUS1R| |23|BELUS|Y|CBY|GG502|PITOM|BIVLO|GG525|GG512|SPR|PETAL|LSGG/N0000F013
8=LSGG|KINES1R| |23|KINES|Y|GG519|ROCCA|GOLEB|VALBU|SUVEL|BIVLO|GG525|GG512|SPR|PETAL|LSGG/N0000F013
9=LSGG|FRI1T|   |23|FRI|Y|ROMOM|SPR|PETAL|LSGG/N0000F013

10=LSGG|AKITO1N| |5|AKITO|Y|GG518|BOLGI|LIRKO|DINIG|SOVAD|KERAD|GG503|INDIS|PAS|LSGG/N0000F013
11=LSGG|DJL1N|   |5|DJL|Y|GG517|LIRKO|DINIG|SOVAD|KERAD|GG503|INDIS|PAS|LSGG/N0000F013
12=LSGG|LUSAR1N| |5|LUSAR|Y|SAUNI|LIRKO|DINIG|SOVAD|KERAD|GG503|INDIS|PAS|LSGG/N0000F013
13=LSGG|BENOT1P| |5|BENOT|Y|NEMOS|VADAR|GG512|BIVLO|PITOM|GG502|INDIS|PAS|LSGG/N0000F013
14=LSGG|ULMES1P| |5|ULMES|Y|VADAR|GG512|BIVLO|PITOM|GG502|INDIS|PAS|LSGG/N0000F013
15=LSGG|BANKO2N| |5|BANKO|Y|GG520|GOLEB|VALBU|SUVEL|BIVLO|PITOM|GG502|INDIS|PAS|LSGG/N0000F013
16=LSGG|BELUS1N| |5|BELUS|Y|CBY|INDIS|PAS|LSGG/N0000F013
17=LSGG|KINES1N| |5|KINES|Y|GG519|ROCCA|GOLEB|VALBU|SUVEL|BIVLO|PITOM|GG502|INDIS|PAS|LSGG/N0000F013
18=LSGG|FRI1S|   |5|FRI|Y|ROMOM|SALEV|PINOT|BELKA|LSGG/N0000F013

;62=LSZH|KELIP2G|CANNE|14|GIPOL|N|ZH713|ZH714|OSNEM
;63=LSZH|KELIP2G|CANNE|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;64=LSZH|KELIP2A|CANNE|14|AMIKI|N|ZUE|ZH701|TRA|ZH714|OSNEM
;65=LSZH|KELIP2A|CANNE|28|AMIKI|N|SONGI|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;66=LSZH|KELIP2G|RESIA|14|GIPOL|N|ZH713|ZH714|OSNEM
;67=LSZH|KELIP2G|RESIA|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;68=LSZH|KELIP2A|RESIA|14|AMIKI|N|ZUE|ZH701|TRA|ZH714|OSNEM
;69=LSZH|KELIP2A|RESIA|28|AMIKI|N|SONGI|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;70=LSZH|KELIP2G|SAFFA|14|GIPOL|N|ZH713|ZH714|OSNEM
;71=LSZH|KELIP2G|SAFFA|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;72=LSZH|KELIP2A|SAFFA|14|AMIKI|N|ZUE|ZH701|TRA|ZH714|OSNEM
;73=LSZH|KELIP2A|SAFFA|28|AMIKI|N|SONGI|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;74=LSZH|BERSU1G|KORED|14|GIPOL|N|ZH713|ZH714|OSNEM
;75=LSZH|BERSU1G|KORED|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;76=LSZH|DOPIL1G|GUDAX|14|GIPOL|N|ZH713|ZH714|OSNEM
;77=LSZH|DOPIL1G|GUDAX|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;78=LSZH|BLM2G|BLM|14|GIPOL|N|ZH713|ZH714|OSNEM
;79=LSZH|BLM2G|BLM|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;80=LSZH|HOC1G|HOC|14|GIPOL|N|ZH713|ZH714|OSNEM
;81=LSZH|HOC1G|HOC|28|GIPOL|N|ZH203|ZH204|ZH205|ZH206|ZH207|ZH208|ZH209|ZH210|ZH280|ZH281|RAMEM
;82=LSZH|RILAX1A|NATOR|28|AMIKI|SONGI|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;83=LSZH|RILAX|NATOR|14|RILAX|N|EDUMI|TRA|ZH714|OSNEM
;84=LSZH|RILAX|NATOR|28|RILAX|N|ZH264|ZH265|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;85=LSZH|RILAX|IBINI|14|RILAX|N|EDUMI|TRA|ZH714|OSNEM
;86=LSZH|RILAX|IBINI|28|RILAX|N|ZH264|ZH265|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;87=LSZH|RILAX|EMKIL|14|RILAX|N|EDUMI|TRA|ZH714|OSNEM
;88=LSZH|RILAX|EMKIL|28|RILAX|N|ZH264|ZH265|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM
;89=LSZH|NEGRA1A|RAVED|14|AMIKI|N|ZUE|ZH701|TRA|ZH714|OSNEM
;90=LSZH|NEGRA1A|RAVED|28|AMIKI|N|SONGI|ZH266|EKRIT|ZH268|ZH269|ZH270|ZH280|ZH281|RAMEM



[XPTCD]
