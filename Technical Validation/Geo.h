// Geo.h: interface for the CGeo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GEO_H__19A73878_BBC0_456C_81A4_AAD0421E7D1C__INCLUDED_)
#define AFX_GEO_H__19A73878_BBC0_456C_81A4_AAD0421E7D1C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



typedef struct
{
	BOOL NORTH ;
	int deg;
	int min;
	int sec;
}Latitude;


typedef struct
{
	BOOL EAST  ;
	int deg  ;
	int min  ;
	int sec  ;
}Longitude;

//typedef enum {Even,Odd,Both} LevelParity;


class CGeo : public CObject  
{
	friend class CLimitPoints;
public:
	CGeo();
	CGeo(CString Name,CPoint Pos);
	CString m_Name;
	CPoint  m_Pos;
	bool    m_IsFir;
	virtual ~CGeo();
	//Constant
	
	static double  GeographicToDecimal(Latitude  lat); 
	static double  GeographicToDecimal (Longitude lng);
	static void DecimalToGeographic (double val, Latitude& lat);
	static void DecimalToGeographic (double val, Longitude& lng);
	static double DecimalToRadian (double lat);
	static double LatRadianToDecimal (double rad);
	static double LongRadianToDecimal (double rad);
	static double GeographicToRadian(Latitude lat) ;
	static double GeographicToRadian(Longitude lng);
	static void RadianToGeographic(double val,Latitude& lat);
	static void RadianToGeographic(double val,Longitude& lng);
	
	static double GeoToConf(double lat);
	static double ConfToGeo(double lat);
	
	static bool StringToGeographic(CString LatLongStr,Latitude& Lat,Longitude& Long);
	static bool StringToLat(CString LatStr,Latitude& Lat);
	static bool StringToLong(CString LongStr,Longitude& Long);
	
	//static BOOL InitCenter(Latitude lat, Longitude lng);
	static BOOL InitCenter(CString center);
	static BOOL InitCenterInIni(CString IniName);

	static void ComputeLocalEarthRadius(double lat, double lng);
	
	
	static void Center(Latitude& lat, Longitude& lng);
	static void Center(double& lat, double& lng);

	static double Center();
	
	static void StereographicToGeographic(double lat, double lng,Latitude& Lat, Longitude& Long);
	
	static void GeographicToStereographic(Latitude lat, Longitude lng, double& Lat, double& Lng);

	static void LoadAirportAndPoints(CString IniName);
	static void LoadNavPoints(CString IniName);
	static void LoadIntermediatePoints(CString IniName);
	static CGeo*  GetPoint(CString name);
	static CGeo*  GetAirport(CString name);
	static CGeo*  GetAirportOrPoint(CString name);
	static void GetPointsName(CArray<CString,CString>* table);
	static long RoundTo32(double val);
	static double RoundFr32(long val);

	static bool IsEntryPoint(CString Name);	
	static bool IsIntermediatePoint(CString Name);
	static int IsHoldingPoint(CString Name);	
	static CString GetSmallName(CString Name);	
	static int IsExitPoint(CString Name,bool& Prio);	
	static int GetWindowNb(CString Name);
	static int GetExitPointDelta(CString Name);
	static int GetExitPointStopTime(CString Name);
	static bool FillFFLBuff(int PointNb,BYTE* Buff);
	static void SetExitFFL(int pntnb,int Level,bool Forbid);
	static bool GetExitFFL(int pntnb,int Level);
	static int  GetAbiTime(int PntNb,int Level,int Tas);
	static int  GetActTime(int PntNb,int Level,int Tas);
	static int  GetRevTime(int PntNb,int Level);
	static int  GetXflMin(int PntNb);
	static int  GetXflMax(int PntNb);
	static void ActiveHld(int HldPnt,bool Active);
	static bool IsActiveHld(int HldPnt);
	static CString GetHoldingPtName(int hldNumber);

/*	static From32NMToStereographic(CPoint pt,double& lat, double& lng);
	static CPoint StereographicTo32NM(double lat, double lng);*/
	static CString FindEntryPoint(CString Ptid);
	static CString FindExitPoint(CString Ptid);
	static CString FindHoldingPoint(CString Ptid);
	static int GetEntryFirNb(CString Ept,int Efl);
	static int GetExitFirNb(CString Xpt,int Xfl);
	static CPoint GetStartPoint(CString Adep);
	static CString GetStartEpt(CString Adep);
	
protected :
	typedef struct
	{
		int low;
		int high;
		int sectornb;
	} DefSectors;
	int m_DistToCentre;
//	CArray<DefSectors,DefSectors> m_SectTable;
	static CPoint m_Center;
	static double m_CenterLatDec;
	static double m_CenterLongDec;
	static const double PI ;
	static const double TWO_PI;
	static const double RADIAN_PER_DEGREE;
	static const double FEET_PER_METER;
	static const double MINUTES_PER_DEGREE;
	static const double SECONDS_PER_MINUTE;
	static const double SECONDS_PER_DEGREE;
	static const double HALF_A_MINUTE        ;
	static const double HALF_A_SECOND ;
	static const double KM_PER_NAUTICAL_MILE;
	
	//  Equatorial earth radius
	static const double KM_EARTH_RADIUS ;
	static const double NM_EARTH_RADIUS ;
	
	//   Local earth radius computed at center initialization.
	static double KM_LOCAL_EARTH_RADIUS ;
	static double NM_LOCAL_EARTH_RADIUS ;
	
	static const double EXCENTRICITY;
	static const double EXCENTRICITY_2;
	static CMap <CString,LPCSTR,CGeo*, CGeo*> m_MapPoint;
	static CMap <CString, LPCSTR, CGeo*, CGeo*> m_MapNavPoint;
	static CMap <CString,LPCSTR,CGeo*, CGeo*> m_MapAirport;

	static void InitCenter(Latitude lat, Longitude lng);

	typedef struct
	{
		int  MinTas;
		int	 MaxTas;
		int  DistBefXpt;
		int  AddTimeBefXpt;
	} OldiTimeParam;

	typedef struct
	{
		bool AtActivation;
		int TimeNb;
		OldiTimeParam Time[3];
	}	OldiMsgParam;

	typedef struct
	{
		int FirNb;
		int LowLevel;
		int HighLevel;
		OldiMsgParam Oldi[3];
		int RevParam;
		int MacParam;
	} FirDefs;
	
	typedef struct
	{
		CString Name;
		CString SmallName;
		DefSectors FirTable[3];
	} EntryDefs;
	static CArray<EntryDefs,EntryDefs> m_EntryPoints;

	typedef struct
	{
		CString Name;
		CString SmallName;
		int     WindowNb;
		int	LevelMin;
		int	LevelMax;
		int	SepTime;
		int StopTime;
		bool	FFL[100];
		FirDefs FirTable[3];
		bool Prio;
	} ExitDefs;
	static CArray<ExitDefs,ExitDefs> m_ExitCops;
	typedef struct
	{
		CString Adep;
		CString Ept;
		CPoint pnt;
	} StartPnt;
	static CArray<StartPnt, StartPnt> m_StartPoint;

	static CStringArray m_IntermediatePoints;
	static CStringArray m_HoldingPoints;
	static CStringArray m_HoldingSmallPoints;
	static CArray<bool,bool> m_ActiveHolding;
	//static PolarToStereographic();
};

#endif // !defined(AFX_GEO_H__19A73878_BBC0_456C_81A4_AAD0421E7D1C__INCLUDED_)
