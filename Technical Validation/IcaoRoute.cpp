#include "stdafx.h"
#include "IcaoRoute.h"


CIcaoRoute::CIcaoRoute(CWnd* pParent /*=NULL*/)
	: CDialog(CIcaoRoute::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewSsr)
	m_IcaoRoute = _T("");
	m_Arcid = _T("");
	//}}AFX_DATA_INIT
}


CIcaoRoute::~CIcaoRoute()
{
}

void CIcaoRoute::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIcaoRoute)
	DDX_Text(pDX, IDC_EDIT2, m_IcaoRoute);
	//DDV_MaxChars(pDX, m_ErrorInformation, 256);
	DDX_Text(pDX, IDC_ARCIDEDIT, m_Arcid);
	//DDV_MaxChars(pDX, m_Arcid, 5);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIcaoRoute, CDialog)
	//{{AFX_MSG_MAP(CIcaoRoute)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
