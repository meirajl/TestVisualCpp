#include "stdafx.h"
#include "Aftn.h"

enum FieldTypes { Int, Str, Enum, Compound, CompoundMulti, CompoundList };

typedef struct {
	AftnSubFields Field;
	int MinNumber;
	int MaxNumber;
} CompoundItem;

const AftnFields LastField = WKTRC;
const AftnSubFields LastSubField = SEQNUM;

const int FplDef[] = { 3,7,8,9,10,13,15,16,18,0 };
const AftnFields AftnValid[] = { ARCID,ADEP,ADES,EOBT,NONE_FIELD };

const AftnMsgTypes LastMsgType = FPL;

typedef struct {
	AftnFields Field;
	int MinNumber;
	int MaxNumber;
} MsgItem;

typedef struct {
	LPCSTR Name;
	const MsgItem* ItemTable;
	const int* AftnItemTable;
	const AftnFields* FieldsForAutomatic;
} MsgDef;

typedef struct
{
	LPCSTR Seq;
	AftnFields Field;
} OthersInf;

const OthersInf Field18[17] = { { "EET/",EETPT },{ "RIF/",RIF },{ "REG/",RIF },{ "SEL/",SEL },{ "OPR/",OPR },{ "STS/",STS },{ "TYP/",TYPZ },{ "PER/",PER },{ "COM/",COM },{ "DAT/",DAT },{ "NAV/",NAV },{ "DEP/",DEPZ },{ "DEST/",DESTZ },{ "ALTN/",ALTNZ },{ "RALT/",RALT },{ "CODE/",ARCADDR },{ "RMK/",RMK } };

const MsgDef MsgTable[] =
{
	{ "",NULL },
	{ "FPL", NULL, &FplDef[0], &AftnValid[0] },
};

CAftn::CAftn()
{
	m_IsValidAuto = true;
}

CAftn::~CAftn()
{
}

int CAftn::FindHyphen(CString Msg, int curpos)
{
	bool fndDelimiter = false;
	for (int i = curpos; i<(int)Msg.GetLength(); i++)
	{
		if (!fndDelimiter && (Msg[i] == '['))
		{
			fndDelimiter = true;
		}
		if (fndDelimiter && (Msg[i] == ']'))
		{
			fndDelimiter = false;
		}
		if (!fndDelimiter && (Msg[i] == '-'))
		{
			return i;
		}
	}
	return -1;
}

void CAftn::DecodeAftnSubField(int fld, CString CurMsgStr)
{
	int fnd1, i, fnd2, fnd3;
	CString TmpStr, DecStr[2], Ptid, Time, Level, SLevel, Sender, Receiver, Csn;
	SubFieldDef subflddef;
	if (CurMsgStr.IsEmpty())
		return;
	switch (fld)
	{
	case 3:
		if (CurMsgStr.GetLength() >= 3)
		{
			TmpStr = CurMsgStr.Right(CurMsgStr.GetLength() - 3);
			fnd1 = TmpStr.FindOneOf("0123456789");
			if ((fnd1 != -1) && (TmpStr.GetLength() >= fnd1 + 3))
			{
				DecStr[0] = TmpStr.Left(fnd1 + 3);
				DecStr[1] = TmpStr.Right(TmpStr.GetLength() - fnd1 - 3);
				for (int d = 0; d<2; d++)
				{
					if (DecStr[d].GetLength())
					{
						AftnFields CurFld = REFDATA;
						if (d == 1)
							CurFld = MSGREF;
						fnd1 = DecStr[d].FindOneOf("0123456789");
						fnd2 = DecStr[d].Find("/");
						if (fnd2 != -1)
						{
							m_FieldTable[CurFld].Presence = true;
							m_FieldTable[CurFld].Str = TmpStr;
							AddSubField(CurFld, SENDER, DecStr[d].Left(fnd2), 1, 1);
							AddSubField(CurFld, RECVR, DecStr[d].Mid(fnd2 + 1, fnd1 - fnd2 - 1), 1, 2);
							AddSubField(CurFld, SEQNUM, DecStr[d].Right(TmpStr.GetLength() - fnd1), 1, 3);
						}
					}
				}
			}

		}
		break;
	case 7:
		fnd1 = CurMsgStr.Find("/");
		if (fnd1 != -1)
		{
			TmpStr = CurMsgStr.Right(CurMsgStr.GetLength() - fnd1 - 1);
			CurMsgStr = CurMsgStr.Left(fnd1);
			m_FieldTable[SSRCODE].Presence = true;
			m_FieldTable[SSRCODE].Str = TmpStr;
		}
		m_FieldTable[ARCID].Presence = true;
		m_FieldTable[ARCID].Str = CurMsgStr;
		break;
	case 8:
		TmpStr = CurMsgStr.Left(1);
		if (TmpStr.GetLength())
		{
			m_FieldTable[FLTRUL].Presence = true;
			m_FieldTable[FLTRUL].Str = TmpStr;
		}
		TmpStr = CurMsgStr.Mid(1, 1);
		if (TmpStr.GetLength())
		{
			m_FieldTable[FLTTYP].Presence = true;
			m_FieldTable[FLTTYP].Str = TmpStr;
		}
		break;
	case 9:
		fnd1 = CurMsgStr.Find("/");
		if (fnd1 != -1)
		{
			TmpStr = CurMsgStr.Right(CurMsgStr.GetLength() - fnd1 - 1);
			CurMsgStr = CurMsgStr.Left(fnd1);
			m_FieldTable[WKTRC].Presence = true;
			m_FieldTable[WKTRC].Str = TmpStr;

		}
		TmpStr = "";
		for (i = 0; i<CurMsgStr.GetLength(); i++)
		{
			if (!isdigit(CurMsgStr[i]))
				break;
		}
		if (i != 0)
		{
			TmpStr = CurMsgStr.Left(i);
			CurMsgStr = CurMsgStr.Right(CurMsgStr.GetLength() - i);
			m_FieldTable[NBARC].Presence = true;
			m_FieldTable[NBARC].Str = TmpStr;
		}
		if (CurMsgStr.GetLength())
		{
			m_FieldTable[ARCTYP].Presence = true;
			m_FieldTable[ARCTYP].Str = CurMsgStr;
		}
		break;
	case 10:
		fnd1 = CurMsgStr.Find("/");
		if (fnd1 != -1)
		{
			TmpStr = CurMsgStr.Right(CurMsgStr.GetLength() - fnd1 - 1);
			CurMsgStr = CurMsgStr.Left(fnd1);
			m_FieldTable[SEQPT].Presence = true;
			m_FieldTable[SEQPT].Str = TmpStr;

		}
		if (CurMsgStr.GetLength())
		{
			m_FieldTable[CEQPT].Presence = true;
			m_FieldTable[CEQPT].Str = CurMsgStr;
		}
		break;
	case 13:
		TmpStr = CurMsgStr.Left(4);
		if (TmpStr.GetLength() == 4)
		{
			m_FieldTable[ADEP].Presence = true;
			m_FieldTable[ADEP].Str = TmpStr;
		}
		TmpStr = CurMsgStr.Mid(4, 4);
		if (TmpStr.GetLength() == 4)
		{
			m_FieldTable[EOBT].Presence = true;
			m_FieldTable[EOBT].Str = TmpStr;
		}
		break;
	case 14:
		fnd1 = CurMsgStr.Find("/");
		if (fnd1 != -1)
		{
			TmpStr = CurMsgStr.Right(CurMsgStr.GetLength() - fnd1 - 1);
			CurMsgStr = CurMsgStr.Left(fnd1);
			if (TmpStr.GetLength() >= 4)
			{
				Time = TmpStr.Left(4);
				TmpStr = TmpStr.Right(TmpStr.GetLength() - 4);
				if (TmpStr[0] == 'F' || TmpStr[0] == 'S' || TmpStr[0] == 'A' || TmpStr[0] == 'M')
				{
					for (i = 1; i<TmpStr.GetLength(); i++)
					{
						if (!isdigit(TmpStr[i]))
							break;
					}
					Level = TmpStr.Mid(0, i);
					SLevel = TmpStr.Right(TmpStr.GetLength() - i);
				}
			}

		}
		if (CurMsgStr.GetLength())
		{
			Ptid = CurMsgStr;
			if (Time.GetLength() && Level.GetLength())
			{
				TmpStr = "-ESTDATA -PTID " + Ptid + " -ETO " + Time + " -FL " + Level;
				if (SLevel.GetLength())
					TmpStr += " -SFL " + SLevel;
				m_FieldTable[ESTDATA].Presence = true;
				m_FieldTable[ESTDATA].Str = TmpStr;
				DecodeSubField(ESTDATA, TmpStr, 0, 1, true);
			}
		}
		break;
	case 15:
		fnd1 = CurMsgStr.Find(" ");
		if (fnd1 != -1)
		{
			TmpStr = CurMsgStr.Right(CurMsgStr.GetLength() - fnd1 - 1);
			CurMsgStr = CurMsgStr.Left(fnd1);
			m_FieldTable[ROUTE].Presence = true;
			m_FieldTable[ROUTE].Str = TmpStr;
		}
		if (CurMsgStr[0] == 'K' || CurMsgStr[0] == 'N' || CurMsgStr[0] == 'M')
		{
			for (i = 1; i<CurMsgStr.GetLength(); i++)
			{
				if (!isdigit(CurMsgStr[i]))
					break;
			}
			TmpStr = CurMsgStr.Mid(0, i);
			m_FieldTable[SPEED].Presence = true;
			m_FieldTable[SPEED].Str = TmpStr;
			CurMsgStr = CurMsgStr.Right(CurMsgStr.GetLength() - i);
			if (CurMsgStr[0] == 'F' || CurMsgStr[0] == 'S' || CurMsgStr[0] == 'A' || CurMsgStr[0] == 'M' || CurMsgStr[0] == 'V')
			{
				m_FieldTable[RFL].Presence = true;
				m_FieldTable[RFL].Str = CurMsgStr;
			}
		}

		break;
	case 16:
		TmpStr = CurMsgStr.Left(4);
		if (TmpStr.GetLength() == 4)
		{
			m_FieldTable[ADES].Presence = true;
			m_FieldTable[ADES].Str = TmpStr;
		}
		TmpStr = CurMsgStr.Mid(4, 4);
		if (TmpStr.GetLength() == 4)
		{
			m_FieldTable[TTLEET].Presence = true;
			m_FieldTable[TTLEET].Str = TmpStr;
		}
		fnd1 = CurMsgStr.Find(" ");
		if (fnd1 != -1)
		{
			CurMsgStr = CurMsgStr.Right(CurMsgStr.GetLength() - fnd1);
			CurMsgStr.TrimLeft();
			TmpStr = CurMsgStr.Left(4);
			if (TmpStr.GetLength() == 4)
			{
				m_FieldTable[ALTRNT1].Presence = true;
				m_FieldTable[ALTRNT1].Str = TmpStr;
			}
			CurMsgStr = CurMsgStr.Right(CurMsgStr.GetLength() - 4);
			CurMsgStr.TrimLeft();
			TmpStr = CurMsgStr.Left(4);
			if (TmpStr.GetLength() == 4)
			{
				m_FieldTable[ALTRNT2].Presence = true;
				m_FieldTable[ALTRNT2].Str = TmpStr;
			}
		}
		break;
	case 18:
		m_FieldTable[FIELD18].Presence = true;
		m_FieldTable[FIELD18].Str = CurMsgStr;
		CurMsgStr += " /";
		for (i = 0; i<17; i++)
		{
			fnd1 = CurMsgStr.Find(Field18[i].Seq);
			if (fnd1 != -1)
			{
				fnd2 = CurMsgStr.Find("/", fnd1 + CString(Field18[i].Seq).GetLength());
				TmpStr = CurMsgStr.Mid(fnd1 + CString(Field18[i].Seq).GetLength(), fnd2 - fnd1 - CString(Field18[i].Seq).GetLength());
				fnd3 = TmpStr.ReverseFind(' ');
				TmpStr = TmpStr.Left(fnd3);
				if (Field18[i].Field != NONE_FIELD)
				{
					m_FieldTable[Field18[i].Field].Presence = true;
					m_FieldTable[Field18[i].Field].Str = TmpStr;
				}
			}
		}
		break;
	}
}

bool CAftn::DecodeAftn(int CurPos)
{
	const int* AftnDef = &MsgTable[m_MsgType].AftnItemTable[0];
	if (!AftnDef)
		return false;
	int curitem = 0, curpos = CurPos, fnd1, fnd2;
	CString CurMsgStr, TmpStr, Ptid, Time, Level, SLevel;
	while (AftnDef[curitem])
	{
		if (curitem == 0)
			fnd1 = curpos;
		else
			fnd1 = FindHyphen(m_Msg, curpos);
		if (fnd1 == -1)
			return false;
		fnd2 = FindHyphen(m_Msg, fnd1 + 1);
		if (fnd2 == -1)
		{
			fnd2 = m_Msg.Find(")", fnd1 + 1);
			if (fnd2 == -1)
				return false;
		}
		CurMsgStr = m_Msg.Mid(fnd1 + 1, fnd2 - fnd1 - 1);
		curpos = fnd2;

		DecodeAftnSubField(AftnDef[curitem], CurMsgStr);
		curitem++;
	}
	const AftnFields* ValidDef = &MsgTable[m_MsgType].FieldsForAutomatic[0];
	if (ValidDef)
	{
		m_IsValidAuto = true;
		curitem = 0;
		while (ValidDef[curitem] != NONE_FIELD)
		{
			if (!m_FieldTable[ValidDef[curitem]].Presence)
			{
				m_IsValidAuto = false;
				break;
			}
			curitem++;
		}
	}
	return true;
}

bool CAftn::DecodeAftnMessage(CString AftnMsg)
{
	CString CurFldStr, CurMsgStr;
	int EndHeader = 0;
	int fnd1 = -1, fnd2=-1;

	AftnFields CurFld = NONE_FIELD;
	m_Msg = AftnMsg;
	fnd1 = AftnMsg.Find("(", EndHeader);
	fnd2 = FindHyphen(AftnMsg, fnd1 + 1);
	CurMsgStr = AftnMsg.Mid(fnd1 + 1, fnd2 - fnd1 - 1);
	CurMsgStr.TrimRight();
	CurMsgStr.TrimLeft();
	fnd2 = CurMsgStr.Find("/", 0);
	if (fnd2 != -1)
		CurMsgStr = CurMsgStr.Left(fnd2);
	m_MsgType = NONE_TITLE;
	for (int msg = 0; msg <= LastMsgType; msg++)
	{
		CurMsgStr.MakeUpper();
		if (CurMsgStr.Left(3).Compare(MsgTable[msg].Name) == 0)
		{
			m_MsgType = (AftnMsgTypes)msg;
			break;
		}
	}
	if (m_MsgType == NONE_TITLE)
		return false;
	m_IsAftn = true;
	return DecodeAftn(fnd1);
}

AftnMsgTypes CAftn::GetType()
{
	return m_MsgType;
}

bool CAftn::IsAftn()
{
	return m_IsAftn;
}

AftnFields CAftn::GetNextField(CString Msg, int& curpos, int& posfld)
{
	CString CurFldStr;
	int fnd1, fnd2, tmp;
	while (true)
	{
		fnd1 = Msg.Find("-", curpos);
		if (fnd1 == -1)
		{
			posfld = Msg.GetLength();
			curpos = Msg.GetLength();
			return NONE_FIELD;
		}
		fnd1++;
		CurFldStr = CurFldStr = GetNextWord(Msg, fnd1, fnd2);
		if (CurFldStr.IsEmpty())
			return NONE_FIELD;
		if (CurFldStr.Compare("BEGIN") == 0)//is it list field ?
		{
			tmp = fnd1;
			fnd1 = fnd2 + 1;
			CurFldStr = GetNextWord(Msg, fnd1, fnd2);
			fnd1 = tmp;
			if (CurFldStr.IsEmpty())
				return NONE_FIELD;
		}
		for (int fld = 0; fld <= LastField; fld++)
		{
			if (CurFldStr.Compare(FieldTable[fld].Name) == 0)
			{
				posfld = fnd1;
				curpos = fnd2;
				return (AftnFields)fld;
			}
		}
		curpos = fnd2;

	}

}

AftnSubFields CAftn::GetNextSubField(CString Msg, int& curpos, int& posfld)
{
	CString CurFldStr;
	int fnd1, fnd2;
	while (true)
	{
		fnd1 = Msg.Find("-", curpos);
		if (fnd1 == -1)
			return NONE_SUBFIELD;
		fnd1++;
		CurFldStr = CurFldStr = GetNextWord(Msg, fnd1, fnd2);
		if (CurFldStr.IsEmpty())
			return NONE_SUBFIELD;
		for (int fld = NONE_SUBFIELD; fld <= LastSubField; fld++)
		{
			if (CurFldStr.Compare(SubFieldTable[fld].Name) == 0)
			{
				posfld = fnd1;
				curpos = fnd2;
				return (AftnSubFields)fld;
			}
		}
		curpos = fnd2;
	}

}

CString CAftn::GetNextWord(CString Msg, int& firstpos, int& lastpos)
{

	while ((firstpos<(int)Msg.GetLength()) && (Msg[firstpos] == ' '))
		firstpos++;
	if (firstpos >= (int)Msg.GetLength())
		return "";
	lastpos = firstpos + 1;
	while ((lastpos<(int)Msg.GetLength()) && (Msg[lastpos] != ' '))
		lastpos++;
	return Msg.Mid(firstpos, lastpos - firstpos);
}

void CAftn::DecodeSubField(AftnFields CurFld, CString CurFldStr, int Level, int IndexInLevel, bool IsField)
{
	int nxtpos = 0, nxtfldpos, curfldpos, curpos, SubIndexInLevel = 1;
	AftnSubFields cursubfld, nxtsubfld;
	SubFieldDef subflddef;
	CString subfldstr;
	FieldTypes CurType;
	if (IsField)
	{
		cursubfld = NONE_SUBFIELD;
		AftnFields tmpfld = GetNextField(CurFldStr, nxtpos, nxtfldpos);
		CurType = FieldTable[tmpfld].Type;
	}
	else
	{
		cursubfld = GetNextSubField(CurFldStr, nxtpos, nxtfldpos);
		CurType = SubFieldTable[cursubfld].Type;
	}
	switch (CurType)
	{
	case Str:
		subflddef.Str = CurFldStr.Mid(nxtpos + 1, CurFldStr.GetLength() - nxtpos);
		subflddef.SubFld = cursubfld;
		subflddef.Level = Level;
		subflddef.IndexInLevel = IndexInLevel;
		m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		break;
	case Enum:
		subflddef.Str = CurFldStr.Mid(nxtpos + 1, CurFldStr.GetLength() - nxtpos);
		subflddef.SubFld = cursubfld;
		subflddef.Level = Level;
		subflddef.IndexInLevel = IndexInLevel;
		m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		break;
	case Int:
		subflddef.Str = CurFldStr.Mid(nxtpos + 1, CurFldStr.GetLength() - nxtpos);
		subflddef.SubFld = cursubfld;
		subflddef.Level = Level;
		subflddef.IndexInLevel = IndexInLevel;
		m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		break;
	case CompoundMulti:
		if (m_FieldTable[CurFld].SubFieldTable.GetSize())
		{
			for (INT_PTR l = m_FieldTable[CurFld].SubFieldTable.GetSize() - 1; l >= 0; l--)
			{
				if (m_FieldTable[CurFld].SubFieldTable[l].Level == Level + 1)
				{
					SubIndexInLevel = m_FieldTable[CurFld].SubFieldTable[l].IndexInLevel + 1;
					break;
				}
			}
		}
	case Compound:
		if (!IsField)
		{
			subflddef.Str = CurFldStr.Mid(nxtpos + 1, CurFldStr.GetLength() - nxtpos);
			subflddef.SubFld = cursubfld;
			subflddef.Level = Level;
			subflddef.IndexInLevel = IndexInLevel;
			m_FieldTable[CurFld].SubFieldTable.Add(subflddef);
		}
		cursubfld = GetNextSubField(CurFldStr, nxtpos, curfldpos);
		curpos = nxtpos;
		while (cursubfld != NONE_FIELD)
		{
			if ((SubFieldTable[cursubfld].Type == Compound) && (SubFieldTable[cursubfld].SubFldTable))
			{
				bool fnd = true;
				while (fnd)
				{
					nxtsubfld = GetNextSubField(CurFldStr, nxtpos, nxtfldpos);
					fnd = false;
					for (int i = 0;; i++)
					{
						if (SubFieldTable[cursubfld].SubFldTable[i].Field == NONE_FIELD)
							break;
						if (SubFieldTable[cursubfld].SubFldTable[i].Field == nxtsubfld)
						{
							fnd = true;
							break;
						}
					}
				}
			}
			else
				nxtsubfld = GetNextSubField(CurFldStr, nxtpos, nxtfldpos);
			if (nxtsubfld == NONE_FIELD)
				subfldstr = "-" + CurFldStr.Mid(curfldpos, CurFldStr.GetLength() - curfldpos);
			else
				subfldstr = "-" + CurFldStr.Mid(curfldpos, nxtfldpos - curfldpos - 2);
			DecodeSubField(CurFld, subfldstr, Level + 1, SubIndexInLevel++);
			cursubfld = nxtsubfld;
			curfldpos = nxtfldpos;
		}

		break;
	default:
		break;
	}
}

void CAftn::AddSubField(AftnFields Fld, AftnSubFields SubFld, CString FldStr, int Level, int IndexInLevel)
{
	SubFieldDef subflddef;
	subflddef.Str = FldStr;
	subflddef.SubFld = SubFld;
	subflddef.Level = Level;
	subflddef.IndexInLevel = IndexInLevel;
	m_FieldTable[Fld].SubFieldTable.Add(subflddef);
}
