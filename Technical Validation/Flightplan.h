#pragma once
#include "afx.h"
#include "Adexp.h"
#include "OurConstants.h"

typedef struct
{
	CString	Name;
	CPoint  Pos;
	CPoint  GeoPos;
	CTime   To;
	int		FdpNb;
	int		PntNb;
	bool	Added;
	CString	LongName;
	int     Alt;
	int     PlannedAlt;
	int		PlannedSpd;
	int		Sector;
	bool	AltSet;
	bool	Modify;
	CString Site;
} PointDef;

class CFlightPlan :
	public CObject
{
public:
	CFlightPlan();
	~CFlightPlan();

	CString		m_Adep;
	CString		m_Ades;
	CString		m_Altn1;
	CString		m_Altn2;
	CString		m_ArcAddr;
	CString		m_Arcid;
	CString		m_ArcTyp;
	CString		m_CEquip;
	CString		m_DlArcid;
	int			m_Efl;
	CString		m_Eobd;
	CString		m_Eobt;
	bool		m_Emerg;
	CTime       m_Etn;
	char		m_FlightRules;
	char		m_FlightType;
	CString		m_FplId;
	INT_PTR		m_FplNbr;
	bool		m_Head;
	bool		m_Hosp;
	bool		m_Hum;
	CString		m_IcaoRoute;
	CString		m_Seq;
	bool		m_No833;
	CString		m_Rfl;
	CString		m_Rmk;
	bool		m_RvsmEquipment;
	bool		m_Sar;
	CString		m_SEquip;
	int			m_Ssr;
	bool		m_StateFlight;
	int			m_TasPln;
	int			m_Xfl;
	char		m_Wtc;

	int			m_EptFdpFix;
	int			m_IptFdpFix;
	int			m_XptFdpFix;

	bool		m_OutsideFir;
	CString		m_XCrossName;
	CString		m_Xpt;
	CString     m_Ept;
	//CString		m_Ipt;
	CString		m_IptCentre;
	CString		m_EptRose;
	CString		m_IptRose;
	CString		m_XptRose;
	int			m_IflCentre;
	int			m_IflRose;
	int			m_EflRose;
	int			m_XflRose;
	CString		m_Sid;
	int			m_Sefl;
	CString		m_SeflSite;
	CString		m_SequenceError;
	CString		m_TrafficError;
	CString		m_OfflineError;
	CString		m_RouteInformation;

	CArray<PointDef, PointDef> m_FdpRoute;
	CArray<PointDef, PointDef> m_TmpFdpRoute;

	int				m_Sequence[MaxSeqNb];
	int				m_FullSequence[MaxSeqNb];
	char			m_SeqType[MaxSeqNb];
	int				m_Ifl[MaxSeqNb];
	CString         m_Xpts[MaxSeqNb];

	typedef struct
	{
		int SectorNb;
		char SectorType;
		CPoint Pnt;
		int Alt;
		CString XptList;
		int FixNb;
		CTime EntryTime;
		int EntryFix;
		int ExitFix;
	} SectorListDef;

	bool		IsDept();
	bool		IsArrival();

	typedef struct {
		int len;
		int elt[30];
	} confFPLnb_T;	// contains flight indices concerned by a conflict with a certain flight

	int			m_ConfNum;
	int			m_ConfNumOld;
	int			m_xfl;
	int			m_XflConf;
	int			m_XflIni;
	int			m_XflMan;
	CString		m_ConfNumSite;
	int				m_ConfFirstDetect;
	confFPLnb_T		m_ConfFplNb;
	CArray<CPoint, CPoint> m_ExitConflictVectors;
	CArray<CPoint, CPoint> m_ExitConflictDistances;
	CArray<CPoint, CPoint> m_ExitIflConflictVectors;
	CArray<CPoint, CPoint> m_ExitIflConflictDistances;
	int				m_Prevxfl;
	int				m_ConfOccur, m_PreConfOccur;
	int				m_IflIni;
	CString			m_IflConfNumSite;
	CArray<CString, CString> m_ExitConflict2ndArcid;
	CArray<CString, CString> m_ExitIflConflict2ndArcid;
	bool			m_FlightToMove;
	int				m_ComputeNextLevel;
	bool			m_IflFlightToMove;
	int				m_IflComputeNextLevel;
	CString			m_IflAircraftChoiceRule;
	CString			m_IflConflictResolutionRule;
	int				m_XflRa;
	bool			m_XflRatt;
	BYTE			m_DisplaySector;
	BYTE			m_DisplayIptSector;
	BYTE			m_SectorIn;
	BYTE			m_SectorOut;
	BYTE			m_InSector;
	int				m_PrevConfNum;
	int				m_IflConfNum;
	int				m_PrevIflConfNum;
	int				m_ifl;
	bool			m_NoConflictDetect;
	int				m_Previfl;
	int				m_IflConf;
	int				m_IflConfNumOld;
	bool			m_IflConfFirstDetect;
	confFPLnb_T	    m_IflConfFplNb;
	int				m_IflConfOccur, m_IflPreConfOccur;
	BYTE				m_SectorIpt;
	int				m_FplConflict;
	CString			m_ConflictResolutionRule;
	bool			m_FoundEpt;

	bool UpdateFromAftn(CAdexpMsg* pMsg);
	bool ExtractRoute(CString Route, CString Adep, CString Ades, int Speed, int Rfl, CString AcType, CString Cls);
	void ComputeSectorList();
	int  GetEntrySector();
	int  GetExitSector();
	void DecodeRemarkField();
	const double CalculDistance(CPoint Pnt1, CPoint Pnt2);
};

