#if !defined(AFX_STRINGUTIL_H_INCLUDED_)
#define AFX_STRINGUTIL_H_INCLUDED_


namespace NStringUtil
{
	int GetNbrOfArgument(CString str,char Separator);
	bool GetArgumentNb(CString str,int ArgNb,char Separator,CString& Arg,bool trim=false);
	CTime EncodeTimeFromString(CString TimeStr);
	CTime EncodeDateFromString(CString TimeStr);
	bool IsNumeric(CString str);
	bool IsNumericOrFloat(CString str);
	in_addr DecodeAddr(CString str);
	UINT DecodePort(CString str,bool tcp=false);

};

#endif