#include "stdafx.h"
#include "OfflineWindow.h"


COfflineWindow::COfflineWindow(CWnd* pParent /*=NULL*/)
	: CDialog(COfflineWindow::IDD, pParent)
{
	//{{AFX_DATA_INIT(COfflineWindow)
	m_OfflineLog = _T("");
	//}}AFX_DATA_INIT
}


COfflineWindow::~COfflineWindow()
{
}

void COfflineWindow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COfflineWindow)
	DDX_Text(pDX, IDC_EDIT2, m_OfflineLog);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COfflineWindow, CDialog)
	//{{AFX_MSG_MAP(COfflineWindow)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void COfflineWindow::OnDestroy()
{
//	if (this)
	//	delete this;
}

void COfflineWindow::OnClose()
{
//	if (this)
	//	delete this;
}
