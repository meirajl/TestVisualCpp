// Area.cpp: implementation of the CArea class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Area.h"
#include "IniFile.h"
#include "StringUtil.h"
#include "Math.h"
#include "FplDoc.h"
#include "AdjacentFir.h"
#include "Geo.h"
#include "LimitPoints.h"
#include "Technical Validation.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CArray<CLayer*,CLayer*> CLayer::m_LayerList;
CArray<CVolume*,CVolume*> CVolume::m_VolumeList;
CArray<CArea*,CArea*> CArea::m_SectorList;
CArray<CArea*,CArea*> CArea::m_SectorAoiList;
CArray<CArea*,CArea*> CArea::m_EhsClamDisabledList;
CArea* CArea::m_XptCdArea=NULL;
CArea* CArea::m_RouteUncompleteArea=NULL;
CArea* CArea::m_MtcaFilterArea=NULL;
CArea* CArea::m_LtcaFilterAreaGva=NULL;
CArea* CArea::m_LtcaFilterAreaZrh = NULL;
CArray<CArea*,CArea*> CArea::m_FirList;
CArray<CArea*, CArea*> CArea::m_FirList2;
CArea* CArea::m_pLocalFir=NULL;
CArea* CArea::m_pLocalFir2 = NULL;
CArea* CArea::m_pCorrelationArea=NULL;
CArray<CArea*,CArea*> CArea::m_XptConflictArea;
CArray<CArea*, CArea*> CArea::m_FirSectorArea;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLayer::CLayer()
{

}

CLayer::~CLayer()
{

}

int CLayer::LayerValueIs(int layernb)
{
	int result=0;
	for (int i=0;i< m_LayerList.GetSize();i++)
	{
		if (m_LayerList[i]->m_Nb == layernb)
		{
			result = m_LayerList[i]->m_UpperLimit;
			break;
		}
	}
	return result;
}

CVolume::CVolume()
{
	m_BoundingRect=CRect(0,0,0,0);
}

CVolume::~CVolume()
{

}

CPoint CVolume::FindIntersection(CPoint FirstPoint,CPoint Vector)
{
	CPoint ret(0,0);
	double CurT1=0.0;
	for (int i=0;i<m_PointList.GetSize()-1;i++)
	{
		if (Vector.x)
		{
			double alpha1=(m_PointList[i+1].y-m_PointList[i].y)-((double)(m_PointList[i+1].x-m_PointList[i].x)*Vector.y/Vector.x);
			if (alpha1!=0.0)
			{
				double t2=((FirstPoint.y-m_PointList[i].y)-((double)(FirstPoint.x-m_PointList[i].x)*Vector.y/Vector.x))/alpha1;
				double t1=(((m_PointList[i].x-FirstPoint.x)+((double)(m_PointList[i+1].x-m_PointList[i].x)*t2))/Vector.x);
				if ((t2>=0.0) && (t2<=1.0) && (t1>=CurT1))
				{
					ret.x=m_PointList[i].x+t2*(m_PointList[i+1].x-m_PointList[i].x);
					ret.y=m_PointList[i].y+t2*(m_PointList[i+1].y-m_PointList[i].y);
					CurT1=t1;
				}
			}

		}
		else
		{
			if (Vector.y &&((m_PointList[i+1].x-m_PointList[i].x)!=0))
			{
				double t2=(FirstPoint.x-m_PointList[i].x)/(double)(m_PointList[i+1].x-m_PointList[i].x);
				double t1=(((m_PointList[i].y-FirstPoint.y)+((double)(m_PointList[i+1].y-m_PointList[i].y)*t2))/Vector.y);
				if (t2>=0.0 && t2<=1.0 && t1>=CurT1)
				{
					ret.x=m_PointList[i].x+t2*(m_PointList[i+1].x-m_PointList[i].x);
					ret.y=m_PointList[i].y+t2*(m_PointList[i+1].y-m_PointList[i].y);
					CurT1=t1;
				}
			}
		}
	}
	return ret;
}

#define Perp(P1,P2) (P1.x*P2.y)-(P1.y*P2.x)
#define SMALL_NUM 0.00000001

int CVolume::GetIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,IntersectDef IntersectTable[2])
{
	if (((Alt1>m_HighLevel) && (Alt2>m_HighLevel)) || ((Alt1<m_LowLevel) && (Alt2<m_LowLevel)))
	{
		return 0;
	}
	int IntersectNb=0,InterAlt;
	double t1,t2,N1,N2,D;
	CPoint inter;
	if (Alt2-Alt1!=0)
	{
		t1=(double)(m_HighLevel-Alt1)/(double)(Alt2-Alt1);
		t2=(double)(m_LowLevel-Alt1)/(double)(Alt2-Alt1);
		if ((t1>=0.0) && (t1<=1.0))
		{
			inter=CPoint(Pnt1.x+(int)(t1*(Pnt2.x-Pnt1.x)),Pnt1.y+(int)(t1*(Pnt2.y-Pnt1.y)));
			if (IsInside2D(inter))
			{
				IntersectTable[IntersectNb].Pos=inter;
				IntersectTable[IntersectNb].t=(float)t1;
				IntersectTable[IntersectNb].Alt=m_HighLevel;
				IntersectNb++;
			}
		}
		if ((t2>=0.0) && (t2<=1.0))
		{
			inter=CPoint(Pnt1.x+(int)(t2*(Pnt2.x-Pnt1.x)),Pnt1.y+(int)(t2*(Pnt2.y-Pnt1.y)));
			if (IsInside2D(inter))
			{
				IntersectTable[IntersectNb].Pos=inter;
				IntersectTable[IntersectNb].t=(float)t2;
				IntersectTable[IntersectNb].Alt=m_LowLevel;
				IntersectNb++;
			}
		}
	}
	if (IntersectNb!=2)
	{
		CPoint e,Ds;
		double tE=0;
		double tL=1;
		Ds=Pnt2-Pnt1;
		for (int i=0;i<m_PointList.GetSize()-1;i++)
		{
			e=m_PointList[i+1]-m_PointList[i];
			N1=Perp(CPoint(Pnt1-m_PointList[i]),e);
			N2=Perp(Ds,CPoint(m_PointList[i]-Pnt1));
			D=Perp(e,Ds);
			if (fabs(D)<SMALL_NUM)
			{
			}
			else
			{
				t1=N1/D;
				t2=N2/D;
				if ((t1>=0.0) && (t1<=1.0) && (t2>=0.0) && (t2<=1.0))
				{
					InterAlt=Alt1+(int)(t1*(Alt2-Alt1));
					if ((InterAlt>=m_LowLevel) && (InterAlt<=m_HighLevel))
					{
						IntersectTable[IntersectNb].Pos=CPoint((int)(Pnt1.x+t1*(Pnt2.x-Pnt1.x)),(int)(Pnt1.y+t1*(Pnt2.y-Pnt1.y)));
						IntersectTable[IntersectNb].t=(float)t1;
						IntersectTable[IntersectNb].Alt=InterAlt;
						IntersectNb++;
						if (IntersectNb==2)
							break;
					}
				}
			}
			
		}
	}
	if (IntersectNb==2)
	{
		if (IntersectTable[0].Pos!=IntersectTable[1].Pos)
		{
			IntersectTable[0].Entry=IntersectTable[0].t<IntersectTable[1].t;
			IntersectTable[1].Entry=IntersectTable[0].t>=IntersectTable[1].t;
		}
		else
		{
			IntersectNb=1;
			IntersectTable[0].Entry=!IsInside(Pnt1,Alt1);
			bool ins1= IsInside(Pnt1,Alt1);
			bool ins2= IsInside(Pnt2,Alt2);
			if ((ins1==ins2) && (Alt1==Alt2))
				IntersectNb=0;
		}
	}
	if (IntersectNb==1)
	{
		IntersectTable[0].Entry=!IsInside(Pnt1,Alt1);
		bool ins1= IsInside(Pnt1,Alt1);
		bool ins2= IsInside(Pnt2,Alt2);
		if ((ins1==ins2) && (Alt1==Alt2))
			IntersectNb=0;
	}
	IntersectTable[0].VolumeNb=m_Nb;
	IntersectTable[1].VolumeNb=m_Nb;
	return IntersectNb;
}

CPoint CVolume::FindFirstIntersection(CPoint FirstPoint,CPoint Vector)
{
	CPoint ret(0,0);
	double CurT1=10000.0;
	for (int i=0;i<m_PointList.GetSize()-1;i++)
	{
		if (Vector.x)
		{
			double alpha1=(m_PointList[i+1].y-m_PointList[i].y)-((double)(m_PointList[i+1].x-m_PointList[i].x)*Vector.y/Vector.x);
			if (alpha1!=0.0)
			{
				double t2=((FirstPoint.y-m_PointList[i].y)-((double)(FirstPoint.x-m_PointList[i].x)*Vector.y/Vector.x))/alpha1;
				double t1=(((m_PointList[i].x-FirstPoint.x)+((double)(m_PointList[i+1].x-m_PointList[i].x)*t2))/Vector.x);
				if ((t2>=0.0) && (t2<=1.0) && (t1>=0) &&  (t1<=CurT1))
				{
					ret.x=m_PointList[i].x+t2*(m_PointList[i+1].x-m_PointList[i].x);
					ret.y=m_PointList[i].y+t2*(m_PointList[i+1].y-m_PointList[i].y);
					CurT1=t1;
				}
			}

		}
		else
		{
			if (Vector.y &&((m_PointList[i+1].x-m_PointList[i].x)!=0))
			{
				double t2=(FirstPoint.x-m_PointList[i].x)/(double)(m_PointList[i+1].x-m_PointList[i].x);
				double t1=(((m_PointList[i].y-FirstPoint.y)+((double)(m_PointList[i+1].y-m_PointList[i].y)*t2))/Vector.y);
				if (t2>=0.0 && t2<=1.0 && (t1>=0) && t1<=CurT1)
				{
					ret.x=m_PointList[i].x+t2*(m_PointList[i+1].x-m_PointList[i].x);
					ret.y=m_PointList[i].y+t2*(m_PointList[i+1].y-m_PointList[i].y);
					CurT1=t1;
				}
			}
		}
	}
	return ret;
}



void CVolume::LoadVolumes(CString IniName,CFplDoc* pFplDoc)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
//	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
//	POSITION fplpos = pApp->pFplTemplate->GetFirstDocPosition();
//	CFplDoc* pFplDoc = (CFplDoc*)pApp->pFplTemplate->GetNextDoc(fplpos);
	int s,i=0, j=0, k=0;
	INT_PTR nb=0;
	CIniFile* AreaFile = new CIniFile(IniName);
	CArray<CString,CString> SectionNamesTable;
	AreaFile->GetIniProfileSectionNames(SectionNamesTable);
	for (i=0;i<SectionNamesTable.GetSize();i++)
	{
		if (SectionNamesTable[i] == "FIR2")
		{
			CArray<CString, CString> SectionTable;

			AreaFile->GetIniProfileSectionData("FIR2", SectionTable, false);
			for (j = 0; j<SectionTable.GetSize(); j++)
			{
				CString strcurrent, strcurr, strlayer, strpoint;
				CArea* currentarea = new CArea();
				//				AreaFile->GetIniProfileString(SectionNamesTable[i],SectionTable[j],strcurrent);
				strcurrent = SectionTable[j];
				int nbitems = NStringUtil::GetNbrOfArgument(strcurrent, '|');
				if (nbitems >= 2)
				{

					NStringUtil::GetArgumentNb(strcurrent, 1, '|', strcurr);
					strcurr.Remove('*');
					strcurr.TrimLeft();
					strcurr.TrimRight();
					currentarea->m_Name = strcurr;
					CAdjacentFir* pFir2 = CAdjacentFir::GetFirByName(strcurr);
					if (pFir2)
						currentarea->m_FirNb = pFir2->m_Nbr;
					if ((pFir2) && (pFir2->m_IsLocal))
						CArea::m_pLocalFir2 = currentarea;
					NStringUtil::GetArgumentNb(strcurrent, 3, '|', strcurr);
					int nbitems = NStringUtil::GetNbrOfArgument(strcurr, ' ');
					bool addnext = true;
					CString item;
					for (k = 0; k<nbitems; k++)
					{
						NStringUtil::GetArgumentNb(strcurr, k + 1, ' ', item);
						if (item.Left(1) == "+")
							addnext = true;
						else
							if (item.Left(1) == "-")
								addnext = false;
							else
							{
								if (addnext)
									currentarea->m_ListIn.Add(CVolume::VolumeId(item));
								else
									currentarea->m_ListOut.Add(CVolume::VolumeId(item));
							}



					}

					// Ajouter l'element courant
					CArea::m_FirList2.Add(currentarea);
				}
			}
		}
		if (SectionNamesTable[i] == "LAYER")
		{
			CArray<CString,CString> SectionTable;
			SectionTable.RemoveAll();
			AreaFile->GetIniProfileSectionData("LAYER",SectionTable,false);
			for (j=0;j<SectionTable.GetSize();j++)
			{
				// layer processing
				CString strcurrent;
				CLayer* curlayer= new CLayer();
				strcurrent=SectionTable[j];
				int nbitems= NStringUtil::GetNbrOfArgument(strcurrent,'|');
				CString strcurr;
				if (nbitems >=2)
				{
					NStringUtil::GetArgumentNb(strcurrent,1,'|',strcurr);
					curlayer->m_Nb=atoi(strcurr);
					NStringUtil::GetArgumentNb(strcurrent,2,'|',strcurr);
					curlayer->m_UpperLimit=atoi(strcurr);
					CLayer::m_LayerList.Add(curlayer);
				}
			}
		}
		if (SectionNamesTable[i] == "VOLUME")
		{
			CArray<CString,CString> SectionTable;
			SectionTable.RemoveAll();
			AreaFile->GetIniProfileSectionData("VOLUME",SectionTable,false);
			for (j=0;j<SectionTable.GetSize();j++)
			{
				// Volumes processing
				// Volume nb | Layer1  Layer2 | Points list
				CString strcurrent,strnb,strlayer,strpoint;
				CVolume* currentvol= new CVolume();
				strcurrent=SectionTable[j];
				if (j == 465)
					int merd = 0;
				int nbitems= NStringUtil::GetNbrOfArgument(strcurrent,'|');
				if ( nbitems >=3)
				{
					// processing can be performed

					// Volume number
					NStringUtil::GetArgumentNb(strcurrent,1,'|',strnb);
					strnb.Remove('*');
					strnb.TrimLeft();
					strnb.TrimRight();
					currentvol->m_Nb=atoi(strnb);

					// Layers : 1 or 2
					NStringUtil::GetArgumentNb(strcurrent,2,'|',strlayer);
					int nb_layers = NStringUtil::GetNbrOfArgument(strlayer,'-');
					CString layer;
					if (nb_layers ==1)
					{
						NStringUtil::GetArgumentNb(strlayer,1,' ',layer);
						currentvol->m_LowLevel=CLayer::LayerValueIs(atoi(layer)-1);
						currentvol->m_HighLevel=CLayer::LayerValueIs(atoi(layer));
					}
					if (nb_layers==2)
					{
						NStringUtil::GetArgumentNb(strlayer,1,'-',layer);
						currentvol->m_LowLevel=CLayer::LayerValueIs(atoi(layer)-1);
						NStringUtil::GetArgumentNb(strlayer,2,'-',layer);
						currentvol->m_HighLevel=CLayer::LayerValueIs(atoi(layer));
					}
					if (currentvol->m_LowLevel)
						currentvol->m_LowLevel++;

					// Point list
					NStringUtil::GetArgumentNb(strcurrent,3,'|',strpoint,true);
					int nbpoints = NStringUtil::GetNbrOfArgument(strpoint,' ');
					strpoint+=' ';
//					currentvol->m_PointList.SetSize(nbpoints+1);
					CString  strpointnb;
					bool found;
					int Count=0;
					CPoint curpoint,prevpoint;
					LPoint pnt;
					for (int k=0;k<=nbpoints;k++)
					{
						int fnd=strpoint.Find(' ');
						strpointnb=strpoint.Left(fnd);
						strpoint=strpoint.Mid(fnd+1);
//						NStringUtil::GetArgumentNb(strpoint,i,' ',strpointnb);
						strpointnb.TrimRight();
						strpointnb.TrimLeft();
						if (!strpointnb.IsEmpty())
						{
						found=CLimitPoints::GetLimitPoint(atoi(strpointnb),pnt);
						if (found)
						{
							curpoint.x = pnt.x;
							curpoint.y = pnt.y;
							currentvol->m_PointId.Add(atoi(strpointnb));
							currentvol->m_PointList.Add(curpoint);
							if (curpoint.x<currentvol->m_BoundingRect.left)
								currentvol->m_BoundingRect.left=curpoint.x;
							if (curpoint.x>currentvol->m_BoundingRect.right)
								currentvol->m_BoundingRect.right=curpoint.x;
							if (curpoint.y<currentvol->m_BoundingRect.top)
								currentvol->m_BoundingRect.top=curpoint.y;
							if (curpoint.y>currentvol->m_BoundingRect.bottom)
								currentvol->m_BoundingRect.bottom=curpoint.y;
							prevpoint=curpoint;
						}
						else
						{
							// problem
						}
						}
					}

					// close the shape
					if (currentvol->m_PointList[currentvol->m_PointList.GetUpperBound()]!=currentvol->m_PointList[0])
						currentvol->m_PointList.Add(currentvol->m_PointList[0]);
				}
				else
				{
					// error
				}

				m_VolumeList.Add(currentvol);

			}
		}



		if (SectionNamesTable[i] == "SECTOR")
		{
			CArray<CString,CString> SectionTable;
			SectionTable.RemoveAll();
			AreaFile->GetIniProfileSectionData("SECTOR",SectionTable,false);
			for (j=0;j<SectionTable.GetSize();j++)
			{
				CString strcurrent,strcurr,strlayer,strpoint;
				CArea* currentarea= new CArea();
//				AreaFile->GetIniProfileString(SectionNamesTable[i],SectionTable[j],strcurrent);
				strcurrent=SectionTable[j];
				int nbitems= NStringUtil::GetNbrOfArgument(strcurrent,'|');
				if ( nbitems >=3)
				{

					NStringUtil::GetArgumentNb(strcurrent,1,'|',strcurr);
					strcurr.Remove('*');
					strcurr.TrimLeft();
					strcurr.TrimRight();
					currentarea->m_Name=strcurr;
					currentarea->m_SectorNb=0;
					for (k=0;k< pApp->m_pOffline->m_SectorTable.GetSize();k++)
					{
						if (pApp->m_pOffline->m_SectorTable[k].UAC.CompareNoCase(strcurr)==0)
						{
							currentarea->m_SectorNb=k;
							break;
						}
					}
					NStringUtil::GetArgumentNb(strcurrent,2,'|',strcurr);
					currentarea->m_Type=transit;
					if (strcurr.CompareNoCase("DEPARTURE")==0)
						currentarea->m_Type=departure;
					if (strcurr.CompareNoCase("ARRIVAL")==0)
						currentarea->m_Type=arrival;
					NStringUtil::GetArgumentNb(strcurrent,3,'|',strcurr);
					int nbitems=NStringUtil::GetNbrOfArgument(strcurr,' ');
					bool addnext=true;
					CString item;
					for (k=0;k<nbitems;k++)
					{
						NStringUtil::GetArgumentNb(strcurr,k+1,' ',item);
						if (item.Left(1) =="+")
							addnext= true;
						else
							if (item.Left(1) =="-")
								addnext=false;
							else
							{
								if (addnext)
									currentarea->m_ListIn.Add(CVolume::VolumeId(item));
									else
									currentarea->m_ListOut.Add(CVolume::VolumeId(item));
							}



					}

					// Ajouter l'element courant
					if (currentarea->m_SectorNb)
						CArea::m_SectorList.Add(currentarea);
				}
			}
		}

		
		if (SectionNamesTable[i] == "FIR")
		{
			CArray<CString,CString> SectionTable;
			SectionTable.RemoveAll();
			AreaFile->GetIniProfileSectionData("FIR",SectionTable,false);
			for (j=0;j<SectionTable.GetSize();j++)
			{
				CString strcurrent,strcurr,strlayer,strpoint;
				CArea* currentarea= new CArea();
//				AreaFile->GetIniProfileString(SectionNamesTable[i],SectionTable[j],strcurrent);
				strcurrent=SectionTable[j];
				int nbitems= NStringUtil::GetNbrOfArgument(strcurrent,'|');
				if ( nbitems >=2)
				{

					NStringUtil::GetArgumentNb(strcurrent,1,'|',strcurr);
					strcurr.Remove('*');
					strcurr.TrimLeft();
					strcurr.TrimRight();
					currentarea->m_Name=strcurr;
					CAdjacentFir* pFir= CAdjacentFir::GetFirByName(strcurr);
					if (pFir)
						currentarea->m_FirNb=pFir->m_Nbr;
					if ((pFir) && (pFir->m_IsLocal))
						CArea::m_pLocalFir=currentarea;
					NStringUtil::GetArgumentNb(strcurrent,3,'|',strcurr);
					int nbitems=NStringUtil::GetNbrOfArgument(strcurr,' ');
					bool addnext=true;
					CString item;
					for (k=0;k<nbitems;k++)
					{
						NStringUtil::GetArgumentNb(strcurr,k+1,' ',item);
						if (item.Left(1) =="+")
							addnext= true;
						else
							if (item.Left(1) =="-")
								addnext=false;
							else
							{
								if (addnext)
									currentarea->m_ListIn.Add(CVolume::VolumeId(item));
									else
									currentarea->m_ListOut.Add(CVolume::VolumeId(item));
							}



					}

					// Ajouter l'element courant
					CArea::m_FirList.Add(currentarea);
				}
			}
		}
		
		if (SectionNamesTable[i] == "FIRSECTORS")
		{
			CArray<CString, CString> SectionTable;
			SectionTable.RemoveAll();
			AreaFile->GetIniProfileSectionData("FIRSECTORS", SectionTable, false);
			for (j = 0; j<SectionTable.GetSize(); j++)
			{
				CString strcurrent, strcurr, strlayer, strpoint;
				CArea* currentarea = new CArea();
				//				AreaFile->GetIniProfileString(SectionNamesTable[i],SectionTable[j],strcurrent);
				strcurrent = SectionTable[j];
				int nbitems = NStringUtil::GetNbrOfArgument(strcurrent, '|');
				if (nbitems >= 3)
				{

					NStringUtil::GetArgumentNb(strcurrent, 1, '|', strcurr);
					strcurr.Remove('*');
					strcurr.TrimLeft();
					strcurr.TrimRight();
					currentarea->m_Name = strcurr;
					NStringUtil::GetArgumentNb(strcurrent, 2, '|', strcurr);
					currentarea->m_SectorName = strcurr;
					NStringUtil::GetArgumentNb(strcurrent, 3, '|', strcurr);
					int nbitems = NStringUtil::GetNbrOfArgument(strcurr, ' ');
					bool addnext = true;
					CString item;
					for (k = 0; k<nbitems; k++)
					{
						NStringUtil::GetArgumentNb(strcurr, k + 1, ' ', item);
						if (item.Left(1) == "+")
							addnext = true;
						else
							if (item.Left(1) == "-")
								addnext = false;
							else
							{
								if (addnext)
									currentarea->m_ListIn.Add(CVolume::VolumeId(item));
								else
									currentarea->m_ListOut.Add(CVolume::VolumeId(item));
							}



					}

					// Ajouter l'element courant
					CArea::m_FirSectorArea.Add(currentarea);
				}
			}
		}
	}
	delete AreaFile;
	CreateDirectory(".\\Out",NULL);	
	int MapNb = 62;
	for (s=0;s<CArea::m_SectorList.GetSize();s++)
		CArea::m_SectorList[s]->CreateAreaMap(MapNb++,"SECTOR");
	for (s=0;s<CArea::m_SectorAoiList.GetSize();s++)
		CArea::m_SectorAoiList[s]->CreateAreaMap(MapNb++,"SECTOR AOI");
	MapNb += CArea::m_SectorAoiList.GetSize();
	if (CArea::m_XptCdArea)
		CArea::m_XptCdArea->CreateAreaMap(MapNb++,"SECTOR");
	if (CArea::m_RouteUncompleteArea)
		CArea::m_RouteUncompleteArea->CreateAreaMap(MapNb++,"SECTOR");
	for (s=0;s<CArea::m_EhsClamDisabledList.GetSize();s++)
		CArea::m_EhsClamDisabledList[s]->CreateAreaMap(MapNb++,"EHS");
	if (CArea::m_pLocalFir)
		CArea::m_pLocalFir->CreateAreaMap(MapNb++,"LOCAL FIR");
	if (CArea::m_pLocalFir2)
		CArea::m_pLocalFir2->CreateAreaMap(MapNb++, "LOCAL FIR2");
	if (CArea::m_MtcaFilterArea)
		CArea::m_MtcaFilterArea->CreateAreaMap(MapNb++,"CDT GVA");
	if (CArea::m_LtcaFilterAreaGva)
		CArea::m_LtcaFilterAreaGva->CreateAreaMap(MapNb++,"CDT GVA");
	if (CArea::m_LtcaFilterAreaZrh)
		CArea::m_LtcaFilterAreaZrh->CreateAreaMap(MapNb++, "CDT ZRH");
	for (int f=0;f<CArea::m_XptConflictArea.GetSize();f++)
	{
		if (CArea::m_XptConflictArea[f])
			CArea::m_XptConflictArea[f]->CreateAreaMap(MapNb++,"FRA");
	}
	for (s = 0; s<CArea::m_FirSectorArea.GetSize(); s++)
		CArea::m_FirSectorArea[s]->CreateAreaMap(MapNb++, "FIR SECTOR");
	for (i=0;i<CArea::m_SectorList.GetSize();i++)
	{
		DWORD int1=GetTickCount();
		int j;
		for (j=0;j<10000;j++)
			CArea::m_SectorList[i]->IsInside(CPoint(0,0),CArea::m_SectorList[i]->m_ListIn[0]->m_LowLevel);
		DWORD int2=GetTickCount();
		int PointNb=0;
		for (j=0;j<CArea::m_SectorList[i]->m_ListIn.GetSize();j++)
		{
			PointNb+=CArea::m_SectorList[i]->m_ListIn[j]->m_PointList.GetSize();
		}
		CString Msg;
		Msg.Format("Sector %d %s Size=%d Time=%.3f ms",CArea::m_SectorList[i]->m_SectorNb,CArea::m_SectorList[i]->m_Name,PointNb,((double)(int2-int1))/10000);
		//CVisuApp* pApp = (CVisuApp*)AfxGetApp();
		//pApp->LogMsg(Msg);
	}
}



bool CVolume ::IsInside(CPoint position, int level)
{
	bool result=false;
	// Determine if the given point in cartesian coordinates is inside or outside the volume
	// First check the level
	bool yi, yj;
	double xj;
	if (m_LowLevel<=level && m_HighLevel >= level)
	{
		if (m_BoundingRect.PtInRect(position))
		{
			//		return (m_Rgn.PtInRegion(position)==TRUE);
			// processing on points
			int i, j, c=0;
			for (i=0,j=m_PointList.GetSize()-1;i<m_PointList.GetSize();j=i++)
			{
				if (((m_PointList[i].y>position.y) != (m_PointList[j].y>position.y)) &&
					((double)(position.x) < (double) ((double)((m_PointList[j].x-m_PointList[i].x) * (position.y-m_PointList[i].y)) / (double) (m_PointList[j].y-m_PointList[i].y) + m_PointList[i].x)))
					
					result=!result;
			}
		}
	}
	return result;
}

bool CVolume ::IsInside2D(CPoint position)
{
//	return m_Rgn.PtInRegion(position);
	bool result=false;
	// Determine if the given point in cartesian coordinates is inside or outside the volume
	// processing on points
	if (m_BoundingRect.PtInRect(position))
	{
		int i, j, c=0;
		for (i=0,j=m_PointList.GetSize()-1;i<m_PointList.GetSize();j=i++)
		{
			if (((m_PointList[i].y>position.y) != (m_PointList[j].y>position.y)) &&
				((double)(position.x) < (double) ((double)((m_PointList[j].x-m_PointList[i].x) * (position.y-m_PointList[i].y)) / (double) (m_PointList[j].y-m_PointList[i].y) + m_PointList[i].x)))
				
				result=!result;
		}
	}
	return result;
}


CVolume* CVolume::VolumeId(CString nb)
{
	CVolume* result=NULL;
	for (int i=0;i<m_VolumeList.GetSize();i++)
	{
		if (m_VolumeList[i]->m_Nb == atoi(nb))
		{
			result= m_VolumeList[i];
			break;
		}
	}
	return result;
}




CArea::CArea()
{

}

CArea::~CArea()
{

}

bool CArea::IsInside(CPoint Target, int Altitude)
{
	bool foundin, foundout;
	foundin=false;
	foundout=false;
	int j;
	// search in th List of included volumes
	for (j=0; j< m_ListIn.GetSize();j++)
	{
		if (m_ListIn[j]->IsInside(Target, Altitude))
		{
			foundin=true;
			break;
		}
	}
	// search in th List of excluded volumes
	for (j=0; j< m_ListOut.GetSize();j++)
	{
		if (m_ListOut[j]->IsInside(Target, Altitude))
		{
			foundout=true;
			break;
		}
	}
	// if foundin and not foundout the point is inside the area
	return (foundin && !foundout);
}


bool CArea::IsInsideIn(CPoint Target, int Altitude)
{
	bool foundin;
	foundin=false;
	// search in th List of included volumes
	for (int j=0; j< m_ListIn.GetSize();j++)
	{
		if (m_ListIn[j]->IsInside(Target, Altitude))
		{
			foundin=true;
			break;
		}
	}
	return foundin;
}

bool CArea::IsInsideOut(CPoint Target, int Altitude)
{
	bool foundout;
	foundout=false;
	// search in th List of included volumes
	for (int j=0; j< m_ListOut.GetSize();j++)
	{
		if (m_ListOut[j]->IsInside(Target, Altitude))
		{
			foundout=true;
			break;
		}
	}
	return foundout;
}


bool CArea::IsInside2D(CPoint Target)
{
	bool foundin, foundout;
	foundin=false;
	foundout=false;
	int j;
	// search in th List of included volumes
	for (j=0; j< m_ListIn.GetSize();j++)
	{
		if (m_ListIn[j]->IsInside2D(Target))
		{
			foundin=true;
			break;
		}
	}
	// search in th List of excluded volumes
	for (j=0; j< m_ListOut.GetSize();j++)
	{
		if (m_ListOut[j]->IsInside2D(Target))
		{
			foundout=true;
			break;
		}
	}
	// if foundin and not foundout the point is inside the area
	return (foundin && !foundout);
}






int CArea::GetSectorNb(CPoint Target,int Altitude,flightkinds kind)
{
	for (int i=0;i<m_SectorList.GetSize();i++)
	{
		if ((m_SectorList[i]->m_SectorNb) &&
			(((kind==transit) && (m_SectorList[i]->m_Type==transit)) ||
			((kind==departure) && ((m_SectorList[i]->m_Type==transit)||(m_SectorList[i]->m_Type==departure))) ||
			((kind==arrival) && ((m_SectorList[i]->m_Type==transit)||(m_SectorList[i]->m_Type==arrival)))) &&
			 ( m_SectorList[i]->IsInside(Target,Altitude)))
			return m_SectorList[i]->m_SectorNb;
	}
	return 0;
}




int CArea::GetSectorNbInSequence(CPoint Target,int Altitude,int Seq[32],int CurSeqNb)
{
	for (int i=__max(0,CurSeqNb-1);i<32;i++)
	{
		if (Seq[i]==0)
			break;
		for (int j=0;j<m_SectorList.GetSize();j++)
		{
			if (m_SectorList[j]->m_SectorNb==Seq[i])
			{
				if (m_SectorList[j]->IsInside(Target,Altitude))
					return i+1;
			}
		}
	}
/*	if (CurSeqNb && Seq[CurSeqNb+1]==0)
	{
		if (!GetSectorNb(Target,Altitude))
			return i+1;
	}*/

	return CurSeqNb;
}

void CArea::CreateAreaMap(int MapNb,CString SubMenu)
{
	CString FileName,TextFile,SectorName,Tmp,PntList,Info;
	FileName.Format("out\\%.4d_1%.4d.mds",MapNb,MapNb);
	SectorName = m_Name;
	CStdioFile mapfile(FileName,CFile::modeCreate|CFile::modeWrite);
	TextFile.Format("<Item=Title> <Text=\"MAP\\AIRSPACES\\SECTORS\\%s %s\"> <Mode=Manual Off> <Info=\"\"> \n",SubMenu,SectorName);
	mapfile.WriteString(TextFile);
	int v;
	for (v=0;v<m_ListIn.GetSize();v++)
	{
		Info.Format("VolumeIn Nb=%d Low=%d,High=%d", m_ListIn[v]->m_Nb, m_ListIn[v]->m_LowLevel, m_ListIn[v]->m_HighLevel);
		PntList.Format("%d", m_ListIn[v]->m_PointList.GetSize());
		for (int p = 0; p < m_ListIn[v]->m_PointList.GetSize(); p++)
		{
			Tmp.Format(" (%d,%d)", m_ListIn[v]->m_PointList[p].x, m_ListIn[v]->m_PointList[p].y);
			PntList += Tmp;
		}
		TextFile.Format("<Item=Surface> <Prio=26> <Color=2e2d2d> <PenStyle=PS_SOLID> <Color2=2e2d2d> <Brushstyle=BS_PATTERN 0100010000010001010001000001000101000100000100010100010000010001> <Info=\"%s\"> <Postable=%s>\n", Info, PntList);
		mapfile.WriteString(TextFile);
		for (int p = 0; p < m_ListIn[v]->m_PointList.GetSize(); p++)
		{
			TextFile.Format("<Item = Beacon> <Prio=4> <Color=808080> <Font=\"GraphicsGva\"> <Size=100> <Beacon=\"BIVLO\"> <Info= \"Point id=%d\"> <Pos = (%d,%d)> <Text=\"1\">\n", m_ListIn[v]->m_PointId[p], m_ListIn[v]->m_PointList[p].x, m_ListIn[v]->m_PointList[p].y);
			//TextFile.Format("<Item = Beacon> <Prio=4> <Color=808080> <Font=\"GraphicsGva\"> <Size=100> <Beacon=""> <Info= ""> <Pos = (%d,%d)> <Text=\"\">\n", m_ListIn[v]->m_PointList[p].x, m_ListIn[v]->m_PointList[p].y);
			mapfile.WriteString(TextFile);
		}
	}
	for (v=0;v<m_ListOut.GetSize();v++)
	{
		Info.Format("VolumeOut Nb=%d Low=%d,High=%d",m_ListOut[v]->m_Nb,m_ListOut[v]->m_LowLevel,m_ListOut[v]->m_HighLevel);
		PntList.Format("%d",m_ListOut[v]->m_PointList.GetSize());
		for (int p=0;p<m_ListOut[v]->m_PointList.GetSize();p++)
		{
			Tmp.Format(" (%d,%d)",m_ListOut[v]->m_PointList[p].x,m_ListOut[v]->m_PointList[p].y);
			PntList+=Tmp;
		}
		TextFile.Format("<Item=Surface> <Prio=25> <Color=2e2d6d> <PenStyle=PS_SOLID> <Color2=2e2d2d> <Brushstyle=BS_PATTERN 0100010000010001010001000001000101000100000100010100010000010001> <Info=\"%s\"> <Postable=%s>\n",Info,PntList);
		mapfile.WriteString(TextFile);
	}
}

bool CArea::IsInDisabledEhsClamArea(CPoint Target,int Altitude)
{
	for (int i=0;i<m_EhsClamDisabledList.GetSize();i++)
	{
		if (m_EhsClamDisabledList[i]->IsInside(Target,Altitude))
			return true;
	}
	return false;
}


CPoint CArea::GetXptCd(CPoint FirstPoint,CPoint Vector)
{
	if (m_XptCdArea && m_XptCdArea->m_ListIn.GetSize())
		return m_XptCdArea->m_ListIn[0]->FindIntersection(FirstPoint,Vector);
	else
		return CPoint(0,0);
}

bool CArea::IsInUncompleteRouteArea(CPoint Target,int Xfl)
{
	if (m_RouteUncompleteArea->IsInside(Target,Xfl))
		return true;
	return false;
}

bool CArea::IsInXptCdArea(CPoint Target)
{
	if (m_XptCdArea->IsInside(Target,0))
		return true;
	return false;
}

bool CArea::IsInMtcaFilterArea(CPoint Target)
{
	if (m_MtcaFilterArea && m_MtcaFilterArea->IsInside2D(Target))
			return true;
	return false;
}

bool CArea::IsInLtcaFilterAreaGva(CPoint Target)
{
	if (m_LtcaFilterAreaGva && m_LtcaFilterAreaGva->IsInside2D(Target))
			return true;
	return false;
}

bool CArea::IsInLtcaFilterAreaZrh(CPoint Target)
{
	if (m_LtcaFilterAreaZrh && m_LtcaFilterAreaZrh->IsInside2D(Target))
		return true;
	return false;
}

bool CArea::IsInsideLocalFir(CPoint pnt)
{
	if (m_pLocalFir)
		return m_pLocalFir->IsInside2D(pnt);
	return true;
}

bool CArea::IsInsideLocalFir2(CPoint pnt)
{
	if (m_pLocalFir2)
		return m_pLocalFir2->IsInside2D(pnt);
	return true;
}

bool CArea::IsInsideCorrelationarea(CPoint pnt)
{
	if (m_pCorrelationArea)
		return m_pCorrelationArea->IsInside2D(pnt);
	return true;
}


bool CArea::GetSectorIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,CArray<IntersectDef,IntersectDef>& IntersectTable,flightkinds kind)
{
	IntersectTable.RemoveAll();
	IntersectDef TmpTable[2],TmpInter;
	int InterNb,i,j;
	CArray<IntersectDef,IntersectDef> TmpIntersectTable;

	CString msglog;

	for (i=0;i<m_SectorList.GetSize();i++)
	{
		if (((kind==transit) && (m_SectorList[i]->m_Type==transit)) ||
			((kind==departure) && ((m_SectorList[i]->m_Type==transit)||(m_SectorList[i]->m_Type==departure))) ||
			((kind==arrival) && ((m_SectorList[i]->m_Type==transit)||((m_SectorList[i]->m_Type==arrival)))) ||
			((kind==internal) && (Alt1>Alt2) && ((m_SectorList[i]->m_Type==transit)||(m_SectorList[i]->m_Type==arrival))) ||
			((kind==internal) && (Alt1<Alt2) && ((m_SectorList[i]->m_Type==transit)||(m_SectorList[i]->m_Type==departure))) ||
			((kind==internal) && (Alt1==Alt2) && (m_SectorList[i]->m_Type==transit)))
		{
			for (j=0;j<m_SectorList[i]->m_ListIn.GetSize();j++)
			{
				InterNb=m_SectorList[i]->m_ListIn[j]->GetIntersect(Pnt1,Alt1,Pnt2,Alt2,TmpTable);
				for (int k=0;k<InterNb;k++)
				{
					if (!m_SectorList[i]->IsInsideOut(TmpTable[k].Pos,TmpTable[k].Alt))
					{
						TmpTable[k].Out=false;
						TmpTable[k].SectorNb=m_SectorList[i]->m_SectorNb;
						bool Ok=true;
						if (TmpTable[k].Entry)
						{
							double dist=sqrt((Pnt2.x-Pnt1.x)*(Pnt2.x-Pnt1.x)+(Pnt2.y-Pnt1.y)*(Pnt2.y-Pnt1.y));
							CPoint extpos(TmpTable[k].Pos.x+(16.0*(Pnt2.x-Pnt1.x))/dist,TmpTable[k].Pos.y+(16.0*(Pnt2.y-Pnt1.y))/dist);
							if (!m_SectorList[i]->IsInside(extpos,TmpTable[k].Alt))
								Ok=false;
						}
						if (Ok)
							TmpIntersectTable.Add(TmpTable[k]);
					}
				}
			}
			for (j=0;j<m_SectorList[i]->m_ListOut.GetSize();j++)
			{
				InterNb=m_SectorList[i]->m_ListOut[j]->GetIntersect(Pnt1,Alt1,Pnt2,Alt2,TmpTable);
				for (int k=0;k<InterNb;k++)
				{
					if (m_SectorList[i]->IsInsideIn(TmpTable[k].Pos,TmpTable[k].Alt))
					{
						TmpTable[k].Out=true;
						TmpTable[k].Entry=!TmpTable[k].Entry;
						bool Ok=true;
						if (TmpTable[k].Entry)
						{
							double dist=sqrt((Pnt2.x-Pnt1.x)*(Pnt2.x-Pnt1.x)+(Pnt2.y-Pnt1.y)*(Pnt2.y-Pnt1.y));
							CPoint extpos(TmpTable[k].Pos.x+(16.0*(Pnt2.x-Pnt1.x))/dist,TmpTable[k].Pos.y+(16.0*(Pnt2.y-Pnt1.y))/dist);
							if (!m_SectorList[i]->IsInside(extpos,TmpTable[k].Alt))
								Ok=false;
						}
						TmpTable[k].SectorNb=m_SectorList[i]->m_SectorNb;
						if (Ok)
							TmpIntersectTable.Add(TmpTable[k]);
					}
				}
			}
		}
	}
	float Min=1.0;
	int curmin=-1;
	for (i=0;i<TmpIntersectTable.GetSize();i++)
	{
		for (j=0;j<IntersectTable.GetSize();j++)
		{
			if ((TmpIntersectTable[i].t<IntersectTable[j].t) || ((TmpIntersectTable[i].t==IntersectTable[j].t) && (TmpIntersectTable[i].Entry<IntersectTable[j].Entry)))
			{
				IntersectTable.InsertAt(j,TmpIntersectTable[i]);
				break;
			}
		}
		if (j == IntersectTable.GetSize())
		{
			IntersectTable.Add(TmpIntersectTable[i]);
		}
	}
	return true;
}

bool CArea::GetSectorAoiIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,CArray<IntersectDef,IntersectDef>& IntersectTable,flightkinds kind)
{
	IntersectTable.RemoveAll();
	IntersectDef TmpTable[2],TmpInter;
	int InterNb,i,j;
	CArray<IntersectDef,IntersectDef> TmpIntersectTable;
	for (int i=0;i<m_SectorAoiList.GetSize();i++)
	{
		if (((kind==transit) && (m_SectorAoiList[i]->m_Type==transit)) ||
			((kind==departure) && ((m_SectorAoiList[i]->m_Type==transit)||(m_SectorAoiList[i]->m_Type==departure))) ||
			((kind==arrival) && ((m_SectorAoiList[i]->m_Type==transit)||(m_SectorAoiList[i]->m_Type==arrival))) ||
			((kind==internal) && (Alt1>Alt2) && ((m_SectorAoiList[i]->m_Type==transit)||(m_SectorAoiList[i]->m_Type==arrival))) ||
			((kind==internal) && (Alt1<Alt2) && ((m_SectorAoiList[i]->m_Type==transit)||(m_SectorAoiList[i]->m_Type==departure))) ||
			((kind==internal) && (Alt1==Alt2) && (m_SectorAoiList[i]->m_Type==transit)))
		{
			for (j=0;j<m_SectorAoiList[i]->m_ListIn.GetSize();j++)
			{
				InterNb=m_SectorAoiList[i]->m_ListIn[j]->GetIntersect(Pnt1,Alt1,Pnt2,Alt2,TmpTable);
				for (int k=0;k<InterNb;k++)
				{
					if (!m_SectorAoiList[i]->IsInsideOut(TmpTable[k].Pos,TmpTable[k].Alt))
					{
						TmpTable[k].Out=false;
						TmpTable[k].SectorNb=m_SectorAoiList[i]->m_SectorNb;
						bool Ok=true;
						if (TmpTable[k].Entry)
						{
							double dist=sqrt((Pnt2.x-Pnt1.x)*(Pnt2.x-Pnt1.x)+(Pnt2.y-Pnt1.y)*(Pnt2.y-Pnt1.y));
							CPoint extpos(TmpTable[k].Pos.x+(16.0*(Pnt2.x-Pnt1.x))/dist,TmpTable[k].Pos.y+(16.0*(Pnt2.y-Pnt1.y))/dist);
							if (!m_SectorAoiList[i]->IsInside(extpos,TmpTable[k].Alt))
								Ok=false;
						}
						if (Ok)
							TmpIntersectTable.Add(TmpTable[k]);
					}
				}
			}
			for (j=0;j<m_SectorAoiList[i]->m_ListOut.GetSize();j++)
			{
				InterNb=m_SectorAoiList[i]->m_ListOut[j]->GetIntersect(Pnt1,Alt1,Pnt2,Alt2,TmpTable);
				for (int k=0;k<InterNb;k++)
				{
					if (m_SectorAoiList[i]->IsInsideIn(TmpTable[k].Pos,TmpTable[k].Alt))
					{
						TmpTable[k].Out=true;
						TmpTable[k].Entry=!TmpTable[k].Entry;
						bool Ok=true;
						if (TmpTable[k].Entry)
						{
							double dist=sqrt((Pnt2.x-Pnt1.x)*(Pnt2.x-Pnt1.x)+(Pnt2.y-Pnt1.y)*(Pnt2.y-Pnt1.y));
							CPoint extpos(TmpTable[k].Pos.x+(16.0*(Pnt2.x-Pnt1.x))/dist,TmpTable[k].Pos.y+(16.0*(Pnt2.y-Pnt1.y))/dist);
							if (!m_SectorAoiList[i]->IsInside(extpos,TmpTable[k].Alt))
								Ok=false;
						}
						TmpTable[k].SectorNb=m_SectorAoiList[i]->m_SectorNb;
						if (Ok)
							TmpIntersectTable.Add(TmpTable[k]);
					}
				}
			}
		}
	}
	float Min=1.0;
	int curmin=-1;
	for (i=0;i<TmpIntersectTable.GetSize();i++)
	{
		for (j=0;j<IntersectTable.GetSize();j++)
		{
			if ((TmpIntersectTable[i].t<IntersectTable[j].t) || ((TmpIntersectTable[i].t==IntersectTable[j].t) && (TmpIntersectTable[i].Entry<IntersectTable[j].Entry)))
			{
				IntersectTable.InsertAt(j,TmpIntersectTable[i]);
				break;
			}
		}
		if (j==IntersectTable.GetSize())
			IntersectTable.Add(TmpIntersectTable[i]);
	}
	return true;
}


bool CArea::GetLocalFirIntersect(CPoint Pnt1,int Alt1,CPoint Pnt2,int Alt2,CArray<IntersectDef,IntersectDef>& IntersectTable)
{
	IntersectTable.RemoveAll();
	IntersectDef TmpTable[2],TmpInter;
	int InterNb,j;
	CArray<IntersectDef,IntersectDef> TmpIntersectTable;
	if (m_pLocalFir2)
	{
		for (j = 0; j < m_pLocalFir2->m_ListIn.GetSize(); j++)
		{
			InterNb = m_pLocalFir2->m_ListIn[j]->GetIntersect(Pnt1, Alt1, Pnt2, Alt2, TmpTable);
			for (int k = 0; k < InterNb; k++)
			{
				if (!m_pLocalFir2->IsInsideOut(TmpTable[k].Pos, TmpTable[k].Alt))
				{
					TmpTable[k].Out = false;
					TmpTable[k].SectorNb = 0;
					bool Ok = true;
					if (TmpTable[k].Entry)
					{
						double dist = sqrt((Pnt2.x - Pnt1.x)*(Pnt2.x - Pnt1.x) + (Pnt2.y - Pnt1.y)*(Pnt2.y - Pnt1.y));
						CPoint extpos(TmpTable[k].Pos.x + (16.0*(Pnt2.x - Pnt1.x)) / dist, TmpTable[k].Pos.y + (16.0*(Pnt2.y - Pnt1.y)) / dist);
						if (!m_pLocalFir2->IsInside(extpos, TmpTable[k].Alt))
							Ok = false;
					}
					if (Ok)
						TmpIntersectTable.Add(TmpTable[k]);
				}
			}
		}
		for (j = 0; j < m_pLocalFir2->m_ListOut.GetSize(); j++)
		{
			InterNb = m_pLocalFir2->m_ListOut[j]->GetIntersect(Pnt1, Alt1, Pnt2, Alt2, TmpTable);
			for (int k = 0; k < InterNb; k++)
			{
				if (m_pLocalFir2->IsInsideIn(TmpTable[k].Pos, TmpTable[k].Alt))
				{
					TmpTable[k].Out = true;
					TmpTable[k].Entry = !TmpTable[k].Entry;
					bool Ok = true;
					if (TmpTable[k].Entry)
					{
						double dist = sqrt((Pnt2.x - Pnt1.x)*(Pnt2.x - Pnt1.x) + (Pnt2.y - Pnt1.y)*(Pnt2.y - Pnt1.y));
						CPoint extpos(TmpTable[k].Pos.x + (16.0*(Pnt2.x - Pnt1.x)) / dist, TmpTable[k].Pos.y + (16.0*(Pnt2.y - Pnt1.y)) / dist);
						if (!m_pLocalFir2->IsInside(extpos, TmpTable[k].Alt))
							Ok = false;
					}
					TmpTable[k].SectorNb = 0;
					if (Ok)
						TmpIntersectTable.Add(TmpTable[k]);
				}
			}
		}
	}
	float Min=1.0;
	int curmin=-1;
	for (int i=0;i<TmpIntersectTable.GetSize();i++)
	{
		for (int j=0;j<IntersectTable.GetSize();j++)
		{
			if ((TmpIntersectTable[i].t<IntersectTable[j].t) || ((TmpIntersectTable[i].t==IntersectTable[j].t) && (TmpIntersectTable[i].Entry<IntersectTable[j].Entry)))
			{
				IntersectTable.InsertAt(j,TmpIntersectTable[i]);
				break;
			}
		}
		if (j==IntersectTable.GetSize())
			IntersectTable.Add(TmpIntersectTable[i]);
	}
	return true;
}

bool CArea::GetFirIntersect(CPoint Pnt1, int Alt1, CPoint Pnt2, int Alt2, CArray<IntersectDef, IntersectDef>& IntersectTable,flightkinds kind)
{
	IntersectTable.RemoveAll();
	IntersectDef TmpTable[2], TmpInter;
	int InterNb, i, j;
	CArray<IntersectDef, IntersectDef> TmpIntersectTable;
	for (int i = 0; i<m_FirSectorArea.GetSize(); i++)
	{
			for (j = 0; j<m_FirSectorArea[i]->m_ListIn.GetSize(); j++)
			{
				InterNb = m_FirSectorArea[i]->m_ListIn[j]->GetIntersect(Pnt1, Alt1, Pnt2, Alt2, TmpTable);
				for (int k = 0; k<InterNb; k++)
				{
					if (!m_FirSectorArea[i]->IsInsideOut(TmpTable[k].Pos, TmpTable[k].Alt))
					{
						TmpTable[k].Out = false;
						TmpTable[k].SectorNb = m_FirSectorArea[i]->m_SectorNb;
						bool Ok = true;
						if (TmpTable[k].Entry)
						{
							double dist = sqrt((Pnt2.x - Pnt1.x)*(Pnt2.x - Pnt1.x) + (Pnt2.y - Pnt1.y)*(Pnt2.y - Pnt1.y));
							CPoint extpos(TmpTable[k].Pos.x + (16.0*(Pnt2.x - Pnt1.x)) / dist, TmpTable[k].Pos.y + (16.0*(Pnt2.y - Pnt1.y)) / dist);
							if (!m_FirSectorArea[i]->IsInside(extpos, TmpTable[k].Alt))
								Ok = false;
						}
						if (Ok)
							TmpIntersectTable.Add(TmpTable[k]);
					}
				}
			}
			for (j = 0; j<m_FirSectorArea[i]->m_ListOut.GetSize(); j++)
			{
				InterNb = m_FirSectorArea[i]->m_ListOut[j]->GetIntersect(Pnt1, Alt1, Pnt2, Alt2, TmpTable);
				for (int k = 0; k<InterNb; k++)
				{
					if (m_FirSectorArea[i]->IsInsideIn(TmpTable[k].Pos, TmpTable[k].Alt))
					{
						TmpTable[k].Out = true;
						TmpTable[k].Entry = !TmpTable[k].Entry;
						bool Ok = true;
						if (TmpTable[k].Entry)
						{
							double dist = sqrt((Pnt2.x - Pnt1.x)*(Pnt2.x - Pnt1.x) + (Pnt2.y - Pnt1.y)*(Pnt2.y - Pnt1.y));
							CPoint extpos(TmpTable[k].Pos.x + (16.0*(Pnt2.x - Pnt1.x)) / dist, TmpTable[k].Pos.y + (16.0*(Pnt2.y - Pnt1.y)) / dist);
							if (!m_FirSectorArea[i]->IsInside(extpos, TmpTable[k].Alt))
								Ok = false;
						}
						TmpTable[k].SectorNb = m_FirSectorArea[i]->m_SectorNb;
						if (Ok)
							TmpIntersectTable.Add(TmpTable[k]);
					}
				}
			}
	}
	float Min = 1.0;
	int curmin = -1;
	for (i = 0; i<TmpIntersectTable.GetSize(); i++)
	{
		for (j = 0; j<IntersectTable.GetSize(); j++)
		{
			if ((TmpIntersectTable[i].t<IntersectTable[j].t) || ((TmpIntersectTable[i].t == IntersectTable[j].t) && (TmpIntersectTable[i].Entry<IntersectTable[j].Entry)))
			{
				IntersectTable.InsertAt(j, TmpIntersectTable[i]);
				break;
			}
		}
		if (j == IntersectTable.GetSize())
			IntersectTable.Add(TmpIntersectTable[i]);
	}
	return true;
}

bool CArea::IsInXptConflictArea(CPoint Target,int WindowNb)
{
	if (WindowNb >=0 && WindowNb<m_XptConflictArea.GetSize() && m_XptConflictArea[WindowNb])
	{
		if (m_XptConflictArea[WindowNb]->IsInside(Target,0))
			return true;
	}
	return false;
}

CPoint CArea::GetXptConflictAreaIntersection(CPoint FirstPoint,CPoint Vector,int WindowNb)
{
	if (WindowNb >= 0 && WindowNb<m_XptConflictArea.GetSize() && m_XptConflictArea[WindowNb])
	{
		return m_XptConflictArea[WindowNb]->m_ListIn[0]->FindFirstIntersection(FirstPoint,Vector);
	}
	else
		return CPoint(0,0);
}

CString CArea::GetFirSectorName(CString SectorName,CPoint pos,int Level)
{
	for (int i = 0; i < m_FirSectorArea.GetSize(); i++)
	{
		if (m_FirSectorArea[i]->m_SectorName.Find("/"+SectorName+"/")!=-1)
		{
			if (m_FirSectorArea[i]->IsInside(pos,Level))
				return m_FirSectorArea[i]->m_Name;

		}
	}
	return "";
}

bool CArea::IsInsideFirSector(CPoint pnt, int Level)
{
	for (int i = 0; i < m_FirSectorArea.GetSize(); i++)
	{
		if (m_FirSectorArea[i]->IsInside(pnt, Level))
			return true;
	}
	return false;
}