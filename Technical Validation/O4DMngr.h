#if !defined(AFX_O4DMNGR_H__7F87B03F_6061_40D2_A36E_6414C123C41F__INCLUDED_)
#define AFX_O4DMNGR_H__7F87B03F_6061_40D2_A36E_6414C123C41F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// O4DMngr.h : header file
//
#include "FlightPlan.h"
/////////////////////////////////////////////////////////////////////////////
// CO4DMngr window

class CTrack;
class CFlightPlan;



namespace CO4DMngr 
{
// Construction
	void Init(CString FileName,CString PathName);
	void ManageTrack(CTrack* pTrack,bool Terminated=false);
	void ManageFpl(CFlightPlan* pFpl,bool Terminated=false);
	bool ManageTrajectory(CFlightPlan* pFpl);
	void ManageFplTrajectory(CFlightPlan* pFpl);
	void ManageTmpFplTrajectory(CFlightPlan* pFpl);
//	void ManageSectorTrajectory(CFlightPlan* pFpl,int SectorIndex,int Delay);
	typedef enum {Steepest,LowAngle} TrajTypes;
	void ManageO4DTrajectory(CFlightPlan* pFpl,int ContextId,int Level,CArray<PointDef,PointDef>& Route,int CurFix,int Delay,TrajTypes type,int CDRate=0);
	void ManageCMTrajectory(CFlightPlan* pFpl,int SectorIndex=-1,int Delay=10,int LevelToGo=0,int Rate=0,bool lowang=false);
	void IasAdvisory(CFlightPlan* pFpl);

	bool IsEnabled();
	bool IsLtcaEnabled();
	void Heading2Bearing(CPoint Pos,int level,double heading, double speed,  double& bearing, double& grdspeed);
	void Bearing2Heading(CPoint Pos,int level,double bearing, double grdspeed, double& heading, double& speed);
	bool GetWindState();
	// Generated message map functions
	void ComputeLatLong (CPoint Pos, const int & Altitude, double & Lat, double & Long);
	CString GetVersion();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_O4DMNGR_H__7F87B03F_6061_40D2_A36E_6414C123C41F__INCLUDED_)
