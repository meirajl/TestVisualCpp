#pragma once
#include "afxwin.h"
#include "resource.h"

class CRemark :
	public CDialog
{
public:
	CRemark(CWnd* pParent = NULL);   // standard constructor
	~CRemark();

	// Dialog Data
	//{{AFX_DATA(CRemark)
	enum { IDD = IDD_REMARK };
	CString	m_Remark;
	CString m_Arcid;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRemark)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRemark)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

