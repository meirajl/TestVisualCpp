#pragma once
#include "afxwin.h"
#include "resource.h"

class CTrafficError :
	public CDialog
{
public:
	CTrafficError(CWnd* pParent = NULL);   // standard constructor
	~CTrafficError();

	// Dialog Data
	//{{AFX_DATA(CTrafficError)
	enum { IDD = IDD_TRAFFIC_ERROR };
	CString	m_TrafficError;
	CString m_Arcid;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrafficError)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTrafficError)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

