#pragma once
#include "afxwin.h"
#include "resource.h"

class CExCDT : public CDialog
{
public:
	CExCDT(CWnd* pParent = NULL);   // standard constructor
	~CExCDT();
	CString	m_ExCDTLog;
	// Dialog Data
	//{{AFX_DATA(CExCDT)
	enum { IDD = IDD_EXCDT };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExCDT)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExCDT)
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

