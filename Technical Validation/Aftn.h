#pragma once
#include "afx.h"

enum AftnMsgTypes
{
	NONE_TITLE,
	FPL
};

enum AftnFields
{
	NONE_FIELD,
	ADEP,
	ALTNZ,
	ALTRNT1,
	ALTRNT2,
	ARCADDR,
	ARCID, 
	ARCTYP,
	ADES, 
	CEQPT,
	COM,
	DAT,
	DEPZ,
	DESTZ,
	EETPT,
	EOBT,
	ESTDATA,
	FIELD18,
	FLTRUL,
	FLTTYP,
	MSGREF,
	NAV,
	NBARC,
	OPR,
	REFDATA,
	PER,
	RALT,
	RFL,
	RIF,
	RMK,
	ROUTE,
	SEL,
	SEQPT,
	SPEED,
	SSRCODE,
	STS,
	TTLEET,
	TYPZ,
	WKTRC,
};

enum AftnSubFields
{
	NONE_SUBFIELD,
	RECVR,
	SENDER,
	SEQNUM,
};

class CAftn :
	public CObject
{
public:
	CAftn();
	~CAftn();

	int FindHyphen(CString Msg, int curpos = 0);
	bool DecodeAftn(int CurPos);
	void DecodeAftnSubField(int fld, CString CurMsgStr);
	bool DecodeAftnMessage(CString AftnMsg);
	AftnMsgTypes GetType();
	bool IsAftn();
private:
	AftnMsgTypes m_MsgType;
	typedef struct
	{
		bool Valid;
		AftnSubFields SubFld;
		CString Str;
		int Level;
		int IndexInLevel;
	} SubFieldDef;

	typedef struct
	{
		bool Presence;
		bool Valid;
		CString  Str;
		CArray<SubFieldDef, SubFieldDef> SubFieldTable;
	} FieldDef;
	FieldDef m_FieldTable[WKTRC + 1];
	bool m_IsAftn;
	CString m_Msg;
	bool m_IsValidAuto;

	AftnFields GetNextField(CString Msg, int& curpos, int &posfld);
	AftnSubFields GetNextSubField(CString Msg, int& curpos, int &posfld);
	CString GetNextWord(CString Msg, int &firstpos, int &lastpos);
	void DecodeSubField(AftnFields CurFld, CString FldStr, int Level, int IndexInLevel, bool IsField = false);
	void AddSubField(AftnFields Fld, AftnSubFields SubFld, CString FldStr, int Level, int IndexInLevel);
};

