#include "stdafx.h"
#include "OurConstants.h"
#include "StringUtil.h"
#include "Offline.h"
#include "OfflineWindow.h"
#include "StringUtil.h"
#include "FontMngr.h"

COffline::COffline()
{
	m_DefPathName = "def_path.ofl";
	m_TechValName = "Technical_Validation.ofl";

	m_DepartureList = "";
}


COffline::~COffline()
{
}

void COffline::LoadDepartureList(CString IniName)
{
	int len;
	CString AllKey;

	len = GetPrivateProfileString("CENTRE", "DEPARTURELIST", NULL, AllKey.GetBuffer(2048), 2048, IniName);
	AllKey.ReleaseBuffer(len);
	if (len)
	{
		m_DepartureList= AllKey;
	}
}

void COffline::LoadArrivalList(CString IniName)
{
	int len;
	CString AllKey;

	len = GetPrivateProfileString("CENTRE", "ARRIVALLIST", NULL, AllKey.GetBuffer(2048), 2048, IniName);
	AllKey.ReleaseBuffer(len);
	if (len)
	{
		m_ArrivalList = AllKey;
	}
}

void COffline::LoadLogicalSectorsDefinition(CString IniName)
{
	CString SKey, AllKey;
	int len, fnd;

	SectorLdefinition_T  elt;
	for (int sector = 1;; sector++)
	{
		SKey.Format("SECTOR%d", sector);
		len = GetPrivateProfileString("LOGIC_SECTORS_DEFINITION", SKey, "", AllKey.GetBuffer(3000), 3000, IniName);
		AllKey.ReleaseBuffer();
		if (!len)
			break;

		AllKey += "|";
		fnd = AllKey.Find("|");
		elt.SL = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		elt.FLmin = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		elt.FLmax = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		elt.PTIDs = "," + AllKey.Left(fnd) + ",";

		m_SectorLogicList.Add(elt);
	}
}

void COffline::LoadSectors(CString IniName)
{
	CString SKey, AllKey;
	int len, fnd;

	m_SectorTable.SetSize(MaxSectorNb + 1);
	for (int sect = 0; sect <= MaxSectorNb; sect++)
	{
		SectorDefs CurSect;
		CString sectype, UACtype, tmp, FullKey;
		bool CflEnabled = false;
		bool Stripless = false;
		bool EflInCfl = false;
		bool FreqWarning = false;
		int ClamEnabled = 0;
		int LateCflClamBuffer = 0;
		bool CpdlcEligible = true;
		int console1 = 0;
		int console2 = 0;
		WORD Min = 0, Max = 999;
		SKey.Format("SECTOR%d", sect);
		len = GetPrivateProfileString("SECTORS", SKey, "   ", AllKey.GetBuffer(500), 500, IniName);
		AllKey.ReleaseBuffer(len);
		AllKey += "|";
		FullKey = AllKey;
		if (len)
		{
			fnd = AllKey.Find('|');
			if (fnd != -1)
			{
				CString keys = AllKey.Mid(fnd + 1);
				AllKey = AllKey.Left(fnd);
				fnd = keys.Find('|');
				if (fnd != -1)
				{
					sectype = keys.Left(fnd);
					keys = keys.Mid(fnd + 1);
					fnd = keys.Find('|');
					if (fnd != -1)
					{
						keys = keys.Mid(fnd + 1);
						fnd = keys.Find('|');
						if (fnd != -1)
						{
							UACtype = keys.Left(fnd);
							keys = keys.Mid(fnd + 1);
							fnd = keys.Find('|');
							if (fnd != -1)
							{
								CflEnabled = keys.Left(fnd) == "Y";
								keys = keys.Mid(fnd + 1);
								fnd = keys.Find('|');
								if (fnd != -1)
								{
									Stripless = keys.Left(fnd) == "Y";
									keys = keys.Mid(fnd + 1);
									fnd = keys.Find('|');
									if (fnd != -1)
									{
										EflInCfl = keys.Left(fnd) == "Y";
										keys = keys.Mid(fnd + 1);
										fnd = keys.Find('|');
										if (fnd != -1)
										{
											FreqWarning = keys.Left(fnd) == "Y";
											keys = keys.Mid(fnd + 1);
											fnd = keys.Find('|');
											if (fnd != -1)
											{
												ClamEnabled = atoi(keys.Left(fnd));
												keys = keys.Mid(fnd + 1);
												fnd = keys.Find('|');
												if (fnd != -1)
												{
													LateCflClamBuffer = atoi(keys.Left(fnd));
													keys = keys.Mid(fnd + 1);
													fnd = keys.Find('|');
													if (fnd != -1)
													{
														CpdlcEligible = keys.Left(fnd) == "Y";
														keys = keys.Mid(fnd + 1);
														fnd = keys.Find('|');
														if (fnd != -1)
														{
															Min = atoi(keys.Left(fnd));
															keys = keys.Mid(fnd + 1);
															fnd = keys.Find('|');
															if (fnd != -1)
															{
																Max = atoi(keys.Left(fnd));
																keys = keys.Mid(fnd + 1);
																fnd = keys.Find('|');
																if (fnd != -1)
																{
																	keys = keys.Mid(fnd + 1);
																	fnd = keys.Find('|');
																	if (fnd != -1)
																	{
																		keys = keys.Mid(fnd + 1);
																		fnd = keys.Find('|');
																		if (fnd != -1)
																		{
																			console1 = atoi(keys.Left(fnd));
																			keys = keys.Mid(fnd + 1);
																			fnd = keys.Find('|');
																			if (fnd != -1)
																			{
																				console2 = atoi(keys.Left(fnd));
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					else
						UACtype = keys;

				}
			}
			for (int j = 0; j<10; j++)
			{
				CurSect.Adj[j] = 0;
				CurSect.AdjType[j] = ' ';
				CurSect.AdjXptList[j].Empty();
			}
			NStringUtil::GetArgumentNb(FullKey, 14, '|', tmp);
			if (tmp.GetLength())
			{
				CString adj = tmp;
				adj += ",";
				for (int j = 0; j<10; j++)
				{
					fnd = adj.Find(",");
					if (fnd != -1)
					{
						int fnd2 = adj.Left(fnd).Find("/");
						if (fnd2 == -1)
						{
							CurSect.Adj[j] = atoi(adj.Left(fnd));
							CurSect.AdjType[j] = adj.Mid(fnd - 1, 1)[0];
						}
						else
						{
							CString tmp2 = adj.Left(fnd);
							CurSect.Adj[j] = atoi(tmp2.Left(fnd2));
							CurSect.AdjType[j] = adj.Mid(fnd2 - 1, 1)[0];
							if (CurSect.AdjType[j] == 's')
								CurSect.AdjXptList[j] = "/" + tmp2.Mid(fnd2 + 1) + "/";

						}
						adj = adj.Mid(fnd + 1);
					}
					else
						break;
				}
			}
			NStringUtil::GetArgumentNb(FullKey, 15, '|', tmp);
			if (tmp.GetLength())
				CurSect.Site = tmp;
			for (int c = 16; c < NStringUtil::GetNbrOfArgument(FullKey, '|'); c++)
			{
				NStringUtil::GetArgumentNb(FullKey, c, '|', tmp);
			//	m_ConsoleSector[atoi(tmp)] = sect;
			}
		}
		else
			AllKey = "   ";
		CurSect.Peri = AllKey;
		CurSect.UAC = UACtype;
		CurSect.Type = sectype;
		CurSect.CflEnabled = CflEnabled;
		CurSect.Stripless = Stripless;
		CurSect.EflInCfl = EflInCfl;
		CurSect.FreqWarning = FreqWarning;
		CurSect.SectorOverload = 0;
		CurSect.SectorCoord = false;
		CurSect.ClamEnabled = ClamEnabled;
		CurSect.LateCflClamBuffer = LateCflClamBuffer;
		CurSect.CpdlcOn = true;
		CurSect.CpdlcEligible = CpdlcEligible;
		CurSect.Min = Min;
		CurSect.Max = Max;
		CurSect.console1 = console1;
		CurSect.console2 = console2;
		m_SectorTable[sect] = CurSect;
	//	if (sect && CurSect.Stripless)
	//		m_LtcaSectorTable.Add(sect);
	}
}

void COffline::LoadEntryPoint(CString IniName)
{
	CString IndexKey, SKey, AllKey;
	int alllen, len;
	EntryPoint Entry;

	m_DefaultEptDistance = 10;
	alllen = GetPrivateProfileString("ENTRYPOINTS", NULL, "None", IndexKey.GetBuffer(1000), 1000, IniName);
	IndexKey.ReleaseBuffer(alllen);
	SKey.Empty();
	int pos = 0;
	CString Key, tmpstr;
	while (pos<alllen)
	{
		while (IndexKey[pos] != 0)
			SKey += IndexKey[pos++];
		len = GetPrivateProfileString("ENTRYPOINTS", SKey, "", AllKey.GetBuffer(256), 256, IniName);
		AllKey.ReleaseBuffer();
		if (SKey.CompareNoCase("Default") == 0)
		{
			m_DefaultEptDistance = atoi(AllKey);
		}
		else
		{
			NStringUtil::GetArgumentNb(AllKey, 1, '|', tmpstr);
			Entry.Name = tmpstr;
			NStringUtil::GetArgumentNb(AllKey, 2, '|', tmpstr);
			Entry.Distance = atoi(tmpstr);
			NStringUtil::GetArgumentNb(AllKey, 3, '|', tmpstr);
			Entry.DistanceEto = atoi(tmpstr);
			NStringUtil::GetArgumentNb(AllKey, 4, '|', tmpstr);
			Entry.FirTable.FirNb = atoi(tmpstr);
			NStringUtil::GetArgumentNb(AllKey, 5, '|', tmpstr);
			Entry.FirTable.LowLevel = atoi(tmpstr);
			NStringUtil::GetArgumentNb(AllKey, 6, '|', tmpstr);
			Entry.FirTable.HighLevel = atoi(tmpstr);
			NStringUtil::GetArgumentNb(AllKey, 7, '|', tmpstr);
			Entry.Site = tmpstr;
			m_EntryPoints.Add(Entry);
		}
		pos++;
		SKey.Empty();
	}
}

void COffline::LoadExitPoint(CString IniName)
{
	CString IndexKey, AllKey, SKey, log;
	int alllen, len, i, fnd;
	ExitCop Exit;

	alllen = GetPrivateProfileString("EXITCOPS", NULL, "None", IndexKey.GetBuffer(1000), 1000, IniName);
	IndexKey.ReleaseBuffer(alllen);
	//int pos = 0, 
	int MaxWnd = 0;
	SKey.Empty();
	//while (pos<alllen)
	for (i = 1; i; i++)
	{
		//while (IndexKey[pos] != 0)
		//	SKey += IndexKey[pos++];
		SKey.Format("%d", i);
		len = GetPrivateProfileString("EXITCOPS", SKey, "", AllKey.GetBuffer(256), 256, IniName);
		if (len == 0)
		{
			log.Format("EXITPOINTS number=%d is not defined\r\n", i);
			m_OfflineErrorLog += log;
			break;
		}
		AllKey.ReleaseBuffer();
		AllKey += "|";
		fnd = AllKey.Find("|");
		Exit.Name = AllKey.Left(fnd);
		Exit.Name.TrimRight();
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		//		Exit.Name=Exit.Name.Left(3);
		fnd = AllKey.Find("|");
		Exit.WindowNum = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		CString tmp = AllKey.Left(fnd);
		if (tmp != "Y")
			Exit.WindowNum = -abs(Exit.WindowNum);
		if (Exit.WindowNum>MaxWnd)
			MaxWnd = Exit.WindowNum;
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);

		fnd = AllKey.Find("|");
		tmp = AllKey.Left(fnd);
		if (tmp == "B")
			Exit.Parity = Both;
		if (tmp == "I")
			Exit.Parity = Odd;
		if (tmp == "P")
			Exit.Parity = Even;
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);


		fnd = AllKey.Find("|");
		Exit.FlMin = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);

		fnd = AllKey.Find("|");
		Exit.FlMax = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);


		fnd = AllKey.Find("|");
		Exit.Delta = atoi(AllKey.Left(fnd));
		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);

		fnd = AllKey.Find("|");
		Exit.StopTime = atoi(AllKey.Left(fnd));

		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		if (fnd != -1)
			Exit.DistAman = atoi(AllKey.Left(fnd)) * 32;
		else
			Exit.DistAman = 0;

		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		Exit.FirTable.FirNb = atoi(AllKey.Left(fnd));

		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		Exit.FirTable.LowLevel = atoi(AllKey.Left(fnd));

		AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
		fnd = AllKey.Find("|");
		Exit.FirTable.HighLevel = atoi(AllKey.Left(fnd));



		for (int index = 0; index<35; index++)
		{
			Exit.ForbiddenLV[index] = FALSE;
		}

		m_ExitCops.Add(Exit);
		//pos++;
		SKey.Empty();

	}
}

void COffline::LoadSid(CString IniName)
{
	CString Key, HSTParam, tmp;
	int i, len;

	PtidBisItem CurItem;
	CurItem.Name.Empty();
	CurItem.Pnt2.Empty();
	for (i = 1; i<200; i++)
	{
		Key.Format("SID%d", i);
		len = GetPrivateProfileString("SID", Key, "", HSTParam.GetBuffer(2048), 2048, IniName);
		HSTParam.ReleaseBuffer(len);
		CString RwyStr;
		if (len)
		{
			if (NStringUtil::GetNbrOfArgument(HSTParam, '|') >= 3)
			{
				NStringUtil::GetArgumentNb(HSTParam, 1, '|', CurItem.Airport);
				NStringUtil::GetArgumentNb(HSTParam, 2, '|', CurItem.Name);
				NStringUtil::GetArgumentNb(HSTParam, 3, '|', CurItem.Pnt1);
				NStringUtil::GetArgumentNb(HSTParam, 4, '|', RwyStr);

				CurItem.RwyNb = atoi(RwyStr);
				CurItem.PointsToAdd.Empty();
				for (int j = 5; j <= NStringUtil::GetNbrOfArgument(HSTParam, '|'); j++)
				{
					NStringUtil::GetArgumentNb(HSTParam, j, '|', tmp);
					CurItem.PointsToAdd += tmp + " ";
				}
				CurItem.PointsToAdd.TrimRight();
				m_SidTable.Add(CurItem);


			}
		}
	}
}

void COffline::LoadStar(CString IniName)
{
	CString Key, HSTParam, tmp;
	int i, len;
	PtidBisItem CurItem;

	for (i = 1; i<200; i++)
	{
		Key.Format("%d", i);
		len = GetPrivateProfileString("STAR", Key, "", HSTParam.GetBuffer(2048), 2048, IniName);
		HSTParam.ReleaseBuffer(len);
		CString RwyStr;
		if (len)
		{
			if (NStringUtil::GetNbrOfArgument(HSTParam, '|') >= 7)
			{
				NStringUtil::GetArgumentNb(HSTParam, 1, '|', CurItem.Airport);
				NStringUtil::GetArgumentNb(HSTParam, 2, '|', CurItem.Name);
				NStringUtil::GetArgumentNb(HSTParam, 3, '|', CurItem.Pnt1);
				NStringUtil::GetArgumentNb(HSTParam, 4, '|', RwyStr);
				CurItem.RwyNb = atoi(RwyStr);
				NStringUtil::GetArgumentNb(HSTParam, 5, '|', CurItem.Pnt2);
				CurItem.PointsToAdd.Empty();
				NStringUtil::GetArgumentNb(HSTParam, 6, '|', tmp);
				if (tmp == "Y")
					m_DefaultStarTable.Add(CurItem);
				for (int j = 7; j <= NStringUtil::GetNbrOfArgument(HSTParam, '|'); j++)
				{
					NStringUtil::GetArgumentNb(HSTParam, j, '|', tmp);
					CurItem.PointsToAdd += tmp + " ";
				}
				CurItem.PointsToAdd.TrimRight();
				m_StarTable.Add(CurItem);


			}
		}
	}
}

void COffline::LoadAirport(CString IniName)
{
	CString SKey, AllKey;
	int len;

	for (int i = 0;; i++)
	{
		SKey.Format("AIRPORT%d", i + 1);
		len = GetPrivateProfileString("AIRPORT_LEVEL", SKey, NULL, AllKey.GetBuffer(1000), 1000, IniName);
		AllKey.ReleaseBuffer();
		if (len)
		{
			AirportLevel airportLevel;
			m_ListAirportLevel.Add(airportLevel);
			sscanf(AllKey, "%[^|]|%[^|]|%[^|]|", m_ListAirportLevel[i].Airport.GetBuffer(1000), m_ListAirportLevel[i].Efl.GetBuffer(1000), m_ListAirportLevel[i].Xfl.GetBuffer(1000));
			m_ListAirportLevel[i].Airport.ReleaseBuffer();
			m_ListAirportLevel[i].Efl.ReleaseBuffer();
			m_ListAirportLevel[i].Xfl.ReleaseBuffer();
		}
		else
			break;
	}
}

bool COffline::ReadOffline()
{
	bool IsOfflineOk = true;
	int len;
	CString oflstr, allkey, sDefPathName, sTechValName, sNavPointsName;
	GetCurrentDirectory(256, oflstr.GetBuffer(256));
	oflstr.ReleaseBuffer();
	CString DirName = oflstr;
	
	sDefPathName = DirName + "\\" + m_DefPathName;
	sTechValName = DirName + "\\" + m_TechValName;

	len = GetPrivateProfileString("GENERAL", "AFTN_PATH", NULL, allkey.GetBuffer(256), 256, sDefPathName);
	allkey.ReleaseBuffer();
	if (allkey.GetLength())
		m_AftnPathDir = allkey;

	len = GetPrivateProfileString("GENERAL", "NAV_POINTS_PATH", NULL, allkey.GetBuffer(256), 256, sDefPathName);
	allkey.ReleaseBuffer();
	if (allkey.GetLength())
		m_NavPointsPathDir = allkey;

	len = GetPrivateProfileString("ITALY", "CROSSBORDER_POINTS", NULL, allkey.GetBuffer(256), 256, sTechValName);
	allkey.ReleaseBuffer();
	if (allkey.GetLength())
		m_ItalyCrossBorderPointList = allkey;

	len = GetPrivateProfileString("IPT_USELESS", "IPT", NULL, allkey.GetBuffer(256), 256, sTechValName);
	allkey.ReleaseBuffer();
	if (allkey.GetLength())
		m_IptUseless = allkey;

	return IsOfflineOk;
}

CString COffline::CheckOffline()
{
	CString offlineError="";
	bool found = false;
	OfflineCheck CurItem;
	int i, j;
	//m_OfflineError = "";

	for (i = 1; i < m_SectorTable.GetSize(); i++)
	{
		if (m_SectorTable[i].Site)
		{
			CurItem.Site = m_SectorTable[i].Site;
			CurItem.SectorNumber = i;
			m_SectorList.Add(CurItem);
		}
	}
	for (i = 0; i < m_ExitCops.GetSize(); i++)
	{
		found = false;
		for (int j = 1; j < m_SectorLogicList.GetSize(); j++)
		{
			if (m_SectorLogicList[j].PTIDs.Find(m_ExitCops[i].Name) != -1)
			{
				found = true;
			}
		}
		if ((!found) && (m_ExitCops[i].WindowNum!=-1))
		{
			offlineError.Format("Exit point=%s in section EXITPOINTS of def_airspace.ofl file does not exist in section LOGIC_SECTORS_DEFINITION of def_airspace.ofl file\r\n", m_ExitCops[i].Name);
			m_OfflineErrorLog += offlineError;
		}
	}

	int count = 0;
	for (i = 0; i < m_ExitCops.GetSize(); i++)
	{
		found = false;
		count = 0;
		for (j = 0; j < m_WindowTable.GetSize(); j++)
		{
			if (m_WindowTable[j]->Type == ExitWdw)
			{
				if (m_WindowTable[j]->PointList.Find(m_ExitCops[i].Name)!=-1)
				{
					found = true;
					count++;
				}
			}
		}
		if ((!found) && (m_ExitCops[i].WindowNum != -1))
		{
			offlineError.Format("Exit point=%s in section EXITPOINTS of def_airspace.ofl file does not exist in def_windows.ofl file\r\n", m_ExitCops[i].Name);
			m_OfflineErrorLog += offlineError;
		}
		if (count>1)
		{
			offlineError.Format("Exit point=%s is defined twice in def_windows.ofl file\r\n", m_ExitCops[i].Name);
			m_OfflineErrorLog += offlineError;
		}
	}

	return m_OfflineErrorLog;
	
}

void COffline::LoadDefWindows(CString IniName)
{
	int len, fnd, lev;
	CString AllKey, Key, tmp, tmp2;

	GetCurrentDirectory(256, IniName.GetBuffer(256));
	IniName.ReleaseBuffer();
	IniName = IniName + "\\Def_Windows.ofl";

	LPCSTR WdwTypeName[] = { "Exit","Bin","Holding","Arrival","Departure","Entry","Sector","Adj","Regio","DataLink" };
	LPCSTR ColTypeName[] = { "ArcidData","EtxData","EflNoXflData","XFLData","PflData","RflData","ArctypData","AdepData","AdesData","To2Data","AdesNotLocalData","EflData","SsrData","AdepNotLocalData","Ptid1Data","AtdData","XptData","AckData","HstData","TextComData","ArcidInfoData","EptData","EtnData","XflNoEflData","Ptid2Data","To2NoSecData","CflData","StarData","EatData","PltData","AflData","EtaData","EteData","ESflData","XSflData","XSflAppData","XflNoXSflData","XSflNoESflData","PrpData","ReqData","EtdAtdData","TtlData","EatTo2Data","HectData","EatTickData","SectorData","IFLData","PIflData","DlData","DlSymbData","DlToData" };
	LPCSTR FldTypeName[] = { "Text","Time","RelTime","Button","Hybrid","Symbol" };
	LPCSTR JustTypeName[] = { "Left","Center","Right" };
	len = GetPrivateProfileSectionNames(AllKey.GetBuffer(4096), 4096, IniName);
	AllKey.ReleaseBuffer(len);
	int t, pos = 0;
	Key.Empty();
	CStringArray WindowNames;
	while (pos<len)
	{
		while (AllKey[pos] != 0)
			Key += AllKey[pos++];
		if (Key.Left(7).CompareNoCase("WINDOW_") == 0)
			WindowNames.Add(Key);
		Key.Empty();
		pos++;
	}
	for (int wdw = 0; wdw<WindowNames.GetSize(); wdw++)
	{
		WindowsDef* pCurdef;
		len = GetPrivateProfileString(WindowNames[wdw], "DEF", "", AllKey.GetBuffer(2048), 2048, IniName);
		AllKey.ReleaseBuffer();
		if (NStringUtil::GetNbrOfArgument(AllKey, '|') >= 11)
		{
			pCurdef = new WindowsDef;
			pCurdef->WindowName = WindowNames[wdw].Right(WindowNames[wdw].GetLength() - 7);
			pCurdef->TitleColNb = 0;
			pCurdef->IsVisible = true;
			pCurdef->ManualOpen = false;
			pCurdef->NotifiedNb = -1;
			NStringUtil::GetArgumentNb(AllKey, 1, '|', tmp);
			pCurdef->Type = -1;
			for (t = 0; t <= 9; t++)
			{
				if (tmp.CompareNoCase(WdwTypeName[t]) == 0)
				{
					pCurdef->Type = t;
					break;
				}
			}
			if (pCurdef->Type == -1)
			{
				tmp2.Format("Window %s has unknowm type %s", pCurdef->WindowName, tmp);
				//pApp->LogMsg(tmp2);
			}
			NStringUtil::GetArgumentNb(AllKey, 2, '|', tmp);
			if (pCurdef->Type == RegioWdw)
			{
				tmp += ",";
				for (int j = 1; j <= NStringUtil::GetNbrOfArgument(tmp, ','); j++)
				{
					NStringUtil::GetArgumentNb(tmp, j, ',', tmp2);
					fnd = tmp2.Find("ADEP=");
					if (fnd != -1)
						pCurdef->PointList = tmp2.Mid(fnd + 5) + "/";
					fnd = tmp2.Find("ADJ=");
					if (fnd != -1)
						pCurdef->SectorList = "/" + tmp2.Mid(fnd + 4) + "/";
					fnd = tmp2.Find("FLTR=");
					if (fnd != -1)
						pCurdef->FltrList = tmp2.Mid(fnd + 5) + "/";
				}
			}
			else
				pCurdef->PointList = tmp;
			NStringUtil::GetArgumentNb(AllKey, 3, '|', tmp);
			pCurdef->WindowPos.x = atoi(tmp);
			NStringUtil::GetArgumentNb(AllKey, 4, '|', tmp);
			pCurdef->WindowPos.y = atoi(tmp);
			pCurdef->Pos = pCurdef->WindowPos;
			NStringUtil::GetArgumentNb(AllKey, 5, '|', tmp);
			pCurdef->FontNb = CFontMngr::GetFontNb(tmp);
			NStringUtil::GetArgumentNb(AllKey, 6, '|', tmp);
			pCurdef->SmallFontNb = CFontMngr::GetFontNb(tmp);
			NStringUtil::GetArgumentNb(AllKey, 7, '|', tmp);
			pCurdef->SelectFontNb = CFontMngr::GetFontNb(tmp);
			NStringUtil::GetArgumentNb(AllKey, 8, '|', tmp);
			pCurdef->SmallSelectFontNb = CFontMngr::GetFontNb(tmp);
			if ((pCurdef->FontNb == -1) || (pCurdef->SmallFontNb == -1) || (pCurdef->SelectFontNb == -1) || (pCurdef->SmallSelectFontNb == -1))
			{
				tmp2.Format("Window %s has unknown Font", pCurdef->WindowName);
				//pApp->LogMsg(tmp2);
			}
			int Offset = 0;
			if (pCurdef->Type == ArrivalWdw)
			{
				NStringUtil::GetArgumentNb(AllKey, 9, '|', tmp);
				pCurdef->ThirdFontNb = CFontMngr::GetFontNb(tmp);
				NStringUtil::GetArgumentNb(AllKey, 10, '|', tmp);
				pCurdef->ThirdSelectFontNb = CFontMngr::GetFontNb(tmp);
				Offset = 2;
				if ((pCurdef->ThirdFontNb == -1) || (pCurdef->ThirdSelectFontNb == -1))
				{
					tmp2.Format("Window %s has unknown Font", pCurdef->WindowName);
					//pApp->LogMsg(tmp2);
				}
			}
			NStringUtil::GetArgumentNb(AllKey, 9 + Offset, '|', tmp);
			pCurdef->Eliminate = tmp == "Y";
			NStringUtil::GetArgumentNb(AllKey, 10 + Offset, '|', pCurdef->BoxName);
			tmp = pCurdef->BoxName;
			tmp.TrimRight();
			//if (tmp.IsEmpty())
			//	pApp->LogMsg("Window " + pCurdef->WindowName + " has empty box name");

			NStringUtil::GetArgumentNb(AllKey, 11 + Offset, '|', pCurdef->SectorDisplay);
			for (lev = 0; lev<FLevNb; lev++)
				pCurdef->ForbidenLevel[lev] = false;
			for (int c = 1;; c++)
			{
				FplFieldDef CurCol;
				Key.Format("FLD%d", c);
				len = GetPrivateProfileString(WindowNames[wdw], Key, "", AllKey.GetBuffer(2048), 2048, IniName);
				AllKey.ReleaseBuffer();
				if (!len)
					break;
				if (NStringUtil::GetNbrOfArgument(AllKey, '|') == 7)
				{
					NStringUtil::GetArgumentNb(AllKey, 1, '|', CurCol.ColumnName);
					if (CurCol.ColumnName.CompareNoCase("TITLE") == 0)
					{
						pCurdef->TitleColNb = c - 1;
						CurCol.ColumnName = pCurdef->WindowName;
					}
					NStringUtil::GetArgumentNb(AllKey, 2, '|', tmp);
					CurCol.ColumnSize = atoi(tmp);
					NStringUtil::GetArgumentNb(AllKey, 3, '|', tmp);
					CurCol.DataNb = -1;
					for (t = 0; t <= DlToData; t++)
					{
						if (tmp.CompareNoCase(ColTypeName[t]) == 0)
						{
							CurCol.DataNb = t;
							break;
						}
					}
					if (CurCol.DataNb == -1)
					{
						tmp2.Format("Window %s : Column %s has unknown Data Type %s", pCurdef->WindowName, CurCol.ColumnName, tmp);
						//pApp->LogMsg(tmp2);
					}
					NStringUtil::GetArgumentNb(AllKey, 4, '|', tmp);
					CurCol.Type = (DisplayType)-1;
					for (t = 0; t<6; t++)
					{
						if (tmp.CompareNoCase(FldTypeName[t]) == 0)
						{
							CurCol.Type = (DisplayType)t;
							break;
						}
					}
					if (CurCol.Type == -1)
					{
						tmp2.Format("Window %s : Column %s has unknown Display Type %s", pCurdef->WindowName, CurCol.ColumnName, tmp);
						//pApp->LogMsg(tmp2);
					}
					NStringUtil::GetArgumentNb(AllKey, 5, '|', tmp);
					for (t = 0; t<3; t++)
					{
						if (tmp.CompareNoCase(JustTypeName[t]) == 0)
						{
							CurCol.Style = t;
							break;
						}
					}
					NStringUtil::GetArgumentNb(AllKey, 6, '|', tmp);
					CurCol.Select.Empty();
					if (tmp != "N")
						CurCol.Select = tmp;
					NStringUtil::GetArgumentNb(AllKey, 7, '|', tmp);
					CurCol.Sort = tmp == "Y";
					CurCol.Hold = tmp == "H";
					CurCol.FontNb = pCurdef->FontNb;
					CurCol.SmallFontNb = pCurdef->SmallFontNb;
					CurCol.SelectFont = pCurdef->SelectFontNb;
					CurCol.SmallSelectFont = pCurdef->SmallSelectFontNb;
					CurCol.ThirdFontNb = pCurdef->ThirdFontNb;
					CurCol.ThirdSelectFont = pCurdef->ThirdSelectFontNb;
					if (CurCol.DataNb == DlSymbData)
					{
						CurCol.FontNb = CFontMngr::GetFontNb("ENTRYUACCPDLC_FONT");
						CurCol.SmallFontNb = CurCol.FontNb;
						CurCol.SelectFont = CurCol.FontNb;
						CurCol.SmallSelectFont = CurCol.FontNb;
					}
					CurCol.Min = 0;
					CurCol.Max = 999;
					if (CurCol.DataNb == DlData)
					{
						CurCol.Min = 9;
						CurCol.Max = 29;
					}
					pCurdef->Columns.Add(CurCol);
				}
				}
			m_WindowTable.Add(pCurdef);
			}
		//else
		//	pApp->LogMsg("Window " + WindowNames[wdw] + " has wrong number of arguments");
	}
}