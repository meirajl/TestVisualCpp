#pragma once
#include "afxwin.h"
#include "resource.h"
class CIcaoRoute :
	public CDialog
{
public:
	CIcaoRoute(CWnd* pParent = NULL);   // standard constructor
	~CIcaoRoute();

	// Dialog Data
	//{{AFX_DATA(CIcaoRoute)
	enum { IDD = IDD_ICAOROUTE };
	CString	m_IcaoRoute;
	CString m_Arcid;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIcaoRoute)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIcaoRoute)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

