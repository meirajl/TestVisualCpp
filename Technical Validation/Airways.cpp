#include "StdAfx.h"
#include "Airways.h"
#include "StringUtil.h"

CArray<CAirways*,CAirways*> CAirways::m_AirwaysTable;
CStringArray CAirways::m_AirportTable;
CWordArray CAirways::m_RwyTable;

CAirways::CAirways()
{
}

CAirways::~CAirways()
{
}

void CAirways::LoadFile(CString Filename)
{
	CStdioFile inpointsfile;
	CString line,route,pnt,tmp;	
	CAirways* pCurAirway=NULL;
	int RouteArg=5;
	if( inpointsfile.Open(Filename,CFile::modeRead))
	{
		
		while (inpointsfile.ReadString(line)) 
		{
			line.TrimLeft();
			line.TrimRight();
			if (line.Left(2)=="AW")
			{
				if (pCurAirway)
					m_AirwaysTable.Add(pCurAirway);

				pCurAirway=new CAirways();
				pCurAirway->m_Way='B';
				pCurAirway->m_Type=Awy;
				pCurAirway->m_Name=line.Mid(3);
			}
			if (line.Left(2)=="SD")
			{
					pCurAirway->m_Way=line.Right(1)[0];
			}
			if (line.Left(2)=="RO")
			{
				if (pCurAirway)
				{
					pCurAirway->m_ListOfPoint.Add(line.Mid(3));
				}
			}

		}
	}
}

/*CAirways::LoadFile(CString Filename)
{
	CStdioFile inpointsfile;
	CString line,route,pnt,tmp;	
	CAirways* pCurAirway;
	AwyTypes CurTyp;
	int RouteArg=5;
	if( inpointsfile.Open(Filename,CFile::modeRead))
	{
		
		while (inpointsfile.ReadString(line)) 
		{
			line.TrimLeft();
			line.TrimRight();
			if (line.Find("/CODED_ROUTE/") != -1)
			{
				CurTyp=Awy;
				RouteArg=5;
			}
			if (line.Find("/SID/") != -1)
			{
				CurTyp=Sid;
				RouteArg=6;
			}
			if (line.Find("/STAR/") != -1)
			{
				CurTyp=Star;
				RouteArg=6;
			}

			if ((line.Find("--") != 0) && (NStringUtil::GetNbrOfArgument(line,'|')>=RouteArg))
			{
				pCurAirway=new CAirways();
				NStringUtil::GetArgumentNb(line,1,'|',pCurAirway->m_Name);
				pCurAirway->m_Name.TrimRight();
				pCurAirway->m_Name.TrimLeft();
				pCurAirway->m_Type=CurTyp;
				if (CurTyp!=Awy)
				{
					NStringUtil::GetArgumentNb(line,2,'|',pCurAirway->m_Airport);
					pCurAirway->m_Airport.TrimRight();
					pCurAirway->m_Airport.TrimLeft();
					NStringUtil::GetArgumentNb(line,4,'|',tmp);
					pCurAirway->m_RwyNb=atoi(tmp);
					for (int a=0;a<m_AirportTable.GetSize();a++)
					{
						if (m_AirportTable[a].CompareNoCase(pCurAirway->m_Airport))
							break;
					}
					if (a==m_AirportTable.GetSize())
					{
						m_AirportTable.Add(pCurAirway->m_Airport);
						m_RwyTable.Add(pCurAirway->m_RwyNb);
					}
				}
				NStringUtil::GetArgumentNb(line,RouteArg,'|',route);
				for (int i=1;i<=NStringUtil::GetNbrOfArgument(route,' ');i++)
				{
					NStringUtil::GetArgumentNb(route,i,' ',pnt);
					pnt.TrimLeft();
					pnt.TrimRight();
					if (pnt.GetLength())
						pCurAirway->m_ListOfPoint.Add(pnt);
				}
				m_AirwaysTable.Add(pCurAirway);
			}
			
		}
	}
	
	
}*/

CString CAirways::GetName()
{
	return m_Name;
}

bool CAirways::GetListOfPoints(CString Point1,CString Point2,CStringArray& ListOfPoint)
{
	CString pnt;
	int FirstPoint=-1,LastPoint=-1,i;
	switch (m_Type)
	{
	case Awy:
		for (i=0;i<m_ListOfPoint.GetSize();i++)
		{
			pnt= m_ListOfPoint[i];
			if (pnt[0]=='?')
				pnt=pnt.Right(pnt.GetLength()-1);
			if (pnt==Point1)
				FirstPoint=i;
			if (pnt==Point2)
				LastPoint=i;
			if ((FirstPoint!=-1) && (LastPoint!=-1))
				break;
		}
		if ((FirstPoint!=-1) && (LastPoint!=-1))
		{
			if (FirstPoint<LastPoint)
			{
				for (int i=FirstPoint;i<=LastPoint;i++)
					ListOfPoint.Add(m_ListOfPoint[i]);
			}
			else
			{
				for (int i=FirstPoint;i>=LastPoint;i--)
					ListOfPoint.Add(m_ListOfPoint[i]);
			}
			return true;
		}
		if ((FirstPoint!=-1) && (LastPoint==-1) && (m_Way=='D'))
		{
			for (int i=FirstPoint;i<m_ListOfPoint.GetSize();i++)
					ListOfPoint.Add(m_ListOfPoint[i]);
			return true;
		}
		if ((FirstPoint!=-1) && (LastPoint==-1) && (m_Way=='U'))
		{
			for (int i=m_ListOfPoint.GetUpperBound();i>=FirstPoint;i--)
					ListOfPoint.Add(m_ListOfPoint[i]);
			return true;
		}
		break;
	case Sid:
		for (i=m_ListOfPoint.GetSize()-1;i>=0;i--)
			ListOfPoint.InsertAt(0,m_ListOfPoint[i]);
		break;
	case Star:
		for (i=0;i<m_ListOfPoint.GetSize();i++)
			ListOfPoint.Add(m_ListOfPoint[i]);
		break;
	}
	return false;
}


CAirways* CAirways::GetAirway(CString AirwayName)
{
	for (int i=0;i<m_AirwaysTable.GetSize();i++)
	{
		if ((m_AirwaysTable[i]->m_Type==Awy) && (m_AirwaysTable[i]->m_Name.CompareNoCase(AirwayName)==0))
		{
			return m_AirwaysTable[i];
		}
	}
	return NULL;
}


CAirways* CAirways::GetSid(CString Adep)
{
	for (int a=0;a<m_AirportTable.GetSize();a++)
	{
		if (m_AirportTable[a].CompareNoCase(Adep)==0)
		{
			for (int i=0;i<m_AirwaysTable.GetSize();i++)
			{
				if ((m_AirwaysTable[i]->m_Type==Sid) && (m_AirwaysTable[i]->m_Airport.CompareNoCase(Adep)==0) && (m_AirwaysTable[i]->m_RwyNb==m_RwyTable[a]))
				{
					return m_AirwaysTable[i];
				}
			}
		}
	}
	return NULL;
}


CAirways* CAirways::GetStar(CString Ades)
{
	for (int a=0;a<m_AirportTable.GetSize();a++)
	{
		if (m_AirportTable[a].CompareNoCase(Ades)==0)
		{
			for (int i=0;i<m_AirwaysTable.GetSize();i++)
			{
				if ((m_AirwaysTable[i]->m_Type==Star) && (m_AirwaysTable[i]->m_Airport.CompareNoCase(Ades)==0) && (m_AirwaysTable[i]->m_RwyNb==m_RwyTable[a]))
				{
					return m_AirwaysTable[i];
				}
			}
		}
	}
	return NULL;
}
