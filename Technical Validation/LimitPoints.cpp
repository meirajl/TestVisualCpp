// LimitPoints.cpp: implementation of the CLimitPoints class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mmsystem.h"
//#include "visu.h"
//#include "track.h"
#include "FplDoc.h"
#include "Geo.h"
#include "LimitPoints.h"
#include "StringUtil.h"
#include "IniFile.h"
#include "Math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CArray<LPoint*,LPoint*> CLimitPoints::m_LimitPointsTable;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLimitPoints::CLimitPoints()
{
	
}

CLimitPoints::~CLimitPoints()
{
	
}

bool CLimitPoints::GetLimitPoint(int rank, LPoint& pnt)
{
	if (rank<m_LimitPointsTable.GetSize())
	{
		pnt.IsDefined= m_LimitPointsTable[rank]->IsDefined;
		pnt.x= m_LimitPointsTable[rank]->x;
		pnt.y = m_LimitPointsTable[rank]->y;
		return true;
	}
	pnt.IsDefined = false;
	pnt.x = -32767;
	pnt.y = -32767;
	return false;

}

void CLimitPoints::LoadLimitPointsXY(CString IniName)
{
	int len, pnt;
	CString SKey, DirName;
	CString SectionKey, AllKey;
	int fnd = -1;

	GetCurrentDirectory(256, DirName.GetBuffer(256));
	DirName.ReleaseBuffer();
	IniName = DirName + "\\def_points.ofl";

	/*TmpPnt->IsDefined = true;
	TmpPnt->x = 0;
	TmpPnt->y = 0;
	m_LimitPointsTable.Add(TmpPnt);*/

	len = GetPrivateProfileString("LIMITPOINTS", "SCALE", "", SectionKey.GetBuffer(10), 10, IniName);
	SectionKey.ReleaseBuffer(len);
	float Scale = 32.0;
	if (len)
		Scale = atof(SectionKey);
	len = GetPrivateProfileSection("LIMITPOINTS", SectionKey.GetBuffer(1000000), 1000000, IniName);
	SectionKey.ReleaseBuffer(len);
	SKey.Empty();
	//m_LimitPointsTable.SetSize(10000);
	m_LimitPointsTable.SetSize(21000);
	for (pnt = 0; pnt < len; pnt++)
	{
		SKey += SectionKey[pnt];
		if (SectionKey[pnt] == 0)
		{
			int len2 = SKey.Find("=");
			AllKey = SKey.Mid(len2 + 1);
			SKey = SKey.Left(len2);
			if (len2 && atoi(SKey))
			{
				LPoint* curpoint = new LPoint;
				AllKey += "|";
				fnd = AllKey.Find("|");
				curpoint->IsDefined = true;
				curpoint->x = (int)(atof(AllKey.Left(fnd))*Scale);
				AllKey = AllKey.Right(AllKey.GetLength() - fnd - 1);
				fnd = AllKey.Find("|");
				curpoint->y = (int)(atof(AllKey.Left(fnd))*Scale);
				m_LimitPointsTable.SetAtGrow(atoi(SKey), curpoint);
			}
			SKey.Empty();
		}

	}
}

void CLimitPoints::LoadLimitPointsLatLong(CString IniName)
{
	CString AllKey, DirName, pointsfile;
	CString line, nline;
	CString rank, dum, infir,deg, min, sec;
	CIniFile file(IniName);
	
	CArray<CString,CString> LineTable;
	file.GetIniProfileSectionData("LIMIT_POINTS",LineTable,false);
	
	for (int i=0;i<LineTable.GetSize();i++)
	{
		line = LineTable[i];
		line.TrimLeft();
		line.TrimRight();
		if ((line.GetLength()!=0) && (line.Find("--") != 0) && (NStringUtil::GetNbrOfArgument(line,'|')>=2))
		{
			//name field decoding
			NStringUtil::GetArgumentNb(line,1,'|',rank);
			rank.TrimLeft();
			rank.TrimRight();
			
			//Coordinates field decoding
			NStringUtil::GetArgumentNb(line,2,'|',dum);
			dum.TrimLeft();
			dum.TrimRight();
			// Latitude decoding
			//degree
			deg = dum.Mid(0,2);
			dum.Delete(0,2);
			//Minutes
			min = dum.Mid(0,2);
			dum.Delete(0,2);
			//Seconds
			sec = dum.Mid(0,2);
			dum.Delete(0,2);
			
			Latitude lat;
			lat.deg = atoi(deg);
			lat.min = atoi(min);
			lat.sec = atoi(sec);
			lat.NORTH = (dum[0]=='N');
			
			dum.Delete(0,1);
			
			// Longitude decoding
			//degree
			dum.TrimLeft();
			dum.TrimRight();
			deg = dum.Mid(0,3);
			dum.Delete(0,3);
			
			//Minutes
			dum.TrimLeft();
			dum.TrimRight();
			min = dum.Mid(0,2);
			dum.Delete(0,2);
			
			//Seconds
			dum.TrimLeft();
			dum.TrimRight();
			sec = dum.Mid(0,2);
			dum.Delete(0,2);
			
			Longitude lng;
			lng.deg = atoi(deg);
			lng.min = atoi(min);
			lng.sec = atoi (sec);
			
			lng.EAST = (dum[0] == 'E');
			
			double dlat,dlong;
			GeographicToStereographic(lat,lng, dlat, dlong);
			int x,y;
			
			x = CGeo::RoundTo32(dlong);
			y = CGeo::RoundTo32(dlat);
			
			// check if the new limit point is not already defined
			if ((atoi(rank) >=  m_LimitPointsTable.GetSize()) || !(m_LimitPointsTable[atoi(rank)]->IsDefined))
			{
				LPoint* curpoint= new LPoint;
				curpoint->IsDefined=true;
				curpoint->x=x;
				curpoint->y=y;
				m_LimitPointsTable.SetAtGrow(atoi(rank),curpoint);
			}
			else
			{
				// Point definition is duplicated
			}
			
		}
	}
	
	
}

bool  CLimitPoints::GetPoint(int rank, LPoint& point)
{
	bool result=false;
	if (rank<m_LimitPointsTable.GetSize() && m_LimitPointsTable[rank]->IsDefined)
	{
		result= true;
		point.x= m_LimitPointsTable[rank]->x;
		point.y= m_LimitPointsTable[rank]->y;
	}
	return result;
}



void CLimitPoints::GeographicToStereographic(Latitude lat, Longitude lng, double& Lat, double& Lng)
{

	double dtlong = CGeo::DecimalToRadian(CGeo::GeographicToDecimal(lng)) - CGeo::DecimalToRadian(CGeo::m_CenterLongDec);
	double latradian = CGeo::DecimalToRadian(CGeo::GeographicToDecimal(lat));
	double conflatradian = CGeo::GeoToConf(latradian);
	double simucenterlatradian = CGeo::DecimalToRadian(CGeo::m_CenterLatDec);
	double simucenterconflatradian = CGeo::GeoToConf(simucenterlatradian);
	double divisor = 1.0 + sin(conflatradian)* sin(simucenterconflatradian)
		+ cos(conflatradian)*cos(simucenterconflatradian)*cos(dtlong);
	double diameter = 2.0 * CGeo::NM_LOCAL_EARTH_RADIUS;

	Lng = ((diameter*sin(dtlong)*cos(conflatradian))/divisor);
	Lat = diameter*(sin(conflatradian)*cos(simucenterconflatradian) - cos(conflatradian)*sin(simucenterconflatradian)*cos(dtlong))/divisor;


}
