// Geo.cpp: implementation of the CGeo class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "OurConstants.h"
#include "Geo.h"
#include "String.h"
#include "StringUtil.h"
#include "IniFile.h"
#include "math.h"
#include "Area.h"
//#include "visu.h"
//#include "fplmngr.h"
#include "AdjacentFir.h"

   const double CGeo::PI = 3.14159265359;
   const double CGeo::TWO_PI = PI*2.0;
   const double CGeo::RADIAN_PER_DEGREE = PI/180.0;
   const double CGeo::FEET_PER_METER = 3.2808399;
   const double CGeo::MINUTES_PER_DEGREE = 60.0;
   const double CGeo::SECONDS_PER_MINUTE = 60.0;
   const double CGeo::SECONDS_PER_DEGREE = CGeo::SECONDS_PER_MINUTE * CGeo::MINUTES_PER_DEGREE;
   const double CGeo::HALF_A_MINUTE = 0.5/60.0;
   const double CGeo::HALF_A_SECOND = 0.5/3600.0;
   const double CGeo::KM_PER_NAUTICAL_MILE = 1.852;

//  Equatorial earth radius
   const double CGeo::KM_EARTH_RADIUS = 6378.160 ;
   const double CGeo::NM_EARTH_RADIUS = CGeo::KM_EARTH_RADIUS / CGeo::KM_PER_NAUTICAL_MILE ;

 //   Local earth radius computed at center init.
   double CGeo::KM_LOCAL_EARTH_RADIUS = CGeo::KM_EARTH_RADIUS;
   double CGeo::NM_LOCAL_EARTH_RADIUS = CGeo::NM_EARTH_RADIUS ;

   //const double CGeo::EXCENTRICITY = 0.081820584;
   //const double CGeo::EXCENTRICITY_2 = 0.006694608;
   
   // New excentricity values on 21th march 2005
   const double CGeo::EXCENTRICITY = 0.081819191;
   const double CGeo::EXCENTRICITY_2 = 0.006694380;

   CPoint CGeo::m_Center;
   double CGeo::m_CenterLatDec;
   double CGeo::m_CenterLongDec;
   CMap <CString,LPCSTR,CGeo*,CGeo*> CGeo::m_MapPoint;
   CMap <CString, LPCSTR, CGeo*, CGeo*> CGeo::m_MapNavPoint;
   CMap <CString,LPCSTR,CGeo*,CGeo*> CGeo::m_MapAirport;

   CArray<CGeo::EntryDefs,CGeo::EntryDefs> CGeo::m_EntryPoints;
   CStringArray CGeo::m_IntermediatePoints;
   CStringArray CGeo::m_HoldingPoints;
   CStringArray CGeo::m_HoldingSmallPoints;
   CArray<bool,bool> CGeo::m_ActiveHolding;
   CArray<CGeo::ExitDefs,CGeo::ExitDefs> CGeo::m_ExitCops;
   CArray<CGeo::StartPnt, CGeo::StartPnt> CGeo::m_StartPoint;


// Flight Level System ...
const int 	PairFL[]= {20,40,60,80,100,120,140,160,180,200,220,240,260,
									 280,310,350,390,410,430,450};
const int 	ImpairFL[]={10,30,50,70,90,110,130,150,170,190,210,230,
									  250,270,290,330,370,410,430,450};
const int 	AllFL[]={10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,
								   160,170,180,190,200,210,220,230,240,250,260,270,
								   280,290,310,330,350,370,390,410,430,450};
// Remarque: Impairs = 410 && 450
//		  	 Pair	 = 430	(Mais � Gen�ve ACC ces trois niveaux sont pairs et impairs)


// Flight Level System for equipped RVSM aircraft ...
const int 	PairFL_rvsm[]= {20,40,60,80,100,120,140,160,180,200,220,240,260,
									 280,300,320,340,360,380,400,430,450};
const int 	ImpairFL_rvsm[]= {10,30,50,70,90,110,130,150,170,190,210,230,
									  250,270,290,310,330,350,370,390,410,430,450};
const int 	AllFL_rvsm[]= {10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,
								   160,170,180,190,200,210,220,230,240,250,260,270,
								   280,290,310,320,330,340,350,360,370,380,390,400,410,430,450};




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGeo::CGeo()
{
}

CGeo::~CGeo()
{
}

void CGeo::Center(double& lat, double& lng)
{
	lat = CGeo::m_CenterLatDec;
	lng = CGeo::m_CenterLongDec;
	
}

double  CGeo::GeographicToDecimal(Latitude  lat)
{
double res;
 res = (double)lat.deg +  (double)(lat.min)/MINUTES_PER_DEGREE + lat.sec/SECONDS_PER_DEGREE;
	
 if (lat.NORTH) 
      return res;
    else
      return -(res);
}
double  CGeo::GeographicToDecimal (Longitude lng)
{
	double res;
	res = (double)lng.deg + (double)(lng.min)/ MINUTES_PER_DEGREE + (double)(lng.sec)/ SECONDS_PER_DEGREE;
    if (lng.EAST)
      return res;
    else
      return -res;
}

void CGeo::DecimalToGeographic (double val, Latitude& lat)
{int deg,min,sec;

	deg = (int)(val);
	sec = (int)( (val - (double)deg)*3600.0);
	min = (sec/60);
	sec = (sec%60);
	lat.deg =deg;
	lat.min = min;
	lat.sec =sec;
	lat.NORTH = (val >=0.0);
}

void CGeo::DecimalToGeographic (double val, Longitude& lng)
{int deg,min,sec;
	deg = (int)(fabs(val));
	sec = (int)( (fabs(val) - (double)deg)*3600.0);
	min = (sec/60);
	sec = (sec%60);
	lng.deg =deg;
	lng.min = min;
	lng.sec =sec;
	lng.EAST = (val >=0);
}
double CGeo::DecimalToRadian (double lat)
{
    return lat * RADIAN_PER_DEGREE;

}

double CGeo::LatRadianToDecimal (double rad)
{
	double res;
	res =  fmod((rad / RADIAN_PER_DEGREE), (2.0 * 90.0));

	if (res >= 90.0)
     return ( res + (2.0 *-90.0));
    else
      return res;
}
double CGeo::LongRadianToDecimal (double rad)
{double res;
    res =  fmod((rad / RADIAN_PER_DEGREE),(2.0 * 180.0));
    if (res >= 180.0)
      return (res + 2.0 *(-180.0));
    else
      return res;
}


double CGeo::GeographicToRadian(Latitude lat)
{
	double res = CGeo::GeographicToDecimal(lat);
	return CGeo::DecimalToRadian(res);
}

double CGeo::GeographicToRadian(Longitude lng)
{
	double res = CGeo::GeographicToDecimal(lng);
	return CGeo::DecimalToRadian(res);
}

void CGeo::RadianToGeographic(double val,Latitude& lat)
{
	CGeo::DecimalToGeographic(CGeo::LatRadianToDecimal(val), lat);
}


void CGeo::RadianToGeographic(double val,Longitude& lng)
{
	CGeo::DecimalToGeographic(CGeo::LongRadianToDecimal(val), lng);
}

void CGeo::InitCenter(Latitude lat, Longitude lng)
{
	CGeo::m_CenterLatDec = CGeo::GeographicToDecimal(lat);
	CGeo::m_CenterLongDec = CGeo::GeographicToDecimal(lng);
	CGeo::ComputeLocalEarthRadius(CGeo::m_CenterLatDec,CGeo::m_CenterLongDec);
}

bool CGeo::StringToGeographic(CString LatLongStr,Latitude &Lat,Longitude& Long)
{
	bool res1,res2;
	if (LatLongStr.GetLength()==15)//format ddmmssNdddmmssE
	{
		res1=StringToLat(LatLongStr.Left(7),Lat);
		res2=StringToLong(LatLongStr.Right(8),Long);
		return res1 & res2;
	}
	if (LatLongStr.GetLength()==11)//format ddmmNdddmmE
	{
		res1=StringToLat(LatLongStr.Left(5),Lat);
		res2=StringToLong(LatLongStr.Right(6),Long);
		return res1 & res2;
	}
	if (LatLongStr.GetLength()==7)//format ddNdddE
	{
		res1=StringToLat(LatLongStr.Left(3),Lat);
		res2=StringToLong(LatLongStr.Right(4),Long);
		return res1 & res2;
	}
	return false;
}

bool CGeo::StringToLat(CString LatStr,Latitude &Lat)
{
	if (LatStr.GetLength()==7) //format ddmmssN
	{
		if ((NStringUtil::IsNumeric(LatStr.Left(6)) && ((LatStr[6]=='N') || (LatStr[6]=='S'))))
		{
			Lat.deg=atoi(LatStr.Left(2));
			Lat.min=atoi(LatStr.Mid(2,2));
			Lat.sec=atoi(LatStr.Mid(4,2));
			Lat.NORTH=LatStr[6]=='N';
			return true;
		}
	}
	if (LatStr.GetLength()==5) //format ddmmN
	{
		if ((NStringUtil::IsNumeric(LatStr.Left(4)) && ((LatStr[4]=='N') || (LatStr[4]=='S'))))
		{
			Lat.deg=atoi(LatStr.Left(2));
			Lat.min=atoi(LatStr.Mid(2,2));
			Lat.sec=0;
			Lat.NORTH=LatStr[4]=='N';
			return true;
		}
	}
	if (LatStr.GetLength()==3) //format ddN
	{
		if ((NStringUtil::IsNumeric(LatStr.Left(2)) && ((LatStr[2]=='N') || (LatStr[2]=='S'))))
		{
			Lat.deg=atoi(LatStr.Left(2));
			Lat.min=0;
			Lat.sec=0;
			Lat.NORTH=LatStr[2]=='N';
			return true;
		}
	}
	return false;
}

bool CGeo::StringToLong(CString LongStr,Longitude &Long)
{
	if (LongStr.GetLength()==8) //format dddmmssE
	{
		if ((NStringUtil::IsNumeric(LongStr.Left(7)) && ((LongStr[7]=='E') || (LongStr[7]=='W'))))
		{
			Long.deg=atoi(LongStr.Left(3));
			Long.min=atoi(LongStr.Mid(3,2));
			Long.sec=atoi(LongStr.Mid(5,2));
			Long.EAST=LongStr[7]=='E';
			return true;
		}
	}

	if (LongStr.GetLength()==6) //format dddmmE
	{
		if ((NStringUtil::IsNumeric(LongStr.Left(5)) && ((LongStr[5]=='E') || (LongStr[5]=='W'))))
		{
			Long.deg=atoi(LongStr.Left(3));
			Long.min=atoi(LongStr.Mid(3,2));
			Long.sec=0;
			Long.EAST=LongStr[5]=='E';
			return true;
		}
	}
	if (LongStr.GetLength()==4) //format dddE
	{
		if ((NStringUtil::IsNumeric(LongStr.Left(3)) && ((LongStr[3]=='E') || (LongStr[3]=='W'))))
		{
			Long.deg=atoi(LongStr.Left(3));
			Long.min=0;
			Long.sec=0;
			Long.EAST=LongStr[3]=='E';
			return true;
		}
	}
	return false;
}


BOOL CGeo::InitCenter(CString center)
{
	// Expected format : ddmmssN or ddmmssS + dddmmssE or dddmmssW
	int p1, p2;
	center.TrimLeft();
	center.TrimRight();
	p1 = center.Find("N",0);
	if(!p1) 
	{
		p1 = center.Find("S");
		if(!p1) return FALSE;
	}
	p2 = center.Find("E");
	if(!p2)
	{
		p2 = center.Find("W");
		if(!p2) return FALSE;
	}
	
	CString dum, deg, min, sec;
	
	dum = center;
	// Latitude decoding
	//degree
	deg = dum.Mid(0,2);
	dum.Delete(0,2);
	//Minutes
	min = dum.Mid(0,2);
	dum.Delete(0,2);
	//Seconds
	sec = dum.Mid(0,2);
	dum.Delete(0,2);
				
	Latitude lat;
	lat.deg = atoi(deg);
	lat.min = atoi(min);
	lat.sec = atoi(sec);
	lat.NORTH = (dum[0]=='N');
	
	dum.Delete(0,1);
	
	// Longitude decoding
	//degree
//	dum=dum.stripWhiteSpace();
	dum.TrimLeft();
	dum.TrimRight();
	deg = dum.Mid(0,3);
	dum.Delete(0,3);
	
	//Minutes
//	dum=dum.stripWhiteSpace();
	dum.TrimLeft();
	dum.TrimRight();
	min = dum.Mid(0,2);
	dum.Delete(0,2);
	
	//Seconds
//	dum=dum.stripWhiteSpace();
	dum.TrimLeft();
	dum.TrimRight();
	sec = dum.Mid(0,2);
	dum.Delete(0,2);
	
	Longitude lng;
	lng.deg = atoi(deg);
	lng.min = atoi(min);
	lng.sec = atoi(sec);
	lng.EAST = (dum[0] == 'E');

	CGeo::InitCenter(lat,lng);
	return TRUE;
}


double CGeo::GeoToConf(double lat)
{
	double latsin = sin(lat);
	double factor1 = (1.0 - (EXCENTRICITY *latsin)) / (1.0 + (EXCENTRICITY * latsin));
	double factor = pow(factor1,(EXCENTRICITY / 2.0));
	return ((2.0 * atan( tan(PI / 4.0 + lat / 2.0) * factor)) - PI/2.0);
}

double CGeo::ConfToGeo(double lat)
{
	double latsin = sin(lat);

	double factor1 = (1.0 + (CGeo::EXCENTRICITY *latsin))/(1.0 - (CGeo::EXCENTRICITY *latsin));
	double factor =   pow(factor1, (CGeo::EXCENTRICITY/2.0));

    return ((2.0 * atan(tan((PI /4.0)+(lat/2.0)) *factor )) - (PI /2.0));
}





void CGeo::ComputeLocalEarthRadius(double lat, double )
{
	double latradian = CGeo::DecimalToRadian(lat);
	double conflatradian = CGeo::GeoToConf(latradian);
	double latsin = sin(latradian);
	double den = cos(conflatradian) *  sqrt(1.0 - (EXCENTRICITY * EXCENTRICITY * latsin * latsin));
	double factor = cos(latradian)/den;
	CGeo::KM_LOCAL_EARTH_RADIUS = CGeo::KM_EARTH_RADIUS * factor;
	CGeo::NM_LOCAL_EARTH_RADIUS = CGeo::NM_EARTH_RADIUS * factor;
}


long CGeo::RoundTo32(double val)
{
	long dum;
	dum = (long)(val*32.0);
	if(fabs(val*32.0 -dum) > 0.5)
	{
	if(dum>=0) dum++;
	else dum--;
	}
	return dum;
}

double CGeo::RoundFr32(long val)
{
	double dum;
	dum = (val/32.0);

	return dum;
}

					 
void CGeo::GeographicToStereographic(Latitude lat, Longitude lng, double& Lat, double& Lng)
{

	double dtlong = CGeo::DecimalToRadian(CGeo::GeographicToDecimal(lng)) - CGeo::DecimalToRadian(CGeo::m_CenterLongDec);
	double latradian = CGeo::DecimalToRadian(CGeo::GeographicToDecimal(lat));
	double conflatradian = CGeo::GeoToConf(latradian);
	double simucenterlatradian = CGeo::DecimalToRadian(CGeo::m_CenterLatDec);
	double simucenterconflatradian = CGeo::GeoToConf(simucenterlatradian);
	double divisor = 1.0 + sin(conflatradian)* sin(simucenterconflatradian)
		+ cos(conflatradian)*cos(simucenterconflatradian)*cos(dtlong);
	double diameter = 2.0 * CGeo::NM_LOCAL_EARTH_RADIUS;

	Lng = ((diameter*sin(dtlong)*cos(conflatradian))/divisor);
	Lat = diameter*(sin(conflatradian)*cos(simucenterconflatradian) - cos(conflatradian)*sin(simucenterconflatradian)*cos(dtlong))/divisor;


}


void CGeo::StereographicToGeographic(double lat, double lng,Latitude& Lat, Longitude& Long)
{
	double x =  CGeo::RoundFr32((long)lng);

 

	double y =  CGeo::RoundFr32((long)lat);


	double lc1 = CGeo::DecimalToRadian(CGeo::m_CenterLatDec);
	double latcenter = CGeo::GeoToConf(lc1);


	double longcenter = CGeo::DecimalToRadian(CGeo::m_CenterLongDec);



	double K = 1.0 - ((x*x +y*y) / (4.0*CGeo::NM_LOCAL_EARTH_RADIUS*CGeo::NM_LOCAL_EARTH_RADIUS));


	double rlong = x/ ((CGeo::NM_LOCAL_EARTH_RADIUS*cos(latcenter)*K) - (y*sin(latcenter)));



	double dlong = longcenter + atan(rlong);

	double rlat =  (cos(dlong - longcenter)
					* (y+(K*CGeo::NM_LOCAL_EARTH_RADIUS*tan(latcenter)))) / 
		((CGeo::NM_LOCAL_EARTH_RADIUS * K) - (y * tan(latcenter)));
	double dlat1 = atan(rlat);
	double dlat2 = CGeo::ConfToGeo(dlat1);
	double dlat = CGeo::LatRadianToDecimal(dlat2);

	CGeo::DecimalToGeographic(CGeo::LongRadianToDecimal(dlong),Long);
	CGeo::DecimalToGeographic(dlat,Lat);

}

CPoint CGeo::GetStartPoint(CString Adep)
{
	CPoint pnt(0,0);
	for (int i=0; i < m_StartPoint.GetSize(); i++)
	{
		if (m_StartPoint[i].Adep.CompareNoCase(Adep)==0)
		{
			pnt = m_StartPoint[i].pnt;
		}
	}

	return pnt;
}

CString CGeo::GetStartEpt(CString Adep)
{
	CString Ept="";
	for (int i = 0; i < m_StartPoint.GetSize(); i++)
	{
		if (m_StartPoint[i].Adep.CompareNoCase(Adep) == 0)
		{
			Ept = m_StartPoint[i].Ept;
		}
	}

	return Ept;
}

void CGeo::LoadAirportAndPoints(CString IniName)
{
	CIniFile file(IniName);

	CString name,dum,sectorname, adep, ept, xPnt, yPnt;
	CArray<CString,CString> LineTable;
	file.GetIniProfileSectionData("NAV_POINTS",LineTable,false);
	Latitude lat;
	Longitude lng;
	CPoint pnt;
	CStdioFile sycopnt("SycoFixes.txt",CFile::modeWrite|CFile::modeCreate);
	int nb=1,i;
	for (i=0;i<LineTable.GetSize();i++)
	{
		if (NStringUtil::GetNbrOfArgument(LineTable[i],'|')>=2)
		{
			//name field decoding
			NStringUtil::GetArgumentNb(LineTable[i],1,'|',name);
			name.TrimLeft();
			name.TrimRight();
			
			//Coordinates field decoding
			NStringUtil::GetArgumentNb(LineTable[i],2,'|',dum);
			dum.TrimLeft();
			dum.TrimRight();

			StringToGeographic(dum,lat,lng);

			double dlat,dlong;
			CGeo::GeographicToStereographic(lat,lng, dlat, dlong);
			int x,y;
			
			x = CGeo::RoundTo32(dlong);
			y = CGeo::RoundTo32(dlat);
			CGeo* pCurGeo;
			if (m_MapPoint.Lookup(name,pCurGeo))
			{
				CString err;
				err.Format(" Point conversion stopped due to 2 equals points name : %s", name);
				//pApp->LogMsg(err);
//				AfxMessageBox(err);
//				return ;
			}
			else
			{
				pCurGeo= new CGeo();
				pCurGeo->m_Name=name;
				pCurGeo->m_Pos.x=x;
				pCurGeo->m_Pos.y=y;
				pCurGeo->m_IsFir= CArea::IsInsideLocalFir(pCurGeo->m_Pos);
				CGeo::m_MapPoint.SetAt(name,pCurGeo);
				CString out;
				out.Format("%d=GEO|%s|%.2f|%.2f\n",nb++,pCurGeo->m_Name,pCurGeo->m_Pos.x/32.0,pCurGeo->m_Pos.y/32.0);
				sycopnt.WriteString(out);
			}

		}
	}

	file.GetIniProfileSectionData("AIRPORTS",LineTable,false);
	for (i=0;i<LineTable.GetSize();i++)
	{
		if (NStringUtil::GetNbrOfArgument(LineTable[i],'|')>=2)
		{
			//name field decoding
			NStringUtil::GetArgumentNb(LineTable[i],1,'|',name);
			name.TrimLeft();
			name.TrimRight();
			
			//Coordinates field decoding
			NStringUtil::GetArgumentNb(LineTable[i],2,'|',dum);
			dum.TrimLeft();
			dum.TrimRight();

			StringToGeographic(dum,lat,lng);

			double dlat,dlong;
			CGeo::GeographicToStereographic(lat,lng, dlat, dlong);
			int x,y;
			
			x = CGeo::RoundTo32(dlong);
			y = CGeo::RoundTo32(dlat);
			CGeo* pCurGeo;
			if (m_MapAirport.Lookup(name,pCurGeo))
			{
				CString err;
				err.Format(" Point conversion stopped due to 2 equals points name : %s", name);
				AfxMessageBox(err);
				return ;
			}

			pCurGeo= new CGeo();
			pCurGeo->m_Name=name;
			pCurGeo->m_Pos.x=x;
			pCurGeo->m_Pos.y=y;
			pCurGeo->m_IsFir= CArea::IsInsideLocalFir(pCurGeo->m_Pos);
			CGeo::m_MapAirport.SetAt(name,pCurGeo);
			CString out;
			out.Format("%d=AIRPORT|%s|%.2f|%.2f\n",nb++,pCurGeo->m_Name,pCurGeo->m_Pos.x/32.0,pCurGeo->m_Pos.y/32.0);
			sycopnt.WriteString(out);

		}
	}

	file.GetIniProfileSectionData("STARTPOINT", LineTable, false);
	for (i = 0; i < LineTable.GetSize(); i++)
	{
		if (NStringUtil::GetNbrOfArgument(LineTable[i], '|') >= 4)
		{
			//adep field decoding
			NStringUtil::GetArgumentNb(LineTable[i], 1, '|', adep);
			int pos=adep.Find("=");
			adep = adep.Right(adep.GetLength()-pos-1);
			adep.TrimLeft();
			adep.TrimRight();

			//EPT field decoding
			NStringUtil::GetArgumentNb(LineTable[i], 2, '|', ept);
			ept.TrimLeft();
			ept.TrimRight();

			//X coordinates field decoding
			NStringUtil::GetArgumentNb(LineTable[i], 3, '|', xPnt);
			xPnt.TrimLeft();
			xPnt.TrimRight();
			pnt.x = atoi(xPnt);

			//Y coordinates field decoding
			NStringUtil::GetArgumentNb(LineTable[i], 4, '|', yPnt);
			yPnt.TrimLeft();
			yPnt.TrimRight();
			pnt.y = atoi(yPnt);
			StartPnt CurDef;
			CurDef.Adep = adep;
			CurDef.Ept = ept;
			CurDef.pnt = pnt;
			m_StartPoint.Add(CurDef);
		}
	}
}	

void CGeo::LoadNavPoints(CString IniName)
{
	CIniFile file(IniName);

	CString name, dum, sectorname, adep, ept, xPnt, yPnt;
	CArray<CString, CString> LineTable;
	file.GetIniProfileSectionData("NAV_POINTS", LineTable, false);
	Latitude lat;
	Longitude lng;
	CPoint pnt;
	CStdioFile navpoints("NavPointsFixes.txt", CFile::modeWrite | CFile::modeCreate);
	int nb = 1, i;
	for (i = 0; i<LineTable.GetSize(); i++)
	{
		if (NStringUtil::GetNbrOfArgument(LineTable[i], '|') >= 2)
		{
			//name field decoding
			NStringUtil::GetArgumentNb(LineTable[i], 1, '|', name);
			name.TrimLeft();
			name.TrimRight();

			//Coordinates field decoding
			NStringUtil::GetArgumentNb(LineTable[i], 2, '|', dum);
			dum.TrimLeft();
			dum.TrimRight();

			StringToGeographic(dum, lat, lng);

			double dlat, dlong;
			CGeo::GeographicToStereographic(lat, lng, dlat, dlong);
			int x, y;

			x = CGeo::RoundTo32(dlong);
			y = CGeo::RoundTo32(dlat);
			CGeo* pCurGeo;
			if (m_MapNavPoint.Lookup(name, pCurGeo))
			{
				CString err;
				err.Format(" Point conversion stopped due to 2 equals points name : %s", name);
				//pApp->LogMsg(err);
				//				AfxMessageBox(err);
				//				return ;
			}
			else
			{
				pCurGeo = new CGeo();
				pCurGeo->m_Name = name;
				pCurGeo->m_Pos.x = x;
				pCurGeo->m_Pos.y = y;
				pCurGeo->m_IsFir = CArea::IsInsideLocalFir(pCurGeo->m_Pos);
				CGeo::m_MapNavPoint.SetAt(name, pCurGeo);
				CString out;
				out.Format("%d=GEO|%s|%.2f|%.2f\n", nb++, pCurGeo->m_Name, pCurGeo->m_Pos.x / 32.0, pCurGeo->m_Pos.y / 32.0);
				navpoints.WriteString(out);
			}
		}
	}
}

void CGeo::LoadIntermediatePoints(CString IniName) 
{
	CIniFile file(IniName);

	CString PntName, Key;
	for (int i=1;;i++)
	{
		Key.Format("PNT%d", i);
		int len = file.GetIniProfileString("INTERMEDIATE_POINTS", Key, PntName);
		if (!len)
			break;
		CGeo::m_IntermediatePoints.Add(PntName);
	}
}

// GetPoint(name of the point to retrieve, retrieved point)
CGeo*  CGeo::GetPoint(CString name)
{
	CGeo* pCurGeo=NULL;
	m_MapPoint.Lookup(name,pCurGeo);
	return pCurGeo;
}

CGeo*  CGeo::GetAirport(CString name)
{
	CGeo* pCurGeo=NULL;
	m_MapAirport.Lookup(name,pCurGeo);
	return pCurGeo;
}

CGeo*  CGeo::GetAirportOrPoint(CString name)
{
	CGeo* pCurGeo=NULL;
	m_MapPoint.Lookup(name,pCurGeo);
	if (pCurGeo==NULL)
		m_MapAirport.Lookup(name,pCurGeo);
	return pCurGeo;
}


void CGeo::GetPointsName(CArray<CString,CString>* table)
{
/*	TPoint point;
	for (int i=0;i<m_MapPoint.GetSize();i++)
	{
		table->Add(m_MapPoint[i].name);
	}*/
}


BOOL  CGeo::InitCenterInIni(CString IniName)
{
	CIniFile file(IniName);

	CString name,dum,sectorname;
	file.GetIniProfileString("CENTRE","POS",dum);
	dum.TrimLeft();
	dum.TrimRight();
	return InitCenter(dum);
}



bool CGeo::IsIntermediatePoint(CString Name)
{
	for (int i = 0; i<m_IntermediatePoints.GetSize(); i++)
	{
		if (CGeo::m_IntermediatePoints[i] == Name)
			return true;
	}
	return false;
}

bool CGeo::IsEntryPoint(CString Name)
{
	for (int i=0;i<m_EntryPoints.GetSize();i++)
	{
		if (m_EntryPoints[i].Name==Name)
			return true;
	}
	return false;
}

CString CGeo::FindEntryPoint(CString Name)
{
	Name.TrimRight();
	for (int i=0;i<m_EntryPoints.GetSize();i++)
	{
		if (m_EntryPoints[i].SmallName==Name)
			return m_EntryPoints[i].Name;
	}
	return "";
}

int CGeo::GetEntryFirNb(CString Ept,int Efl)
{
	for (int i=0;i<m_EntryPoints.GetSize();i++)
	{
		if (m_EntryPoints[i].Name==Ept)
		{
			for (int j=0;j<3;j++)
			{
				if (m_EntryPoints[i].FirTable[j].sectornb==0)
					break;
				if ((m_EntryPoints[i].FirTable[j].low<=Efl) && (m_EntryPoints[i].FirTable[j].high>=Efl))
					return m_EntryPoints[i].FirTable[j].sectornb;
			}
			break;
		}
	}
	return 0;

}

CString CGeo::FindHoldingPoint(CString Name)
{
	for (int i=0;i<m_HoldingPoints.GetSize();i++)
	{
		if (m_HoldingSmallPoints[i]==Name)
			return m_HoldingPoints[i];
	}
	return "";
}

int CGeo::IsHoldingPoint(CString Name)
{
	for (int i=0;i<m_HoldingPoints.GetSize();i++)
	{
		if (m_HoldingPoints[i]==Name)
			return i+1;
	}
	return 0;
}

void CGeo::ActiveHld(int HldPnt,bool active)
{
	m_ActiveHolding[HldPnt-1]=active;
}

bool CGeo::IsActiveHld(int HldPnt)
{
	return m_ActiveHolding[HldPnt-1];
}

CString CGeo::GetHoldingPtName(int hldNumber)
{
	CString empty("");
	if ((hldNumber>0) && (hldNumber<=m_HoldingPoints.GetSize()))
		return m_HoldingPoints[hldNumber-1];
	else
		return empty;
}

int CGeo::IsExitPoint(CString Name,bool& Prio)
{
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].Name==Name)
		{
			Prio= m_ExitCops[i].Prio;
			return i+1;
		}
	}
	return 0;
}

CString CGeo::FindExitPoint(CString Name)
{
	Name.TrimRight();
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].SmallName==Name)
			return m_ExitCops[i].Name;
	}
	return "";
}

int CGeo::GetExitFirNb(CString Xpt,int Xfl)
{
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].Name==Xpt)
		{
			for (int j=0;j<3;j++)
			{
				if (m_ExitCops[i].FirTable[j].FirNb==0)
					break;
				if ((m_ExitCops[i].FirTable[j].LowLevel<=Xfl) && (m_ExitCops[i].FirTable[j].HighLevel>=Xfl))
					return m_ExitCops[i].FirTable[j].FirNb;
			}
			break;
		}
	}
	return 0;

}


void CGeo::SetExitFFL(int PntNb,int Level,bool Forbid)
{
	if ((PntNb>0) && (PntNb<= m_ExitCops.GetSize()))
	{
		m_ExitCops[PntNb-1].FFL[Level/10]=Forbid;
	}
}

bool CGeo::GetExitFFL(int PntNb,int Level)
{
	if ((PntNb>0) && (PntNb<= m_ExitCops.GetSize()))
	{
		return m_ExitCops[PntNb-1].FFL[Level/10];
	}
	return false;
}

int CGeo::GetXflMin(int PntNb)
{
	if ((PntNb>0) && (PntNb<= m_ExitCops.GetSize()))
	{
		return m_ExitCops[PntNb-1].LevelMin;
	}
	return 0;
}

int CGeo::GetXflMax(int PntNb)
{
	if ((PntNb>0) && (PntNb<= m_ExitCops.GetSize()))
	{
		return m_ExitCops[PntNb-1].LevelMax;
	}
	return 999;
}


int CGeo::GetWindowNb(CString Name)
{
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].Name==Name)
			return m_ExitCops[i].WindowNb;
	}
	return 0;
}

bool CGeo::FillFFLBuff(int PntNb,BYTE* buff)
{
	if ((PntNb>0) && (PntNb<= m_ExitCops.GetSize()))
	{
		strcpy(((char*)&buff[4]), m_ExitCops[PntNb-1].SmallName);
		for (int i=7;i<=41;i++)
			buff[i+1]= m_ExitCops[PntNb-1].FFL[i];
		return true;
	}
	return false;
}
int CGeo::GetAbiTime(int PntNb,int Level,int Tas)
{
	if (!PntNb)
		return -1;
	for (int f=0;f<3;f++)
	{
		if ((m_ExitCops[PntNb-1].FirTable[f].LowLevel<=Level) && (m_ExitCops[PntNb-1].FirTable[f].HighLevel>=Level))
		{
			if (m_ExitCops[PntNb-1].FirTable[f].Oldi[0].AtActivation)
				return 0;
			else
			{
				for (int t=0;t<m_ExitCops[PntNb-1].FirTable[f].Oldi[0].TimeNb;t++)
				{
					if ((m_ExitCops[PntNb-1].FirTable[f].Oldi[0].Time[t].MinTas<=Tas) && (m_ExitCops[PntNb-1].FirTable[f].Oldi[0].Time[t].MaxTas>=Tas))
						return ((m_ExitCops[PntNb-1].FirTable[f].Oldi[0].Time[t].DistBefXpt*3600)/Tas+ m_ExitCops[PntNb-1].FirTable[f].Oldi[0].Time[t].AddTimeBefXpt);
				}
			}

		}
	}
	return -1;
}

int CGeo::GetActTime(int PntNb,int Level,int Tas)
{
	if (!PntNb)
		return -1;
	for (int f=0;f<3;f++)
	{
		if ((m_ExitCops[PntNb-1].FirTable[f].LowLevel<=Level) && (m_ExitCops[PntNb-1].FirTable[f].HighLevel>=Level))
		{
			if (m_ExitCops[PntNb-1].FirTable[f].Oldi[2].AtActivation)
				return 0;
			else
			{
				for (int t=0;t<m_ExitCops[PntNb-1].FirTable[f].Oldi[2].TimeNb;t++)
				{
					if ((m_ExitCops[PntNb-1].FirTable[f].Oldi[2].Time[t].MinTas<=Tas) && (m_ExitCops[PntNb-1].FirTable[f].Oldi[2].Time[t].MaxTas>=Tas))
						return ((m_ExitCops[PntNb-1].FirTable[f].Oldi[2].Time[t].DistBefXpt*3600)/Tas+ m_ExitCops[PntNb-1].FirTable[f].Oldi[2].Time[t].AddTimeBefXpt);
				}
			}

		}
	}
	return -1;
}

int CGeo::GetRevTime(int PntNb,int Level)
{
	if (!PntNb)
		return -1;
	for (int f=0;f<3;f++)
	{
		if ((m_ExitCops[PntNb-1].FirTable[f].LowLevel<=Level) && (m_ExitCops[PntNb-1].FirTable[f].HighLevel>=Level))
		{
			return (m_ExitCops[PntNb-1].FirTable[f].RevParam);

		}
	}
	return -1;
}


CString CGeo::GetSmallName(CString Name)
{
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].Name==Name)
		{
			return m_ExitCops[i].SmallName;
		}
	}
	return "";
}

int CGeo::GetExitPointDelta(CString Name)
{
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].Name==Name)
		{
			return m_ExitCops[i].SepTime;
		}
	}
	return 0;
}

int CGeo::GetExitPointStopTime(CString Name)
{
	for (int i=0;i<m_ExitCops.GetSize();i++)
	{
		if (m_ExitCops[i].Name==Name)
		{
			return m_ExitCops[i].StopTime;
		}
	}
	return 0;
}


