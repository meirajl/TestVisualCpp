#include "stdafx.h"
#include "resource.h"
#include "FplWnd.h"
#include "FplDoc.h"
#include "SequenceError.h"
#include "OfflineError.h"
#include "TrafficError.h"
#include "RouteInformation.h"
#include "IcaoRoute.h"
#include "Remark.h"
#include "Technical Validation.h"
#include "Geo.h"

CFplWnd::CFplWnd()
{
	IsOpen = false;
	m_font.CreatePointFont(100, "courier");
	m_hext = 0;
	m_Popup.LoadMenu(IDR_MENU1);
	m_CurFplNb = -1;
	m_CurColNb = -1;
	m_CurSorting = 0;
}


CFplWnd::~CFplWnd()
{
}

BEGIN_MESSAGE_MAP(CFplWnd, CListCtrl)
	//{{AFX_MSG_MAP(CFplWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
	ON_WM_NCMBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_COMMAND(ID_FPL_SEQUENCE_ERROR, OnSequenceError)
	ON_COMMAND(ID_FPL_OFFLINE_ERROR, OnOfflineError)
	ON_COMMAND(ID_FPL_TRAFFIC_ERROR, OnTrafficError)
	ON_COMMAND(ID_FPL_ROUTE_INFORMATION, OnRouteInformation)
	ON_COMMAND(ID_FPL_ICAOROUTE, OnIcaoRoute)
	ON_COMMAND(ID_FPL_REMARKFIELD, OnRemark)
END_MESSAGE_MAP()

int CFplWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetExtendedStyle(LVS_EX_HEADERDRAGDROP | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	SetFont(&m_font);
	InsertColumn(0, "Arcid", LVCFMT_LEFT, 80);
	InsertColumn(1, "Adep", LVCFMT_LEFT, 60);
	InsertColumn(2, "Ades", LVCFMT_LEFT, 60);
	InsertColumn(3, "EPT", LVCFMT_LEFT, 65);
	InsertColumn(4, "EFL", LVCFMT_LEFT, 60);
	InsertColumn(5, "IPT", LVCFMT_LEFT, 65);
	InsertColumn(6, "IFL", LVCFMT_LEFT, 60);
	InsertColumn(7, "XPT", LVCFMT_LEFT, 65);
	InsertColumn(8, "XFL", LVCFMT_LEFT, 60);
	InsertColumn(9, "RFL", LVCFMT_LEFT, 60);
	InsertColumn(10, "Sequence", LVCFMT_LEFT, 200);
	InsertColumn(11, "Route information", LVCFMT_LEFT, 160);
	InsertColumn(12, "Icao Route", LVCFMT_LEFT, 200);
	InsertColumn(13, "Traffic Error", LVCFMT_LEFT, 160);
	InsertColumn(14, "Offline Error", LVCFMT_LEFT, 160);
	InsertColumn(15, "Sequence Error", LVCFMT_LEFT, 160);
	IsOpen = true;
	return 0;
}

void CFplWnd::OnDestroy()
{
	CListCtrl::OnDestroy();
	IsOpen = false;
	m_hext = 0;
	// TODO: Add your message handler code here

}

bool CFplWnd::OfflineError(int FplNb)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString sOfflineError = "";
	bool IsOfflineError = false;

	if (!m_pDoc->m_FplTable[FplNb]->m_Ept.IsEmpty() && !pApp->m_pDoc->IsEntryPoint(m_pDoc->m_FplTable[FplNb]->m_Ept))
	{
		sOfflineError.Format("EPT=%s is not present in section [ENTRYPOINTS] of def_airspace.ofl file\r\n", m_pDoc->m_FplTable[FplNb]->m_Ept);
		m_pDoc->m_FplTable[FplNb]->m_OfflineError += sOfflineError;
	}
	if (!m_pDoc->m_FplTable[FplNb]->m_IptCentre.IsEmpty() && !CGeo::IsIntermediatePoint(m_pDoc->m_FplTable[FplNb]->m_IptCentre))
	{
		sOfflineError.Format("IPT=%s is not present in section [INTERMEDIATEPOINTS] of def_airspace.ofl file\r\n", m_pDoc->m_FplTable[FplNb]->m_IptCentre);
		m_pDoc->m_FplTable[FplNb]->m_OfflineError += sOfflineError;
	}
	if (!m_pDoc->m_FplTable[FplNb]->m_Xpt.IsEmpty() && !pApp->m_pDoc->IsExitPoint(m_pDoc->m_FplTable[FplNb]->m_Xpt))
	{
		sOfflineError.Format("XPT=%s is not present in section [EXITPOINTS] of def_airspace.ofl file\r\n", m_pDoc->m_FplTable[FplNb]->m_Xpt);
		m_pDoc->m_FplTable[FplNb]->m_OfflineError += sOfflineError;
	}
	if (m_pDoc->m_FplTable[FplNb]->m_Ept.CompareNoCase(m_pDoc->m_FplTable[FplNb]->m_Xpt) == 0)
	{
		sOfflineError.Format("EPT=%s is equal to XPT=%s\r\n", m_pDoc->m_FplTable[FplNb]->m_Ept, m_pDoc->m_FplTable[FplNb]->m_Xpt);
		m_pDoc->m_FplTable[FplNb]->m_OfflineError += sOfflineError;
	}

	if (!m_pDoc->m_FplTable[FplNb]->m_OfflineError.IsEmpty())
		IsOfflineError = true;

	return IsOfflineError;
}

bool CFplWnd::TrafficError(int FplNb)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString sTrafficError = "";
	bool IsTrafficError = false;
	//CString sIpt = "VEVAR/IRMAR";

	if ((m_pDoc->m_FplTable[FplNb]->m_Ept.CompareNoCase(m_pDoc->m_FplTable[FplNb]->m_EptRose) != 0) && (pApp->m_pOffline->m_ItalyCrossBorderPointList.Find(m_pDoc->m_FplTable[FplNb]->m_Ept) == -1))
	{
		sTrafficError.Format("EPT=%s is different from EPT Rose=%s\r\n", m_pDoc->m_FplTable[FplNb]->m_Ept, m_pDoc->m_FplTable[FplNb]->m_EptRose);
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if (m_pDoc->m_FplTable[FplNb]->m_Ept.IsEmpty() && m_pDoc->m_FplTable[FplNb]->m_EptRose.IsEmpty())
	{
		sTrafficError.Format("EPT and EPT Rose are empty\r\n");
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if (m_pDoc->m_FplTable[FplNb]->m_IptCentre.CompareNoCase(m_pDoc->m_FplTable[FplNb]->m_IptRose) != 0)
	{
		sTrafficError.Format("IPT=%s is different from IPT Rose=%s\r\n", m_pDoc->m_FplTable[FplNb]->m_IptCentre, m_pDoc->m_FplTable[FplNb]->m_IptRose);
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if ((m_pDoc->m_FplTable[FplNb]->m_Xpt.CompareNoCase(m_pDoc->m_FplTable[FplNb]->m_XptRose) != 0) && (pApp->m_pOffline->m_ItalyCrossBorderPointList.Find(m_pDoc->m_FplTable[FplNb]->m_Xpt) == -1))
	{
		sTrafficError.Format("XPT=%s is different from XPT Rose=%s\r\n", m_pDoc->m_FplTable[FplNb]->m_Xpt, m_pDoc->m_FplTable[FplNb]->m_XptRose);
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if (m_pDoc->m_FplTable[FplNb]->m_Xpt.IsEmpty() && m_pDoc->m_FplTable[FplNb]->m_XptRose.IsEmpty() && (!m_pDoc->m_FplTable[FplNb]->IsArrival()))
	{
		sTrafficError.Format("XPT and XPT Rose are empty\r\n");
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if (m_pDoc->m_FplTable[FplNb]->m_Efl != m_pDoc->m_FplTable[FplNb]->m_EflRose)
	{
		sTrafficError.Format("EFL=%d is different from EFL Rose=%d\r\n", m_pDoc->m_FplTable[FplNb]->m_Efl, m_pDoc->m_FplTable[FplNb]->m_EflRose);
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if ((m_pDoc->m_FplTable[FplNb]->m_Efl == 0 || m_pDoc->m_FplTable[FplNb]->m_Efl == -1) && (m_pDoc->m_FplTable[FplNb]->m_EflRose == 0 || m_pDoc->m_FplTable[FplNb]->m_EflRose == -1))
	{
		sTrafficError.Format("EFL and EFL Rose are set to 0\r\n");
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if ((m_pDoc->m_FplTable[FplNb]->m_IflCentre != m_pDoc->m_FplTable[FplNb]->m_IflRose) && (pApp->m_pOffline->m_IptUseless.Find(m_pDoc->m_FplTable[FplNb]->m_IptCentre)==-1))
	{
		sTrafficError.Format("IFL=%d is different from IFL Rose=%d\r\n", m_pDoc->m_FplTable[FplNb]->m_IflCentre, m_pDoc->m_FplTable[FplNb]->m_IflRose);
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if ((m_pDoc->m_FplTable[FplNb]->m_Xfl != m_pDoc->m_FplTable[FplNb]->m_XflRose) && (!m_pDoc->m_FplTable[FplNb]->IsArrival()))
	{
		sTrafficError.Format("XFL=%d is different from XFL Rose=%d\r\n", m_pDoc->m_FplTable[FplNb]->m_Xfl, m_pDoc->m_FplTable[FplNb]->m_XflRose);
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
	if ((m_pDoc->m_FplTable[FplNb]->m_Xfl == 0 || m_pDoc->m_FplTable[FplNb]->m_Xfl == -1) && (m_pDoc->m_FplTable[FplNb]->m_XflRose == 0 || m_pDoc->m_FplTable[FplNb]->m_XflRose == -1) && (!m_pDoc->m_FplTable[FplNb]->IsArrival()))
	{
		sTrafficError.Format("XFL and XFL Rose are set to 0\r\n");
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}

	if (m_pDoc->m_FplTable[FplNb]->m_Ept.CompareNoCase(m_pDoc->m_FplTable[FplNb]->m_Xpt)==0)
	{
		sTrafficError.Format("EPT and XPT are the same\r\n");
		m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
	}
/*	for (int i = 0; i < m_pDoc->m_FplTable[FplNb]->m_FdpRoute.GetSize(); i++)
	{
		if ((sIpt.Find(m_pDoc->m_FplTable[FplNb]->m_FdpRoute[i].LongName) != -1) && (m_pDoc->m_FplTable[FplNb]->m_IptRose.IsEmpty()) && (sIpt.Find(m_pDoc->m_FplTable[FplNb]->m_XptRose)==-1))
		{
			sTrafficError.Format("IPT should not be empty\r\n");
			m_pDoc->m_FplTable[FplNb]->m_TrafficError += sTrafficError;
			break;
		}
	}*/

	if (!m_pDoc->m_FplTable[FplNb]->m_TrafficError.IsEmpty())
		IsTrafficError = true;

	return IsTrafficError;
}

void CFplWnd::AddFpl()
{
	CString sEfl="", sXfl="", sIfl="", sOfflineTick, sTrafficTick, sSequenceTick, sRouteInfoTick;
	bool bTrafficError, bOfflineError;
//	bool check[5000];
	if (IsOpen)
	{
		//INT_PTR i = m_pDoc->m_FplTable.GetSize() - 1;
		for (int i=0; i<m_pDoc->m_FplTable.GetSize();i++)
		{
			sEfl.Format("%d", m_pDoc->m_FplTable[i]->m_Efl);
			sIfl.Format("%d", m_pDoc->m_FplTable[i]->m_IflCentre);
			sXfl.Format("%d", m_pDoc->m_FplTable[i]->m_Xfl);
			bTrafficError = TrafficError(i);
			if (bTrafficError)
				sTrafficTick = "XXXXX";
			else
				sTrafficTick = "";
			bOfflineError = OfflineError(i);
			if (bOfflineError)
				sOfflineTick = "XXXXXX";
			else
				sOfflineTick = "";
			if (!m_pDoc->m_FplTable[i]->m_SequenceError.IsEmpty())
				sSequenceTick = "XXXXXX";
			else
				sSequenceTick = "";
			if (!m_pDoc->m_FplTable[i]->m_RouteInformation.IsEmpty())
				sRouteInfoTick = "XXXXXX";
			else
				sRouteInfoTick = "";

			int res = InsertItem(i, m_pDoc->m_FplTable[i]->m_Arcid);
			SetItemText(res, 1, m_pDoc->m_FplTable[i]->m_Adep);
			SetItemText(res, 2, m_pDoc->m_FplTable[i]->m_Ades);
			SetItemText(res, 3, m_pDoc->m_FplTable[i]->m_Ept);
			SetItemText(res, 4, sEfl);
			SetItemText(res, 5, m_pDoc->m_FplTable[i]->m_IptCentre);
			SetItemText(res, 6, sIfl);
			SetItemText(res, 7, m_pDoc->m_FplTable[i]->m_Xpt);
			SetItemText(res, 8, sXfl);
			SetItemText(res, 9, m_pDoc->m_FplTable[i]->m_Rfl);
			SetItemText(res, 10, m_pDoc->m_FplTable[i]->m_Seq);
			SetItemText(res, 11, sRouteInfoTick);
			SetItemText(res, 12, m_pDoc->m_FplTable[i]->m_IcaoRoute);
			SetItemText(res, 13, sTrafficTick);
			SetItemText(res, 14, sOfflineTick);
			SetItemText(res, 15, sSequenceTick);
			SetItemData(res, i);
		}
		/*for (i = 0; i<500; i++)
		{
			check[i] = false;
		}
		for (i = 0; i<GetItemCount(); i++)
		{
			int data = GetItemData(i);
			check[data] = true;
		}
		for (i = 0; i<GetItemCount(); i++)
			ASSERT(check[i]);*/
	}
}

void CFplWnd::UpdateFpl(int fplnb)
{
	int i = -1;
	DWORD_PTR data;
	CString sEpt = "", sEfl = "", sXpt = "", sXfl = "", sIpt = "", sIfl = "";
	if (!IsOpen)
		return;
	for (int j = 0; j<GetItemCount(); j++)
	{
		data = GetItemData(j);
		if (data == fplnb)
		{
			i = j;
			break;
		}
	}
	if (i != -1)
	{
		SetItemText(i, 0, m_pDoc->m_FplTable[fplnb]->m_Arcid);
		SetItemText(i, 1, m_pDoc->m_FplTable[fplnb]->m_Adep);
		SetItemText(i, 2, m_pDoc->m_FplTable[fplnb]->m_Ades);
		SetItemText(i, 3, sEpt);
		SetItemText(i, 4, sEfl);
		SetItemText(i, 5, sIpt);
		SetItemText(i, 6, sIfl);
		SetItemText(i, 7, sXpt);
		SetItemText(i, 8, sXfl);
		SetItemText(i, 9, m_pDoc->m_FplTable[fplnb]->m_Rfl);
		SetItemText(i, 10, m_pDoc->m_FplTable[fplnb]->m_Seq);
		SetItemText(i, 11, m_pDoc->m_FplTable[fplnb]->m_IcaoRoute);
		SetItemText(i, 12, m_pDoc->m_FplTable[fplnb]->m_SequenceError);
	}
	return;
}

void CFplWnd::OnSequenceError()
{
	CSequenceError dlg;
	dlg.m_SequenceError = m_pDoc->m_FplTable[m_CurFplNb]->m_SequenceError;
	dlg.m_Arcid = m_pDoc->m_FplTable[m_CurFplNb]->m_Arcid;
	UINT res = dlg.DoModal();
}

void CFplWnd::OnOfflineError()
{
	COfflineError dlg;
	dlg.m_OfflineError = m_pDoc->m_FplTable[m_CurFplNb]->m_OfflineError;
	dlg.m_Arcid = m_pDoc->m_FplTable[m_CurFplNb]->m_Arcid;
	UINT res = dlg.DoModal();
}

void CFplWnd::OnTrafficError()
{
	CTrafficError dlg;
	dlg.m_TrafficError = m_pDoc->m_FplTable[m_CurFplNb]->m_TrafficError;
	dlg.m_Arcid = m_pDoc->m_FplTable[m_CurFplNb]->m_Arcid;
	UINT res = dlg.DoModal();
}

void CFplWnd::OnRouteInformation()
{
	CRouteInformation dlg;
	dlg.m_RouteInformation = m_pDoc->m_FplTable[m_CurFplNb]->m_RouteInformation;
	dlg.m_Arcid = m_pDoc->m_FplTable[m_CurFplNb]->m_Arcid;
	UINT res = dlg.DoModal();
}

void CFplWnd::OnIcaoRoute()
{
	CString sIcaoRoute="";
	CIcaoRoute dlg;
	sIcaoRoute = DecodeIcaoRoute(m_CurFplNb);
	dlg.m_IcaoRoute = sIcaoRoute;
	dlg.m_Arcid = m_pDoc->m_FplTable[m_CurFplNb]->m_Arcid;
	UINT res = dlg.DoModal();
}

void CFplWnd::OnRemark()
{
	CString sRemark = "";
	CRemark dlg;
	sRemark = DecodeRemark(m_CurFplNb);
	dlg.m_Remark = sRemark;
	dlg.m_Arcid = m_pDoc->m_FplTable[m_CurFplNb]->m_Arcid;
	UINT res = dlg.DoModal();
}

void CFplWnd::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	*pResult = 0;
	if (abs(m_CurSorting) == (pNMListView->iSubItem+1))
		m_CurSorting=-m_CurSorting;
	else
		m_CurSorting = pNMListView->iSubItem+1;
	SortItems(CompareFunc,m_CurSorting);
}

int CFplWnd::CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	switch (lParamSort)
	{
	case 1:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Arcid<pApp->m_pDoc->m_FplTable[lParam2]->m_Arcid);
	case 2:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Adep<pApp->m_pDoc->m_FplTable[lParam2]->m_Adep);
	case 3:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Ades<pApp->m_pDoc->m_FplTable[lParam2]->m_Ades);
	case 4:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Ept<pApp->m_pDoc->m_FplTable[lParam2]->m_Ept);
	case 6:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_IptCentre<pApp->m_pDoc->m_FplTable[lParam2]->m_IptCentre);
	case 8:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Xpt<pApp->m_pDoc->m_FplTable[lParam2]->m_Xpt);
	case -1:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Arcid>pApp->m_pDoc->m_FplTable[lParam2]->m_Arcid);
	case -2:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Adep>pApp->m_pDoc->m_FplTable[lParam2]->m_Adep);
	case -3:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Ades>pApp->m_pDoc->m_FplTable[lParam2]->m_Ades);
	case -4:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Ept>pApp->m_pDoc->m_FplTable[lParam2]->m_Ept);
	case -6:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_IptCentre>pApp->m_pDoc->m_FplTable[lParam2]->m_IptCentre);
	case -8:
		return (pApp->m_pDoc->m_FplTable[lParam1]->m_Xpt>pApp->m_pDoc->m_FplTable[lParam2]->m_Xpt);
	}
	return false;
}

void CFplWnd::OnNcMButtonDown(UINT nHitTest, CPoint point)
{

	CListCtrl::OnNcMButtonDown(nHitTest, point);
	DestroyWindow();
}

void CFplWnd::OnRclick(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here

	*pResult = 0;
	LPNMLISTVIEW lpnmlv = (LPNMLISTVIEW)pNMHDR;
	m_CurFplNb = GetItemData(lpnmlv->iItem);
	m_CurColNb = lpnmlv->iSubItem;
	CPoint pnt;
	GetCursorPos(&pnt);
	m_Popup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, pnt.x, pnt.y, this);
}

CString CFplWnd::DecodeIcaoRoute(int fplNb)
{
	CString sIcaoRoute = "", sRoute="";
	int fnd = -1;

	sRoute = m_pDoc->m_FplTable[fplNb]->m_IcaoRoute +" ";
	fnd = sRoute.Find(' ');
	while (fnd!=-1)
	{
		sIcaoRoute += sRoute.Left(fnd) + "\r\n";
		sRoute = sRoute.Mid(fnd+1, sRoute.GetLength()-fnd-1);
		fnd = sRoute.Find(' ');
	}

	return sIcaoRoute;
}

CString CFplWnd::DecodeRemark(int fplNb)
{
	CString sEptStr = "EPT:", sEflStr = "EFL:", sIptStr = "IPT:", sIflStr = "IFL:", sXptStr = "XPT:", sXflStr = "XFL:";
	CString sFieldRemark = "", sRemark = "";
	int fnd1 = -1, fnd2=-1;

	sRemark = m_pDoc->m_FplTable[fplNb]->m_Rmk + " ";
	fnd1 = sRemark.Find(sEptStr);
	sRemark = sRemark.Mid(fnd1 + sEptStr.GetLength(), sRemark.GetLength());
	if (fnd1 != -1)
	{
		fnd2 = sRemark.Find(' ');
		if (fnd2!=-1)
			sFieldRemark += "EPT=" + sRemark.Left(fnd2)+"\r\n";
	}
	fnd1 = sRemark.Find(sEflStr);
	sRemark = sRemark.Mid(fnd1 + sEflStr.GetLength(), sRemark.GetLength());
	if (fnd1 != -1)
	{
		fnd2 = sRemark.Find(' ');
		if (fnd2 != -1)
			sFieldRemark += "EFL=" + sRemark.Left(fnd2) + "\r\n";
	}
	fnd1 = sRemark.Find(sIptStr);
	sRemark = sRemark.Mid(fnd1 + sIptStr.GetLength(), sRemark.GetLength());
	if (fnd1 != -1)
	{
		fnd2 = sRemark.Find(' ');
		if (fnd2 != -1)
			sFieldRemark += "IPT=" + sRemark.Left(fnd2) + "\r\n";
	}
	fnd1 = sRemark.Find(sIflStr);
	sRemark = sRemark.Mid(fnd1 + sIflStr.GetLength(), sRemark.GetLength());
	if (fnd1 != -1)
	{
		fnd2 = sRemark.Find(' ');
		if (fnd2 != -1)
			sFieldRemark += "IFL=" + sRemark.Left(fnd2) + "\r\n";
	}
	fnd1 = sRemark.Find(sXptStr);
	sRemark = sRemark.Mid(fnd1 + sXptStr.GetLength(), sRemark.GetLength());
	if (fnd1 != -1)
	{
		fnd2 = sRemark.Find(' ');
		if (fnd2 != -1)
			sFieldRemark += "XPT=" + sRemark.Left(fnd2) + "\r\n";
	}
	fnd1 = sRemark.Find(sXflStr);
	sRemark = sRemark.Mid(fnd1 + sXflStr.GetLength(), sRemark.GetLength());
	if (fnd1 != -1)
	{
		fnd2 = sRemark.Find(' ');
		if (fnd2 != -1)
			sFieldRemark += "XFL=" + sRemark.Left(fnd2) + "\r\n";
	}

	return sFieldRemark;
}
