#include "stdafx.h"
#include "OfflineError.h"

COfflineError::COfflineError(CWnd* pParent /*=NULL*/)
	: CDialog(COfflineError::IDD, pParent)
{
	//{{AFX_DATA_INIT(COfflineError)
	m_OfflineError = _T("");
	m_Arcid = _T("");
	//}}AFX_DATA_INIT
}


COfflineError::~COfflineError()
{
}

void COfflineError::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COfflineError)
	DDX_Text(pDX, IDC_EDIT2, m_OfflineError);
	DDX_Text(pDX, IDC_ARCIDEDIT, m_Arcid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COfflineError, CDialog)
	//{{AFX_MSG_MAP(COfflineError)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()