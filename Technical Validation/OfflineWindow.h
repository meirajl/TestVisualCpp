#pragma once
#include "afxwin.h"
#include "resource.h"

class COfflineWindow : public CDialog
{
public:
	COfflineWindow(CWnd* pParent = NULL);   // standard constructor
	~COfflineWindow();
	CString	m_OfflineLog;
	// Dialog Data
	//{{AFX_DATA(COfflineWindow)
	enum { IDD = IDD_OFFLINE };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COfflineWindow)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COfflineWindow)
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

