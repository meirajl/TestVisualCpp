#pragma once
#include "afxwin.h"
#include "resource.h"

class CFplDlg : public CDialog
{
public:
	CFplDlg(CWnd* pParent = NULL);   // standard constructor
	~CFplDlg();
	CString m_FplXfl;
	// Dialog Data
	//{{AFX_DATA(CFplDlg)
	enum { IDD = IDD_FPL_SET };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFplDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFplDlg)
	afx_msg void OnSetXfl();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};
