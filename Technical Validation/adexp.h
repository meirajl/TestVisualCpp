#if !defined(ADEXP_H)
#define ADEXP_H


enum AdexpMsgTypes
  { NONE_TITLE,
	ABI, 
    ACK,
    ACP,
    ACT,
    AUP,
    BFD,
    CFD,
    CNLCOND,
    CNLREG,
    COD,
    COF,
    CRAM,
    DES,
    ERR,
    EXCOND,
    FCM,
    FLS,
    FSA,
    HOP,
    IACH,
    IAFP,
    IAPL,
    IARR,
    ICHG,
    ICNL,
    IDEP,
    IDLA,
    IFPL,
    INF,
    IRPL,
    IRQS,
    ISPL,
    LAM,
    LRM,
    MAC,
    MAN,
    MAS,
    MODCOND,
    MODREG,
    NTA,
    NTACNL,
    NTAMOD,
    OLRA,
    OLRCNL,
	OLRMOD,
	PAC,
	PNT,
	RAP,
    RCHG,
    RCNL,
    RDY,
    REJ,
    REV,
	RLS,
    RJC,
    RJT,
    ROF,
    RRP,
	RRQ,
    RRV,
	RTI,
    SAM,
    SBY,
    SDM,
    SIP,
    SLC,
    SMM,
	SPA,
	SRJ,
	SRM,
	SRR,
	TIM,
	TIP,
	UUP,
	XAP,
	XCM,
	XIN,
	XRQ,
	//AFTN MSG
	ALR,
	RCF,
	FPL,
	CHG,
	CNL,
	DLA,
	DEP,
	ARR,
	CPL,
	EST,
	CDN,
	RQP,
	RQS,
	SPL,
  };



enum AdexpFields
{
	NONE_FIELD,
	AD,
	ADA,
	ADARR,
	ADARRZ,
	ADD,
	ADDR,
	ADEP,
	ADEPK,
	ADEPOLD,
	ADES,
	ADESK,
	ADESOLD,
	ADEXPTXT,
	AFILDATA,
	AHEAD,
	ALTNZ,
	ALTRNT1,
	ALTRNT2,
	AOBD,
	AOBT,
	ARCADDR,
	ARCID,
	ARCIDK,
	ARCIDOLD,
	ARCTYP,
	ASPEED,
	ATA,
	ATD,
	ATOT,
	ATSRT,
	CASSADDR,
	CEQPT,
	CFL,
	CHGRUL,
	COBD,
	COBT,
	COM,
	COMMENT,
	CONDID,
	COORDATA,
	COP,
	CRSCLIMB,
	CSTAT,
	CTOD,
	CTOT,
	DAT,
	DAYS,
	DAYSK,
	DAYSOLD,
	DCT,
	DELAY,
	DEPZ,
	DESC,
	DESTZ,
	EETFIR,
	EETLAT,
	EETLONG,
	EETPT,
	ENDTIME,
	ENTRYDATA,
	EOBD,
	EOBDK,
	EOBDOLD,
	EOBT,
	EOBTK,
	EOBTOLD,
	EQCST,
	ERRFIELD,
	ERROR_FLD,
	ESTDATA,
	ETOD,
	ETOT,
	EXTADDR,
	FILRTE,
	FILTIM,
	FLBAND,
	FLTRUL,
	FLTTYP,
	FMP,
	FMPLIST,
	FREQ,
	FSTDAY,
	FURTHRTE,
	GEO,
	IFP,
	IFPDLIST,
	IFPDSLIST,
	IFPLID,
	IFPSMOD,
	IFPURESP,
	IGNORE_FLD,
	IOBD,
	IOBT,
	LACDR,
	LATSA,
	LCATSRTE,
	LFIR,
	LRAR,
	LRCA,
	LSTDAY,
	MACH,
	MESVALPERIOD,
	MINLINEUP,
	MODIFNB,
	MSGREF,
	MSGSUM,
	MSGTXT,
	MSGTYP,
	NAV,
	NBARC,
	NBRFPD,
	NEWCTOT,
	NEWENDTIME,
	NEWEOBD,
	NEWEOBT,
	NEWPTOT,
	NEWRTE,
	NEWSTARTTIME,
	OLDMSG,
	OPR,
	ORGMSG,
	ORGN,
	ORGNID,
	ORGRTE,
	ORIGIN,
	ORIGINDT,
	PART,
	PER,
	POSITION_FLD,
	PREVARCID,
	PREVSSRCODE,
	PROPFL,
	PTOT,
	QRORGN,
	RALT,
	RATE,
	RATEPDLST,
	REASON,
	REF,
	REFDATA,
	REG,
	REGCAUSE,
	REGLOC,
	REGUL,
	REJCTOT,
	RELEASE,
	RENAME,
	RESPBY,
	RFL,
	RFP,
	RFPDLIST,
	RFPDSLIST,
	RIF,
	RMK,
	ROUTE,
	RRTEFROM,
	RRTEREF,
	RRTETO,
	RTEPTS,
	RVR,
	RVRCOND,
	RVRPERIOD,
	SECTOR,
	SEL,
	SENDTO,
	SEQPT,
	SID_FLD,
	SPEED,
	SPLA,
	SPLADDR,
	SPLC,
	SPLDCAP,
	SPLDCOL,
	SPLDCOV,
	SPLDNB,
	SPLE,
	SPLJ,
	SPLN,
	SPLP,
	SPLR,
	SPLS,
	SRC,
	SSRCODE,
	STAR,
	STARTTIME,
	STAY,
	STAYINFO,
	STS,
	TAXITIME,
	TFCVOL,
	TFV,
	TITLE,
	TTLEET,
	TYPZ,
	UNIT,
	VALFROM,
	VALFROMK,
	VALFROMOLD,
	VALIDITYDATE,
	VALUNTIL,
	VALUNTILK,
	VALUNTILOLD,
	FIELD18,
	WKTRC
};

enum AdexpSubFields
{
	NONE_SUBFIELD,
	ADDR_INFO,
	ADEP_SUBFLD,
	ADES_SUBFLD,
	ADID,
	AIRROUTE,
	AIRSPACE,
	AIRSPDES,
	ARCID_SUBFLD,
	BRNG,
	CONDITION,
	CRFL1,
	CRFL2,
	CRMACH,
	CRSPEED,
	CTO,
	DISTNC,
	EFL,
	ENDREG,
	EOBT_SUBFLD,
	EQPT,
	ETO,
	EXCCOND,
	FAC,
	FIR,
	FL,
	FLBLOCK,
	FLOW,
	FLOWLST,
	FLOWRATE,
	FLOWROLE,
	FMP_SUBFLD,
	FROM,
	FROMPOS,
	GEOID_SUBFLD,
	IFPDLONG,
	IFPDSUM,
	LASTNUM,
	LATTD,
	LONGTD,
	NETWORKTYPE,
	NUM,
	ORIGIN_SUBLFD,
	PENRATE,
	POSTPROCTXT,
	PREPROCTXT,
	PT,
	PTCRSCLIMB,
	PTFLRUL,
	PTID,
	PTMACH,
	PTMILRUL,
	PTRFL,
	PTRTE,
	PTRULCHG,
	PTSPEED,
	PTSTAY,
	RATEPERIOD,
	RECVR,
	REFATSRTE,
	REFID,
	REFLOC,
	REGCOND,
	REGDESC,
	REGID,
	REGLIST,
	REGNUM,
	REGREASON,
	REGULATION,
	REMARK,
	RENID,
	RESPUNIT,
	RFPDLONG,
	RFPDSUM,
	RVRLIMIT,
	RVRPERIOD_SUBFLD,
	SENDER,
	SEQNUM,
	SFL,
	STARTREG,
	STATID,
	STATREASON,
	STAYIDENT,
	STO,
	TFL,
	TFVID,
	TIME,
	TO,
	TOPOS,
	UNIT_SUBFLD,
	UNITID,
	UNTIL,
	VALPERIOD,
	VIA1,
	VIA2,
	VIA3,
	VIA4
};



	class CAdexpMsg : public CObject
	{
	public:
		CAdexpMsg();
		~CAdexpMsg();
		static CAdexpMsg* ReceiveMessage(BYTE* pMsg,int len);
		//bool DecodeMessage(BYTE* pMsg,int len);
		bool DecodeMessage(CString AftnMsg);
		bool DecodeHeader();
		bool IsAftn();
		bool IsValidForAutomatic();
		AdexpMsgTypes GetType();
		bool IsPresent(AdexpFields field);
		bool IsValid;
		CString GetText();
		CString GetMsgBody();
		bool IsCheckMessage();
		bool IsSvcMessage();
		int GetMsgNumber();
		CString GetDeviceId();
		CString GetOriginator();
		INT_PTR GetAddresses(CStringArray& Addr);
		CString GetPriority();
		CString GetField(AdexpFields field);
		static CString GetName(AdexpFields field);
		static bool IsCompoundList(AdexpFields field);
		bool GetSubField(AdexpFields Field,AdexpSubFields& SubField,CString& SubFieldStr,int IndexInLevel1,int IndexInLevel2=0,int IndexInLevel3=0);
		int  FindSubFieldLevel1(AdexpFields Field,AdexpSubFields SubField,CString& SubFieldStr);
		//encode function
		void SetAftn();
		void SetType(AdexpMsgTypes type);

		CString EncodeMessage(bool WithHeader=true);
		int  GetMessage(BYTE* buff,int MaxLen);
		bool SetField(AdexpFields Field,CString str);
		bool SetSubField(AdexpFields Field,AdexpSubFields SubField,CString,int IndexInLevel1,int IndexInLevel2,int IndexInLevel3);
	private:
		AdexpMsgTypes m_MsgType;
		typedef struct
		{
			bool Valid;
			AdexpSubFields SubFld;
			CString Str;
			int Level;
			int IndexInLevel;
		} SubFieldDef;

		typedef struct
		{
			bool Presence;
			bool Valid;
			CString  Str;
			CArray<SubFieldDef,SubFieldDef> SubFieldTable;
		} FieldsDef;
		FieldsDef m_FieldTable[WKTRC+1];
		CString m_Msg;
		CString m_Header;
		CString m_MessageBody;
		CString m_DeviceId;
		int m_MessageNumber;
		CTime m_MessageTime;
		CString m_Priority;
		CString m_Originator;
		CStringArray m_Addresses;
		bool m_IsValid;
		bool m_IsAftn;
		bool m_IsValidAuto;
		bool m_IsCheck;
		bool m_IsService;
		//Decoding helpers
		bool DecodeAdexpMessage(int CurPos);
		bool DecodeAftnMessage(int CurPos);
		void DecodeAftnSubField(int fld,CString CurMsgStr);
		int FindHyphen(CString Msg,int curpos=0);
		AdexpFields GetNextField(CString Msg,int& curpos,int &posfld);
		AdexpSubFields GetNextSubField(CString Msg,int& curpos,int &posfld);
		CString GetNextWord(CString Msg,int &firstpos,int &lastpos);
		void DecodeSubField(AdexpFields CurFld,CString FldStr,int Level,int IndexInLevel,bool IsField=false);
		void AddSubField(AdexpFields Fld,AdexpSubFields SubFld,CString FldStr,int Level,int IndexInLevel);

		//Encoding Helpers
		CString EncodeAftnMessage(bool WithHeader=true);
		CString EncodeAdexpMessage(bool WithHeader=true);
	};

#endif
