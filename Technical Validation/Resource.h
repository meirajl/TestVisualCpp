//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Technical Validation.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TECHNICALVALIDATION_DIALOG  102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       130
#define IDD_SEQUENCE_ERROR              131
#define IDD_ICAOROUTE                   132
#define IDD_REMARK                      133
#define IDD_OFFLINE_ERROR               134
#define IDD_TRAFFIC_ERROR               135
#define IDD_ROUTE_INFORMATION           136
#define IDD_OFFLINE                     137
#define IDD_EXCDT                       138
#define IDD_FPL_SET                     139
#define IDC_CHOOSE_AFTN_FILE            1000
#define IDC_SEQUENCE                    1001
#define IDC_ARCIDEDIT                   1002
#define IDC_CONVERT_LATLONG_INTO_XY     1003
#define IDC_OFFLINE_CHECK               1004
#define IDC_ARCID                       1005
#define IDC_EXCDT_CHECK                 1006
#define IDC_CHOOSE_LAT_LONG_FILE        1007
#define IDC_EDIT2                       1008
#define IDC_CONVERT_LATLONG_INTO_XY2    1009
#define IDC_COMBO1                      1010
#define IDC_SETXFL                      1011
#define IDC_XFLSET                      1012
#define ID_FPL_ERRORINFORMATION         32771
#define ID_FPL_ICAOROUTE                32772
#define ID_FPL_REMARKFIELD              32773
#define ID_FPL_SEQUENCE_ERROR           32774
#define ID_FPL_OFFLINEERROR             32775
#define ID_FPL_OFFLINE_ERROR            32776
#define ID_FPL_TRAFFICERROR             32777
#define ID_FPL_TRAFFIC_ERROR            32778
#define ID_Menu                         32779
#define ID_FPL_ROUTE_INFORMATION        32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
