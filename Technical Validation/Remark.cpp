#include "stdafx.h"
#include "Remark.h"


CRemark::CRemark(CWnd* pParent /*=NULL*/)
	: CDialog(CRemark::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewSsr)
	m_Remark = _T("");
	m_Arcid = _T("");
	//}}AFX_DATA_INIT
}


CRemark::~CRemark()
{
}

void CRemark::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRemark)
	DDX_Text(pDX, IDC_EDIT2, m_Remark);
	DDX_Text(pDX, IDC_ARCIDEDIT, m_Arcid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRemark, CDialog)
	//{{AFX_MSG_MAP(CRemark)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
