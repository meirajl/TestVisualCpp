#include "stdafx.h"
#include "Flightplan.h"
#include "Technical Validation.h"
#include "StringUtil.h"
#include "Geo.h"
#include "Area.h"
#include "Airways.h"
#include "Condition.h"


CFlightPlan::CFlightPlan()
{
	m_Sefl = 0;
	m_SeflSite = "";
	m_SequenceError = "";
	m_TrafficError = "";
	m_OfflineError = "";
	m_RouteInformation = "";
}


CFlightPlan::~CFlightPlan()
{
}

const double    CFlightPlan::CalculDistance(CPoint Pos1, CPoint Pos2)
{
	return sqrt((double)(Pos1.x - Pos2.x)*(Pos1.x - Pos2.x) + (double)(Pos1.y - Pos2.y)*(Pos1.y - Pos2.y));
}

bool CFlightPlan::UpdateFromAftn(CAdexpMsg* pMsg)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString TmpStr, SubFldStr;
	AdexpMsgTypes CurMsgType = pMsg->GetType();
	bool res = false;
	CTimeSpan FirEet;
	CTime Eobt;
	CString CruiseStr, tmp;
	CPoint pos;
	switch (CurMsgType)
	{
	case FPL:
	case CPL:
	{
		m_Arcid = pMsg->GetField(ARCID);
		m_DlArcid = m_Arcid;
		m_FplId = CTime::GetCurrentTime().Format("%Y%m%d%H%M");
		m_Eobd = m_FplId.Left(8);
		m_Adep = pMsg->GetField(ADEP);
		m_Ades = pMsg->GetField(ADES);
		CString TTLEET = pMsg->GetField(AdexpFields::TTLEET);
		m_Eobt = pMsg->GetField(EOBT);
		if (pMsg->IsPresent(ARCTYP))
			m_ArcTyp = pMsg->GetField(ARCTYP);
		if (pMsg->IsPresent(WKTRC))
			m_Wtc = pMsg->GetField(WKTRC)[0];
		if (pMsg->IsPresent(SSRCODE))
			m_Ssr = atoi(pMsg->GetField(SSRCODE).Right(4));
		if (pMsg->IsPresent(SPEED))
		{
			TmpStr = pMsg->GetField(SPEED);
			if (TmpStr[0] == 'N')
				m_TasPln = atoi(TmpStr.Mid(1, 4));
		}
		if (pMsg->IsPresent(RFL))
		{
			TmpStr = pMsg->GetField(RFL);
			if (TmpStr[0] == 'F')
				m_Rfl = TmpStr.Mid(1, 3);
		}
		if (pMsg->IsPresent(FLTRUL))
			m_FlightRules = pMsg->GetField(FLTRUL)[0];
		if (pMsg->IsPresent(FLTTYP))
			m_FlightType = pMsg->GetField(FLTTYP)[0];
		if (pMsg->IsPresent(CEQPT))
			m_CEquip = pMsg->GetField(CEQPT);
		if (pMsg->IsPresent(SEQPT))
			m_SEquip = pMsg->GetField(SEQPT);
		if (pMsg->IsPresent(ALTRNT1))
			m_Altn1 = pMsg->GetField(ALTRNT1);
		if (pMsg->IsPresent(ALTRNT2))
			m_Altn2 = pMsg->GetField(ALTRNT2);
		if (pMsg->IsPresent(RMK))
		{
			m_Rmk = pMsg->GetField(RMK);
			DecodeRemarkField();
		}
		if (pMsg->IsPresent(STS))
		{
			tmp = pMsg->GetField(STS);
			m_Emerg = tmp.Find("EMER") != -1;
			m_Hum = tmp.Find("HUM") != -1;
			m_Hosp = tmp.Find("HOSP") != -1;
			m_Sar = tmp.Find("SAR") != -1;
			m_Head = tmp.Find("HEAD") != -1;
			m_StateFlight = tmp.Find("STATE") != -1;
			m_No833 = tmp.Find("EXM833") != -1;
			m_RvsmEquipment = tmp.Find("NONRVSM") == -1;

			int merd = 7;
		}
		if (pMsg->IsPresent(ARCADDR))
		{
			m_ArcAddr = pMsg->GetField(ARCADDR);
			m_ArcAddr.TrimLeft();
			m_ArcAddr.TrimRight();
		}
		if (pMsg->IsPresent(ROUTE))
		{
			m_IcaoRoute = pMsg->GetField(ROUTE);
			res = ExtractRoute(m_IcaoRoute, m_Adep, m_Ades, m_TasPln, atoi(m_Rfl), m_ArcTyp, m_Arcid);
			ComputeSectorList();
		}
		pApp->m_pDoc->m_FplInactiveTable[m_FplNbr] = this;
		pApp->m_pDoc->m_FplTable[m_FplNbr] = this;
		break;
	}
	default:
		return true;
		break;
	}

	return 0;
}

void CFlightPlan::DecodeRemarkField()
{
	int fnd1 = -1, fnd2=-1;
	CString Remark = m_Rmk, sEfl;
	CString sEptStr = "EPT:", sEflStr="EFL:", sIptStr = "IPT:", sIflStr = "IFL:", sXptStr = "XPT:", sXflStr = "XFL:";
	CString sEpt = "", sIpt="", sXpt="";
	int iEfl = -1, iIfl=-1, iXfl = -1;
	m_TrafficError = "";
	if (!Remark.IsEmpty())
	{
		// Extract EPT from field REMARK
		fnd1 = Remark.Find(sEptStr);
		Remark = Remark.Mid(fnd1 + sEptStr.GetLength(), Remark.GetLength());
		if (fnd1 != -1)
		{
			fnd2 = Remark.Find(' ');
			sEpt = Remark.Left(fnd2);	
		}
		if (m_Ept.IsEmpty())
		{
			if (!sEpt.IsEmpty())
			{
				m_Ept = sEpt;
				m_EptRose = sEpt;
				//m_TrafficError += "EPT from ROSE used as EPT was empty\r\n";
			}
			//else
			//	m_TrafficError += "EPT from ROSE not found and EPT is empty\r\n";
		}
		else
		{
			if (!sEpt.IsEmpty())
				m_EptRose = sEpt;
			else
				m_EptRose = "";
			if (m_Ept.CompareNoCase(m_EptRose)!=0)
				m_TrafficError += "EPT from ROSE is different from EPT\r\n";
		}

		// Extract EFL from field REMARK
		fnd1 = Remark.Find(sEflStr);
		Remark = Remark.Mid(fnd1 + sEflStr.GetLength(), Remark.GetLength());
		if (fnd1 != -1)
		{
			fnd2 = Remark.Find(' ');
			iEfl = atoi(Remark.Left(fnd2));
		}
		if (m_Efl == 0)
		{
			if ((iEfl != -1) && (iEfl != 0))
			{
				m_Efl = iEfl;
				m_EflRose = iEfl;
				//m_TrafficError += "EFL from ROSE used as EFL value is 0\r\n";
			}
			//else
			//	m_TrafficError += "EFL from ROSE not found and EFL value is 0\r\n";
		}
		else
		{
			if ((iEfl != -1) && (iEfl!=0))
				m_EflRose = iEfl;
			else
				m_EflRose = -1;
			if (m_Efl != m_EflRose)
				m_TrafficError += "EFL from ROSE is different from EFL\r\n";
		}

		// Extract IPT from field REMARK
		fnd1 = Remark.Find(sIptStr);
		Remark = Remark.Mid(fnd1 + sIptStr.GetLength(), Remark.GetLength());
		if (fnd1 != -1)
		{
			fnd2 = Remark.Find(' ');
			sIpt = Remark.Left(fnd2);
		}
		if (m_IptCentre.IsEmpty())
		{
			if (!sIpt.IsEmpty())
			{
				m_IptCentre = sIpt;
				m_IptRose = sIpt;
				//m_TrafficError += "IPT from ROSE used as IPT was empty\r\n";
			}
			//else
			//	m_TrafficError += "IPT from ROSE not found and IPT is empty\r\n";
		}
		else
		{
			if (!sIpt.IsEmpty())
				m_IptRose = sIpt;
			else
				m_IptRose = "";
			if (m_IptCentre.CompareNoCase(m_IptRose) != 0)
				m_TrafficError += "IPT from ROSE is different from IPT\r\n";
		}

		// Extract IFL from field REMARK
		fnd1 = Remark.Find(sIflStr);
		Remark = Remark.Mid(fnd1 + sIflStr.GetLength(), Remark.GetLength());
		if (fnd1 != -1)
		{
			fnd2 = Remark.Find(' ');
			iIfl = atoi(Remark.Left(fnd2));
		}
		if (m_IflCentre == 0)
		{
			if ((iIfl != -1) && (iIfl!=0))
			{
				m_IflCentre = iIfl;
				m_IflRose = iIfl;
				//m_TrafficError += "IFL from ROSE used as IFL value is 0\r\n";
			}
			//else
			//	m_TrafficError += "IFL from ROSE not found and IFL value is 0\r\n";
		}
		else
		{
			if ((iIfl != -1) && (iIfl != 0))
				m_IflRose = iIfl;
			else
				m_IflRose = -1;
			if (m_IflCentre != m_IflRose)
				m_TrafficError += "IFL from ROSE is different from IFL\r\n";
		}

		// Extract XPT from field REMARK
		fnd1 = Remark.Find(sXptStr);
		Remark = Remark.Mid(fnd1 + sXptStr.GetLength(), Remark.GetLength());
		if (fnd1 != -1)
		{
			fnd2 = Remark.Find(' ');
			sXpt = Remark.Left(fnd2);
		}
		if (m_Xpt.IsEmpty() && !IsArrival())
		{
			if (!sXpt.IsEmpty())
			{
				m_Xpt = sXpt;
				m_XptRose = sXpt;
				//m_TrafficError += "XPT from ROSE used as XPT was empty\r\n";
			}
			//else
			//	m_TrafficError += "XPT from ROSE not found and XPT is empty\r\n";
		}
		else
		{
			if (m_Xpt.IsEmpty() && IsArrival() && (!sXpt.IsEmpty()))
				m_Xpt = sXpt;
			if (!sXpt.IsEmpty())
				m_XptRose = sXpt;
			else
				m_XptRose = "";
			if (m_Xpt.CompareNoCase(m_XptRose) != 0)
				m_TrafficError += "XPT from ROSE is different from XPT\r\n";
		}

		// Extract XFL from field REMARK
		fnd1 = Remark.Find(sXflStr);
		Remark = Remark.Mid(fnd1 + sXflStr.GetLength(), Remark.GetLength());
		if (fnd1 != -1)
		{
			fnd2 = Remark.Find(' ');
			iXfl = atoi(Remark.Left(fnd2));
		}
		if (m_Xfl == 0)
		{
			if ((iXfl != -1) && (iXfl!=0))
			{
				m_Xfl = iXfl;
				m_XflRose = iXfl;
				//m_TrafficError += "XFL from ROSE used as XFL value is 0\r\n";
			}
			//else if (!IsArrival())
			//	m_TrafficError += "XFL from ROSE not found and XFL value is 0\r\n";
		}
		else
		{
			if ((iXfl != -1) && (iXfl != 0))
				m_XflRose = iXfl;
			else
				m_XflRose = -1;
			if (m_Xfl != m_XflRose)
				m_TrafficError += "XFL from ROSE is different from XFL\r\n";
		}
	}
}

bool CFlightPlan::IsDept()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	if (pApp->m_pOffline->m_DepartureList.Find(m_Adep) != -1)
		return true;

	return false;
}

bool CFlightPlan::IsArrival()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	if (pApp->m_pOffline->m_ArrivalList.Find(m_Ades) != -1)
		return true;

	return false;
}

bool CFlightPlan::ExtractRoute(CString Route, CString Adep, CString Ades, int Speed, int Rfl, CString AcType, CString Cls)
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	CString Pnt;
	CPoint Pos;
	CAirways* pCurAwy = NULL;
	CGeo* pCurGeo = NULL;
	CString LastPnt, LastFullPnt, LastPntIn, FullPnt, TmpStr;
	CStringArray CurRoute;
	int LastIndex, fnd, i;
	typedef enum { NoItem, PntItem, AwyItem } RteItemType;
	RteItemType CurItem = NoItem, PrevItem = NoItem;
	bool finished = false;
	int Error = 0;
	m_FdpRoute.RemoveAll();
	m_EptFdpFix = -1;
	m_IptFdpFix = -1;
	m_XptFdpFix = -1;
	m_ArcTyp = AcType;
	m_Arcid = Cls;
	for (int i = 1; i <= NStringUtil::GetNbrOfArgument(Route, ' '); i++)
	{
		NStringUtil::GetArgumentNb(Route, i, ' ', Pnt);
		Pnt.TrimLeft();
		Pnt.TrimRight();
		if (Pnt.GetLength() && (Pnt.CompareNoCase("DCT") != 0) && (Pnt.CompareNoCase("GAT") != 0))
		{
			PrevItem = CurItem;
			CAirways* pAwy = CAirways::GetAirway(Pnt);
			if (pAwy)
				CurItem = AwyItem;
			else
			{
				FullPnt = Pnt;
				fnd = Pnt.Find("/");
				if (fnd != -1)
					Pnt = Pnt.Left(fnd);
				pCurGeo = CGeo::GetPoint(Pnt);
				if (pCurGeo)
					CurItem = PntItem;
				else
				{
					if (Pnt.GetLength()>4 && (Pnt[4] == 'N' || Pnt[4] == 'S') && (Pnt[Pnt.GetLength() - 1] == 'W' || Pnt[Pnt.GetLength() - 1] == 'E'))
					{
						Latitude lat;
						Longitude lng;
						CGeo::StringToGeographic(Pnt, lat, lng);

						double dlat, dlong;
						CGeo::GeographicToStereographic(lat, lng, dlat, dlong);
						int x, y;

						x = CGeo::RoundTo32(dlong);
						y = CGeo::RoundTo32(dlat);
						CGeo* pCurGeo;
						pCurGeo = new CGeo();
						pCurGeo->m_Name = "GEO";
						pCurGeo->m_Pos.x = x;
						pCurGeo->m_Pos.y = y;
						CurItem = PntItem;
					}
					else
						CurItem = NoItem;
				}

			}
			switch (CurItem)
			{
			case NoItem:
				//				if (CurRoute.GetSize())
				//				{
				if (PrevItem == AwyItem)
				{
					LastIndex = -1;
					if (CurRoute.GetSize())
						LastIndex = CurRoute.GetSize();
					pCurAwy->GetListOfPoints(LastPnt, Pnt, CurRoute);
					if (LastIndex != CurRoute.GetSize())
					{
						if (LastIndex != -1)
						{
							LastPntIn = CurRoute[LastIndex - 1];
							fnd = LastPntIn.Find("/");
							if (fnd != -1)
								LastPntIn = LastPntIn.Left(fnd);
							if (LastPntIn.CompareNoCase(LastPnt) == 0)
								CurRoute.RemoveAt(LastIndex);
						}
						else
						{
							if (CurRoute.GetSize() && (CurRoute[0] == LastPnt))
								CurRoute[0] = LastFullPnt;
						}
						if (CurRoute.GetSize())
							CurRoute[CurRoute.GetUpperBound()] = FullPnt;
					}
					else
					{
						CurRoute.Add(FullPnt);
					}
					pCurAwy = NULL;
				}
				LastPnt.Empty();
				if (CurRoute.GetSize())
					finished = true;
				//				}
				else
				{
					LastPnt = Pnt;
					LastFullPnt = FullPnt;
				}
				break;
			case AwyItem:
			{
				switch (PrevItem)
				{
				case AwyItem:
					Error = 2;
					break;
				default:
					pCurAwy = pAwy;
					break;
				}
			}
			break;
			case PntItem:
			{
				switch (PrevItem)
				{
				case AwyItem:
					LastIndex = -1;
					if (CurRoute.GetSize())
						LastIndex = CurRoute.GetSize();
					pCurAwy->GetListOfPoints(LastPnt, Pnt, CurRoute);
					if (LastIndex != CurRoute.GetSize())
					{
						if (LastIndex != -1)
						{
							LastPntIn = CurRoute[LastIndex - 1];
							fnd = LastPntIn.Find("/");
							if (fnd != -1)
								LastPntIn = LastPntIn.Left(fnd);
							if (LastPntIn.CompareNoCase(LastPnt) == 0)
								CurRoute.RemoveAt(LastIndex);
						}
						else
						{
							if (CurRoute.GetSize() && (CurRoute[0] == LastPnt))
								CurRoute[0] = LastFullPnt;
						}
						if (CurRoute.GetSize())
							CurRoute[CurRoute.GetUpperBound()] = FullPnt;
					}
					else
					{
						CurRoute.Add(FullPnt);
					}
					pCurAwy = NULL;
					break;
				case PntItem:
				case NoItem:
					CurRoute.Add(FullPnt);
					break;
				}
				LastPnt = Pnt;
				LastFullPnt = FullPnt;
			}
			break;
			}
		}
		if ((Error) || (finished))
			break;

	}
	//m_OutsideFir = true;
	if (CurRoute.GetSize())
	{
		PointDef CurPnt;
		CurPnt.FdpNb = 0;
		int CurRfl = Rfl, NxtRfl = Rfl;
		int CurSpd = Speed, NxtSpd = Speed, FndLevel, FndSpd;
		pCurGeo = CGeo::GetAirport(Adep);
		if (pCurGeo && CArea::IsInsideLocalFir(pCurGeo->m_Pos))
		{
			NStringUtil::GetArgumentNb(Route, 1, ' ', Pnt);
			Pnt.TrimLeft();
			Pnt.TrimRight();
			if ((Pnt != CurRoute[0]) && (Pnt.Find(CurRoute[0]) == 0))
			{
				m_Sid = Pnt;
			}
			/*CAirways* pSid=CAirways::GetSid(Adep);
			if (pSid)
			{
			m_Sid=pSid->GetName();
			pSid->GetListOfPoints("","",CurRoute);
			}*/
			//JPM
			//if (IsDept())
			//	CurRoute.InsertAt(0, Adep);
		}
		//if (IsArrival())
		//	CurRoute.InsertAt(CurRoute.GetUpperBound() + 1, Ades);
		/*		pCurGeo=CGeo::GetAirport(Ades);
		if (pCurGeo && CArea::IsInsideLocalFir(pCurGeo->m_Pos))
		{
		CAirways* pStar=CAirways::GetStar(Ades);
		if (pStar)
		{
		m_Star=pStar->GetName();
		pStar->GetListOfPoints("","",CurRoute);
		}
		CurRoute.Add(Ades);
		}*/
		for (i = 0; i < CurRoute.GetSize(); i++)
		{
			FullPnt = CurRoute[i];
			FndLevel = 0;
			FndSpd = 0;
			fnd = FullPnt.Find("/");
			if (fnd != -1)
			{
				TmpStr = FullPnt.Right(FullPnt.GetLength() - fnd - 1);
				Pnt = FullPnt.Left(fnd);
				if (TmpStr[0] == 'K' || TmpStr[0] == 'N' || TmpStr[0] == 'M')
				{
					if (TmpStr[0] == 'N')
					{
						NxtSpd = atoi(TmpStr.Mid(1, 4));
						FndSpd = NxtSpd;
					}
					TmpStr = TmpStr.Right(TmpStr.GetLength() - 5);
					if (TmpStr[0] == 'F' || TmpStr[0] == 'S' || TmpStr[0] == 'A' || TmpStr[0] == 'M' || TmpStr[0] == 'V')
					{
						if (TmpStr[0] == 'F')
						{
							NxtRfl = atoi(TmpStr.Mid(1, 3));
							FndLevel = NxtRfl;
						}
					}
				}
			}
			else
				Pnt = FullPnt;
			pCurGeo = CGeo::GetAirportOrPoint(Pnt);
			if (pCurGeo == NULL)
			{
				if ((Pnt[4] == 'N' || Pnt[4] == 'S') && (Pnt[Pnt.GetLength() - 1] == 'W' || Pnt[Pnt.GetLength() - 1] == 'E'))
				{
					Latitude lat;
					Longitude lng;
					CGeo::StringToGeographic(Pnt, lat, lng);

					double dlat, dlong;
					CGeo::GeographicToStereographic(lat, lng, dlat, dlong);
					int x, y;

					x = CGeo::RoundTo32(dlong);
					y = CGeo::RoundTo32(dlat);
					pCurGeo = new CGeo();
					pCurGeo->m_Name = "GEO";
					pCurGeo->m_Pos.x = x;
					pCurGeo->m_Pos.y = y;
				}
			}
			if (pCurGeo)
			{
				if (CArea::IsInsideLocalFir(pCurGeo->m_Pos))
					m_OutsideFir = false;
				CurPnt.Name = pCurGeo->m_Name;
				CurPnt.LongName = pCurGeo->m_Name;
				CurPnt.Pos = pCurGeo->m_Pos;
				CurPnt.GeoPos = pCurGeo->m_Pos;
				//					CurPnt.CurDist=0;
				//					CurPnt.PrvDist=0;
				if ((pCurGeo->m_Name == Adep) || (pCurGeo->m_Name == Ades))
				{
					//						CurPnt.Speed=0;
					if (FndLevel)
					{
						CurPnt.Alt = NxtRfl;
						CurPnt.PlannedAlt = FndLevel;
						CurPnt.PlannedSpd = FndSpd;
						if (FndLevel)
							CurPnt.AltSet = true;
						else
							CurPnt.AltSet = false;
					}
					else
					{
						CurPnt.Alt = 0;
						CurPnt.PlannedAlt = 0;
						CurPnt.PlannedSpd = 0;
						CurPnt.AltSet = false;
					}
					//						CurPnt.FixedAlt=-1;
				}
				else
				{
					//						CurPnt.Speed=CurSpd;
					//JPM CurPnt.Alt = NxtRfl;
					CurPnt.Alt = FndLevel;
					CurPnt.PlannedAlt = FndLevel;
					CurPnt.PlannedSpd = FndSpd;
					if (FndLevel)
						CurPnt.AltSet = true;
					else
						CurPnt.AltSet = false;
					//						CurPnt.FixedAlt=-1;
				}
				m_FdpRoute.Add(CurPnt);
			}
			else
			{
				if (m_FdpRoute.GetSize())
					break;
			}
			CurSpd = NxtSpd;
			CurRfl = NxtRfl;
		}

		if (!IsArrival())
		{
			for (i = m_FdpRoute.GetUpperBound(); i >= 0; i--)
			{
				if (pApp->m_pDoc->IsExitPoint(m_FdpRoute[i].LongName) && m_FdpRoute[i].AltSet && m_XflRose!=0 && m_XflRose!=-1 && m_XflRose== m_FdpRoute[i].Alt)
				{
					if (m_FdpRoute[i].Alt)
						m_Xfl = m_FdpRoute[i].Alt;
					break;
				}
			}
		}
		else
			m_Xfl = 0;
		
		// For Italy crossing border
		bool IsInItaly = false;
		flightkinds FlightKind = transit;
		int sectornb = 0;
		CArray<IntersectDef, IntersectDef> InterTable;
		bool foundInItaly = false;
		if (m_EptRose.IsEmpty() || m_XptRose.IsEmpty())
		{
			for (i = 0; i < m_FdpRoute.GetSize() - 1; i++)
			{
				if (CArea::GetSectorNb(m_FdpRoute[i].GeoPos, m_FdpRoute[i].Alt))
				{
					IsInItaly = false;
					break;
				}
				else
				{
					sectornb = pApp->m_pDoc->ComputeSector(m_FdpRoute[i].Alt, m_FdpRoute[i].LongName);
					if (sectornb)
						break;
				}
				/*if (CArea::IsInsideFirSector(m_FdpRoute[i].GeoPos, m_FdpRoute[i].Alt))
				{
					IsInItaly = true;
					break;
				}*/

				if ((!IsInItaly) && (CArea::GetLocalFirIntersect(m_FdpRoute[i].GeoPos, m_FdpRoute[i].Alt, m_FdpRoute[i+1].GeoPos, m_FdpRoute[i+1].Alt, InterTable)))
				//if ((!IsInItaly) && (CArea::GetFirIntersect(m_FdpRoute[i].GeoPos, m_FdpRoute[i].Alt, m_FdpRoute[i + 1].GeoPos, m_FdpRoute[i + 1].Alt, InterTable, FlightKind)))
				{
					if (InterTable.GetSize())
					{
						for (int j = 0; j < InterTable.GetSize(); j++)
						{
							if (CArea::IsInsideFirSector(InterTable[j].Pos, InterTable[j].Alt))
							{
								IsInItaly = true;
								foundInItaly = true;
								break;
							}
						}
					}
					if (foundInItaly)
						break;
				}
				if (CArea::GetSectorIntersect(m_FdpRoute[i].GeoPos, m_FdpRoute[i].Alt, m_FdpRoute[i + 1].GeoPos, m_FdpRoute[i + 1].Alt, InterTable, transit))
				{
					if (InterTable.GetSize())
					{
						if (CArea::GetSectorNb(InterTable[0].Pos, m_FdpRoute[i].Alt))
						{
							IsInItaly = false;
							break;
						}
					}
				}

			}
		}
		if (pApp->m_pDoc->IsRwyAirport(m_Adep))
		{
			CString PointToAdd;
			int pntSid = -1;
			for (int i = 0; i < m_FdpRoute.GetSize(); i++)
			{
				if (pApp->m_pDoc->IsSidPoint(m_Adep, m_FdpRoute[i].LongName))
				{
					pntSid = i;
					m_Ept = m_FdpRoute[i].LongName;
					break;
				}
			}
			CString Sid = pApp->m_pDoc->FindSid(m_Adep, m_Ept, PointToAdd);
			if (Sid.GetLength())
			{
				if (pntSid > 1)
				{
					for (int i = pntSid - 1; i > 0; i--)
						m_FdpRoute.RemoveAt(1);
				}
				PointDef CurPnt;
				CurPnt.Added = false;
				int Level = 0;
				int Spd = 0;
				int pos = 0;
				for (int i = 1; i <= NStringUtil::GetNbrOfArgument(PointToAdd, ' '); i++)
				{
					NStringUtil::GetArgumentNb(PointToAdd, i, ' ', FullPnt);
					fnd = FullPnt.Find("/");
					if (fnd != -1)
					{
						TmpStr = FullPnt.Right(FullPnt.GetLength() - fnd - 1);
						Pnt = FullPnt.Left(fnd);
						if (TmpStr[0] == 'K' || TmpStr[0] == 'N' || TmpStr[0] == 'M')
						{
							if (TmpStr[0] == 'N')
							{
								NxtSpd = atoi(TmpStr.Mid(1, 4));
								Spd = NxtSpd;
							}
							TmpStr = TmpStr.Right(TmpStr.GetLength() - 5);
							if (TmpStr[0] == 'F' || TmpStr[0] == 'S' || TmpStr[0] == 'A' || TmpStr[0] == 'M' || TmpStr[0] == 'V')
							{
								if (TmpStr[0] == 'F')
								{
									NxtRfl = atoi(TmpStr.Mid(1, 3));
									Level = NxtRfl;
								}
							}
						}
					}
					else
						Pnt = FullPnt;
					pCurGeo = CGeo::GetAirportOrPoint(Pnt);
					if (pCurGeo)
					{
						CurPnt.Name = pCurGeo->m_Name;
						CurPnt.LongName = pCurGeo->m_Name;
						CurPnt.Pos = pCurGeo->m_Pos;
						CurPnt.GeoPos = pCurGeo->m_Pos;
						CurPnt.Alt = Level;
						CurPnt.PlannedAlt = Level;
						CurPnt.PlannedSpd = 0;
						if (Level)
							CurPnt.AltSet = true;
						else
							CurPnt.AltSet = false;
						m_FdpRoute.InsertAt(pos + 1, CurPnt); // pos + 1 corresponds to the position ADEP + 1
						pos++;
					}
				}
			}
			for (int i = 0; i < pApp->m_pOffline->m_ListAirportLevel.GetSize(); i++)
			{
				if (pApp->m_pOffline->m_ListAirportLevel[i].Airport.Compare(m_Adep) == 0)
				{
					m_Ept = pApp->m_pOffline->m_ListAirportLevel[i].Airport;
					m_Efl = 0;
					break;
				}
			}
		}
		else if ((m_EptFdpFix == -1) && (IsInItaly))
		{
			int SectNb = 0;
			for (int i = 0; i < m_FdpRoute.GetUpperBound(); i++)
			{
				if (m_Efl == 0)
				{
					for (int j = 0; j < m_FdpRoute.GetUpperBound(); j++)
					{
						if (m_FdpRoute[j].Alt != 0)
						{
							m_Efl = m_FdpRoute[j].Alt;
							break;
						}
					}
				}
				CArea::GetSectorIntersect(m_FdpRoute[i].GeoPos, m_Efl, m_FdpRoute[i + 1].GeoPos, m_Efl, InterTable, transit);
				if (InterTable.GetSize())
				{
					SectNb = InterTable[0].SectorNb;
				}
				foundInItaly = false;
				//if (CArea::GetFirIntersect(m_FdpRoute[i].GeoPos, m_Efl, m_FdpRoute[i + 1].GeoPos, m_Efl, InterTable, transit))
				if (CArea::GetLocalFirIntersect(m_FdpRoute[i].GeoPos, m_Efl, m_FdpRoute[i + 1].GeoPos, m_Efl, InterTable))
				{
					if (InterTable.GetSize())
					{
						for (int j = 0; j < InterTable.GetSize(); j++)
						{
							CurPnt.Name = CArea::GetFirSectorName(pApp->m_pOffline->m_SectorTable[SectNb].UAC, InterTable[j].Pos, m_Efl);
							if (CurPnt.Name != "")
							{
								foundInItaly = true;
								CurPnt.LongName = CurPnt.Name;
								CurPnt.Alt = m_Efl;
								CurPnt.PlannedAlt = m_Efl;
								CurPnt.GeoPos = InterTable[0].Pos;
								CurPnt.Pos = InterTable[0].Pos;
								if (m_Efl)
									CurPnt.AltSet = true;
								else
									CurPnt.AltSet = false;
								m_FdpRoute.InsertAt(i + 1, CurPnt);
								m_EptFdpFix = i + 1;
								break;
							}
						}
						if (foundInItaly)
							break;
					}
				}
			}
		}
		
		bool /*foundEpt = false,*/ foundIptBis=false;
		int count = 0;
		// JPM
		m_FoundEpt = false;
		//CString IntermediatePoint = "IRMAR/VEVAR";
		CString EntryPointBis = "MAXIR";
		if (!m_EptRose.IsEmpty())
		{
			for (i = 0; i < m_FdpRoute.GetSize(); i++)
			{
				if (m_EptRose.CompareNoCase(m_FdpRoute[i].LongName) == 0)
				{
					m_FoundEpt = true;
				}
			/*	if (IntermediatePoint.Find(m_FdpRoute[i].LongName) == 0)
				{
					foundIptBis = true;
					break;
				}*/

			}
			if (!m_FoundEpt)
			{
				PointDef CurItem;
				CurItem.Alt = m_EflRose;
				CurItem.PlannedAlt = m_EflRose;
				CurItem.AltSet = true;
				CurItem.LongName = m_EptRose;
				CurItem.Name = m_EptRose;
				pCurGeo = CGeo::GetPoint(m_EptRose);
				if (pCurGeo)
				{
					CurItem.GeoPos = pCurGeo->m_Pos;
					CurItem.Pos = pCurGeo->m_Pos;
				}
				m_FdpRoute.InsertAt(count, CurItem);
				count++;
			}
			if ((!foundIptBis) && (EntryPointBis.Find(m_EptRose)==0))
			{
				PointDef CurItem;
				CurItem.Alt = m_EflRose;
				CurItem.PlannedAlt = m_EflRose;
				CurItem.AltSet = true;
				CurItem.LongName = "IRMAR";
				CurItem.Name = "IRMAR";
				pCurGeo = CGeo::GetPoint("IRMAR");
				if (pCurGeo)
				{
					CurItem.GeoPos = pCurGeo->m_Pos;
					CurItem.Pos = pCurGeo->m_Pos;
				}
				m_FdpRoute.InsertAt(count, CurItem);
				count++;
			}
		}
		for (i = 0; i < m_FdpRoute.GetSize(); i++)
		{
			if (pApp->m_pDoc->IsEntryPoint(m_FdpRoute[i].LongName) && (!IsDept()) && m_FdpRoute[i].AltSet) /*|| (m_FdpRoute[i].LongName == Adep)*/
			{
				m_EptFdpFix = i;
				m_Ept = m_FdpRoute[i].LongName;
				break;
			}
		}
		bool foundIpt = false;
		CString MsgLog = "";
		if (!m_IptRose.IsEmpty())
		{
			for (i = 0; i < m_FdpRoute.GetSize(); i++)
			{
				if (m_IptRose.CompareNoCase(m_FdpRoute[i].LongName) == 0)
				{
					foundIpt = true;
					break;
				}
			}
			if ((!foundIpt) && (!m_FoundEpt))
			{
				PointDef CurItem;
				CurItem.Alt = m_IflRose;
				CurItem.PlannedAlt = m_IflRose;
				CurItem.AltSet = true;
				CurItem.LongName = m_IptRose;
				CurItem.Name = m_IptRose;
				pCurGeo = CGeo::GetPoint(m_IptRose);
				if (pCurGeo)
				{
					CurItem.GeoPos = pCurGeo->m_Pos;
					CurItem.Pos = pCurGeo->m_Pos;
				}
				m_FdpRoute.InsertAt(count, CurItem);
				count++;
			}
			if ((!foundIpt) && (m_FoundEpt))
			//if (!foundIpt)
			{
				MsgLog.Format("IPT=%s is not in the route\r\n", m_IptRose);
				m_TrafficError += MsgLog;
			}
		}
		for (i = 0; i < m_FdpRoute.GetSize(); i++)
		{
			if ((CGeo::IsIntermediatePoint(m_FdpRoute[i].LongName)) && m_FdpRoute[i].AltSet && (!m_IptRose.IsEmpty()) && (m_IptRose.CompareNoCase(m_FdpRoute[i].LongName) == 0))
			{
				m_IptFdpFix = i;
				m_IptCentre = m_FdpRoute[i].LongName;
				m_FdpRoute[i].FdpNb = 2;
				break;
			}
		}

		if (pApp->m_pDoc->IsRwyAirport(m_Ades))
		{
			CString PointToAdd;
			int iaf = -1;
			for (int i = m_FdpRoute.GetUpperBound(); i >= 0; i--)
			{
				if (pApp->m_pDoc->IsStarPoint(m_Ades, m_FdpRoute[i].LongName))
				{
					iaf = i;
					break;
				}
			}
			CString Star = "";
			if (iaf >= 0)
				Star = pApp->m_pDoc->FindStar(m_Ades, m_Ept, m_FdpRoute[iaf].LongName, PointToAdd);
			if (Star.GetLength())
			{
				if (iaf < m_FdpRoute.GetUpperBound())
				{
					for (int i = m_FdpRoute.GetUpperBound(); i > iaf; i--)
						m_FdpRoute.RemoveAt(i);
				}
				PointDef CurPnt;
				CurPnt.Added = false;
				int Level = m_FdpRoute[m_FdpRoute.GetUpperBound()].Alt;
				int Spd = m_FdpRoute[m_FdpRoute.GetUpperBound()].Alt;
				for (int i = 1; i <= NStringUtil::GetNbrOfArgument(PointToAdd, ' '); i++)
				{
					NStringUtil::GetArgumentNb(PointToAdd, i, ' ', FullPnt);
					fnd = FullPnt.Find("/");
					if (fnd != -1)
					{
						TmpStr = FullPnt.Right(FullPnt.GetLength() - fnd - 1);
						Pnt = FullPnt.Left(fnd);
						if (TmpStr[0] == 'K' || TmpStr[0] == 'N' || TmpStr[0] == 'M')
						{
							if (TmpStr[0] == 'N')
							{
								NxtSpd = atoi(TmpStr.Mid(1, 4));
								Spd = NxtSpd;
							}
							TmpStr = TmpStr.Right(TmpStr.GetLength() - 5);
							if (TmpStr[0] == 'F' || TmpStr[0] == 'S' || TmpStr[0] == 'A' || TmpStr[0] == 'M' || TmpStr[0] == 'V')
							{
								if (TmpStr[0] == 'F')
								{
									NxtRfl = atoi(TmpStr.Mid(1, 3));
									Level = NxtRfl;
								}
							}
						}
					}
					else
						Pnt = FullPnt;
					pCurGeo = CGeo::GetAirportOrPoint(Pnt);
					if (pCurGeo)
					{
						if (i == 1 && pCurGeo->m_Name == m_FdpRoute[m_FdpRoute.GetUpperBound()].LongName)
						{
							if (fnd != -1)
							{
								m_FdpRoute[m_FdpRoute.GetUpperBound()].PlannedAlt = Level;
								m_FdpRoute[m_FdpRoute.GetUpperBound()].Alt = Level;
								m_XptFdpFix = m_FdpRoute.GetUpperBound();
								m_Xfl = Level;
								if (Level)
									CurPnt.AltSet = true;
								else
									CurPnt.AltSet = false;
							}
						}
						else
						{
							CurPnt.Name = pCurGeo->m_Name;
							CurPnt.LongName = pCurGeo->m_Name;
							CurPnt.Pos = pCurGeo->m_Pos;
							CurPnt.GeoPos = pCurGeo->m_Pos;
							CurPnt.Alt = Level;
							CurPnt.PlannedAlt = Level;
							CurPnt.PlannedSpd = 0;
							if (Level)
								CurPnt.AltSet = true;
							else
								CurPnt.AltSet = false;
							CurPnt.Added = false;
							int pos = m_FdpRoute.Add(CurPnt);
							if (fnd != -1)
							{
								m_XptFdpFix = pos;
								//	m_Xfl = Level;
							}
						}
					}
				}
				pCurGeo = CGeo::GetAirportOrPoint(m_Ades); // uncomment if [STAR] not filled
				if (pCurGeo && pCurGeo->m_Name != m_FdpRoute[m_FdpRoute.GetUpperBound()].LongName)
				{
					CurPnt.Name = pCurGeo->m_Name;
					CurPnt.LongName = pCurGeo->m_Name;
					CurPnt.Pos = pCurGeo->m_Pos;
					CurPnt.GeoPos = pCurGeo->m_Pos;
					CurPnt.Alt = 13;
					CurPnt.PlannedAlt = 13;
					CurPnt.PlannedSpd = 0;
					CurPnt.AltSet = true;
					CurPnt.Added = false;
					m_FdpRoute.Add(CurPnt);
				}
			}
		}
		else if ((m_XptFdpFix == -1) && (!IsInItaly) && (m_XptRose.IsEmpty()))
		{
			int SectNb = 0;
			for (int i = m_FdpRoute.GetUpperBound(); i > 0; i--)
			{
				if (m_Xfl == 0)
				{
					for (int j = m_FdpRoute.GetUpperBound(); j >= 0; j--)
					{
						if (m_FdpRoute[j].Alt != 0)
						{
							m_Xfl = m_FdpRoute[j].Alt;
							break;
						}
					}
				}
				CArea::GetSectorIntersect(m_FdpRoute[i].GeoPos, m_Xfl, m_FdpRoute[i - 1].GeoPos, m_Xfl, InterTable, transit);
				if (InterTable.GetSize())
				{
					SectNb = InterTable[0].SectorNb;
				}
				if (CArea::GetLocalFirIntersect(m_FdpRoute[i].GeoPos, m_Xfl, m_FdpRoute[i - 1].GeoPos, m_Xfl, InterTable))
				//if (CArea::GetFirIntersect(m_FdpRoute[i].GeoPos, m_Xfl, m_FdpRoute[i - 1].GeoPos, m_Xfl, InterTable, transit))
				{
					if (InterTable.GetSize())
					{
						for (int j = 0; j < InterTable.GetSize(); j++)
						{
							CurPnt.Name = CArea::GetFirSectorName(pApp->m_pOffline->m_SectorTable[SectNb].UAC, InterTable[j].Pos, m_Xfl);

							if (CurPnt.Name != "")
							{
								foundInItaly = true;
								CurPnt.LongName = CurPnt.Name;
								CurPnt.Alt = m_Xfl;
								CurPnt.PlannedAlt = m_Xfl;
								CurPnt.GeoPos = InterTable[0].Pos;
								CurPnt.Pos = InterTable[0].Pos;
								if (m_Xfl)
									CurPnt.AltSet = true;
								else
									CurPnt.AltSet = false;
								if (m_Xpt.IsEmpty())
									m_XCrossName = m_FdpRoute[i].LongName;
								else
									m_XCrossName = m_Xpt;
								m_FdpRoute.InsertAt(i, CurPnt);
								m_XptFdpFix = i;
									break;
							}
						}
						if (foundInItaly)
							break;
					}
				}
			}
		}
		bool Prio = false;

		bool foundXpt = false;
		if (!m_XptRose.IsEmpty())
		{
			for (i = 0; i < m_FdpRoute.GetSize(); i++)
			{
				if (m_XptRose.CompareNoCase(m_FdpRoute[i].LongName) == 0)
				{
					foundXpt = true;
					break;
				}
			}
			if (!foundXpt)
			{
				PointDef CurItem;
				CurItem.Alt = m_XflRose;
				CurItem.PlannedAlt = m_XflRose;
				CurItem.AltSet = true;
				CurItem.LongName = m_XptRose;
				CurItem.Name = m_XptRose;
				pCurGeo = CGeo::GetPoint(m_XptRose);
				if (pCurGeo)
				{
					CurItem.GeoPos = pCurGeo->m_Pos;
					CurItem.Pos = pCurGeo->m_Pos;
				}
				m_FdpRoute.InsertAt(count, CurItem);
				count++;
			}
		}

		if (IsArrival())
		{
			m_XptFdpFix = m_FdpRoute.GetUpperBound();
			m_Xpt = m_FdpRoute[m_XptFdpFix].LongName;
		}
		else
		{
			if (IsDept())
			{
				m_EptFdpFix = -1;
			}
			for (i = m_FdpRoute.GetSize() - 1; i >= 0; i--)
			{
				if (pApp->m_pDoc->IsExitPoint(m_FdpRoute[i].LongName) && m_FdpRoute[i].AltSet)
					//if ((CGeo::IsExitPoint(m_FdpRoute[i].LongName, Prio)))// || (m_FdpRoute[i].LongName==Ades))
				{
					//if (Ades == "LSGG" || Ades == "LSZH") // TODO check airport list
					m_XptFdpFix = i;
					m_Xpt = m_FdpRoute[i].LongName;
					//if (Prio || (m_FdpRoute[i].LongName == Ades))
					break;
				}
			}
		}

	}
	else
	{
		Error = 3;
	}
	for (i=0; i<m_FdpRoute.GetSize(); i++)
	{
		m_TmpFdpRoute.Add(m_FdpRoute[i]);
	}
	return (Error == 0);
}

void CFlightPlan::ComputeSectorList()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	CArray<PointDef, PointDef> FdpRoute;
	CArray<PointDef, PointDef> Intersection;
	CArray<IntersectDef, IntersectDef> InterTable;
	int CurLevel = m_Efl, i, j;
	bool InsideFir = false, FoundInside = false, ESec = false, XSec = false, ISec = false;
	CString IflSite1 = "", IflSite2 = "";
	PointDef CurPnt;
	//CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	CPoint pos;
	int EntrySector, ExitSector, ExitCurFix = -1, PreviousSect = 0;
	SectorListDef CurDef;
	CArray<SectorListDef, SectorListDef> SectorList;
	CString tmp, tmp1, Msg = "";

	m_TmpFdpRoute.RemoveAll();

	flightkinds FlightKind = transit;
	CGeo* pAdep = CGeo::GetAirport(m_Adep);
	if (pAdep && CArea::IsInsideLocalFir(pAdep->m_Pos))
		FlightKind = departure;
	CGeo* pAdes = CGeo::GetAirport(m_Ades);
	//if (pAdes && CArea::IsInsideLocalFir(pAdes->m_Pos))
	if (pAdes && IsArrival())
		FlightKind = arrival;
	/*if (m_pDoc->m_RouteInfo)
	{
		if (FlightKind == departure)
			tmp = "Flight " + m_Arcid + " is departure ";
		if (FlightKind == arrival)
			tmp = "Flight " + m_Arcid + " is arrival ";
		if (FlightKind == transit)
			tmp = "Flight " + m_Arcid + " is transit ";
		if (pAdep)
			tmp1.Format("Adep in Fir %d ", pAdep->m_IsFir);
		else
			tmp1 = "Adep not defined ";
		tmp += tmp1;
		if (pAdes)
			tmp1.Format("Ades in Fir %d ", pAdes->m_IsFir);
		else
			tmp1 = "Ades not defined ";
		tmp += tmp1;
		pApp->LogMsg(tmp);
	}*/

	CString SectorSite1 = "", SectorSite2 = "";
	for (i = 0; i < m_FdpRoute.GetUpperBound(); i++)
	{
		if (!m_FdpRoute[i].AltSet)
		{
			m_FdpRoute[i].PlannedAlt = 0;
			m_FdpRoute[i].Alt = 0;
		}
	}
	int StartIndex = 0;
/*	if (m_CurFdpFix != -1)
		StartIndex = m_CurFdpFix;

	if (StartIndex == 0)
	{
		if (m_pTrack && m_FdpRoute.GetSize())
		{
			if ((m_pTrack->GetPoint(0).x != -32767) || (m_pTrack->GetPoint(0).y != -32767))
			{
				if (m_FdpRoute[0].Name.CompareNoCase("~TRK") != 0)
				{
					m_FdpRoute.Add(m_FdpRoute[m_FdpRoute.GetUpperBound()]);
					for (i = m_FdpRoute.GetUpperBound(); i > 0; i--)
					{
						m_FdpRoute[i] = m_FdpRoute[i - 1];
					}
					RecomputeIndex(false, true);
				}
				m_FdpRoute[0].Name = "~TRK";
				m_FdpRoute[0].GeoPos = m_pTrack->GetPoint(0);
				m_FdpRoute[0].Pos = m_pTrack->GetPoint(0);
				m_FdpRoute[0].Alt = m_Efl;
				m_FdpRoute[0].PlannedAlt = m_Efl;
			}
		}
	}*/
	if (m_FdpRoute.GetSize())
	{
		if (CArea::IsInsideLocalFir2(m_FdpRoute[0].GeoPos))
		{
			InsideFir = true;
			CPoint pnt;
			//if (m_pTrack && ((m_pTrack->GetPoint(0).x != -32767) || (m_pTrack->GetPoint(0).y != -32767)))
			//	pnt = m_pTrack->GetPoint(0);
			//else
				pnt = m_FdpRoute[0].GeoPos;
			CurPnt.Name = "~EFIR";
			CurPnt.LongName = "~EFIR";
			CurPnt.GeoPos = pnt;
			CurPnt.Pos = pnt;
			CurPnt.Alt = m_Efl;
			CurPnt.PlannedAlt = m_Efl;
			m_TmpFdpRoute.Add(CurPnt);
			// JPM to be changed in wave2
		//	m_TmpFdpRoute.Add(m_FdpRoute[0]);
		}
	}
	int IntermediateSectorNb = 0;
	int IntermediateIndex = -1;
	CString PointName = "";
	//CString IntermediatePoint = "IRMAR/VEVAR";
	Intersection.RemoveAll();

	for (i = 0; i < m_FdpRoute.GetUpperBound(); i++)
	{
		InterTable.RemoveAll();
		if (CArea::GetSectorIntersect(m_FdpRoute[i].GeoPos, m_FdpRoute[i].Alt, m_FdpRoute[i + 1].GeoPos, m_FdpRoute[i + 1].Alt, InterTable, FlightKind))
		{
			if (InterTable.GetSize() >= 1)
			{
				for (j = 0; j < InterTable.GetSize(); j++)
				{
					if ((InterTable[j].SectorNb) && (InterTable[j].SectorNb != PreviousSect) && (PreviousSect == 0 || InterTable[j].Entry))
					{
						if (/*!ISec && */ESec)
						{
							IflSite1 = pApp->m_pDoc->GetSectorSite(PreviousSect);
							IflSite2 = pApp->m_pDoc->GetSectorSite(InterTable[j].SectorNb);
							//JPM
							if ((!IsDept() || !m_IptRose.IsEmpty()) && m_FoundEpt && !IflSite1.IsEmpty() && !IflSite2.IsEmpty() && IflSite1.CompareNoCase(IflSite2) != 0)
							{
								CurPnt.Name.Format("~SEC%d", IntermediateSectorNb);
								CurPnt.LongName.Format("~SEC%d", IntermediateSectorNb);
								IntermediateSectorNb += 1;
								ISec = true;
								CurPnt.GeoPos = InterTable[j].Pos;
								CurPnt.Pos = InterTable[j].Pos;
								CurPnt.Sector = InterTable[j].SectorNb;
								CurPnt.PntNb = i;
								if ((m_Sefl != 0) && (m_SeflSite.CompareNoCase(IflSite1) == 0))
								{
									CurPnt.Alt = m_Sefl;
									CurPnt.PlannedAlt = m_Sefl;
									if ((CGeo::IsIntermediatePoint(m_FdpRoute[i].Name) && (m_FdpRoute[i].AltSet)) /*|| (IntermediatePoint.Find(m_FdpRoute[i].Name) == 0)*/)
									{
										m_FdpRoute[i].Alt = m_Sefl;
										m_FdpRoute[i].PlannedAlt = m_Sefl;
									}
									else if ((CGeo::IsIntermediatePoint(m_FdpRoute[i + 1].Name) && (m_FdpRoute[i + 1].AltSet)) /*|| (IntermediatePoint.Find(m_FdpRoute[i + 1].Name) == 0)*/)
									{
										m_FdpRoute[i + 1].Alt = m_Sefl;
										m_FdpRoute[i + 1].PlannedAlt = m_Sefl;
									}
								}
								else
								{
									IntermediateIndex = -1;
									if ((CGeo::IsIntermediatePoint(m_FdpRoute[i].Name) && (m_FdpRoute[i].AltSet)) /*|| (IntermediatePoint.Find(m_FdpRoute[i].Name) == 0)*/)
									{
										CurPnt.Alt = m_FdpRoute[i].Alt;
										CurPnt.PlannedAlt = m_FdpRoute[i].Alt;
										IntermediateIndex = i;
										PointName = m_FdpRoute[i].Name;

									}
									else if ((CGeo::IsIntermediatePoint(m_FdpRoute[i + 1].Name) && (m_FdpRoute[i + 1].AltSet)) /*|| (IntermediatePoint.Find(m_FdpRoute[i + 1].Name) == 0)*/)
									{
										CurPnt.Alt = m_FdpRoute[i + 1].Alt;
										CurPnt.PlannedAlt = m_FdpRoute[i + 1].Alt;
										IntermediateIndex = i + 1;
										PointName = m_FdpRoute[i + 1].Name;
									}
									if ((CurPnt.Alt == 0) && (CurPnt.Name.CompareNoCase("~SEC0") == 0) && (IntermediateIndex != -1) /*&& (IntermediatePoint.Find(PointName) == 0)*/)
									{
										CurPnt.Alt = m_Efl;
										m_FdpRoute[IntermediateIndex].Alt = m_Efl;

									}
									else if ((CurPnt.Alt == 0) && (CurPnt.Name.CompareNoCase("~SEC1") == 0) && (IntermediateIndex != -1) /*&& (IntermediatePoint.Find(PointName) == 0)*/)
									{
										CurPnt.Alt = m_Xfl;
										m_FdpRoute[IntermediateIndex].Alt = m_Xfl;
									}
									else
									{
										CurPnt.Alt = m_XflRose;
									}
								}
								Intersection.Add(CurPnt);
							}
						}
						if (!ESec)
						{
							ESec = true;
							CurPnt.Name = "~ESEC";
							CurPnt.LongName = "~ESEC";
							CurPnt.Alt = m_Efl;
							CurPnt.PlannedAlt = m_Efl;
							CurLevel = m_Xfl;
							CurPnt.GeoPos = InterTable[0].Pos;
							CurPnt.Pos = InterTable[0].Pos;
							CurPnt.Sector = InterTable[0].SectorNb;
							CurPnt.PntNb = i;
							Intersection.Add(CurPnt);
						}
					}
					else if ((j == InterTable.GetUpperBound()))
					{
						CurPnt.Name = "~XSEC";
						CurPnt.LongName = "~XSEC";
						CurPnt.Alt = m_Xfl;
						CurPnt.PlannedAlt = m_Xfl;
						XSec = true;
						CurPnt.GeoPos = InterTable[j].Pos;
						CurPnt.Pos = InterTable[j].Pos;
						CurPnt.Sector = InterTable[j].SectorNb;
						CurPnt.PntNb = i;
					}
					PreviousSect = InterTable[j].SectorNb;
				}
			}
		}
		if (i == m_FdpRoute.GetUpperBound() - 1)
		{
			if (CurPnt.Name.CompareNoCase("~XSEC") == 0 && (FlightKind != arrival))
				Intersection.Add(CurPnt);
		}
	}

	for (i = 0; i < m_FdpRoute.GetUpperBound(); i++)
	{
		m_FdpRoute[i].PntNb = i;
	}

	bool bInserted = false;
	for (i = 0; i < m_FdpRoute.GetUpperBound(); i++)
	{
		if (CArea::GetLocalFirIntersect(m_FdpRoute[i].GeoPos, CurLevel, m_FdpRoute[i + 1].GeoPos, CurLevel, InterTable))
		{
			if (InterTable.GetSize() == 1)
			{
				if (InsideFir)
				{
					CurPnt.Name = "~XFIR";
					CurPnt.LongName = "~XFIR";
					InsideFir = false;
					CurPnt.Alt = m_Xfl;
					CurPnt.PlannedAlt = m_Xfl;
				}
				else
				{
					InsideFir = true;
					CurPnt.Name = "~EFIR";
					CurPnt.LongName = "~EFIR";
					CurPnt.Alt = m_Efl;
					CurPnt.PlannedAlt = m_Efl;
				}
				CurPnt.GeoPos = InterTable[0].Pos;
				CurPnt.Pos = InterTable[0].Pos;

				m_TmpFdpRoute.Add(CurPnt);
			}
			if (InsideFir)
			{
				for (j = 0; j < Intersection.GetSize(); j++)
				{
					if (Intersection[j].PntNb != -1 && Intersection[j].PntNb <= m_FdpRoute[i].PntNb)
					{
						Intersection[j].PntNb = -1;
						m_TmpFdpRoute.Add(Intersection[j]);
					}
				}
				InterTable.RemoveAll();
				if (m_Ept.CompareNoCase(m_FdpRoute[i].Name) == 0 && !bInserted /*&& !IsInsideRoute(m_FdpRoute[i].Name)*/)
				{
					m_TmpFdpRoute.Add(m_FdpRoute[i]);
					bInserted = true;
				}
				if (m_Ept.CompareNoCase(m_FdpRoute[i + 1].Name) != 0 /*&& !IsInsideRoute(m_FdpRoute[i + 1].Name)*/)
					m_TmpFdpRoute.Add(m_FdpRoute[i+1]);
			}
		}
	}

	CurDef.SectorType = ' ';
	CurDef.EntryFix = -1;
	CurDef.ExitFix = -1;
	if (m_TmpFdpRoute.GetSize() == 0)
	{
		Msg="No Fir intersection\r\n";
		m_SequenceError += Msg;
		return;
	}

	//CO4DMngr::ManageTmpFplTrajectory(this);
	//if (m_ManualSequence)
	//	return;
	//if (m_CurFix > m_XptFix && m_XptFix != -1)
	//	return;

	PointDef TmpPoint;
	CArray<IntersectDef, IntersectDef> FullInterTable;
	CArray<IntersectDef, IntersectDef> InterAoiTable;
	CArray<IntersectDef, IntersectDef> FullInterAoiTable;
	int FirstPnt = 0;


	int LastPnt = m_TmpFdpRoute.GetUpperBound();
	pos = m_TmpFdpRoute[0].GeoPos;

	for (i = 0; i < m_TmpFdpRoute.GetSize(); i++)
	{
		FdpRoute.Add(m_TmpFdpRoute[i]);
	}

	// Look for the Ept, Ipt and Xpt fixes
	// Look for ESecFix, SecFixes and XSecFix
	int EptFix = -1, XptFix = -1;
	int ESecFix = -1, XSecFix = -1, TelnoFix = -1;
	bool bFoundEntry = false, bFoundExit = false;
	CArray <int, int> IptFix;
	CArray <int, int> ISecFix;
	int indexNb1 = 0, indexNb2 = 0;
	CString sISec = "";
	int IptFixIndex = -1;

	for (i = 0; i < FdpRoute.GetSize(); i++)
	{
		if (pApp->m_pDoc->IsEntryPoint(FdpRoute[i].Name) && FdpRoute[i].AltSet && !bFoundEntry)
		{
			EptFix = i;
			FdpRoute[i].Alt = m_Efl;
			bFoundEntry = true;
		}
		else if ((CGeo::IsIntermediatePoint(FdpRoute[i].Name) && FdpRoute[i].AltSet && FdpRoute[i].Name.CompareNoCase(m_Ept) != 0 && FdpRoute[i].Name.CompareNoCase(m_Xpt) != 0) /*|| (IntermediatePoint.Find(FdpRoute[i].Name) == 0)*/)
		{
			if (indexNb1 < IntermediateSectorNb)
			{
				IptFix.Add(i);
				indexNb1++;
			}
			IptFixIndex = i;
		}
		else if (pApp->m_pDoc->IsExitPoint(FdpRoute[i].Name) && FdpRoute[i].AltSet && !bFoundExit)
		{
			XptFix = i;
			if (FdpRoute[i].Alt == 0)
				FdpRoute[i].Alt = m_Xfl;
			bFoundExit = true;
		}

		if ((FdpRoute[i].Name.CompareNoCase("~ESEC") == 0))
		{
			ESecFix = i;
		}

		if ((FdpRoute[i].Name.CompareNoCase("~XSEC") == 0))
		{
			XSecFix = i;
		}

		if (indexNb2 < IntermediateSectorNb)
		{
			sISec.Format("~SEC%d", indexNb2);
			if ((FdpRoute[i].Name.CompareNoCase(sISec) == 0))
			{
				ISecFix.Add(i);
				indexNb2++;
			}
		}

		/*if ((FdpRoute[i].Name.CompareNoCase("LSGG") == 0))
		FdpRoute[i].Alt = 13;*/
	}

	if (IptFix.GetSize() > 2)
	{
		Msg.Format("Route has more than 2 IptFix\r\n");
		m_SequenceError += Msg;
	//	pApp->LogMsg(Msg);
	}
	if (ISecFix.GetSize() > 2)
	{
		Msg.Format("Route has more than 2 ISecFix\r\n");
		m_SequenceError += Msg;
		//	pApp->LogMsg(Msg);
	}
	if ((IptFix.GetSize() != ISecFix.GetSize()) && !IsArrival() && !IsDept() && pApp->m_pOffline->m_IptUseless.CompareNoCase(m_Ept)==-1)
	{
		Msg.Format("IptFix size=%d ISecFix size=%d\r\n", IptFix.GetSize(), ISecFix.GetSize());
		m_SequenceError += Msg;
	//	pApp->LogMsg(Msg);
	}

	// Fill the altitude to the EFL from the beginning of the route until EptFix or EntrySectorFix
	int EndEntryFix = 0;
	if (((EptFix != -1) && (ESecFix != -1) && (EptFix > ESecFix)) || ((ESecFix == -1) && (EptFix != -1)))
	{
		EndEntryFix = EptFix;
		for (i = 0; i <= EptFix; i++)
		{
			if (FdpRoute[i].Alt == 0)
				FdpRoute[i].Alt = m_Efl;
		}
	}
	if (((EptFix != -1) && (ESecFix != -1) && (ESecFix > EptFix)) || ((EptFix == -1) && (ESecFix != -1)))
	{
		EndEntryFix = ESecFix;
		for (i = 0; i <= ESecFix; i++)
		{
			if (FdpRoute[i].Alt == 0)
				FdpRoute[i].Alt = m_Efl;
		}
	}

	// Fill the altitude to the XFL from the XPTFix or ExitSectorFix of the route until the end
	int StartExitFix = FdpRoute.GetSize() - 1;
	if (!IsArrival())
	{
		if (((XptFix != -1) && (XSecFix != -1) && (XptFix < XSecFix)) || ((XSecFix == -1) && (XptFix != -1)))
		{
			StartExitFix = XptFix;
			for (i = XptFix; i < FdpRoute.GetSize(); i++)
			{
				if (FdpRoute[i].Alt == 0)
					FdpRoute[i].Alt = m_Xfl;
			}
		}
		if (((XptFix != -1) && (XSecFix != -1) && (XSecFix < XptFix)) || ((XptFix == -1) && (XSecFix != -1)))
		{
			StartExitFix = XSecFix;
			for (i = XSecFix; i < FdpRoute.GetSize(); i++)
			{
				if (FdpRoute[i].Alt == 0)
					FdpRoute[i].Alt = m_Xfl;
			}
		}
	}
	else if (FdpRoute[StartExitFix].Alt == 0)
		FdpRoute[StartExitFix].Alt = m_Xfl;

	// There is no crossborder (IntermediateSectorFix==-1). Fill the altitude to the XFL from EntrySectorFix/EptFix+1 and ExitSectorFix/XptFix-1
	bool found = false, NotFound=false;
	int IntermediateSecFix = 0, IntermediateSecFix2 = 0;
	if ((ISecFix.GetSize() == 0) && !IsDept())
	{
		for (i = EndEntryFix + 1; i < StartExitFix; i++)
		{
			if (!NotFound)
			{
				CArea::GetSectorIntersect(FdpRoute[i].Pos, FdpRoute[i].Alt, FdpRoute[i + 1].Pos, FdpRoute[i + 1].Alt, InterTable, FlightKind);
				if (InterTable.GetSize() > 0)
				{
					for (int k = 0; k < InterTable.GetSize(); k++)
					{
						if (InterTable[k].SectorNb == 8)
							found = true;
						else
						{
							found = false;
							NotFound = true;
							break;
						}
					}
				}
			}
			//JPM
			if ((i == StartExitFix - 1) && (!FdpRoute[i].AltSet))
			{
				FdpRoute[i].Alt = m_Xfl;
			}
			else
			{
				if ((FdpRoute[i].Alt == 0) && (!found))
					if (m_Efl > m_Xfl)
						FdpRoute[i].Alt = m_Efl;
					else
						FdpRoute[i].Alt = m_Xfl;
				else if ((FdpRoute[i].Alt == 0) && (found))
					FdpRoute[i].Alt = m_Efl;
			}
		}
	}
	else if (!IsDept())
	{
		for (i = 0; i < ISecFix.GetSize(); i++)
		{

			if (i == 0)
			{
				if (IptFix.GetSize() && (IptFix[i]>ISecFix[i]))
					IntermediateSecFix = IptFix[i];
				else
					IntermediateSecFix = ISecFix[i];
				for (j = EndEntryFix + 1; j < IntermediateSecFix; j++)
				{
					if (j == EndEntryFix + 1)
					{
						//if (m_Efl > m_Xfl)
						if (FdpRoute[j].Alt == 0)
							FdpRoute[j].Alt = m_Efl;
						//else
						//	FdpRoute[j].Alt = FdpRoute[IntermediateSecFix].Alt;
					}
					else
						FdpRoute[j].Alt = FdpRoute[IntermediateSecFix].Alt;
				}
			}
			if (i != 0)
			{
				if (i<IptFix.GetSize() && (IptFix[i]>ISecFix[i]))
					IntermediateSecFix2 = IptFix[i];
				else
					IntermediateSecFix2 = ISecFix[i];
				for (j = IntermediateSecFix + 1; j < IntermediateSecFix2; j++)
				{
					if ((j == IntermediateSecFix + 1) && (FdpRoute[j].GeoPos.x == FdpRoute[IntermediateSecFix].GeoPos.x) && (FdpRoute[j].GeoPos.y == FdpRoute[IntermediateSecFix].GeoPos.y))
					{
						if (FdpRoute[j].Alt == 0)
							FdpRoute[j].Alt = FdpRoute[IntermediateSecFix].Alt;
					}
					else
					{
						if (FdpRoute[j].Alt == 0)
							FdpRoute[j].Alt = FdpRoute[IntermediateSecFix2].Alt;
					}
				}
			}
			if (i == ISecFix.GetSize() - 1)
			{
				if (i<IptFix.GetSize() && (IptFix[i]>ISecFix[i]))
					IntermediateSecFix = IptFix[i];
				else
					IntermediateSecFix = ISecFix[i];
				for (j = IntermediateSecFix + 1; j < StartExitFix; j++)
				{
					//JPM
					if ((j == StartExitFix - 1) && (!FdpRoute[j].AltSet))
					{
						if (m_Xfl)
							FdpRoute[j].Alt = m_Xfl;
						else
							FdpRoute[j].Alt = m_XflRose;
					}
					//JPM
					else if (!FdpRoute[j].AltSet)
						FdpRoute[j].Alt = FdpRoute[StartExitFix].Alt;
				}
			}
		}
	}
	//JPM
	else if (IsDept())
	{
		if (IptFixIndex > -1)
		{
			for (i = EndEntryFix + 1; i < IptFixIndex; i++)
				FdpRoute[i].Alt = FdpRoute[IptFixIndex].Alt;
			for (i = IptFixIndex + 1; i < StartExitFix; i++)
				FdpRoute[i].Alt = FdpRoute[StartExitFix].Alt;
		}
		else if (IptFixIndex == -1)
		{
			for (i = EndEntryFix + 1; i < StartExitFix; i++)
			{
				if (FdpRoute[i].Alt == 0)
				{
					if (m_Efl > m_Xfl)
						FdpRoute[i].Alt = m_Efl;
					else
						FdpRoute[i].Alt = m_Xfl;
				}
			}
		}
	}

	if ((EndEntryFix + 1) < FdpRoute.GetSize())
	{
		if (FdpRoute[EndEntryFix].Pos == FdpRoute[EndEntryFix + 1].Pos)
			FdpRoute[EndEntryFix + 1].Alt = FdpRoute[EndEntryFix].Alt;
	}
	int dist1 = -1, dist2 = -1;
	if ((EptFix != -1) && (EptFix + 3 <FdpRoute.GetSize()) && (!IsArrival()) && (!IsDept()))
	{
		if ((FdpRoute[EptFix + 1].Name.CompareNoCase("~ESEC") == 0) && (FdpRoute[EptFix + 2].Name.CompareNoCase("~XSEC") != 0))
			//dist1 = sqrt((FdpRoute[EptFix+1].Pos.x - FdpRoute[EptFix].Pos.x)*(FdpRoute[EptFix + 1].Pos.x - FdpRoute[EptFix].Pos.x) + (FdpRoute[EptFix + 1].Pos.y - FdpRoute[EptFix].Pos.y)*(FdpRoute[EptFix + 1].Pos.y - FdpRoute[EptFix].Pos.y));
			dist1 = CalculDistance(FdpRoute[EptFix + 2].Pos, FdpRoute[EptFix + 1].Pos);
		if ((FdpRoute[EptFix + 3].Name.CompareNoCase("~XSEC") == 0) && (FdpRoute[EptFix + 2].Name.CompareNoCase("~ESEC") != 0))
			//dist2 = sqrt((FdpRoute[EptFix + 2].Pos.x - FdpRoute[EptFix+1].Pos.x)*(FdpRoute[EptFix + 2].Pos.x - FdpRoute[EptFix+1].Pos.x) + (FdpRoute[EptFix + 2].Pos.y - FdpRoute[EptFix+1].Pos.y)*(FdpRoute[EptFix + 2].Pos.y - FdpRoute[EptFix+1].Pos.y));
			dist2 = CalculDistance(FdpRoute[EptFix + 3].Pos, FdpRoute[EptFix + 2].Pos);

		if ((dist1 != -1) && (dist2 != -1))
		{
			int sectorNb = CArea::GetSectorNb(FdpRoute[EptFix].GeoPos, FdpRoute[EptFix].Alt);
			if ((sectorNb == 0) || (sectorNb == 8))
			{
				for (i = EptFix + 1; i <= EptFix + 2; i++)
				{
					if ((FdpRoute[i].Name.CompareNoCase("~SEC0") == 0) || (FdpRoute[i].Name.CompareNoCase("~XSEC") == 0))
						break;
					if ((FdpRoute[i].Name.CompareNoCase("~ESEC") != 0) && ((IptFix.GetSize() == 0) || (IptFix.GetSize() && (FdpRoute[i].Name.CompareNoCase(FdpRoute[IptFix[0]].Name) != 0))))
					{
						if (dist1 < dist2)
							FdpRoute[i].Alt = FdpRoute[EptFix].Alt;
						else
							FdpRoute[i].Alt = FdpRoute[EptFix + 2].Alt;
						break;
					}
				}
			}
		}
	}

	/*if ((SecFix != -1) && (ESecFix != -1) && (SecFix > ESecFix))
	{
	for (j = ESecFix + 1; j < SecFix; j++)
	{
	if ((FdpRoute[j].Alt != m_Efl) && (m_Sefl != 0))
	FdpRoute[j].Alt = m_Sefl;
	else if (FdpRoute[j].Alt == 0)
	FdpRoute[j].Alt = FdpRoute[SecFix].Alt;
	}
	}*/



	/*if ((ESecFix != -1) && (EptFix == -1))
	{
	if (SecFix == -1)
	FdpRoute[ESecFix + 1].Alt = m_Xfl;
	}*/
	/*for (i = 0; i < FdpRoute.GetSize(); i++)
	{
	if ((FdpRoute[i].Name.CompareNoCase("TELNO") == 0) && m_Ades.CompareNoCase("LSZB") == 0)
	{
	FdpRoute[i].Alt = 100;
	TelnoFix = i;
	}
	}*/



	/*if ((IptFix != -1) && (SecFix != -1))
	{
	if (FdpRoute[SecFix].Alt==0)
	FdpRoute[SecFix].Alt = FdpRoute[IptFix].Alt;
	}
	else if ((TelnoFix!=-1) && (SecFix!=-1))
	{
	if (FdpRoute[SecFix].Alt == 0)
	FdpRoute[SecFix].Alt = FdpRoute[TelnoFix].Alt;
	}*/

	// Set the proper altitude if the XFL has been changed
	int TOCDFix = -1;


	/*if ((IptFix.GetSize() == 0) && (SecFix != -1) && (XSecFix != -1))
	{
	if (FdpRoute[SecFix].Alt == 0)
	FdpRoute[SecFix].Alt = FdpRoute[XSecFix].Alt;
	}*/



	// Set the proper XFL associated to the Exit point and propagate it until the exit sector point (~XSEC)

	/*if (SecFix != -1)
	{
	int ExitFix = -1;
	if (XptFix != -1)
	ExitFix = XptFix;
	else
	ExitFix = XSecFix;
	if ((ExitFix != -1) && (SecFix < ExitFix))
	{
	for (j = SecFix + 1; j < ExitFix; j++)
	{
	if (FdpRoute[j].Alt == 0)
	FdpRoute[j].Alt = m_Xfl;
	}
	}
	}
	else
	{

	}*/

	/*if ((XptFix != -1) && (XSecFix != -1) && (XptFix < XSecFix))
	{
	for (j = XptFix; j<XSecFix; j++)
	FdpRoute[j].Alt = m_Xfl;
	}*/
	/*for (i = 0; i < FdpRoute.GetSize(); i++)
	{
	FdpRoute[i].PntNb = i;
	if ((FdpRoute[i].Name.CompareNoCase("~TOC") == 0) || (FdpRoute[i].Name.CompareNoCase("~TOD") == 0))
	TOCDFix = i;

	}*/
	/*if ((XSecFix != -1) && (ESecFix!=-1) && (TOCDFix !=-1) && (SecFix==-1) && TOCDFix<XSecFix && TOCDFix>ESecFix)
	{
	for (j = ESecFix; j<TOCDFix; j++)
	FdpRoute[j].Alt = m_Efl;
	for (j = TOCDFix; j<XptFix; j++)
	FdpRoute[j].Alt = m_Xfl;
	}
	else if (XSecFix == -1)
	{
	if (SecFix != -1)
	{
	for (j = SecFix; j < FdpRoute.GetSize(); j++)
	{
	if (FdpRoute[j].Alt==0)
	FdpRoute[j].Alt = m_Xfl;

	}
	}
	else if (ESecFix !=-1)
	{
	for (j = ESecFix; j < FdpRoute.GetSize(); j++)
	{
	if (FdpRoute[j].Alt == 0)
	FdpRoute[j].Alt = m_Xfl;
	}
	}
	}
	if (XptFix != -1 && XSecFix == -1)
	{
	for (j = XptFix; j < FdpRoute.GetSize(); j++)
	{
	if (FdpRoute[j].Alt == 0)
	FdpRoute[j].Alt = m_Xfl;
	}
	}*/
	if (FdpRoute.GetSize() && !IsArrival())
	{
		CString Msg1 = "", Msg2 = "";
		Msg.Format("SEQUENCE ISSUE ARCID=%s ", m_Arcid);
		for (i = 0; i < FdpRoute.GetSize(); i++)
		{
			if (FdpRoute[i].Alt == 0)
			{
				Msg1.Format("PointName=%s Alt=%d ", FdpRoute[i].LongName, FdpRoute[i].Alt);
				Msg2 += Msg1;
			}
		}
		if (!Msg2.IsEmpty())
		{
			Msg = Msg2 + "\r\n";
			m_SequenceError += Msg;
		}
		//	pApp->LogMsg(Msg + Msg2);

	}
	if ((TOCDFix != -1) && IsArrival() && (FdpRoute.GetSize() >= 3) && (TOCDFix == FdpRoute.GetSize() - 2))
	{
		FdpRoute[TOCDFix].Alt = FdpRoute[TOCDFix - 1].Alt;
	}
	//CO4DMngr::ManageTmpFplTrajectory(this);
	/*if (m_pDoc->m_RouteInfo)
	{
		if (FdpRoute.GetSize())
		{
			CString str1, str3;
			CString str = "Internal Rte for " + m_Arcid + " ";
			for (i = 0; i < FdpRoute.GetSize(); i++)
			{
				str3.Format("Name=%s Alt=%d X=%d Y=%d ", FdpRoute[i].Name, FdpRoute[i].Alt, FdpRoute[i].GeoPos.x, FdpRoute[i].GeoPos.y);
				str += str3;
			}
			str1.Format(" Efl=%d Xfl=%d", m_Efl, m_Xfl);
			pApp->LogMsg(str + str1);
		}
	}*/

	CString message;
	LastPnt = FdpRoute.GetUpperBound();
	for (i = FirstPnt; i < LastPnt; i++)
	{
		//debug message franck pour la v2 7/12/17
		InterTable.RemoveAll();
		int index = 0;
		CArea::GetSectorIntersect(FdpRoute[i].Pos, FdpRoute[i].Alt, FdpRoute[i + 1].Pos, FdpRoute[i + 1].Alt, InterTable, FlightKind);
		for (j = 0; j < InterTable.GetSize(); j++)
		{
			InterTable[j].FixNb = i;
			FullInterTable.Add(InterTable[j]);
			//message.Format("FullInterTable[%d].SectorNb=%d FullInterTable[%d].Entry=%d FullInterTable[%d].Alt=%d FdpRoute[%d].Name=%s FdpRoute[%d].Alt=%d FdpRoute[%d].Name=%s FdpRoute[%d].Alt=%d", j, FullInterTable[j].SectorNb, j, FullInterTable[j].Entry, j, FullInterTable[j].Alt, i, FdpRoute[i].Name, i, FdpRoute[i].Alt, i+1, FdpRoute[i+1].Name, i+1, FdpRoute[i+1].Alt);
			//pApp->LogMsg(message);
		}
		CArea::GetSectorAoiIntersect(FdpRoute[i].Pos, FdpRoute[i].Alt, FdpRoute[i + 1].Pos, FdpRoute[i + 1].Alt, InterAoiTable, FlightKind);
		for (j = 0; j < InterAoiTable.GetSize(); j++)
		{
			InterAoiTable[j].FixNb = i;
			if (InterAoiTable[j].Entry)
				FullInterAoiTable.Add(InterAoiTable[j]);
		}
	}

	int PrvSect = 0;
	CString SectList;
	if ((PrvSect))
	{
		CurDef.Pnt = FdpRoute[FirstPnt].Pos;
		CurDef.FixNb = 0;
		CurDef.Alt = m_Efl;
		CurDef.SectorNb = PrvSect;
		CurDef.EntryTime = m_Etn;
		SectorList.Add(CurDef);
	}
	for (int j = 0; j < FullInterTable.GetSize(); j++)
	{
		//message.Format("FullInterTable[%d].SectorNb=%d PrvSect=%d FullInterTable[%d].Entry=%d FullInterTable[%d].Alt=%d", j, FullInterTable[j].SectorNb, PrvSect, j, FullInterTable[j].Entry, j, FullInterTable[j].Alt);
		//pApp->LogMsg(message);
		if ((FullInterTable[j].SectorNb) && (FullInterTable[j].SectorNb != PrvSect) && (PrvSect == 0 || FullInterTable[j].Entry))
		{
			//InterTable.Add(FullInterTable[j]);
			CurDef.Pnt = FullInterTable[j].Pos;
			CurDef.Alt = FullInterTable[j].Alt;
			CurDef.FixNb = FullInterTable[j].FixNb;
			CurDef.SectorNb = FullInterTable[j].SectorNb;
			CurDef.EntryTime = FdpRoute[CurDef.FixNb].To + (FullInterTable[j].t*(FdpRoute[CurDef.FixNb + 1].To - FdpRoute[CurDef.FixNb].To).GetTotalSeconds());
			//			CString tmp2;
			//			tmp2.Format("%.2f",FullInterTable[j].t);
			//			CString tmp=m_OrigRoute[CurDef.FixNb].PointName+" "+m_OrigRoute[CurDef.FixNb].Eto.Format("%H:%M:%S")+" "+m_OrigRoute[CurDef.FixNb+1].PointName+" t="+tmp2+" time="+m_OrigRoute[CurDef.FixNb+1].Eto.Format("%H:%M:%S")+" "+CurDef.EntryTime.Format("%H:%M:%S");
			//			CFdpApp* pApp = (CFdpApp*)AfxGetApp();
			//			pApp->LogMsg(tmp);
			SectorList.Add(CurDef);
			PrvSect = FullInterTable[j].SectorNb;
		}
	}

	PrvSect = 0;
	Msg = "";
	Msg.Format("Distri List %s", m_Arcid);

	bool AutoSkip = false;
	for (j = 0; j < SectorList.GetSize(); j++)
	{
		if (SectorList[j].SectorType == ' ')
		{
			if (PrvSect == 0)
			{
				if (j != 0)
					int merd = 7;
				if (AutoSkip)
				{
					SectorList[j].SectorType = 'i';
					//					m_InfoSectors[m_SectorList[j].SectorNb]=2;
					AutoSkip = false;
				}
				else
					SectorList[j].SectorType = 'e';
			}
			else
				SectorList[j].SectorType = pApp->m_pDoc->GetAdjSectorType(PrvSect, SectorList[j].SectorNb);
		}
		if (SectorList[j].SectorType == 's')
		{
			SectorList[j].Alt = ((SectorList[j].Alt + 5) / 10) * 10;
			SectorList[j].XptList = pApp->m_pDoc->GetAdjSectorXptList(PrvSect, SectorList[j].SectorNb);
		}
		else
		{
			//SectorList[j].Alt = 0;
			SectorList[j].Alt = ((SectorList[j].Alt + 5) / 10) * 10;
			SectorList[j].XptList.Empty();
		}
		//		if (m_InfoSectors[m_SectorList[j].SectorNb]!=2)
		PrvSect = SectorList[j].SectorNb;
		//		if (j<m_SectorList.GetUpperBound())
		//		{
		//			float dist=CalculDistance(m_SectorList[j].Pnt,m_SectorList[j+1].Pnt);
		//			if (dist<5.0*32)
		//				m_InfoSectors[PrvSect]=2;
		//		}	
		//Msg+=" "+m_pFplMngr->GetSectorName(m_SectorList[j].SectorNb)+m_SectorList[j].SectorType;
	}
	for (i = 0; i < MaxSeqNb; i++)
	{
		m_Sequence[i] = 0;
		m_FullSequence[i] = 0;
		m_Ifl[i] = 0;
		m_IflCentre = 0;
		m_Xpts[i].Empty();
		//m_Xtns[i] = 0;
	}
	for (j = 0; j < SectorList.GetSize(); j++)
	{
		m_Sequence[j] = SectorList[j].SectorNb;
		m_SeqType[j] = SectorList[j].SectorType;
		if (m_SeqType[j] == 's')
		{
			for (int r = 0; r < m_FdpRoute.GetSize(); r++)
			{
				if ((SectorList[j].XptList.Find("/" + m_FdpRoute[r].LongName + "/") != -1) && (m_FdpRoute[r].AltSet)&& (!m_IptRose.IsEmpty()) && ((m_IptRose.CompareNoCase(m_FdpRoute[r].LongName)==0) /*|| (IntermediatePoint.Find(m_FdpRoute[r].LongName)!=-1)*/))
				{
					m_Xpts[j - 1] = m_FdpRoute[r].LongName;
					m_Ifl[j - 1] = m_FdpRoute[r].Alt;
					m_IflCentre = m_Ifl[j - 1];
					break;
				}
			}
		}
		m_FullSequence[j] = SectorList[j].SectorNb;
	}

	CString site = "", sectName = "";
	int sect;
	int sect1 = m_Sequence[0];
	CString AdesList = "LSGG/LSGS";
	bool fnd = false;
	for (i = 0; i < MaxSeqNb; i++)
	{
		sect = m_Sequence[i];
		for (j = 0; j < pApp->m_pOffline->m_SectorTable.GetSize(); j++)
		{
			if ((sect1 == j) && (i == 0))
			{
				site = pApp->m_pOffline->m_SectorTable[j].Site;
				break;
			}
			if (sect == j)
			{
				sectName = pApp->m_pOffline->m_SectorTable[j].UAC;
				fnd = AdesList.Find(m_Ades) != -1;
				if ((fnd) && (site.CompareNoCase("ZRH") == 0) && (sectName.CompareNoCase("L1") == 0))
				{
					m_SeqType[i] = 'a';
				}
			}

		}
	}
	m_IptCentre.IsEmpty();
	m_IflCentre = 0;
	int count = 0;
	for (i = 0; i < MaxSeqNb; i++)
	{
		if (!m_Xpts[i].IsEmpty())
			count++;
	}
	for (i = 0; i < MaxSeqNb; i++)
	{
		if (count > 1)
		{
			if (!m_Xpts[i].IsEmpty() /*&& (IntermediatePoint.Find(m_Xpts[i]) == -1)*/)
			{
				m_IptCentre = m_Xpts[i];
				m_IflCentre = m_Ifl[i];
			}
		}
		else
		{
			if (!m_Xpts[i].IsEmpty())
			{
				m_IptCentre = m_Xpts[i];
				m_IflCentre = m_Ifl[i];
			}
		}
	}
	if (FdpRoute.GetSize())
	{
		Msg = "";
		for (i = 0; i<MaxSeqNb; i++)
		{
			if (m_Sequence[i] == 0)
				break;
			if (i + 1 < MaxSectorNb && m_Sequence[i + 1] != 0)
			{
				if (pApp->m_pDoc->GetAdjSectorType(m_Sequence[i], m_Sequence[i + 1]) == ' ')
				{
					Msg.Format("SOME SECTORS are not adjacent ");
					for (i = 0; i < SectorList.GetSize(); i++)
						Msg += pApp->m_pOffline->m_SectorTable[SectorList[i].SectorNb].UAC + " ";
					Msg = Msg + "\r\n";
					m_SequenceError += Msg;
					//pApp->LogMsg(Msg);
				}
			}
		}

		CString Msg1;
		Msg = "";
		for (i = 0; i < FdpRoute.GetSize(); i++)
		{
			Msg1.Format("PointName=%s Alt=%d X=%ld Y=%ld\r\n", FdpRoute[i].LongName, FdpRoute[i].Alt, FdpRoute[i].Pos.x, FdpRoute[i].Pos.y);
			Msg += Msg1;
		}
		m_RouteInformation += Msg;

		int EntrySectorInSeq = GetEntrySector();
		int EntrySector = 0;
		for (int i = 0; i < FdpRoute.GetSize(); i++)
		{
			if (pApp->m_pDoc->IsEntryPoint(FdpRoute[i].LongName) && FdpRoute[i].AltSet)
			{
				EntrySector = CArea::GetSectorNb(FdpRoute[i].GeoPos, m_Efl);
				if (EntrySector)
					break;
			}
		}
		int ExitSectorInSeq = GetExitSector();
		int ExitSector = 0;
		for (i = FdpRoute.GetUpperBound(); i >= 0; i--)
		{
			if (pApp->m_pDoc->IsExitPoint(FdpRoute[i].LongName) && FdpRoute[i].AltSet)
			{
				ExitSector = CArea::GetSectorNb(FdpRoute[i].GeoPos, FdpRoute[i].Alt);
				if (ExitSector)
					break;
			}
		}
		//if ((EntrySectorInSeq != EntrySector) && (EntrySector) && EntrySectorInSeq)
		{
		//	Msg.Format("Entry Sector (%d) is different from Entry sector in sequence (%d)\r\n", EntrySector, EntrySectorInSeq);
		//	m_SequenceError += Msg;
			//pApp->LogMsg(Msg);
		}
		//if ((ExitSectorInSeq != ExitSector)) //&& (ExitSector) && ExitSectorInSeq)
		{
		//	Msg.Format("Exit Sector (%d) is different from Exit sector in sequence (%d)\r\n", ExitSector, ExitSectorInSeq);
		//	m_SequenceError += Msg;
			//pApp->LogMsg(Msg);
		}

		Msg = "";
		for (i = 0; i < MaxSeqNb; i++)
		{
			for (j = i + 1; j < MaxSeqNb; j++)
			{
				if ((m_Sequence[i] == m_Sequence[j]) && (m_Sequence[i] != 0) && (m_Sequence[j] != 0))
				{
					Msg.Format("Duplicate sector=%s\r\n", pApp->m_pOffline->m_SectorTable[m_Sequence[i]].UAC);
					m_SequenceError += Msg;
					//pApp->LogMsg(Msg);
				}
			}
		}
		if (!Msg.IsEmpty())
			m_SequenceError += "\r\n";

	}
	//if (m_pDoc->m_SeqInfo)
	{
		CString msglog = "";
		//msglog.Format("Sequence for %s ", m_Arcid);
		for (i = 0; i < SectorList.GetSize(); i++)
			msglog += pApp->m_pOffline->m_SectorTable[SectorList[i].SectorNb].UAC + " ";
		m_Seq = msglog;
		CString str = "";
		str.Format(" EFL=%d XFL=%d", m_Efl, m_Xfl);
		msglog += str;
		//pApp->LogMsg(msglog);
	}
}

int CFlightPlan::GetEntrySector()
{

	if (m_Sequence[1] && m_SeqType[0] == 'a')
	{
		return m_Sequence[1];
	}
	return m_Sequence[0];
}

int CFlightPlan::GetExitSector()
{
	for (int i = MaxSeqNb - 1; i >= 0; i--)
	{
		if (m_Sequence[i])
			return m_Sequence[i];
	}
	return 0;
}






