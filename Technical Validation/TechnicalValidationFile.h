#pragma once
#include "afx.h"

enum FileType
{
	NONE_TYPE,
	AFTN,
	LATLONG,
};

class CTechnicalValidationFile :
	public CStdioFile
{
public:
	CTechnicalValidationFile();
	CTechnicalValidationFile(CString FileName, FileType fileType);
	~CTechnicalValidationFile();
	bool Open(CString FileName, FileType fileType);
	void Close();
	bool IsOpen();

protected:
	CStdioFile m_file;
private:
	CArray<CString, CString> m_StringTable;
	bool m_Modified;
	bool m_Open;
};

