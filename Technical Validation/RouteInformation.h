#pragma once
#include "afxwin.h"
#include "resource.h"

class CRouteInformation :
	public CDialog
{
public:
	CRouteInformation(CWnd* pParent = NULL);   // standard constructor
	~CRouteInformation();

	// Dialog Data
	//{{AFX_DATA(CRouteInformation)
	enum { IDD = IDD_ROUTE_INFORMATION };
	CString	m_RouteInformation;
	CString m_Arcid;
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRouteInformation)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
														//}}AFX_VIRTUAL

														// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRouteInformation)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

