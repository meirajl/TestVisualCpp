#include "StdAfx.h"
#include "StringUtil.h"

#pragma warning(disable:4996) 

int NStringUtil::GetNbrOfArgument(CString str,char Separator)
{
	int NbArg=0,Fnd=-1;
	CString ArgStr=str;
	ArgStr.TrimLeft();
	ArgStr.TrimRight();
	if (ArgStr.GetLength()==0)
		return 0;
	if (ArgStr[ArgStr.GetLength()-1]!=Separator)
		ArgStr+=Separator;
	do
	{
		Fnd=ArgStr.Find(Separator,Fnd+1);
		if (Fnd==-1)
			break;
		NbArg++;
	}
	while(1);
	return NbArg;
}

bool NStringUtil::GetArgumentNb(CString str,int ArgNb,char Separator,CString& Arg,bool trim)
{
	int Fnd=-1,Nxt;
	Arg.Empty();
	CString ArgStr=str;
	ArgStr.TrimLeft();
	ArgStr.TrimRight();
	if (ArgStr[ArgStr.GetLength()-1]!=Separator)
		ArgStr+=Separator;
	for (int i=1;i<ArgNb;i++)
	{
		Fnd=ArgStr.Find(Separator,Fnd+1);
		if (Fnd==-1)
			return false;
	}
	Nxt=ArgStr.Find(Separator,Fnd+1);
	if (Nxt==-1)
		return false;
	Arg=ArgStr.Mid(Fnd+1,Nxt-1-Fnd);
	if (trim)
	{
		Arg.TrimLeft();
		Arg.TrimRight();
	}
	return true;
}



CTime NStringUtil::EncodeTimeFromString(CString TimeStr)
{
	CTime time=CTime::GetCurrentTime();
	SYSTEMTIME stime;
	time.GetAsSystemTime(stime);
	switch (TimeStr.GetLength())
	{
	case 4:
		stime.wHour=atoi(TimeStr.Left(2));
		stime.wMinute=atoi(TimeStr.Right(2));
		stime.wSecond=0;
		break;
	case 12:
		stime.wHour=atoi(TimeStr.Mid(6,2));
		stime.wMinute=atoi(TimeStr.Mid(8,2));
		stime.wSecond=atoi(TimeStr.Right(2));
		break;
	}
	time =CTime(stime);
	return time;
}

CTime NStringUtil::EncodeDateFromString(CString TimeStr)
{
	CTime time=CTime::GetCurrentTime();
	SYSTEMTIME stime;
	stime.wHour=0;
	stime.wMinute=0;
	stime.wSecond=0;
	stime.wMilliseconds=0;
	switch (TimeStr.GetLength())
	{
	case 6:
	case 12:
		stime.wYear=atoi(TimeStr.Left(2));
		stime.wMonth=atoi(TimeStr.Mid(2,2));
		stime.wDay=atoi(TimeStr.Right(2));
		break;
	case 8:
		stime.wYear=atoi(TimeStr.Left(4));
		stime.wMonth=atoi(TimeStr.Mid(4,2));
		stime.wDay=atoi(TimeStr.Right(2));
		break;
	}
	time =CTime(stime);
	return time;
}



bool NStringUtil::IsNumeric(CString str)
{
	if (!str.GetLength())
		return false;
	for  (int i=0;i<(int)str.GetLength();i++)
	{
		if (!isdigit(str[i]))
			return false;
	}
	return true;
}

bool NStringUtil::IsNumericOrFloat(CString str)
{
	if (!str.GetLength())
		return false;
	for  (int i=0;i<(int)str.GetLength();i++)
	{
		if ((!isdigit(str[i])) && (str[i]!='.') && (str[i]!='-'))
			return false;
	}
	return true;
}



in_addr NStringUtil::DecodeAddr(CString str)
{
	in_addr addr;
	addr.s_addr=inet_addr(str);
	if (addr.s_addr==INADDR_NONE)
	{
		hostent* pHost=gethostbyname(str);
		if (pHost)
		{
			addr.s_addr=*((DWORD*)pHost->h_addr_list[0]);
		}
	}
	return addr;

}


UINT NStringUtil::DecodePort(CString str,bool tcp)
{
	UINT port=0;
	if (IsNumeric(str))
		port=htons(atoi(str));
	else
	{
		servent* pServ=NULL;
		if (tcp)
			pServ=getservbyname(str,"TCP");
		else
			pServ=getservbyname(str,"UDP");
		if (pServ)
			port=pServ->s_port;
	}
	return port;	
}
