#pragma once
#include "afxwin.h"
#include "FplWnd.h"
#include "Flightplan.h"
#include "Condition.h"

class CFplDoc :
	public CDocument
{
public:
	CFplDoc();
	~CFplDoc();

	CFplWnd m_FplWnd;

	CArray <CFlightPlan*, CFlightPlan*>	m_FplTable;
	CArray <CFlightPlan*, CFlightPlan*>	m_FplInactiveTable;

	CFlightPlan* FindFpl(CString Arcid, CString Adep, CString Ades, CString Eobt);
	CFlightPlan* CreateNewFpl(bool Inactive = false);
	void ReceiveFromAftn(CString AftnMsg);
	bool IsEntryPoint(CString Name);
	BOOL IsExitPoint(CString point);
	BYTE ComputeSector(int FL, CString PTID);
	bool IsRwyAirport(CString Airport);
	bool IsSidPoint(CString Adep, CString Point);
	bool IsStarPoint(CString Ades, CString Point);
	int GetCurrentDepRwy(CString Ades);
	int GetCurrentArrRwy(CString Ades);
	CString FindSid(CString Ades, CString FirstPoint, CString& PointToAdd);
	CString FindStar(CString Ades, CString FirstPoint, CString LastPoint, CString& PointToAdd);
	char GetAdjSectorType(int SectorNb1, int SectorNb2);
	CString GetSectorSite(int SectorNb);
	CString GetAdjSectorXptList(int SectorNb1, int SectorNb2);
	void LoadDefAirspaceFile();
	void LoadDefToolsFile();
	void LoadDefWindowsFile();
	CFlightPlan* GetFplByNbr(int FplNbr);

	CArray<CCondition*, CCondition*> m_LogicalSector1Table;
	CArray<CCondition*, CCondition*> m_LogicalSector2Table;

	double	GetXptDistanceDelta(CString XPT);
	int		GetWindowNum(CString XPT);
	void GetExCdtError();
	void UpdateTrig();
	void UpdateIflTrig();
	const int  TestConflict(CFlightPlan*  pFplA, CFlightPlan*  pFplB, bool OnXfl = false, bool IflMode = false);

};

