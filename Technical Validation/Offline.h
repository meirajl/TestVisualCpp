#pragma once
#include "afx.h"
#include "OurConstants.h"

#define ArcidData 0
#define To3Data 1
#define EflNoXflData 2
#define XFLData 3
#define XflData 4
#define RflData 5
#define ArctypData 6
#define AdepData 7
#define AdesData 8
#define To2Data 9
#define AdesNoGvaData 10
#define EflData 11
#define SsrData 12
#define AdepNoGvaData 13
#define Ptid1Data 14
#define AtdData 15
#define XptData 16
#define AckData 17
#define HstData 18
#define TextComData 19
#define ArcidInfoData 20
#define EptData 21
#define To1Data 22
#define XflNoEflData 23
#define Ptid2Data 24
#define To2NoSecData 25
#define CflData 26
#define StarData 27
#define EatData 28
#define PltData 29
#define AflData  30
#define EtaData 31
#define EteData 32
#define ESflData 33
#define XSflData 34
#define XSflAppData 35
#define XflNoXSflData 36
#define XSflNoESflData 37
#define PrpData 38
#define ReqData 39
#define EtdAtdData 40
#define TTLData 41
#define EatTo2Data 42
#define HectData 43
#define EatTickData 44
#define SectorData 45
#define IFLData 46
#define IflData 47
#define DlData  48
#define DlSymbData 49
#define DlToData 50
#define MaxData 51

class COfflineWindow;
class CExCDT;
class COffline :	public CObject
{
public:
	COffline();
	~COffline();
	COfflineWindow* m_pOflWnd;
	CExCDT* m_pExCdt;

	CString m_DefPathName;
	CString m_AftnPathDir;
	CString m_NavPointsPathDir;
	CString m_TechValName;
	CString m_ItalyCrossBorderPointList;
	CString m_IptUseless;
	CString m_DepartureList;
	CString m_ArrivalList;
	int m_DefaultEptDistance;
	typedef struct {
		int							SL;			// sector logic number
		int							FLmin;
		int							FLmax;
		CString						PTIDs;
	} SectorLdefinition_T;
	CArray <SectorLdefinition_T, SectorLdefinition_T> m_SectorLogicList;
	typedef struct
	{
		CString Peri;
		CString UAC;
		CString Type;
		BYTE SectorOverload;
		bool SectorCoord;
		CString SectFreq;
		bool  CflEnabled;
		bool  Stripless;
		bool  EflInCfl;
		bool  FreqWarning;
		int   ClamEnabled;
		int	LateCflClamBuffer;
		bool  CpdlcOn;
		bool  CpdlcEligible;
		WORD   Min;
		WORD   Max;
		int    console1;
		int    console2;
		bool  IsGeo;
		CString Freq;
		int		Adj[10];
		char	AdjType[10];
		CString AdjXptList[10];
		CString Site;
	} SectorDefs;
	CArray <SectorDefs, SectorDefs> m_SectorTable;
	typedef struct
	{
		int FirNb;
		int LowLevel;
		int HighLevel;
	} Fir;
	typedef struct {
		CString Name;
		int  WindowNum;
		BOOL ForbiddenLV[35];
		int  Delta;
		int StopTime;
		int FlMin;
		int FlMax;
		LevelParity Parity;
		//double DistDelta;
		int DistAman;
		Fir FirTable;
	} ExitCop;

	typedef struct {
		CString Name;
		int Distance;
		int DistanceEto;
		Fir FirTable;
		CString Site;
	} EntryPoint;
	CArray<EntryPoint, EntryPoint> m_EntryPoints;
	CArray<ExitCop, ExitCop> m_ExitCops;
	typedef struct
	{
		CString Airport;
		int DepRwy[10];
		int ArrRwy[10];
		int CurDepRwy;
		int CurArrRwy;
	} AirportDef;
	CArray<AirportDef, AirportDef> m_AirportTable;

	typedef struct
	{
		CString Name;
		CString Pnt1;
		CString Pnt2;
		CString PointsToAdd;
		bool FreeRoute;
		int RwyNb;
		int FirstAngle;
		int LastAngle;
		CString Airport;
	} PtidBisItem;
	CArray <PtidBisItem, PtidBisItem> m_SidTable;
	CArray <PtidBisItem, PtidBisItem> m_StarTable;
	CArray <PtidBisItem, PtidBisItem> m_DefaultStarTable;

	typedef struct {
		CString Efl;
		CString Xfl;
		CString Airport;
	}AirportLevel;
	CArray<AirportLevel, AirportLevel> m_ListAirportLevel;

	typedef enum { ExitWdw, BinWdw, HoldingWdw, ArrivalWdw, DepartureWdw, EntryWdw, SectorWdw, AdjWdw, RegioWdw, DataLink } ViewType;
	typedef enum { Text, Time, RelTime, Button, Hybrid, Symbol } DisplayType;
	typedef struct
	{
		CString  ColumnName;
		int		 ColumnSize;
		int      DataNb;
		int      FontNb;
		int		 SmallFontNb;
		int		 ThirdFontNb;
		int		 SelectFont;
		int		 SmallSelectFont;
		int		 ThirdSelectFont;
		DisplayType Type;
		UINT	 Style;
		CString  Select;
		bool	 Sort;
		bool	 Hold;
		bool     WasSelected;
		int      Min;
		int		 Max;
	} FplFieldDef;
	typedef struct
	{
		CString WindowName;
		CString PointList;
		CString SectorList;
		CString FltrList;
		CPoint Pos;
		CPoint WindowPos;
		bool Eliminate;
		bool IsVisible;
		bool ManualOpen;
		int  Type;
		bool ForbidenLevel[FLevNb];
		CString SectorDisplay;
		int TitleColNb;
		int FontNb;
		int SmallFontNb;
		int ThirdFontNb;
		int SelectFontNb;
		int SmallSelectFontNb;
		int ThirdSelectFontNb;
		CArray<FplFieldDef, FplFieldDef> Columns;
		int NotifiedNb;
		//CLineView* pView;
		CString BoxName;
	} WindowsDef;

	CString m_OfflineErrorLog;

	typedef struct {
		CString Site;
		int SectorNumber;
		CString PTIDs;
	}OfflineCheck;
	CArray<OfflineCheck, OfflineCheck> m_SectorList;
	CArray<WindowsDef*, WindowsDef*> m_WindowTable;

	void LoadDepartureList(CString IniName);
	void LoadArrivalList(CString IniName);
	void LoadLogicalSectorsDefinition(CString IniName);
	void LoadSectors(CString IniName);
	void LoadEntryPoint(CString IniName);
	void LoadExitPoint(CString IniName);
	void LoadSid(CString IniName);
	void LoadStar(CString IniName);
	void LoadAirport(CString IniName);
	bool ReadOffline();
	CString CheckOffline();
	void LoadDefWindows(CString IniName);
};

