#include "stdafx.h"
#include "TrafficError.h"


CTrafficError::CTrafficError(CWnd* pParent /*=NULL*/)
	: CDialog(CTrafficError::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTrafficError)
	m_TrafficError = _T("");
	m_Arcid = _T("");
	//}}AFX_DATA_INIT
}


CTrafficError::~CTrafficError()
{
}

void CTrafficError::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTrafficError)
	DDX_Text(pDX, IDC_EDIT2, m_TrafficError);
	DDX_Text(pDX, IDC_ARCIDEDIT, m_Arcid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTrafficError, CDialog)
	//{{AFX_MSG_MAP(CTrafficError)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()