
// Technical ValidationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Technical Validation.h"
#include "Technical ValidationDlg.h"
#include "TechnicalValidationFile.h"
#include "afxdialogex.h"
#include "geo.h"
#include "OfflineWindow.h"
#include "ExCDT.h"
#include "FplDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTechnicalValidationDlg dialog



CTechnicalValidationDlg::CTechnicalValidationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_TECHNICALVALIDATION_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTechnicalValidationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTechnicalValidationDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CHOOSE_AFTN_FILE, &CTechnicalValidationDlg::OnChooseAFTNFile)
	ON_BN_CLICKED(IDC_SEQUENCE, &CTechnicalValidationDlg::OnSequence)
	ON_BN_CLICKED(IDC_CHOOSE_LAT_LONG_FILE, &CTechnicalValidationDlg::OnChooseLatLongFile)
	ON_BN_CLICKED(IDC_CONVERT_LATLONG_INTO_XY, &CTechnicalValidationDlg::OnConvertLatLongIntoXY)
	ON_BN_CLICKED(IDC_OFFLINE_CHECK, &CTechnicalValidationDlg::OnOfflineCheck)
	ON_BN_CLICKED(IDC_EXCDT_CHECK, &CTechnicalValidationDlg::OnExCdtCheck)
	ON_BN_CLICKED(IDC_XFLSET, &CTechnicalValidationDlg::OnXflSet)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CTechnicalValidationDlg message handlers

BOOL CTechnicalValidationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTechnicalValidationDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTechnicalValidationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTechnicalValidationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTechnicalValidationDlg::OnSequence()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	if (!m_AftnFileName.IsEmpty())
	{
		if (!pApp->m_pDoc->m_FplWnd.IsOpen)
		{
			pApp->m_pDoc->m_FplWnd.m_pDoc = pApp->m_pDoc;
			((CWnd*)&pApp->m_pDoc->m_FplWnd)->CreateEx(0, WC_LISTVIEW, "FPL List", LVS_SINGLESEL | LVS_SORTASCENDING | LVS_REPORT | WS_THICKFRAME | WS_OVERLAPPED | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL, 400, 400, 1700, 800, this->m_hWnd, 0);
			DWORD err = GetLastError();
		}
		else
		{
			pApp->m_pDoc->m_FplWnd.DestroyWindow();
		}
	}
	if (pApp->m_pDoc->m_FplWnd.IsOpen)
		pApp->m_pDoc->m_FplWnd.AddFpl();
}

void CTechnicalValidationDlg::OnChooseAFTNFile()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	CFileDialog filedlg(TRUE);

	filedlg.m_pOFN->lpstrInitialDir = (LPCSTR)pApp->m_pOffline->m_AftnPathDir;
	INT_PTR res = filedlg.DoModal();
	//filedlg.m_ofn.lpstrInitialDir = 
	if (res == IDOK)
	{
		m_AftnFileName = filedlg.GetPathName();
		CTechnicalValidationFile* AftnFile = new CTechnicalValidationFile(m_AftnFileName, AFTN);
		//OnSequence();
	}

}

void CTechnicalValidationDlg::OnConvertLatLongIntoXY()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString ackLog;

	if (!m_NavPointsFileName.IsEmpty())
	{
		CGeo::LoadNavPoints(m_NavPointsFileName);

		ackLog.Format("Conversion from Latitude/Longitude into X/Y coordinates done");
		AfxMessageBox(ackLog);
		return;
	}
}

void CTechnicalValidationDlg::OnChooseLatLongFile()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	CFileDialog filedlg(TRUE);

	filedlg.m_pOFN->lpstrInitialDir = (LPCSTR)pApp->m_pOffline->m_NavPointsPathDir;
	INT_PTR res = filedlg.DoModal();

	if (res == IDOK)
	{
		m_NavPointsFileName = filedlg.GetPathName();
	}

}

void CTechnicalValidationDlg::OnOfflineCheck()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString oflLog;
	
	if (pApp->m_pOffline->m_pOflWnd)
	{
		delete pApp->m_pOffline->m_pOflWnd;
		pApp->m_pOffline->m_pOflWnd = NULL;
	}
	else
	{
		oflLog=pApp->m_pOffline->CheckOffline();
		pApp->m_pOffline->m_pOflWnd = new COfflineWindow;
		pApp->m_pOffline->m_pOflWnd->m_OfflineLog = oflLog;
		pApp->m_pOffline->m_pOflWnd->Create(IDD_OFFLINE, this);
	}
}

void CTechnicalValidationDlg::OnExCdtCheck()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();
	CString ExCdtLog;

	if (pApp->m_pOffline->m_pExCdt)
	{
		delete pApp->m_pOffline->m_pExCdt;
		pApp->m_pOffline->m_pExCdt = NULL;
	}
	else
	{
		pApp->m_pOffline->m_pExCdt = new CExCDT;
		pApp->m_pDoc->GetExCdtError();
		//pApp->m_pOffline->m_pExCdt->m_ExCDTLog = ExCdtLog;
		pApp->m_pOffline->m_pExCdt->Create(IDD_EXCDT, this);
	}
}

void CTechnicalValidationDlg::OnXflSet()
{
	CTechnicalValidationApp* pApp = (CTechnicalValidationApp*)AfxGetApp();

	CFplDlg* pFplDlg = new CFplDlg;
	pFplDlg->Create(IDD_FPL_SET, this);
}

void CTechnicalValidationDlg::OnDestroy()
{
	//if (this)
	//	delete this;
}

void CTechnicalValidationDlg::OnClose()
{
	//if (this)
	//	delete this;
}
