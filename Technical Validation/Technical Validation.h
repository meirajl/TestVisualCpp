
// Technical Validation.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "Flightplan.h"
#include "Offline.h"
#include "FplDoc.h"
#include "ExCDT.h"

// CTechnicalValidationApp:
// See Technical Validation.cpp for the implementation of this class
//

class CTechnicalValidationApp : public CWinApp
{
public:
	CTechnicalValidationApp();
	CFplDoc* m_pDoc;
	COffline* m_pOffline;
	CExCDT* m_pExCdt;
	CMultiDocTemplate* pFplTemplate;

// Overrides
public:
	virtual BOOL InitInstance();
	static bool CheckFile(CString Name);
// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTechnicalValidationApp theApp;