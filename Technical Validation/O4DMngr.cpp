// O4DMngr.cpp : implementation file
//




#include "StdAfx.h"
#define SetO4D 
#include "O4DMngr.h"


//#include "Platform/algorithm"
#ifdef SetO4D
#include <O4D.h>

#include <OsyrisCommon.h>
#include <OsyrisLog.h>
#include <messages/EnsureOsyrisMsgRegistration.C>
#include <EnsureClpMsgRegistration.C>
#include <PhysicalEnvironment.h>
#include <AircraftDatabase.h>
using namespace Osyris::Messages;
#endif
//#include "ConvMvWGS84.h"
//#include "track.h"
#include "OurConstants.h"
#include "FlightPlan.h"
//#include "visu.h"
#include "FplDoc.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
class PhysicalEnvironment;

/////////////////////////////////////////////////////////////////////////////
// CO4DMngr
int Argc=5;
char* Argv[5];
bool m_Enabled=false;
bool m_LtcaEnabled=true;
CString AllKey,Cfg,Log,WindPath;
CTimeSpan WindTimeOut;
double MtcaSteepestMassFactor=1.0;
double MtcaLowAngleMassFactor=1.0;
double LtcaSteepestMassFactor=1.0;
double LtcaLowAngleMassFactor=1.0;

CString m_O4DVersion="No O4D Version";
#ifdef SetO4D
O4D* m_pO4D;
PhysicalEnvironment* m_pEnv=NULL;
SET_LOG_MODULE(Osyris::Log::OSYRIS_O4D);
#endif


void DebugFpl(CFlightPlan* pFpl,CString str)
{
#ifdef DebugO4D
	OutputDebugString(str+"\n");
#endif
	if (pFpl)
		pFpl->DebugStr(str);
}

void CO4DMngr::ComputeLatLong (CPoint Pos, const int & Altitude, double & Lat, double & Long)
{
	
	double X,Y;
	X=(double)Pos.x/32.0;
	Y=(double)Pos.y/32.0;
    const char* ErrorText;
    double alt = 80.0;
	if (Altitude)
		alt = Altitude;
	ConvMvWGS84::MV_type type = ConvMvWGS84::MV_GE;
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	if (pApp->m_Centre==CVisuApp::ZRH)
		type=ConvMvWGS84::MV_ZR;
	
	if (ConvMvWGS84::convert(X, Y, alt, type, Lat, Long, ErrorText))
	{
		Lat = Lat * 180.0 / PI;
		Long = Long * 180.0 / PI;
	}
	else
	{
		if (DEBUGTRACE)
			OutputDebugString("ComputeLatLong error");
	};
}



#ifdef SetO4D


static void O4dOutputHandler (OsyrisMessage *msg)
{
	CString err,err2;
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
   if (msg->MsgId() == TrajectoryMsg::msgId)
    {
        TrajectoryMsg *trjMsg = static_cast<TrajectoryMsg *>(msg);
		const int FplId = trjMsg->flightId;
		if (trjMsg->HasError() > 0)
		{
			
			err.Format("***reception of Trajectory message with errors FplId=%d TrjId=%d ContextId=%d", FplId,trjMsg->requestId,trjMsg->contextId);
			CFlightPlan* pFpl = CFlightPlan::m_pDoc->GetFplByNbr(FplId);
			if (pFpl == NULL)
				return;
			DebugFpl(pFpl,err);
			std::vector<ErrorMsg *>::const_iterator it_error = trjMsg->exceptions.begin();
			CTime O4D_estimTime=0;
			OutputDebugString("O4D error"+pFpl->m_Arcid+"\n");
			while (it_error != trjMsg->exceptions.end())
			{
				err.Format("Error %d %s",(*it_error)->errorType,(*it_error)->errorDescription.c_str());
				OutputDebugString(err+"\n");
				DebugFpl(pFpl,err);
				++it_error;
			}
		}
		else
		{
			CFlightPlan* pFpl = CFlightPlan::m_pDoc->GetFplByNbr(FplId);
			if (pFpl == NULL)
				return;
			pFpl->m_ReqToO4DOk=true;
			if (trjMsg->contextId>=0)
				pFpl->m_O4DCalculMode=CFlightPlan::TrackCalcul;
			err.Format("reception OK of Trajectory message Fpl=%s Arctyp=%s FplId=%d TrjId=%d ContextId=%d", pFpl->m_Arcid,pFpl->m_ArcTyp,FplId,trjMsg->requestId,trjMsg->contextId);
			DebugFpl(pFpl,err);
			std::vector<TrajectoryPointMsg *>::const_iterator it_traj = trjMsg->trajectoryPoints.begin();
			CTime O4D_estimTime=0,IniTime=0;
			bool fndcfl=false;
			int PrvAlt=-1,IniAlt=-1;
			int prvpnt=-1;
			int lastpnt=-1,BocdDelay=0;
			while ( (it_traj != trjMsg->trajectoryPoints.end()) )
			{
				const int eto = (*it_traj)->eto;
				int Alt = (*it_traj)->alt;
				O4D_estimTime =CTime( eto); 
				WaypointMsg O4DPoint;
				O4DPoint.lat = (*it_traj)->lat;
				O4DPoint.lon = (*it_traj)->lon;
				std::vector<OsyrisMessage *>::const_iterator extMsg = (*it_traj)->ext.begin();
				CString rmk;
				err="";
				while ( (extMsg != (*it_traj)->ext.end()) )
				{
					if ((*extMsg)->MsgId() == PointAttachmentMsg::msgId)
					{
						PointAttachmentMsg *pntMsg = static_cast<PointAttachmentMsg *>(*extMsg);
						rmk = (*pntMsg).remark.c_str();
						break;
					}
					else
					{
						if ((*extMsg)->MsgId() == TrajectoryPointAlternativeMsg::msgId)
						{
							TrajectoryPointAlternativeMsg *AltMsg = static_cast<TrajectoryPointAlternativeMsg *>(*extMsg);
							err.Format("%s : extMsg Id =%d Kind=%d",pFpl->m_Arcid,(*extMsg)->MsgId(),(*AltMsg).kind);
							DebugFpl(pFpl,err);
							if (DEBUGTRACE)
								OutputDebugString(err);
						}
						int merd=7;
					}
					++extMsg;
				}
				if (!rmk.IsEmpty())
//				if (!(*it_traj)->ext.empty())
				{
//					int id=(*extMsg)->MsgId();
//					if (id!=133)
//						int merd=7;
//					PointAttachmentMsg *pntMsg = static_cast<PointAttachmentMsg *>(*extMsg);
//					rmk = (*pntMsg).remark.c_str();
					lastpnt=atoi(rmk);
					if (trjMsg->contextId>=100 && (prvpnt!=-1) && (lastpnt-prvpnt)!=1)
						int merd=7;
					if (!isdigit(rmk[0]))
						lastpnt=-1;
					if (lastpnt >= 0 && lastpnt<pFpl->m_CurRoute.GetSize() &&  ((trjMsg->contextId == 1) || (trjMsg->contextId == 2)))
						err.Format("Traj Ret Point(%f,%f) Alt=%d Time %s PntNb=%s Name=%s ", O4DPoint.lat, O4DPoint.lon, Alt, O4D_estimTime.Format("%H:%M:%S"), rmk, pFpl->m_CurRoute[lastpnt].LongName);
					if (lastpnt >= 0 && ((trjMsg->contextId >= 100) && (trjMsg->contextId < 300)))
					{
						if (pFpl->m_SectorRoute)
						{
							if (lastpnt <= pFpl->m_SectorRoute[trjMsg->contextId - 100].GetSize()-1)
								err.Format("Traj Ret Point(%f,%f) Alt=%d Time %s PntNb=%s Name=%s ", O4DPoint.lat, O4DPoint.lon, Alt, O4D_estimTime.Format("%H:%M:%S"), rmk, pFpl->m_SectorRoute[trjMsg->contextId - 100][lastpnt].LongName);
						}
					}
					if (lastpnt >= 0 && ((trjMsg->contextId >= 500) && (trjMsg->contextId < 700)))
					{
						if (pFpl->m_SectorRoute)
						{
							if (lastpnt <= pFpl->m_SectorRoute[trjMsg->contextId - 500].GetSize() - 1)
								err.Format("Traj Ret Point(%f,%f) Alt=%d Time %s PntNb=%s Name=%s ", O4DPoint.lat, O4DPoint.lon, Alt, O4D_estimTime.Format("%H:%M:%S"), rmk, pFpl->m_SectorRoute[trjMsg->contextId - 500][lastpnt].LongName);
						}
					}
					if (lastpnt>=0 && lastpnt<pFpl->m_FdpRoute.GetSize() && (trjMsg->contextId==801))
						err.Format("Traj Ret Point(%f,%f) Alt=%d Time %s PntNb=%s Name=%s ",O4DPoint.lat,O4DPoint.lon,Alt,O4D_estimTime.Format("%H:%M:%S"),rmk,pFpl->m_FdpRoute[lastpnt].LongName);
					if ((lastpnt>=0)&&lastpnt<pFpl->m_CurRoute.GetSize() && (trjMsg->contextId==-1))
					{
						pFpl->m_CurRoute[lastpnt].To=O4D_estimTime;
						pFpl->m_CurRoute[lastpnt].Alt=(*it_traj)->alt;
						err.Format("Traj Fpl Ret Point(%f,%f) Alt=%d Time %s PntNb=%s Name=%s ",O4DPoint.lat,O4DPoint.lon,Alt,pFpl->m_CurRoute[lastpnt].To.Format("%H:%M:%S"),rmk,pFpl->m_CurRoute[lastpnt].LongName);
					}
					if ((lastpnt>=0) && (lastpnt<pFpl->m_TmpFdpRoute.GetSize())&& (trjMsg->contextId==-2))
					{
						pFpl->m_TmpFdpRoute[lastpnt].To=O4D_estimTime;
						pFpl->m_TmpFdpRoute[lastpnt].Alt=(*it_traj)->alt/100;
						err.Format("Traj Tmp Fpl Ret Point(%f,%f) Alt=%d Time %s PntNb=%s Name=%s ",O4DPoint.lat,O4DPoint.lon,Alt,pFpl->m_TmpFdpRoute[lastpnt].To.Format("%H:%M:%S"),rmk,pFpl->m_TmpFdpRoute[lastpnt].LongName);
					}
					if (trjMsg->contextId==0 || trjMsg->contextId==1)
					{
						if ((lastpnt>=0)&& lastpnt<pFpl->m_CurRoute.GetSize() && ((pFpl->m_XptFix!=-1)||trjMsg->contextId==1))
						{
							pFpl->m_CurRoute[lastpnt].To=O4D_estimTime;
							pFpl->m_CurRoute[lastpnt].Alt=(*it_traj)->alt;
						}
						if (lastpnt>=0 && (lastpnt<pFpl->m_CurRoute.GetSize()) && trjMsg->contextId==0)
						{
							if (pFpl->m_XptFix!=-1)// || (pFpl->m_IptFix!=-1 && pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible()))
							{
								if (lastpnt==pFpl->m_EptFix)
									pFpl->m_Etn=pFpl->m_CurRoute[lastpnt].To;
								if (lastpnt==pFpl->m_IptFix)
								{
									if (!INNOV_KEY)
										pFpl->m_Eti=pFpl->m_CurRoute[lastpnt].To; 
								}
								if (lastpnt==pFpl->m_XptFix)
								{
									if (!pFpl->m_EtxManualInput)
										pFpl->m_Etx=pFpl->m_CurRoute[lastpnt].To;
									if(!INNOV_KEY)
										pFpl->m_Eta=pFpl->m_Etx; // marc PH02-08 eta filled by dta from skykeeper
									err2.Format("Xpt Name=%s XptFix=%d EtxManual=%d Etx Time %s",pFpl->m_CurRoute[lastpnt].Name,pFpl->m_XptFix,pFpl->m_EtxManualInput,pFpl->m_Etx.Format("%H:%M:%S"));
									DebugFpl(pFpl,err2);
								}
								if (!INNOV_KEY && CString("LFLB/LFLP").Find(pFpl->m_Ades)!=-1 && lastpnt==pFpl->m_CurRoute.GetUpperBound())
									pFpl->m_Eta=pFpl->m_CurRoute[lastpnt].To;// marc PH02-08 eta filled by dta from skykeeper
							}
							else
							{
								err.Format("Traj Ret Point(%f,%f) Alt=%d Time %s FpdPnt %d",O4DPoint.lat,O4DPoint.lon,Alt,O4D_estimTime.Format("%H:%M:%S"),lastpnt);
								if (lastpnt==pFpl->m_IptFdpFix)
								{
									if (!INNOV_KEY)
										pFpl->m_Eti=O4D_estimTime;
								}
								if (lastpnt==pFpl->m_XptFdpFix && (lastpnt<pFpl->m_FdpRoute.GetSize()))
								{
									if (!pFpl->m_EtxManualInput)
										pFpl->m_Etx=O4D_estimTime;
									if (pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible())	
									{
										if (!INNOV_KEY)
											pFpl->m_Eta=O4D_estimTime;// marc PH02-08 eta filled by dta from skykeeper
										err2.Format("HECT:Xpt Name=%s XptFix=%d  Eta Time %s",pFpl->m_FdpRoute[lastpnt].Name,pFpl->m_XptFdpFix,pFpl->m_Eta.Format("%H:%M:%S"));
										DebugFpl(pFpl,err2);
									}
								}
							}
						}
					}
				}
				else
					err.Format("Traj Ret Point(%f,%f) Alt=%d Time %s",O4DPoint.lat,O4DPoint.lon,Alt,O4D_estimTime.Format("%H:%M:%S"));
				
				if ((trjMsg->contextId==1) && (PrvAlt==-1 || IniAlt==Alt))
				{
					if (PrvAlt==-1)
						IniTime=O4D_estimTime;
					else
						BocdDelay=(O4D_estimTime-IniTime).GetTotalSeconds();
					IniAlt=Alt;
					err2.Format(" %s after %d delay=%d","BOCD",lastpnt,BocdDelay);
					err+=err2;
				}
				if ((trjMsg->contextId==1||trjMsg->contextId==2) && PrvAlt!=-1 && PrvAlt!=Alt)
				{
					CString plus;
					int PosTod=0;
					if (trjMsg->contextId==2)
					{
						PosTod=1;
						plus="2";
					}
//					if  (Alt==pFpl->m_Cfl*100)
					{
						if (PrvAlt<=Alt)
							pFpl->m_TOCD[PosTod].Name="~TOC"+plus;
						else
							pFpl->m_TOCD[PosTod].Name="~TOD"+plus;
						pFpl->m_TOCD[PosTod].Pos=pFpl->m_pDoc->LatLongToPoint(O4DPoint.lat,O4DPoint.lon);
						pFpl->m_TOCD[PosTod].To=O4D_estimTime;
						pFpl->m_TOCD[PosTod].FdpNb=lastpnt;
						pFpl->m_TOCD[PosTod].Alt=Alt;
						if (BocdDelay)
						{
							pFpl->m_TOCD[PosTod].To-=BocdDelay;
							if (BocdDelay>200)
								int merd=0;
							CString err2;
							err2.Format(" %s after %d delay=%d Cfl=%d",pFpl->m_TOCD[PosTod].Name,lastpnt,BocdDelay,pFpl->m_Cfl);
//							OutputDebugStr(pFpl->m_Arcid+" "+err2+"\n");
							err+=err2;
						}
						
					}
				}
				if ((trjMsg->contextId==-2) && PrvAlt!=-1 && PrvAlt!=Alt)
				{
					if (PrvAlt<=Alt)
						pFpl->m_FplTOCD.Name="~TOC";
					else
						pFpl->m_FplTOCD.Name="~TOD";
					pFpl->m_FplTOCD.Pos=pFpl->m_pDoc->LatLongToPoint(O4DPoint.lat,O4DPoint.lon);
					pFpl->m_FplTOCD.GeoPos=pFpl->m_FplTOCD.Pos;
					pFpl->m_FplTOCD.To=O4D_estimTime;
					pFpl->m_FplTOCD.FdpNb=lastpnt;
					pFpl->m_FplTOCD.Alt=Alt/100;
				}
				if ((trjMsg->contextId>=100 && trjMsg->contextId<500) && (PrvAlt==-1 || IniAlt==Alt))
				{
					if (PrvAlt==-1)
						IniTime=O4D_estimTime;
					else
						BocdDelay=(O4D_estimTime-IniTime).GetTotalSeconds();
					IniAlt=Alt;
					err2.Format(" %s after %d delay=%d","BOCD",lastpnt,BocdDelay);
					err+=err2;
				}
				if (trjMsg->contextId>=100 && trjMsg->contextId<=700)
				{
					int Index=trjMsg->contextId-100;
					if (trjMsg->contextId>=500)
						Index=trjMsg->contextId-500;
					if (!rmk.IsEmpty() && lastpnt>=0 && trjMsg->contextId<500)
					{
						if (pFpl->m_SectorRoute && (lastpnt<pFpl->m_SectorRoute[Index].GetSize()))
						{
							pFpl->m_SectorRoute[Index][lastpnt].To = O4D_estimTime;
							if (prvpnt != -1)
							{
								double dist = pFpl->CalculDistance(pFpl->m_SectorRoute[Index][lastpnt].Pos, pFpl->m_SectorRoute[Index][prvpnt].Pos);
								double time = (pFpl->m_SectorRoute[Index][lastpnt].To - pFpl->m_SectorRoute[Index][prvpnt].To).GetTotalSeconds();
								double spd = dist*112.5 / time;
								if (spd > 1000)
									int merd = 7;
							}
							pFpl->m_SectorRoute[Index][lastpnt].Alt = (*it_traj)->alt;
						}
					}
					if (PrvAlt!=-1 && PrvAlt!=Alt)
					{
						CString plus;
						int PosTod=0;
						if (trjMsg->contextId>=500)
						{
							PosTod=1;
							plus="2";
						}
//						if  (Alt==pFpl->m_Cfl*100)
						{
							if (PrvAlt<=Alt)
								pFpl->m_SectorTOCD[Index*2+PosTod].Name="~TOC"+plus;
							else
								pFpl->m_SectorTOCD[Index*2+PosTod].Name="~TOD"+plus;
							pFpl->m_SectorTOCD[Index*2+PosTod].Pos=pFpl->m_pDoc->LatLongToPoint(O4DPoint.lat,O4DPoint.lon);
							pFpl->m_SectorTOCD[Index*2+PosTod].To=O4D_estimTime;
							pFpl->m_SectorTOCD[Index*2+PosTod].FdpNb=lastpnt;
							pFpl->m_SectorTOCD[Index*2+PosTod].Alt=Alt;
							if (BocdDelay)
							{
								pFpl->m_SectorTOCD[Index*2+PosTod].To-=BocdDelay;
								if (BocdDelay>200)
									int merd=0;
								CString err2;
								err2.Format(" %s after %d delay=%d Cfl=%d",pFpl->m_SectorTOCD[Index*2+PosTod].Name,lastpnt,BocdDelay,pFpl->m_Cfl);
								//							OutputDebugStr(pFpl->m_Arcid+" "+err2+"\n");
								err+=err2;
							}
							else
							{
								CString err2;
								err2.Format(" %s after %d",pFpl->m_SectorTOCD[Index*2+PosTod].Name,lastpnt);
								err+=err2;
							}
						
						}
					}
				}
				if (trjMsg->contextId==800)
				{
					if (!rmk.IsEmpty() && atoi(rmk)==pFpl->m_IptFix)
					{
						pFpl->m_AdvSpeed=trjMsg->advSpdValue;
						int merd=7;
					}
				}
				if (trjMsg->contextId==801)
				{
					if (!rmk.IsEmpty() && atoi(rmk)==pFpl->m_XptFdpFix)
					{
						pFpl->m_EA3=O4D_estimTime;
						int merd=7;
					}
				}
				if (DEBUGTRACE)
					OutputDebugString(err+"\n");
				DebugFpl(pFpl,err);
				PrvAlt=Alt;
				++it_traj;
				if (!rmk.IsEmpty())
					prvpnt=lastpnt;
			}
			
		}
	}
	// store the results in the TabFlights
}

#endif

void CO4DMngr::Init(CString FileName,CString PathName)
{
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	pApp->LogMsg("O4D: Go to init");
	int len=GetPrivateProfileString("O4D","ENABLED","",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
#ifndef SetO4D
	return;
#endif
	if (AllKey!="YES")
		return;
	len=GetPrivateProfileString("O4D","LTCAENABLED","YES",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
	if (AllKey!="YES")
		m_LtcaEnabled=false;
	len=GetPrivateProfileString("O4D","WINDTIMEOUT","720",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
	WindTimeOut=CTimeSpan(atoi(AllKey)*60);

	len=GetPrivateProfileString("O4D","LTCA_STEEPEST_MASSFACTOR","1.0",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
	LtcaSteepestMassFactor=atof(AllKey);
	len=GetPrivateProfileString("O4D","LTCA_LOWANGLE_MASSFACTOR","1.0",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
	LtcaLowAngleMassFactor=atof(AllKey);

	len=GetPrivateProfileString("O4D","MTCA_STEEPEST_MASSFACTOR","1.0",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
	MtcaSteepestMassFactor=atof(AllKey);
	len=GetPrivateProfileString("O4D","MTCA_LOWANGLE_MASSFACTOR","1.0",AllKey.GetBuffer(2048),2048,FileName);
	AllKey.ReleaseBuffer();
	MtcaLowAngleMassFactor=atof(AllKey);
	
	pApp->LogMsg("O4D: Go to init2");
	m_Enabled=true;
	len=GetPrivateProfileString("O4D","CFG","",Cfg.GetBuffer(2048),2048,PathName);
	Cfg.ReleaseBuffer(len);
	len=GetPrivateProfileString("O4D","LOG","",Log.GetBuffer(2048),2048,PathName);
	Log.ReleaseBuffer(len);
	len=GetPrivateProfileString("O4D","WIND","",WindPath.GetBuffer(2048),2048,PathName);
	WindPath.ReleaseBuffer(len);

	
	Argv[0]="visu.exe";
	Argv[1]="-cfg";
	Argv[2]= (char*)(LPCSTR)Cfg;//new char[Cfg.GetLength()+4];
//	strncpy(Argv[2],Cfg,Cfg.GetLength()+2);
	Argv[3]="-logctrl";
	Argv[4]=  (char*)(LPCSTR)Log;//;new char[Log.GetLength()+4];
//	strncpy(Argv[4],Log,Log.GetLength()+2);
	//	Init2(Argc,ArgTable);
	//}
	//CO4DMngr::Init2(int Argc,char** Argv)
	//{
#ifdef SetO4D
	SET_LOG_MODULE(Osyris::Log::OSYRIS_O4D);
	
	pApp->LogMsg("O4D: init Registration");
	EnsureOsyrisMsgRegistration();
    
	
	pApp->LogMsg(CString("O4D: init Osyris ")+Argv[2]+" "+Argv[4]);
	OsyrisCommon::Initialize(&Argc, Argv, "O4D");
	DebugFpl(NULL,"init1");
	m_O4DVersion=CString("O4D Version : ")+OsyrisCommon::Version();
    /** Create O4D instance */
	Argv[2]= (char*)(LPCSTR)Cfg;//new char[Cfg.GetLength()+4];
	pApp->LogMsg(CString("O4D: init O4D ")+Argv[2]+" "+Argv[4]);
	O4D::Initialize(&Argc, Argv);
	Argv[0] = "visu.exe";
	Argv[1] = "-cfg";
	Argv[2] = (char*)(LPCSTR)Cfg;//new char[Cfg.GetLength()+4];
								 //	strncpy(Argv[2],Cfg,Cfg.GetLength()+2);
	Argv[3] = "-logctrl";
	Argv[4] = (char*)(LPCSTR)Log;//;new char[Log.GetLength()+4];
	DebugFpl(NULL,"init2");
    /** Register our output handler function in O4D instance. */
	m_pO4D = new O4D();
    m_pO4D->RegisterOutputHandler( O4dOutputHandler, FALSE );
	pApp->LogMsg("O4D: end of init");
	DebugFpl(NULL,"init3");
	m_pEnv = (PhysicalEnvironment*)AircraftDataBase::Instance()->GetPhysicalEnvironment();
//	m_pEnv->SetWindMode(PhysicalEnvironment::CURRENT_WIND_FIELD);
//	delete Argv[2];
//	delete Argv[4];
#endif
}

/////////////////////////////////////////////////////////////////////////////
// CO4DMngr message handlers


void CO4DMngr::ManageTrack(CTrack* pTrack,bool Terminated)
{
	if (!m_Enabled)
		return;
	if (!pTrack->m_pFpl)
		return;
#ifdef SetO4D
	TrackMsg trkmsg;
	trkmsg.flightId = pTrack->m_pFpl->m_FplNbr;
	trkmsg.callSign=pTrack->m_pFpl->m_Arcid;
	if (Terminated)
		trkmsg.terminated = 1;
	else 
		trkmsg.terminated = 0;
	
	trkmsg.time = (long)(CVisuApp::GetCurrentTime().GetTime());
	
	if (pTrack->m_Altitude>0)
	{
		trkmsg.altValid = 1;
		trkmsg.altMode = Osyris::AltitudeMode::ISA;
		trkmsg.alt = (int)pTrack->m_Altitude*100;
	}
	else 
		trkmsg.altValid = 0;
	
	trkmsg.altimeterSettingValid = 0;
	
	if (pTrack->m_SPEED)
	{
		trkmsg.spdValid = 1;
		trkmsg.spd = pTrack->m_SPEED;
	}
	else trkmsg.spdValid = 0;
	
	double Hdg = CFlightPlan::calculCAPA(pTrack->GetPoint(0),pTrack->m_Velpos);
	if (Hdg !=-1.0)
	{
		trkmsg.trackValid = 1;
		trkmsg.track = Hdg;
	}
	else 
		trkmsg.trackValid = 0;
	
//	if (pTrack->m_VSpeed)
	{
		trkmsg.vcrValid = 1;
		trkmsg.vcr = pTrack->m_VSpeed * 100;
	}
//	else trkmsg.vcrValid = 0;
	
	double  Latitude; 
	double Longitude;
	ComputeLatLong (pTrack->GetPoint(0),pTrack->m_Altitude, Latitude, Longitude);
	trkmsg.lat = Latitude;
	trkmsg.lon = Longitude;
	
	
	// Pass the flight plan message to O4D:
	if (pTrack->m_pFpl->m_Arcid=="XGO5DK ")
	{
		CString str;
		str.Format("Trk Lat=%f,Long=%f,alt=%d spd=%f hdg=%f vcr=%f",Latitude,Longitude,trkmsg.alt,trkmsg.spd,trkmsg.track,trkmsg.vcr);
		OutputDebugString(str+"\n");
	}
    m_pO4D->Handle(&trkmsg, 0);
	pTrack->m_SentToO4D=true;
#endif
	
	
}

void CO4DMngr::ManageFpl(CFlightPlan* pFpl,bool Terminated)
{
	if (!m_Enabled)
		return;
#ifdef SetO4D
//	if (!pFpl->m_pTrack || !pFpl->m_pTrack->m_SentToO4D)
//		return;
	FplMsg fplmsg;
	fplmsg.contextId = Osyris::DEFAULT_CONTEXT; 
	
	fplmsg.callSign = pFpl->m_Arcid;
	fplmsg.flightId = pFpl->m_FplNbr;
	if (Terminated)
		fplmsg.terminated = 1;
	else
		fplmsg.terminated = 0;
	
	fplmsg.valid[FplMsg::ssrCodeFlag] = true;
	fplmsg.ssrCode = pFpl->m_Ssr;
	
	fplmsg.valid[FplMsg::acDataFlag] = true;
	fplmsg.acData.icaoCode = pFpl->m_ArcTyp;
	fplmsg.acData.wakeCategory = pFpl->m_Wtc;
	
	fplmsg.acData.engineTypeValid = 0;
	// field not used because engineTypeValid = 0 :m_fplMsg.acData.engineType = 0;
	fplmsg.acData.speedCategory = "";
	
	fplmsg.valid[FplMsg::origDestFlag] = true;
	fplmsg.origin = pFpl->m_Adep;
	fplmsg.destination = pFpl->m_Ades;
	
	fplmsg.valid[FplMsg::sidFlag] = false;
	fplmsg.sid = ""; // invalid
	
	fplmsg.valid[FplMsg::starFlag] = false;
	fplmsg.star = ""; // invalid
	
	fplmsg.valid[FplMsg::rflFlag] = true;
	fplmsg.rfl = (long)pFpl->m_Rfl; 
	
	if (pFpl->m_Cfl > 0)
	{
		fplmsg.valid[FplMsg::cflFlag] = true;
		fplmsg.cfl = (long)pFpl->m_Cfl;//*100; // in FL 
	}
	else 
		fplmsg.valid[FplMsg::cflFlag] = false;
	
	fplmsg.valid[FplMsg::cruiseSpdFlag] = false;
	fplmsg.cruiseSpd = pFpl->m_TasPln;
	fplmsg.cruiseSpdType = Osyris::TAS;
	
	fplmsg.valid[FplMsg::atdFlag] = false;
	fplmsg.atd = 0; // invalid
	
//	if (pFpl->m_CurRoute.GetSize() -__max(pFpl->m_CurFix,0)> 0)
//		fplmsg.valid[FplMsg::wayPointsFlag] = true;
//	else		
		fplmsg.valid[FplMsg::wayPointsFlag] = false;

/*	fplmsg.wayPoints = std::vector<WaypointMsg *>();
	CString str1;
	for (int i=__max(pFpl->m_CurFix,0);i<pFpl->m_CurRoute.GetSize();i++)
	{
		double Lat,Lon;
		WaypointMsg * W = new WaypointMsg();
		W->name = "";
		ComputeLatLong(pFpl->m_CurRoute[i].Pos,0,Lat,Lon);
		W->lat = Lat;
		W->lon = Lon;
		fplmsg.wayPoints.push_back(W);
	}*/ 
	
	fplmsg.valid[FplMsg::flightRuleFlag] = true;
	if ((pFpl->m_FlightRules == ' ') || (pFpl->m_FlightRules == 'I') )
		fplmsg.flightRule = Osyris::FlightRule::I;
	else if (pFpl->m_FlightRules == 'Z')
		fplmsg.flightRule = Osyris::FlightRule::Z;
	else if (pFpl->m_FlightRules == 'Y')
		fplmsg.flightRule = Osyris::FlightRule::Y;
	else
		fplmsg.flightRule = Osyris::FlightRule::V;
	
/*	if (pFpl->m_Arcid=="XGO5DK ")
	{
		CString str;
		str.Format("Fpl Ssr=%d Type=%s Wtc=%s Rfl=%d Cfl=%d Ades=%s Ades=%s Tas=%f Route=%s FltRule=%d",fplmsg.ssrCode,&fplmsg.acData.icaoCode[0],&fplmsg.acData.wakeCategory[0],fplmsg.rfl,fplmsg.cfl,&fplmsg.origin[0],&fplmsg.destination[0],fplmsg.cruiseSpd,str1,fplmsg.flightRule);
		OutputDebugString(str+"\n");
	}*/
	
	m_pO4D->Handle(&fplmsg, 0);
	
	pFpl->m_SentToO4D=true;
#endif
}


bool CO4DMngr::ManageTrajectory(CFlightPlan* pFpl)
{
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	if (!m_Enabled)
		return 0;
#ifdef SetO4D
	if (!pFpl->m_SentToO4D || !pFpl->m_pTrack || !pFpl->m_pTrack->m_SentToO4D)
		return 0;
	TrajectoryReqMsg trajectoryreqmsg;
	CString tmp,str;
	
//    m_pO4D->Periodic((long)(CTime::GetCurrentTime().GetTime()));
	bool Compute=true;
//	if (pApp->m_Centre==CVisuApp::GVA && pFpl->m_HeadingType==1 && pFpl->IsArrival())
//		Compute=false;
//	if (pFpl->m_Xpt=="HLD")
//		Compute=false;
//	if ((pFpl->m_CurFdpFix >= pFpl->m_IptFdpFix) && (pFpl->m_UnableToApproach) && (CTime::GetCurrentTime()>pFpl->m_Eti))
//		Compute=false;
	if (pFpl->m_Arcid=="AFR301D")
		int merd=7;
	if ((pFpl->m_XptFix!=-1|| (/*pFpl->m_IptFix!=-1 &&*/ pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible()) || (pFpl->m_RoutingKind==NormalRtg)||(pFpl->m_RoutingKind==WxRtg)) /*&& (pFpl->m_HldStatus!=AutoNoHld || pFpl->m_HldAck) && (pFpl->m_HldStatus!=ExitHld)*/ && Compute)
	{
        // Request for real world, not for what-if scenario:
		trajectoryreqmsg.contextId =  Osyris::DEFAULT_CONTEXT;
		
        // The requestId will be written to the resulting trajectory message
        // and can be used in order to identify the output trajectory
        // that is the answer on the current request:
		const int requestId = OsyrisCommon::NewUniqueRequestId();
		
		CString Msg;
		Msg.Format("O4d:%s,Cfl=%d,Efl=%d,Xfl=%d",pFpl->m_Arcid,pFpl->m_Cfl,pFpl->m_Efl,pFpl->m_Xfl);
		DebugFpl(pFpl,Msg);
		trajectoryreqmsg.requestId = requestId;
		//   trajectoryreqmsg.contextId = 1;
		
        // Identify the flight for which a trajectory shall be calculated:
		trajectoryreqmsg.callSign = pFpl->m_Arcid;
		trajectoryreqmsg.flightId = pFpl->m_FplNbr;
		
        // Request the calculation of a trajectory between two given waypoints
        // (not a calculation starting from the current radar position):
        //m_trajectoryReqMsg.trajectoryType = Osyris::TRJ_PLANNED;
		trajectoryreqmsg.trajectoryType = Osyris::TRJ_FORECAST;
		
		//m_trajectoryReqMsg.speedProfileType = Osyris::MAXACC;
		trajectoryreqmsg.speedProfileType = Osyris::PREFERRED;
		trajectoryreqmsg.requestLifetime = 0;		
		
		
		// Constraints from message -2 can be released (to avoid memory leak)
		
		trajectoryreqmsg.constraintPoints = std::vector<ConstraintPointMsg *>();
		bool DoIt=false;
		
		if ((pFpl->m_XptFix!=-1))// || (pFpl->m_IptFix!=-1 && pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible()))
		{
			// next constraint on the points of the route
			for (int i=__max(pFpl->m_CurFix,0);i<pFpl->m_CurRoute.GetSize();i++)
			{
				DoIt=true;
				double Lat,Lon;
				ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
				CString str;
				constraintPointMsg->wayPoint.name = "";
				ComputeLatLong(pFpl->m_CurRoute[i].Pos,0,Lat,Lon);
				constraintPointMsg->wayPoint.lat = Lat;
				constraintPointMsg->wayPoint.lon = Lon;
				int PointAltitude=0;
				if ( (i==pFpl->m_EptFix) && (!pFpl->IsDept()) )
				{
					// put the EFL in the first point if no CFL
					PointAltitude = pFpl->m_Efl * 100;
				}
				else 
				{
					if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible())
					{
						PointAltitude = __min(pFpl->m_Efl,180) * 100;
					}
					if (i==pFpl->m_XptFix-2  && pFpl->m_Star.GetLength() && pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible())
					{
						PointAltitude = 70  * 100;
					}
				/*	if (pFpl->IsHectPointName(pFpl->m_CurRoute[i].Name))
					{
						PointAltitude = __min(pFpl->m_Efl, 180) * 100;
					}*/
					if (i==pFpl->m_XptFix) 
					{
						// put the XFL in the last point
						PointAltitude = pFpl->m_Xfl  * 100;
					}
					if (i==pFpl->m_XptFix  && pApp->m_Centre==CVisuApp::ZRH && pFpl->IsArrival())
					{
						PointAltitude = 130  * 100;
					}
				}
				if (i>pFpl->m_XptFix)
					PointAltitude=pFpl->m_Xfl  * 100;
				
				// to avoid duplicate points : that may provoque error ERR_DUPLICATE_CONSTRAINT_INCONSISTENT
				//    check that the track is not near all points
				//const int NearTheTrack = Osyris::Messages::WaypointMsg::Equal(&constraintPointMsg->wayPoint, &trk_constraintPointMsg->wayPoint);
				//if (NearTheTrack == 0) AtLeastOneDifferentPoint = true;
				
				if (PointAltitude)
				{
					constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->altAlapValue = PointAltitude;
				}
				else
				{
					constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
				}
				
				// no speed constraint for the route points
				constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;
				int Speed=0;
				if (pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible())
				{
					constraintPointMsg->spdAlapType = Osyris::CAS;
					if ((pFpl->m_Ades=="LFLB")||(pFpl->m_Ades=="LFLP"))
					{
						if ((i==pFpl->m_EptFix  && pFpl->m_pDoc->m_HldAuto && (pFpl->m_pTrack->m_UpdateNBFDB<=pFpl->m_pDoc->m_DelayHect||pFpl->m_HectAt)) ||
							(i==pFpl->m_IptFix  && pFpl->m_pDoc->m_HldAuto==0))
						{
							constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->spdAlapValue = 250;
							Speed=250;
						}
						
					}
					else
					{
						if (i==pFpl->m_EptFix  && pFpl->m_Star.GetLength() && pFpl->m_pDoc->m_HldAuto && (pFpl->m_pTrack->m_UpdateNBFDB<=pFpl->m_pDoc->m_DelayHect ||pFpl->m_HectAt))
						{
							constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->spdAlapValue = 250;
							Speed=250;
						}
						if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pFpl->m_pDoc->m_HldAuto && (pFpl->m_pTrack->m_UpdateNBFDB<=pFpl->m_pDoc->m_DelayHect ||pFpl->m_HectAt))
						{
							constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->spdAlapValue = 220;
							Speed=220;
						}
						if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pFpl->m_pDoc->m_HldAuto==0)
						{
							constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->spdAlapValue = 250;
							Speed=250;
						}
						if ((i==pFpl->m_XptFix-2||i==pFpl->m_XptFix) && pFpl->m_Star.GetLength() && (pFpl->m_pDoc->m_HldAuto==0 || (pFpl->m_pTrack->m_UpdateNBFDB<=pFpl->m_pDoc->m_DelayHect||pFpl->m_HectAt)))
						{
							constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->spdAlapValue = 160;
							Speed=160;
						}
						
					}
				}
				{
					str.Format("Traj:Fpl Point %s (%f,%f) Alt=%d Spd=%d(%d) ",pFpl->m_CurRoute[i].LongName,Lat,Lon,PointAltitude,Speed,constraintPointMsg->spdAlapType);
					pFpl->DebugStr(str);
				}
				
				PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
				tmp.Format("%d",i);
				pntMsg->remark = tmp;
				constraintPointMsg->ext.push_back(pntMsg);
				trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
			}
		}
		else
		{
			double Lat,Lon;
			if (pFpl->IsHectPossible() && (pFpl->m_IptFix==-1||pFpl->m_XptFix==-1) && pFpl->m_CurFdpFix<pFpl->m_FdpRoute.GetSize())
			{
				double bearingtrk =  CFlightPlan::calculCAPA(pFpl->m_pTrack->GetPoint(0),pFpl->m_pTrack->m_Velpos);
				if ((pFpl->m_HeadingType==1) && pFpl->m_CurRoute.GetSize())
					bearingtrk=pFpl->calculCAPA(pFpl->m_pTrack->GetPoint(0),pFpl->m_CurRoute[pFpl->m_CurRoute.GetUpperBound()].GeoPos);
				for (int i=pFpl->m_CurFdpFix;i<=pFpl->m_XptFdpFix;i++)
				{
					DoIt=true;
					ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
					CString str;
					constraintPointMsg->wayPoint.name = "";
					CPoint Pos=pFpl->m_FdpRoute[i].GeoPos;
					if (i==pFpl->m_CurFdpFix)
						Pos=pFpl->CalculProjection(pFpl->m_pTrack->GetPoint(0),pFpl->m_FdpRoute[pFpl->m_CurFdpFix].GeoPos,bearingtrk);
					ComputeLatLong(Pos,0,Lat,Lon);
					constraintPointMsg->wayPoint.lat = Lat;
					constraintPointMsg->wayPoint.lon = Lon;
					int PointAltitude=0;
					// put the XFL in the last point
					if (i==pFpl->m_XptFdpFix)
						PointAltitude = pFpl->m_Xfl  * 100;
					if (PointAltitude)
					{
						constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
						constraintPointMsg->altAlapValue = PointAltitude;
					}
					else
					{
						constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
					}
					{
						str.Format("Traj for HECT:Fpl Point %s (%f,%f) Alt=%d",pFpl->m_FdpRoute[i].LongName,Lat,Lon,PointAltitude);
						DebugFpl(pFpl,str);
					}
					
					// no speed constraint for the route points
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;
					
					PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
					tmp.Format("%d",i);
					pntMsg->remark = tmp;
					constraintPointMsg->ext.push_back(pntMsg);
					trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
				}

				
			}
			else
			{
				int FirstPnt=pFpl->FindFirstTurningFdpPoint();
				if (((pFpl->m_HeadingType==1)||(pFpl->m_RoutingKind==WxRtg)) && (FirstPnt!=-1))
				{
					for (int i=FirstPnt;i<=pFpl->m_XptFdpFix;i++)
					{
						DoIt=true;
						ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
						CString str;
						constraintPointMsg->wayPoint.name = "";
						CPoint Pos=pFpl->m_FdpRoute[i].GeoPos;
						if (pFpl->m_pTrack && i==FirstPnt)
						{
							double bearing = pFpl->calculCAPA(pFpl->m_pTrack->GetPoint(0),pFpl->m_pTrack->m_Velpos);
							if ((pFpl->m_HeadingType==1) && pFpl->m_CurRoute.GetSize())
								bearing=pFpl->calculCAPA(pFpl->m_pTrack->GetPoint(0),pFpl->m_CurRoute[pFpl->m_CurRoute.GetUpperBound()].GeoPos);
							Pos=pFpl->CalculProjection(pFpl->m_pTrack->GetPoint(0),pFpl->m_FdpRoute[i].GeoPos,bearing);
						}
						ComputeLatLong(Pos,0,Lat,Lon);
						constraintPointMsg->wayPoint.lat = Lat;
						constraintPointMsg->wayPoint.lon = Lon;
						int PointAltitude=0;
						// put the XFL in the last point
						if (i==pFpl->m_XptFdpFix)
							PointAltitude = pFpl->m_Xfl  * 100;
						
						// to avoid duplicate points : that may provoque error ERR_DUPLICATE_CONSTRAINT_INCONSISTENT
						//    check that the track is not near all points
						//const int NearTheTrack = Osyris::Messages::WaypointMsg::Equal(&constraintPointMsg->wayPoint, &trk_constraintPointMsg->wayPoint);
						//if (NearTheTrack == 0) AtLeastOneDifferentPoint = true;
						
						if (PointAltitude)
						{
							constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->altAlapValue = PointAltitude;
						}
						else
						{
							constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
						}
						{
							str.Format("Traj:Fpl Point %s (%f,%f) Alt=%d",pFpl->m_FdpRoute[i].LongName,Lat,Lon,PointAltitude);
							DebugFpl(pFpl,str);
						}
						
						// no speed constraint for the route points
						constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;
						
						PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
						tmp.Format("%d",i);
						pntMsg->remark = tmp;
						constraintPointMsg->ext.push_back(pntMsg);
						trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
					}
				}
				else
				{
					if (pFpl->m_CurFdpFix<=pFpl->m_XptFdpFix)
					{
						DoIt=true;
						double distanceXpt = pFpl->CalculDistance(pFpl->m_FdpRoute[pFpl->m_XptFdpFix].Pos,pFpl->m_pTrack->GetPoint(0));
						CPoint Pos;
						double bearingtrk =  CFlightPlan::calculCAPA(pFpl->m_pTrack->GetPoint(0),pFpl->m_pTrack->m_Velpos);
						if (pFpl->m_RefPoint.x || pFpl->m_RefPoint.y)
						{
							double distance1 = pFpl->CalculDistance(pFpl->m_RefPoint,pFpl->m_pTrack->GetPoint(0));
							double distance2 = pFpl->CalculDistance(pFpl->m_RefPoint,pFpl->m_FdpRoute[pFpl->m_XptFdpFix].Pos);
							if (distance1>distance2)
								DoIt=false;
							Pos.x = pFpl->m_pTrack->GetPoint(0).x + (distance2-distance1)*sin(bearingtrk*PI/180.);
							Pos.y = pFpl->m_pTrack->GetPoint(0).y + (distance2-distance1)*cos(bearingtrk*PI/180.);
						}
						else
						{
							if ((pFpl->m_HeadingType==1) && pFpl->m_CurRoute.GetSize())
								bearingtrk=pFpl->calculCAPA(pFpl->m_pTrack->GetPoint(0),pFpl->m_CurRoute[pFpl->m_CurRoute.GetUpperBound()].GeoPos);
							Pos.x = pFpl->m_pTrack->GetPoint(0).x + distanceXpt*sin(bearingtrk*PI/180.);
							Pos.y = pFpl->m_pTrack->GetPoint(0).y + distanceXpt*cos(bearingtrk*PI/180.);
						}
						ComputeLatLong(Pos,0,Lat,Lon);
						ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
						constraintPointMsg->wayPoint.name = "";
						constraintPointMsg->wayPoint.lat = Lat;
						constraintPointMsg->wayPoint.lon = Lon;
						int PointAltitude=0;
						// put the XFL in the last point
						PointAltitude = pFpl->m_Xfl  * 100;
						if (PointAltitude)
						{
							constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
							constraintPointMsg->altAlapValue = PointAltitude;
						}
						else
						{
							constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
						}
						constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;
						
						PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
						tmp.Format("%d",pFpl->m_XptFdpFix);
						pntMsg->remark = tmp;
						constraintPointMsg->ext.push_back(pntMsg);
						trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
					}
				}
			}
		}
		
		if (DoIt)
		{
			
			trajectoryreqmsg.constraintPointsDefineRoute = 1;
			
			trajectoryreqmsg.rflValid=1;
			trajectoryreqmsg.rfl= pFpl->m_Rfl*100.0;

			// Define some aircraft data for calculation
			trajectoryreqmsg.acDataValid = 1;
			trajectoryreqmsg.acData.icaoCode = pFpl->m_ArcTyp;
			trajectoryreqmsg.acData.wakeCategory = pFpl->m_Wtc;
			
			RadarMergeParameterMsg* MergeMsg = new RadarMergeParameterMsg;
			MergeMsg->constraintPointIndex=0;
			trajectoryreqmsg.ext = std::vector< OsyrisMessage * >();
			trajectoryreqmsg.ext.push_back(MergeMsg);
			
			
			// Pass the trajectory request message to O4D:
			DebugFpl(pFpl,"O4D:Begin of Trajectory");
			m_pO4D->Handle(&trajectoryreqmsg, 0);
			DebugFpl(pFpl,"O4D:End of Trajectory");
		}
		}
		if (pApp->m_Centre==CVisuApp::GVA && pFpl->m_Ades=="LSGG")
		{
			TrajectoryReqMsg trajectoryreqmsg2;
			trajectoryreqmsg2.contextId =  801;
	
			trajectoryreqmsg2.requestId = OsyrisCommon::NewUniqueRequestId();
			
			// Identify the flight for which a trajectory shall be calculated:
			trajectoryreqmsg2.callSign = pFpl->m_Arcid;
			trajectoryreqmsg2.flightId = pFpl->m_FplNbr;
			
			trajectoryreqmsg2.trajectoryType = Osyris::TRJ_FORECAST;
			
			trajectoryreqmsg2.speedProfileType = Osyris::PREFERRED;
			trajectoryreqmsg2.requestLifetime = 0;		
			
			
			// Constraints from message -2 can be released (to avoid memory leak)
			
			trajectoryreqmsg2.constraintPoints = std::vector<ConstraintPointMsg *>();
			bool DoIt=false;
			double Lat,Lon;
			int PointAltitude=0;
			for (int i=__max(pFpl->m_CurFdpFix,0);i<pFpl->m_FdpRoute.GetSize();i++)
			{
				bool Add=false;
				int Speed=0;
				if (i==pFpl->m_EptFdpFix)
				{
					// put the EFL in the first point if no CFL
					PointAltitude = min(pFpl->m_Efl,pFpl->m_pTrack->m_Altitude) * 100;
					if (pFpl->m_pDoc->m_HldAuto)
						Speed=250;
					Add=true;
				}
				if (i==pFpl->m_XptFdpFix-2  && pFpl->m_Star.GetLength() && pApp->m_Centre==CVisuApp::GVA && pFpl->IsHectEligible())
				{
					PointAltitude = 70  * 100;
					Speed=160;
					Add=true;
				}
				if (i==pFpl->m_XptFdpFix) 
				{
					// put the XFL in the last point
					PointAltitude = pFpl->m_Xfl  * 100;
					Speed=160;
					Add=true;
				}
				
				// to avoid duplicate points : that may provoque error ERR_DUPLICATE_CONSTRAINT_INCONSISTENT
				//    check that the track is not near all points
				//const int NearTheTrack = Osyris::Messages::WaypointMsg::Equal(&constraintPointMsg->wayPoint, &trk_constraintPointMsg->wayPoint);
				//if (NearTheTrack == 0) AtLeastOneDifferentPoint = true;
				


				if (Add)
				{
					DoIt=true;
					ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
					constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
					if (PointAltitude)
					{
						constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
						constraintPointMsg->altAlapValue = PointAltitude;
					}
					
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;
					if (Speed)
					{
						constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
						constraintPointMsg->spdAlapValue = Speed;
					}
					CString str;
					constraintPointMsg->wayPoint.name = "";
					ComputeLatLong(pFpl->m_FdpRoute[i].Pos,0,Lat,Lon);
					constraintPointMsg->wayPoint.lat = Lat;
					constraintPointMsg->wayPoint.lon = Lon;
					
					str.Format("Traj EA3:Fpl Point %s (%f,%f) Alt=%d Speed=%d",pFpl->m_FdpRoute[i].LongName,Lat,Lon,PointAltitude,Speed);
					DebugFpl(pFpl,str);
					
					PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
					tmp.Format("%d",i);
					pntMsg->remark = tmp;
					constraintPointMsg->ext.push_back(pntMsg);
					trajectoryreqmsg2.constraintPoints.push_back(constraintPointMsg);
				}
			}
			if (DoIt)
			{
				
				trajectoryreqmsg2.constraintPointsDefineRoute = 1;
				
				trajectoryreqmsg2.rflValid=1;
				trajectoryreqmsg2.rfl= pFpl->m_Rfl*100.0;
				
				// Define some aircraft data for calculation
				trajectoryreqmsg2.acDataValid = 1;
				trajectoryreqmsg2.acData.icaoCode = pFpl->m_ArcTyp;
				trajectoryreqmsg2.acData.wakeCategory = pFpl->m_Wtc;
				
				RadarMergeParameterMsg* MergeMsg = new RadarMergeParameterMsg;
				MergeMsg->constraintPointIndex=0;
				trajectoryreqmsg2.ext = std::vector< OsyrisMessage * >();
				trajectoryreqmsg2.ext.push_back(MergeMsg);
				
				
				// Pass the trajectory request message to O4D:
				DebugFpl(pFpl,"O4D:Begin of Trajectory EA3");
				m_pO4D->Handle(&trajectoryreqmsg2, 0);
				DebugFpl(pFpl,"O4D:End of Trajectory EA3");
			}

		
		}
		return true;

#endif

return true;
}


bool CO4DMngr::IsEnabled()
{
	return m_Enabled;
}

bool CO4DMngr::IsLtcaEnabled()
{
	return m_LtcaEnabled;
}


void CO4DMngr::Heading2Bearing(CPoint Pos,int level,double heading, double speed,  double& bearing, double& grdspeed)
{
#ifdef SetO4D
	double Lat,Long,u,v,x,y;
	ComputeLatLong (Pos, level, Lat,Long);
	Trafo::LLDeg2XY(Lat,Long,x,y);
	m_pEnv->GetWind(x,y,level*100,u,v);
	double pi = 3.141592654;
	double xsp = speed * sin(heading*pi/180);
	double ysp = speed * cos(heading*pi/180);
	double xtrk = xsp + (u*3600.0/1852.0);
	double ytrk = ysp + (v*3600.0/1852.0);
	bearing = CFlightPlan::calculCAPA(CPoint(0, 0), CPoint(xtrk, ytrk));
	grdspeed = sqrt( xtrk * xtrk + ytrk * ytrk);
#endif
}

void CO4DMngr::Bearing2Heading(CPoint Pos,int level,double bearing, double grdspeed, double& heading, double& speed)
{
#ifdef SetO4D
	double Lat,Long,u,v,x,y;
	ComputeLatLong (Pos, level, Lat,Long);
	Trafo::LLDeg2XY(Lat,Long,x,y);
	m_pEnv->GetWind(x,y,level*100,u,v);
	double pi = 3.141592654;
	double xsp = grdspeed * sin(bearing*pi/180);
	double ysp = grdspeed * cos(bearing*pi/180);
	double xhgd = xsp - (u*3600.0/1852.0);;
	double yhdg = ysp - (v*3600.0/1852.0);;
	heading = CFlightPlan::calculCAPA(CPoint(0, 0),CPoint(xhgd, yhdg));
	speed = sqrt( xhgd * xhgd + yhdg * yhdg);
#endif
}

bool CO4DMngr::GetWindState()
{
#ifdef SetO4D
    m_pO4D->Periodic((long)(CVisuApp::GetCurrentTime().GetTime()));
	if (!m_pEnv)
		return false;
	Osyris::MeteoState state=m_pEnv->CurrentMeteoState();
	CString Log,filename;
	CFileStatus status;
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	if (state.windState!=Osyris::WindState::FILE_WIND)
	{
		Log.Format("O4D is not using wind files");
		//pApp->LogMsg(Log);
		return false;
	}
	else
	{
		CTime curtime=CVisuApp::GetCurrentTime();
		
		int Hour=curtime.GetHour();
		if (curtime.GetHour()>=6 && curtime.GetHour()<18)
			filename.Format("WIND.M.%.2d",curtime.GetHour());
		else
		{
			if (curtime.GetHour()<6)
				filename.Format("WIND.E.%.2d",curtime.GetHour()+24);
			else
				filename.Format("WIND.E.%.2d",curtime.GetHour());
		}

		BOOL res=CFile::GetStatus(WindPath+"\\"+filename,status);
		if (!res || (curtime-status.m_mtime>=WindTimeOut))
		{
			pApp->LogMsg("Wind file "+filename+" is obsolete");
			return false;
		}
		
	}
#endif
	return true;
}


void CO4DMngr::ManageFplTrajectory(CFlightPlan* pFpl)
{
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	if (!m_Enabled)
		return;
#ifdef SetO4D
//	if (!pFpl->m_SentToO4D)
//		return 0;
	TrajectoryReqMsg trajectoryreqmsg;
	CString tmp,str;
	
    //m_pO4D->Periodic((long)(CTime::GetCurrentTime().GetTime()));
	// Request for real world, not for what-if scenario:
	trajectoryreqmsg.contextId =  Osyris::DEFAULT_CONTEXT;
	
	// The requestId will be written to the resulting trajectory message
	// and can be used in order to identify the output trajectory
	// that is the answer on the current request:
	const int requestId = OsyrisCommon::NewUniqueRequestId();
	
	CString Msg;
	Msg.Format("O4d:%s,Cfl=%d,Efl=%d,Xfl=%d",pFpl->m_Arcid,pFpl->m_Cfl,pFpl->m_Efl,pFpl->m_Xfl);
	DebugFpl(pFpl,Msg);
	trajectoryreqmsg.requestId = requestId;
	trajectoryreqmsg.contextId = -1;
	
	// Identify the flight for which a trajectory shall be calculated:
	trajectoryreqmsg.callSign = pFpl->m_Arcid;
	trajectoryreqmsg.flightId = pFpl->m_FplNbr;
	
	// Request the calculation of a trajectory between two given waypoints
	// (not a calculation starting from the current radar position):
	trajectoryreqmsg.trajectoryType = Osyris::TRJ_PLANNED;
	
	trajectoryreqmsg.speedProfileType = Osyris::PREFERRED;
	
	
	// Constraints from message -2 can be released (to avoid memory leak)
	
	trajectoryreqmsg.constraintPoints = std::vector<ConstraintPointMsg *>();
	CTime TimeRef;
	int   TimeRefIndex=-2;
	// next constraint on the points of the route
	for (int i=0;i<pFpl->m_CurRoute.GetSize();i++)
	{
		double Lat,Lon;
		ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
		CString str;
		str.Format("%x",constraintPointMsg);
		constraintPointMsg->wayPoint.name = "";
		if (i==0 && pFpl->m_EptFix==-1 && pFpl->m_EptFdpFix!=-1 && TimeRefIndex==-2)
		{
			ComputeLatLong(pFpl->m_FdpRoute[pFpl->m_EptFdpFix].Pos,0,Lat,Lon);
			i=-1;
		}
		else
			ComputeLatLong(pFpl->m_CurRoute[i].Pos,0,Lat,Lon);
		constraintPointMsg->wayPoint.lat = Lat;
		constraintPointMsg->wayPoint.lon = Lon;
		int PointAltitude=0;
		if ( (i<=pFpl->m_EptFix))// && (!pFpl->IsDept()) )
		{
			// put the EFL in the first point if no CFL
			PointAltitude = pFpl->m_Efl * 100;
			TimeRef=pFpl->m_Etn;
			TimeRefIndex=i;
		}
		else 
		{
			if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pApp->m_Centre==CVisuApp::GVA && pFpl->IsArrival())
			{
				PointAltitude = __min(pFpl->m_Efl,100)  * 100;
			}
			if (i==pFpl->m_XptFix-1  && pFpl->m_Star.GetLength() && pApp->m_Centre==CVisuApp::GVA && pFpl->IsArrival())
			{
				PointAltitude = 70  * 100;
			}
			if (i==pFpl->m_XptFix) 
			{
				// put the XFL in the last point
				if ((pFpl->m_Ades=="LFLB")||(pFpl->m_Ades=="LFLP"))
					PointAltitude = 13  * 100;
				else
					PointAltitude = pFpl->m_Xfl  * 100;
			}
			if (i==pFpl->m_XptFix  && pApp->m_Centre==CVisuApp::ZRH && pFpl->IsArrival())
			{
				PointAltitude = 130  * 100;
			}
		}
		if (i>pFpl->m_XptFix)
			PointAltitude=pFpl->m_Xfl  * 100;
		
		// to avoid duplicate points : that may provoque error ERR_DUPLICATE_CONSTRAINT_INCONSISTENT
		//    check that the track is not near all points
		//const int NearTheTrack = Osyris::Messages::WaypointMsg::Equal(&constraintPointMsg->wayPoint, &trk_constraintPointMsg->wayPoint);
		//if (NearTheTrack == 0) AtLeastOneDifferentPoint = true;
		
		if (PointAltitude)
		{
			constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
			constraintPointMsg->altAlapValue = PointAltitude;
		}
		else
		{
			constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
		}
		
		// no speed constraint for the route points
		constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;
		int Speed=0;
		if (pApp->m_Centre==CVisuApp::GVA)
		{
			constraintPointMsg->spdAlapType = Osyris::CAS;
			if (pFpl->m_Ades=="LSGG")
			{
				if (i>pFpl->m_EptFix  && pFpl->m_Star.GetLength() && pFpl->m_pDoc->m_HldAuto)
				{
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->spdAlapValue = 220;
					Speed=250;
				}
				if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pFpl->m_pDoc->m_HldAuto)
				{
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->spdAlapValue = 220;
					Speed=220;
				}
				if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pFpl->m_pDoc->m_HldAuto==0)
				{
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->spdAlapValue = 250;
					Speed=220;
				}
				if (i==pFpl->m_XptFix-1 && pFpl->m_Star.GetLength())
				{
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->spdAlapValue = 160;
					Speed=160;
				}
				
			}
			if ((pFpl->m_Ades=="LFLB")||(pFpl->m_Ades=="LFLP"))
			{
				if (i==pFpl->m_IptFix)
				{
					constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->spdAlapValue = 250;
					Speed=constraintPointMsg->spdAlapValue;
				}
				
			}
		}
		if (i>=0)
		{
			str.Format("Traj:Fpl Point %s (%f,%f) Alt=%d Spd=%d ",pFpl->m_CurRoute[i].LongName,Lat,Lon,PointAltitude,Speed);
			DebugFpl(pFpl,str);
		}
		
		PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
		tmp.Format("%d",i);
		pntMsg->remark = tmp;
		constraintPointMsg->ext.push_back(pntMsg);
		trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
	}
	
	trajectoryreqmsg.constraintPointsDefineRoute = 1;
	
	// Define some aircraft data for calculation
	trajectoryreqmsg.acDataValid = 1;
	trajectoryreqmsg.acData.icaoCode = pFpl->m_ArcTyp;
	trajectoryreqmsg.acData.wakeCategory = pFpl->m_Wtc;


	pFpl->m_FplTOCD.Name.Empty();
	// Pass the trajectory request message to O4D:
	DebugFpl(pFpl,"O4D:Begin of Fpl Trajectory");
	pFpl->m_ReqToO4DOk=false;
	m_pO4D->Handle(&trajectoryreqmsg, 0);
	CString err;
	if (pFpl->m_ReqToO4DOk && (TimeRefIndex!=-2))
	{
		pFpl->m_O4DCalculMode=CFlightPlan::FplCalcul;
		err.Format("Traj Fpl Etn=%s Etx=%s TimeRef=%s",pFpl->m_Etn.Format("%H:%M:%S"),pFpl->m_Etx.Format("%H:%M:%S"),TimeRef.Format("%H:%M:%S"));
		CTimeSpan Offset=TimeRef-CTime(0);
		if (TimeRefIndex!=-1)
			Offset=TimeRef-pFpl->m_CurRoute[TimeRefIndex].To;
		for (int i=0;i<pFpl->m_CurRoute.GetSize();i++)
		{
			pFpl->m_CurRoute[i].To+=Offset;
			err.Format("Traj Fpl Alt=%d Time %s Name=%s",pFpl->m_CurRoute[i].Alt,pFpl->m_CurRoute[i].To.Format("%H:%M:%S"),pFpl->m_CurRoute[i].Name);
			DebugFpl(pFpl,err);
			if (i==pFpl->m_XptFix)
			{
				int Diff=(pFpl->m_Etx-pFpl->m_CurRoute[i].To).GetTotalSeconds();
				err.Format("Traj Fpl Etn=%s Etx=%s",pFpl->m_Etn.Format("%H:%M:%S"),pFpl->m_Etx.Format("%H:%M:%S"));
				DebugFpl(pFpl,err);
				if (abs(Diff)>20)
					int merd=7;
			}

		}
	}
	DebugFpl(pFpl,"O4D:End of Fpl Trajectory");
#endif
}


void CO4DMngr::ManageTmpFplTrajectory(CFlightPlan* pFpl)
{
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	if (!m_Enabled)
		return;
#ifdef SetO4D
//	if (!pFpl->m_SentToO4D)
//		return 0;
	TrajectoryReqMsg trajectoryreqmsg;
	CString tmp,str;
	bool found = false;
	
    //m_pO4D->Periodic((long)(CTime::GetCurrentTime().GetTime()));
	// Request for real world, not for what-if scenario:
	trajectoryreqmsg.contextId =  Osyris::DEFAULT_CONTEXT;
	
	// The requestId will be written to the resulting trajectory message
	// and can be used in order to identify the output trajectory
	// that is the answer on the current request:
	const int requestId = OsyrisCommon::NewUniqueRequestId();
	
	CString Msg;
	Msg.Format("O4d:%s,Cfl=%d,Efl=%d,Xfl=%d",pFpl->m_Arcid,pFpl->m_Cfl,pFpl->m_Efl,pFpl->m_Xfl);
	DebugFpl(pFpl,Msg);
	trajectoryreqmsg.requestId = requestId;
	trajectoryreqmsg.contextId = -2;
	
	// Identify the flight for which a trajectory shall be calculated:
	trajectoryreqmsg.callSign = pFpl->m_Arcid;
	trajectoryreqmsg.flightId = pFpl->m_FplNbr;
	
	// Request the calculation of a trajectory between two given waypoints
	// (not a calculation starting from the current radar position):
	trajectoryreqmsg.trajectoryType = Osyris::TRJ_PLANNED;
	
	trajectoryreqmsg.speedProfileType = Osyris::PREFERRED;
	
	
	// Constraints from message -2 can be released (to avoid memory leak)
	
	trajectoryreqmsg.constraintPoints = std::vector<ConstraintPointMsg *>();
	CTime TimeRef;
	int   TimeRefIndex=-2;
	// next constraint on the points of the route
	for (int i=0;i<pFpl->m_TmpFdpRoute.GetSize();i++)
	{
		double Lat,Lon;
		ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
		CString str;
		str.Format("%x",constraintPointMsg);
		constraintPointMsg->wayPoint.name = "";
		ComputeLatLong(pFpl->m_TmpFdpRoute[i].Pos,0,Lat,Lon);
		constraintPointMsg->wayPoint.lat = Lat;
		constraintPointMsg->wayPoint.lon = Lon;
		int PointAltitude=0;
		if (i==0)// && (!pFpl->IsDept()) )
		{
			// put the EFL in the first point if no CFL
			PointAltitude = pFpl->m_Efl * 100;
			TimeRef=pFpl->m_Etn;
			TimeRefIndex=i;
		}
		else 
		{
			if (pFpl->m_TmpFdpRoute[i].PlannedAlt !=0)
			{
				PointAltitude = pFpl->m_TmpFdpRoute[i].PlannedAlt * 100;
			}
			else if (i==pFpl->m_TmpFdpRoute.GetUpperBound()) 
			{
				PointAltitude = pFpl->m_Xfl  * 100;
			}
			else
				PointAltitude=0;
//			else
//				PointAltitude= pFpl->m_TmpFdpRoute[i].PlannedAlt *100;

			if (pFpl->IsHectPointName(pFpl->m_TmpFdpRoute[i].Name) || (found&&i != pFpl->m_TmpFdpRoute.GetUpperBound()))
			{
				PointAltitude = __min(pFpl->m_Efl, 240) * 100;
				found = true;
			}
		}
		
		// to avoid duplicate points : that may provoque error ERR_DUPLICATE_CONSTRAINT_INCONSISTENT
		//    check that the track is not near all points
		//const int NearTheTrack = Osyris::Messages::WaypointMsg::Equal(&constraintPointMsg->wayPoint, &trk_constraintPointMsg->wayPoint);
		//if (NearTheTrack == 0) AtLeastOneDifferentPoint = true;
		
		if (PointAltitude)
		{
			constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
			constraintPointMsg->altAlapValue = PointAltitude;
		}
		else
		{
			constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
		}
		
		if (i>=0)
		{
			str.Format("Traj:Tmp Fpl Point %s (%f,%f) Alt=%d ",pFpl->m_TmpFdpRoute[i].LongName,Lat,Lon,PointAltitude);
			DebugFpl(pFpl,str);
		}
		
		PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
		tmp.Format("%d",i);
		pntMsg->remark = tmp;
		constraintPointMsg->ext.push_back(pntMsg);
		trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
	}
	
	trajectoryreqmsg.constraintPointsDefineRoute = 1;
	
	// Define some aircraft data for calculation
	trajectoryreqmsg.acDataValid = 1;
	trajectoryreqmsg.acData.icaoCode = pFpl->m_ArcTyp;
	trajectoryreqmsg.acData.wakeCategory = pFpl->m_Wtc;


	pFpl->m_FplTOCD.Name.Empty();
	// Pass the trajectory request message to O4D:
	DebugFpl(pFpl,"O4D:Begin of Tmp Fpl Trajectory");
	m_pO4D->Handle(&trajectoryreqmsg, 0);
	DebugFpl(pFpl,"O4D:End of Tmp Fpl Trajectory");
#endif
}

void CO4DMngr::ManageO4DTrajectory(CFlightPlan* pFpl,int ContextId,int Level,CArray<CFlightPlan::PointDef,CFlightPlan::PointDef>& Route,int CurFix,int Delay,TrajTypes type,int CDRate)
{

	FILETIME dummy,time1,time2,timek1,timek2;
		TrajectoryReqMsg trajectoryreqmsg;
		CString tmp,Id;
		
		// The requestId will be written to the resulting trajectory message
		// and can be used in order to identify the output trajectory
		// that is the answer on the current request:
		trajectoryreqmsg.requestId = OsyrisCommon::NewUniqueRequestId();
		trajectoryreqmsg.contextId = ContextId;
		
		// Identify the flight for which a trajectory shall be calculated:
		trajectoryreqmsg.callSign = pFpl->m_Arcid;
		trajectoryreqmsg.flightId = pFpl->m_FplNbr;
		
		// Request the calculation of a trajectory between two given waypoints
		// (not a calculation starting from the current radar position):
		trajectoryreqmsg.trajectoryType = Osyris::TRJ_FORECAST;
		
		trajectoryreqmsg.speedProfileType = Osyris::PREFERRED;
		
		trajectoryreqmsg.cflValid = Osyris::CONSTRAINT_VALID;
		trajectoryreqmsg.cfl = Level;
		trajectoryreqmsg.calculationMode = Osyris::TRJ_CALC_MODE_CONSIDER_CFL;
		trajectoryreqmsg.requestLifetime = 0;		
		
		trajectoryreqmsg.constraintPoints = std::vector<ConstraintPointMsg *>();
		
		bool DoIt=false;
		// next constraint on the points of the route
		for (int i=CurFix;i<Route.GetSize();i++)
		{
			DoIt=true;
			double Lat,Lon;
			ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
			constraintPointMsg->wayPoint.name = "";
			ComputeLatLong(Route.GetAt(i).Pos,0,Lat,Lon);
			constraintPointMsg->wayPoint.lat = Lat;
			constraintPointMsg->wayPoint.lon = Lon;
			constraintPointMsg->altAlapValue = Level*100;
			if ( i==Route.GetUpperBound())
				constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
			else
				constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
			
			// no speed constraint for the route points
			constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;

			PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
			tmp.Format("%d",i);
			pntMsg->remark = tmp;
			constraintPointMsg->ext.push_back(pntMsg);
			trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
		}
		
		if (!DoIt)
			return;
		
		trajectoryreqmsg.constraintPointsDefineRoute = 1;
		
		// Define some aircraft data for calculation
//		trajectoryreqmsg.rflValid=1;
//		trajectoryreqmsg.rfl= Level;


		trajectoryreqmsg.acDataValid = 1;
		trajectoryreqmsg.acData.icaoCode = pFpl->m_ArcTyp;
		trajectoryreqmsg.acData.wakeCategory = pFpl->m_Wtc;
		
		RadarMergeParameterMsg* MergeMsg = new RadarMergeParameterMsg;
		MergeMsg->constraintPointIndex=0;
		trajectoryreqmsg.ext = std::vector< OsyrisMessage * >();
		trajectoryreqmsg.ext.push_back(MergeMsg);
		

		switch (type)
		{
		case Steepest:
			{
				TrajectoryVcrMsg* vcrMsg = new TrajectoryVcrMsg;
				vcrMsg->mode = Osyris::VcrMode::STEEPEST;
				if (Level>pFpl->m_pTrack->m_Altitude)
					vcrMsg->kind = Osyris::CLIMB;
				else
					vcrMsg->kind = Osyris::DESCENT;
				if (ContextId>=100)
					vcrMsg->mass_factor = LtcaSteepestMassFactor;
				else
					vcrMsg->mass_factor = MtcaSteepestMassFactor;
				trajectoryreqmsg.ext.push_back(vcrMsg);
			}
			break;
		case LowAngle:
			{
				TrajectoryVcrMsg* vcrMsg = new TrajectoryVcrMsg;
				vcrMsg->mode = Osyris::VcrMode::LOW_ANGLE;
				if (CDRate)
					vcrMsg->low_angle_vcr=CDRate*100.0;
				else
					vcrMsg->low_angle_vcr=10000;
				if (Level>pFpl->m_pTrack->m_Altitude)
					vcrMsg->kind = Osyris::CLIMB;
				else
					vcrMsg->kind = Osyris::DESCENT;
				if (ContextId>=100)
					vcrMsg->mass_factor = LtcaLowAngleMassFactor;
				else
					vcrMsg->mass_factor = MtcaLowAngleMassFactor;
				trajectoryreqmsg.ext.push_back(vcrMsg);
		
				TrajectoryLatencyMsg* latencyMsg = new TrajectoryLatencyMsg;
				Osyris::Duration duration(Delay);
				latencyMsg->delay = duration;
				trajectoryreqmsg.ext.push_back(latencyMsg);
				Id="2";
			}
			break;
		}
		
	GetProcessTimes(GetCurrentProcess(),&dummy,&dummy,&timek1,&time1);
		
		// Pass the trajectory request message to O4D:
		m_pO4D->Handle(&trajectoryreqmsg, 0);
		
	GetProcessTimes(GetCurrentProcess(),&dummy,&dummy,&timek2,&time2);
	CString str;
	str.Format("%s O4D contextId=%d CpuTime=%d",pFpl->m_Arcid,ContextId,time2.dwLowDateTime-time1.dwLowDateTime);
//	if (time2.dwLowDateTime-time1.dwLowDateTime>0)
//		OutputDebugString(str+"\n");


}


void CO4DMngr::ManageCMTrajectory(CFlightPlan* pFpl,int SectorIndex,int delay,int LevelToGo,int Rate,bool lowang)
{
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	//	OutputDebugString("O4D:Trajectory\n");
	if (!m_Enabled)
		return ;
#ifdef SetO4D
	if (!pFpl->m_SentToO4D || !pFpl->m_pTrack || !pFpl->m_pTrack->m_SentToO4D)
		return;
	int Level=0;
	int CDRate=0;
	if (SectorIndex==-1)
	{
		if (LevelToGo)
		{
			Level=LevelToGo;
			if (Rate)
				CDRate=Rate;
		}
		else
		{
			Level=pFpl->m_Cfl;
			if (!pFpl->m_Cfl)
			{
				if (pFpl->m_TransferringFPL || ((pFpl->m_CurAocIndex>0) && (pFpl->m_Sequence[pFpl->m_CurAocIndex-1]>128)))
				{
					Level=pFpl->m_pTrack->m_Altitude;
					if (pFpl->m_TransferringFPL)
					{
						if (pFpl->m_pTrack->m_ROE>0)
							Level=((pFpl->m_pTrack->m_Altitude+9)/10)*10;
						if (pFpl->m_pTrack->m_ROE<0)
							Level=((pFpl->m_pTrack->m_Altitude)/10)*10;
					}
				}
				else
					Level=pFpl->m_Efl;
			}
			else
				CDRate=pFpl->m_CDRate;
		}
		for (int i=0;i<2;i++)
			pFpl->m_TOCD[i].Name.Empty();

	}
	else
	{
		Level=pFpl->m_SectorLevel[SectorIndex];
		CDRate=pFpl->m_SectorCDRate[SectorIndex];
		for (int i=0;i<2;i++)
			pFpl->m_SectorTOCD[2*SectorIndex+i].Name.Empty();
	}
	if (pFpl->m_pTrack->m_ROE)
		delay=0;

	if (SectorIndex==-1)
		ManageO4DTrajectory(pFpl,1,Level,pFpl->m_CurRoute,pFpl->m_CurFix,0,Steepest,CDRate);
	else
	{
		if (pFpl->m_SectorRoute->GetSize())
			ManageO4DTrajectory(pFpl, 100 + SectorIndex, Level, pFpl->m_SectorRoute[SectorIndex], 0, 0, Steepest, CDRate);
	}
	if (SectorIndex==-1 && pFpl->m_TOCD[0].Name.GetLength())
	{
		CFlightPlan::PointDef TOCD; 
		pFpl->ComputeTOCDPos(&pFpl->m_CurRoute,pFpl->m_CurFix,Level,0.0,0,pFpl->m_TOCD[0].To,pFpl->m_TOCD[0]);
//		if (TOCD.Pos!=pFpl->m_TOCD[0].Pos)
//			pFpl->ComputeTOCDPos(&pFpl->m_CurRoute,pFpl->m_CurFix,Level,0.0,0,pFpl->m_TOCD[0].To,TOCD);
	}


	if ((Level>=pFpl->m_pTrack->m_Altitude || lowang) && CDRate==0)
	{
		if (SectorIndex==-1)
			ManageO4DTrajectory(pFpl,2,Level,pFpl->m_CurRoute,pFpl->m_CurFix,delay,LowAngle,CDRate);
		else
		{
			if (pFpl->m_SectorRoute->GetSize())
				ManageO4DTrajectory(pFpl, 500 + SectorIndex, Level, pFpl->m_SectorRoute[SectorIndex], 0, delay, LowAngle, CDRate);
		}
	}
	else
	{
		CFlightPlan::PointDef* pTocd2;
		CArray<CFlightPlan::PointDef,CFlightPlan::PointDef>* pRoute;
		int FirstPnt=0;
		if (SectorIndex==-1)
		{
			pTocd2=&pFpl->m_TOCD[1];
			pRoute=&pFpl->m_CurRoute;
			FirstPnt=pFpl->m_CurFix;
		}
		else
		{
			pTocd2=&pFpl->m_SectorTOCD[1+SectorIndex*2];
			pRoute=&pFpl->m_SectorRoute[SectorIndex];
		}
		double Rate=5.0;
		if (CDRate)
			Rate=CDRate;
		int TimeToDescent=(((double)abs(pFpl->m_pTrack->m_Altitude-Level))/Rate)*60.0;
		CTime To=pFpl->m_pTrack->m_timestamp+delay+TimeToDescent;
		CTime FTime=pFpl->m_pTrack->m_timestamp;
		CPoint Pos=pFpl->m_pTrack->GetPoint(0);
		if (pRoute->GetSize() && To>=pRoute->GetAt(pRoute->GetUpperBound()).To)
		{
			*pTocd2=pRoute->GetAt(pRoute->GetUpperBound());
			if (Level>=pFpl->m_pTrack->m_Altitude)
			{
				pTocd2->Name="~TOC2";
				pTocd2->Alt=(pFpl->m_pTrack->m_Altitude+(Rate*(pTocd2->To-pFpl->m_pTrack->m_timestamp-delay).GetTotalSeconds())/60.0)*100;
			}
			else
			{
				pTocd2->Name="~TOD2";
				pTocd2->Alt=(pFpl->m_pTrack->m_Altitude-(Rate*(pTocd2->To-pFpl->m_pTrack->m_timestamp-delay).GetTotalSeconds())/60.0)*100;
			}
			pTocd2->FdpNb=pRoute->GetUpperBound()-1;
		}
		else
		{
			for (int i=FirstPnt;i<pRoute->GetSize();i++)
			{
				if (To>=FTime && To<pRoute->GetAt(i).To)
				{
					pTocd2->To=To;
					if (Level>=pFpl->m_pTrack->m_Altitude)
						pTocd2->Name="~TOC2";
					else
						pTocd2->Name="~TOD2";
					pTocd2->Pos.x=Pos.x+((pRoute->GetAt(i).Pos.x-Pos.x)*(To-FTime).GetTotalSeconds())/(pRoute->GetAt(i).To-FTime).GetTotalSeconds();
					pTocd2->Pos.y=Pos.y+((pRoute->GetAt(i).Pos.y-Pos.y)*(To-FTime).GetTotalSeconds())/(pRoute->GetAt(i).To-FTime).GetTotalSeconds();
					pTocd2->GeoPos=pTocd2->Pos;
					if (i==FirstPnt)
						pTocd2->FdpNb=-1;
					else
						pTocd2->FdpNb=i-1;
					pTocd2->Alt=Level*100;
					break;
				}
				FTime=pRoute->GetAt(i).To;
				Pos=pRoute->GetAt(i).Pos;
			}
		}
/*		if (pTocd2->Name.GetLength())
		{
		CFlightPlan::PointDef TOCD; 
		pFpl->ComputeTOCDPos(pRoute,FirstPnt,Level,Rate,delay,pTocd2->To,TOCD);
		if ((TOCD.Pos!=pTocd2->Pos) || (TOCD.Alt!=pTocd2->Alt))
			pFpl->ComputeTOCDPos(pRoute,FirstPnt,Level,Rate,delay,pTocd2->To,TOCD);
		}*/
		
	}
	if (DEBUGTRACE)
	{
	if (SectorIndex==-1 && (pFpl->m_TOCD[0].Name.GetLength()) && (pFpl->m_TOCD[0].Alt==pFpl->m_TOCD[1].Alt) && (pFpl->m_TOCD[0].To>pFpl->m_TOCD[1].To))
	{
		CString str,tmp;
		if (Level>=pFpl->m_pTrack->m_Altitude || lowang)
			tmp="O4d";
		str.Format("%s CurAlt=%d Rate=%d TOC1=%s Alt=%d Time=%s TOC2=%s Alt=%d Time=%s %s",pFpl->m_Arcid,pFpl->m_pTrack->m_Altitude,CDRate,pFpl->m_TOCD[0].Name,pFpl->m_TOCD[0].Alt,pFpl->m_TOCD[0].To.Format("%H:%M:%S"),pFpl->m_TOCD[1].Name,pFpl->m_TOCD[1].Alt,pFpl->m_TOCD[1].To.Format("%H:%M:%S"),tmp);
			OutputDebugString(str+"\n");
	}
	}


	#endif
}


CString CO4DMngr::GetVersion()
{
	return m_O4DVersion;
}

void CO4DMngr::IasAdvisory(CFlightPlan* pFpl)
{
	CVisuApp* pApp = (CVisuApp*)AfxGetApp();
	if (!m_Enabled)
		return;
#ifdef SetO4D
	if (!pFpl->m_SentToO4D || !pFpl->m_pTrack || !pFpl->m_pTrack->m_SentToO4D)
		return;
	TrajectoryReqMsg trajectoryreqmsg;
	CString tmp,str;
	
//    m_pO4D->Periodic((long)(CTime::GetCurrentTime().GetTime()));
	bool Compute=true;
	if (pFpl->m_IptFix!=-1)
	{
        // Request for real world, not for what-if scenario:
		trajectoryreqmsg.contextId =  Osyris::DEFAULT_CONTEXT;
		
        // The requestId will be written to the resulting trajectory message
        // and can be used in order to identify the output trajectory
        // that is the answer on the current request:
		const int requestId = OsyrisCommon::NewUniqueRequestId();
		
		CString Msg;
		Msg.Format("O4d:%s,Cfl=%d,Efl=%d,Xfl=%d",pFpl->m_Arcid,pFpl->m_Cfl,pFpl->m_Efl,pFpl->m_Xfl);
		DebugFpl(pFpl,Msg);
		trajectoryreqmsg.requestId = requestId;
		trajectoryreqmsg.contextId = 800;
		
        // Identify the flight for which a trajectory shall be calculated:
		trajectoryreqmsg.callSign = pFpl->m_Arcid;
		trajectoryreqmsg.flightId = pFpl->m_FplNbr;
		
        // Request the calculation of a trajectory between two given waypoints
        // (not a calculation starting from the current radar position):
        //m_trajectoryReqMsg.trajectoryType = Osyris::TRJ_PLANNED;
		trajectoryreqmsg.trajectoryType = Osyris::TRJ_FORECAST;
		
		//m_trajectoryReqMsg.speedProfileType = Osyris::MAXACC;
		trajectoryreqmsg.speedProfileType = Osyris::ADVISORY;
		trajectoryreqmsg.advisoryType = Osyris::AdvisoryTrjType::SPEED;
		
		trajectoryreqmsg.calculationMode= Osyris::TRJ_CALC_MODE_ADVISORY_MACH_CAS;
		
		// Constraints from message -2 can be released (to avoid memory leak)
		
		trajectoryreqmsg.constraintPoints = std::vector<ConstraintPointMsg *>();
		bool DoIt=false;
		
		if (pFpl->m_IptFix!=-1)
		{
			// next constraint on the points of the route
			for (int i=__max(pFpl->m_CurFix,0);i<=pFpl->m_IptFix;i++)
			{
				DoIt=true;
				double Lat,Lon;
				ConstraintPointMsg *constraintPointMsg = new ConstraintPointMsg();
				CString str;
				constraintPointMsg->wayPoint.name = "";
				ComputeLatLong(pFpl->m_CurRoute[i].Pos,0,Lat,Lon);
				constraintPointMsg->wayPoint.lat = Lat;
				constraintPointMsg->wayPoint.lon = Lon;
				int PointAltitude=0;
				if ( (i==pFpl->m_EptFix) && (!pFpl->IsDept()) )
				{
					// put the EFL in the first point if no CFL
					PointAltitude = pFpl->m_Efl * 100;
				}
				else 
				{
					if (i==pFpl->m_IptFix  && pFpl->m_Star.GetLength() && pApp->m_Centre==CVisuApp::GVA && pFpl->IsArrival())
					{
						if (pFpl->m_pTrack && pFpl->m_pTrack->m_Altitude<=180)
							PointAltitude = pFpl->m_pTrack->m_Altitude * 100;
						else
							PointAltitude = 180  * 100;
					}
				}
				
				// to avoid duplicate points : that may provoque error ERR_DUPLICATE_CONSTRAINT_INCONSISTENT
				//    check that the track is not near all points
				//const int NearTheTrack = Osyris::Messages::WaypointMsg::Equal(&constraintPointMsg->wayPoint, &trk_constraintPointMsg->wayPoint);
				//if (NearTheTrack == 0) AtLeastOneDifferentPoint = true;
				
				if (PointAltitude)
				{
					constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_VALID;
					constraintPointMsg->altAlapValue = PointAltitude;
				}
				else
				{
					constraintPointMsg->altAlapValid = Osyris::CONSTRAINT_NOT_VALID;
				}
				
				// no speed constraint for the route points
				constraintPointMsg->spdAlapValid = Osyris::CONSTRAINT_NOT_VALID;

				PointAttachmentMsg* pntMsg = new PointAttachmentMsg;
				tmp.Format("%d",i);
				pntMsg->remark = tmp;
				constraintPointMsg->ext.push_back(pntMsg);
				trajectoryreqmsg.constraintPoints.push_back(constraintPointMsg);
			}
		}

		
		if (DoIt)
		{
			
			trajectoryreqmsg.constraintPointsDefineRoute = 1;
			
			trajectoryreqmsg.rtoAtLastPoint=pFpl->m_Eat.GetTime();
//			trajectoryreqmsg.rflValid=1;
//			trajectoryreqmsg.rfl= pFpl->m_Rfl*100.0;

			// Define some aircraft data for calculation
			trajectoryreqmsg.acDataValid = 1;
			trajectoryreqmsg.acData.icaoCode = pFpl->m_ArcTyp;
			trajectoryreqmsg.acData.wakeCategory = pFpl->m_Wtc;
			
			RadarMergeParameterMsg* MergeMsg = new RadarMergeParameterMsg;
			MergeMsg->constraintPointIndex=0;
			trajectoryreqmsg.ext = std::vector< OsyrisMessage * >();
			trajectoryreqmsg.ext.push_back(MergeMsg);
			
			
			// Pass the trajectory request message to O4D:
			DebugFpl(pFpl,"O4D:Begin of Trajectory");
			m_pO4D->Handle(&trajectoryreqmsg, 0);
			DebugFpl(pFpl,"O4D:End of Trajectory");
		}
		}

#endif

}